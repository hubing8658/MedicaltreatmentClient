package com.kelin.client;

import android.app.ActivityManager;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.facebook.stetho.Stetho;
import com.isoftstone.mis.mmsdk.common.utils.log.CrashHandler;
import com.kelin.client.db.AbstractDatabaseManager;
import com.kelin.client.gui.live.presenters.InitBusinessHelper;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.kelin.client.im.Foreground;
import com.kelin.client.util.MyLog;
import com.kelin.client.util.MyUtils;
import com.mob.MobSDK;
import com.monty.library.AppManager;
import com.monty.library.BaseApp;
import com.tencent.bugly.imsdk.Bugly;

import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;


/**
 * Created by kelin on 2017/6/9.
 * Application
 */

public class MyApplication extends BaseApp {
    private static MyApplication application;
    private static Context context;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

    }
    public static Context getContext() {
        return context;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        Foreground.init(this);
        context = getApplicationContext();
        application = this;

        //由于个推起了一个新的进程，每个进程启动都会执行Application中的onCreate(),所以在这里判断是否是主进程，主进程去初始化一些事情
        String processName = getProcessName(this, android.os.Process.myPid());
        Log.d("monty", "processName:" + processName);
        Log.d("monty", "packageName:" + MyApplication.this.getPackageName());

        if (processName != null) {
            boolean defaultProcess = processName.equals(MyApplication.this.getPackageName()); // 主进程
            if (defaultProcess) {
                init();
                MyLog.isLogable = BuildConfig.DEBUG;
            }
        }

        // 捕获异常日志
        CrashHandler.getInstance().init();

        // 批量替换字体
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("font/ZHSRXT_GBK2_0.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
    }

    private void init() {
        //内存分析工具
//        if (BuildConfig.DEBUG) {
//            LeakCanary.install(this);
//        }
        //初始化Glide
        Glide.get(this);

        MobSDK.init(this, MyUtils.getMetaData(this,"Mob-AppKey"),MyUtils.getMetaData(this,"Mob-AppSecret"));

        Stetho.initializeWithDefaults(this);
        //初始化直播业务
        InitBusinessHelper.initApp(MyApplication.getApplication());
        //初始化数据库
        AbstractDatabaseManager.initOpenHelper(getApplicationContext());//初始化数据库
        MultiDex.install(this);
       /* PushManager.getInstance().initialize(this.getApplicationContext(), PushService.class);
        PushManager.getInstance().registerPushIntentService(this.getApplicationContext(), PushReceiver.class);*/

        Bugly.init(this,"a3369cae85",true);
    }

    public static MyApplication getApplication() {
        return application;
    }

    // 获取进程名
    public static String getProcessName(Context cxt, int pid) {
        ActivityManager am = (ActivityManager) cxt.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> runningApps = am.getRunningAppProcesses();
        if (runningApps == null) {
            return null;
        }
        for (ActivityManager.RunningAppProcessInfo procInfo : runningApps) {
            if (procInfo.pid == pid) {
                return procInfo.processName;
            }
        }
        return null;
    }

    @Override
    public void startLoginActivity() {
        AppManager.getAppManager().finishAllActivity();

        MyselfInfo.startLoginActivity();
    }

}
