package com.kelin.client.widget;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kelin.client.R;
import com.kelin.client.util.DeviceInfoUtil;


/**
 * backBtn 为返回按钮可设置返回键的点击事件，默认事件为关掉本页面
 * ivRight 右侧图片按钮
 * tvRight 右侧文字按钮
 * centerText 靠左侧的文字标题
 * 当需要使用一些自定义或者新的控件，在调用JPBaseTitle的xml中，在JPBaseTitle控件内部加入
 */
public class BaseTitleBar extends RelativeLayout {

    // private Context mContext;

    protected ImageView backBtn, ivRight;

    protected TextView centerLeftText,centerText, tvRight;

    protected LinearLayout customLeftContainer;

    protected LinearLayout customContainer;

    protected LinearLayout rightContainer;

    public RelativeLayout jp_goodsinfo_titleLy;

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    public BaseTitleBar(Context context) {
        super(context);
        init();
    }

    public BaseTitleBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        View view = View.inflate(getContext(), R.layout.base_title_layout, null);
        jp_goodsinfo_titleLy = (RelativeLayout) view.findViewById(R.id.jp_goodsinfo_titleLy);
        this.backBtn = (ImageView) view.findViewById(R.id.jp_title_back);
        this.customLeftContainer = (LinearLayout) view.findViewById(R.id.custom_left_container);
        this.customContainer = (LinearLayout) view.findViewById(R.id.custom_container);
        rightContainer = (LinearLayout) view.findViewById(R.id.right_btn_container);
        this.centerText = (TextView) view.findViewById(R.id.jp_title_text);
        this.centerLeftText = view.findViewById(R.id.jp_left_title_text);
        this.ivRight = (ImageView) view.findViewById(R.id.iv_right);
        this.tvRight = (TextView) view.findViewById(R.id.tv_right);
        this.backBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Activity activity = (Activity) getContext();
                    activity.finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        addView(view);
    }


    /**
     * 设置返回键的点击事件
     *
     * @param click
     */
    public void setBackBtnClick(OnClickListener click) {
        this.backBtn.setOnClickListener(click);
    }

    public void setBackBtnVis(boolean bl) {
        if (bl) {
            this.backBtn.setVisibility(VISIBLE);
        } else {
            this.backBtn.setVisibility(GONE);
        }
    }

    public void setBackBtnImg(int resid) {
        this.backBtn.setImageResource(resid);
    }

    /**
     * @param text
     * @param textSize  默认14sp
     * @param textColor
     * @param click
     */
    public void setRightText(String text, int textSize, int textColor, OnClickListener click) {
        tvRight.setVisibility(VISIBLE);
        tvRight.setText(text);
        if (textSize != 0) {
            tvRight.setTextSize(textSize);
        }
        if (textColor != 0) {
            tvRight.setTextColor(textColor);
        }

        tvRight.setOnClickListener(click);
    }

    public void setRightText(int text, int textSize, int textColor, OnClickListener click) {
        setRightText(getContext().getString(text), textSize, textColor, click);
    }

    public void setRightText(String text) {
        tvRight.setText(text);
    }

    public TextView getRightText() {
        return tvRight;
    }

    public void setRightIcon(int sour, OnClickListener click) {
        ivRight.setVisibility(VISIBLE);
        ivRight.setImageResource(sour);
        ivRight.setOnClickListener(click);
    }

    public ImageView getRightIcon() {
        return ivRight;
    }

    public void addCustomView(View view, OnClickListener click) {
        customContainer.addView(view);
        view.setOnClickListener(click);
    }

    /**
     * 标题显示文字
     *
     * @param resId
     */
    public void showCenterText(int resId) {
        showCenterText(getContext().getString(resId));
    }

    public void showCenterText(String text) {
        centerText.setText(text);
    }

    public void showCenterText(String leftText,String rightText){
        centerLeftText.setVisibility(VISIBLE);
        centerLeftText.setText(leftText);
        centerText.setText(rightText);
    }
    public void setCenterLeftTextSize(int sp){
        centerLeftText.setTextSize(TypedValue.COMPLEX_UNIT_SP,sp);
    }

    /**
     * 根据字体大小设置标题显示文字
     *
     * @param resId
     * @param size  字体大小
     */
    public void showCenterText(int resId, int size) {
        showCenterText(getContext().getString(resId), size);
    }

    public void showCenterText(String text, int size) {
        showCenterText(text);
        if (size != 0) {
            centerText.setTextSize(size);
        }
    }

    public void showCenterText(String text, int drawableLeftId, int size){
        centerText.setText(text);
        centerText.setCompoundDrawablesWithIntrinsicBounds(drawableLeftId, 0, 0, 0);
        centerText.setCompoundDrawablePadding(DeviceInfoUtil.dip2px(getContext(), 2));
        centerText.setGravity(Gravity.CENTER);
        if (size != 0) {
            centerText.setTextSize(size);
        }
    }

    public void showCenterText(int textId, int drawableLeftId, int size) {
        showCenterText(getContext().getResources().getString(textId),drawableLeftId,size);
    }


    /**
     * 传入如：RelativeLayout.CENTER_IN_PARENT
     *
     * @param
     */
    public void setCenterTextGrivaty(int gravity) {
        RelativeLayout.LayoutParams params =
                new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
        params.addRule(gravity, RelativeLayout.TRUE);

        centerText.setLayoutParams(params);
    }

    /**
     * 设置 相对位置   相对于返回键
     *
     * @param gravity
     */
    public void setCenterTextRelativeToBackBtn(int gravity) {
        RelativeLayout.LayoutParams params =
                new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
        params.addRule(gravity, R.id.jp_title_back);
        params.addRule(RelativeLayout.CENTER_VERTICAL);

        centerText.setLayoutParams(params);
    }

    public TextView getCenterText() {
        return centerText;
    }

    /**
     * 右边Custom容器add 一个View
     *
     * @param view            view
     * @param onClickListener 点击事件listener
     */
    public void addCustomRightView(View view, OnClickListener onClickListener) {
        customContainer.removeAllViews();
        customContainer.addView(view);
        view.setOnClickListener(onClickListener);
    }
}
