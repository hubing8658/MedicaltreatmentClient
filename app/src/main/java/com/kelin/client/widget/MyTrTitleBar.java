package com.kelin.client.widget;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import com.kelin.client.R;

/**
 * Created by wangbin on 2017/7/9.
 */

public class MyTrTitleBar extends FrameLayout implements View.OnClickListener {
    private View currentSelectedView;
    private onTabSelectedListener mOnTabSelectedListener;

    public MyTrTitleBar(@NonNull Context context) {
        this(context, null);
    }

    public MyTrTitleBar(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MyTrTitleBar(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        View mainBottomView = LayoutInflater.from(context).inflate(R.layout.my_tr_title_bar_lauyout, this);
        View tab1 = mainBottomView.findViewById(R.id.tab1);
        tab1.setOnClickListener(this);
        View tab2 = mainBottomView.findViewById(R.id.tab2);
        tab2.setOnClickListener(this);
    }

    public void setOnTabSelectedListener(onTabSelectedListener mOnTabSelectedListener) {
        this.mOnTabSelectedListener = mOnTabSelectedListener;
    }
    public void setFistTab(){
        currentSelectedView=findViewById(R.id.tab1);
        currentSelectedView.setSelected(true);
        if(this.mOnTabSelectedListener!=null){
            mOnTabSelectedListener.onTagSelected(R.id.tab1, "tab1");
        }
    }
    @Override
    public void onClick(View v) {
        if (currentSelectedView == v) {
            return;
        }
        int tabId = 0;
        String tabName = "";
        if (currentSelectedView != null) {
            currentSelectedView.setSelected(false);
        }
        v.setSelected(true);
        currentSelectedView = v;
        switch (v.getId()) {
            case R.id.tab1:
                tabId = R.id.tab1;
                tabName = "tab1";
                break;
            case R.id.tab2:
                tabId = R.id.tab2;
                tabName = "tab2";
                break;

        }
        if (mOnTabSelectedListener != null) {
            mOnTabSelectedListener.onTagSelected(tabId, tabName);
        }

    }

    public interface onTabSelectedListener {
        void onTagSelected(int tabId, String tabName);
    }
}
