package com.kelin.client.widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;

import android.widget.Button;
import android.widget.TextView;

import com.kelin.client.R;

/**
 * Created by guangjiqin on 2017/8/15.
 */

public class ForTicktsDialog extends Dialog {

    private String msg;

    public ForTicktsDialog(@NonNull Context context, String msg) {
        super(context);
        this.msg = msg;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.for_tickets_dialog);

        TextView tvContent = (TextView) findViewById(R.id.tv_content);
        tvContent.setText(msg);

        Button btCommit = (Button) findViewById(R.id.bt_commit);
        btCommit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ForTicktsDialog.this.dismiss();
            }
        });

    }

}
