package com.kelin.client.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.jude.rollviewpager.RollPagerView;

/**
 * Created by monty on 2017/11/3.
 */

public class MyRollPagerView extends RollPagerView {
    public MyRollPagerView(Context context) {
        this(context,null);
    }

    public MyRollPagerView(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public MyRollPagerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int height = 0;
        for (int i = 0; i < getChildCount(); i++) {
            View child = getChildAt(i);
            child.measure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
            int h = child.getMeasuredHeight();
            if (h > height)height = h;
        }

        heightMeasureSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
