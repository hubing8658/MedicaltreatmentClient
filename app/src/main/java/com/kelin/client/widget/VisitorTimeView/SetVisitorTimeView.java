package com.kelin.client.widget.VisitorTimeView;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.bigkoo.pickerview.TimePickerView;
import com.kelin.client.R;
import com.kelin.client.util.DisplayUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by guangjiqin on 2017/8/4.
 * 添加坐诊时间的自定义view
 */

public class SetVisitorTimeView extends View {

    private Context context;
    private String TAG = getClass().getName();

    public List<TimeBoxEntity> timeBoxEntities;//view时间小盒子的实体
    private List<String> visitorTmies;//显示在界面上的时间集合
    private int num = 3;// 没一行的个数

    private Paint textAdd;//添加时间的笔
    private Paint timeBox;//时间盒子的画笔
    private Paint textTimeBox;//时间盒子里面的文字画笔
    private TimePickerView startTimePickerView;
    private TimePickerView endTimePickerView;

    public void setHasFocus(boolean hasFocus) {
        this.hasFocus = hasFocus;
    }

    private boolean hasFocus = true;//有焦点、可议操作


    public void setTime(List<String> visitorTmies) {//设置访问时间
        this.visitorTmies = visitorTmies;
        initview();
        invalidate();
    }

    public SetVisitorTimeView(Context context) {
        super(context);
        this.context = context;
        initview();
    }

    public SetVisitorTimeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initview();
    }

    public SetVisitorTimeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initview();
    }

    private void initview() {
        if (null == visitorTmies) {
            visitorTmies = new ArrayList<>();
        }

        //===测试数据====
//        visitorTmies.add("1");
//        visitorTmies.add("2");
//        visitorTmies.add("3");
//        visitorTmies.add("4");
//        visitorTmies.add("5");
//        visitorTmies.add("6");


        //===============

        timeBoxEntities = new ArrayList<>();
        for (String strTime : visitorTmies) {
            TimeBoxEntity timeBoxEntity = new TimeBoxEntity();
            timeBoxEntity.time = strTime;
            timeBoxEntities.add(timeBoxEntity);

        }

        if (timeBoxEntities.size() < 6 && hasFocus) {            //手动添加最后一个添加按钮
            TimeBoxEntity timeBoxEntity = new TimeBoxEntity();
            timeBoxEntity.time = "添加时间";
            timeBoxEntity.isAddView = true;
            timeBoxEntities.add(timeBoxEntity);
        }


        timeBox = new Paint();
        timeBox.setAntiAlias(true);
        timeBox.setColor(Color.parseColor("#11345e"));

        textTimeBox = new Paint();
        textTimeBox.setTextSize(DisplayUtil.dp2px(context, 10));
        textTimeBox.setAntiAlias(true);
        textTimeBox.setColor(Color.parseColor("#ff7fb9"));

        textAdd = new Paint();
        textAdd.setTextSize(DisplayUtil.dp2px(context, 10));
        textAdd.setAntiAlias(true);
        textAdd.setColor(Color.parseColor("#ff7fb9"));

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int size = timeBoxEntities.size();
        if (size < 4) {
            for (int i = 0; i < size; i++) {
                TimeBoxEntity timeBoxEntity = timeBoxEntities.get(i);

                int x = (getMeasuredWidth() / (2 * num)) + ((i % num) * (getMeasuredWidth() / num));
                int y = getMeasuredHeight() / 2;


                if (timeBoxEntity.isAddView) {
                    canvas.drawText(timeBoxEntity.time, x - getWidth() / 10, y + DisplayUtil.dp2px(context, 2), textAdd);
                } else {
                    RectF rectf = new RectF(x - getWidth() / 8, y - getHeight() / 5, x + getWidth() / 8, y + getHeight() / 5);
                    canvas.drawRect(rectf, timeBox);
                    canvas.drawText(timeBoxEntity.time, x - getWidth() / 10, y + DisplayUtil.dp2px(context, 2), textTimeBox);
                    if (hasFocus) {
                        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), 0);
                        canvas.drawBitmap(bitmap, x + getWidth() / 9, y - getHeight() / 4, textTimeBox);
                    }

                }


                timeBoxEntity.top = y - 30;
                timeBoxEntity.bottom = y + 30;
                timeBoxEntity.left = x - 100;
                timeBoxEntity.right = x + 100;

            }
        } else {
            for (int i = 0; i < size; i++) {
                TimeBoxEntity timeBoxEntity = timeBoxEntities.get(i);
                int y = 0;
                if (i < 3) {
                    y = getMeasuredHeight() / 3 - (getHeight() / 10);
                } else {
                    y = ((2 * getMeasuredHeight()) / 3) + (getHeight() / 10);
                }
                int x = (getMeasuredWidth() / (2 * num)) + ((i % num) * (getMeasuredWidth() / num));

                if (timeBoxEntity.isAddView) {
                    canvas.drawText(timeBoxEntity.time, x - getWidth() / 10, y + DisplayUtil.dp2px(context, 2), textAdd);
                } else {
                    RectF rectf = new RectF(x - getWidth() / 8, y - getHeight() / 5, x + getWidth() / 8, y + getHeight() / 5);
                    canvas.drawRect(rectf, timeBox);
                    canvas.drawText(timeBoxEntity.time, x - getWidth() / 10, y + DisplayUtil.dp2px(context, 2), textTimeBox);
                    if (hasFocus) {
                        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), 0);
                        canvas.drawBitmap(bitmap, x + getWidth() / 9, y - getHeight() / 4, textTimeBox);
                    }
                }

                timeBoxEntity.top = y - 30;
                timeBoxEntity.bottom = y + 30;
                timeBoxEntity.left = x - 100;
                timeBoxEntity.right = x + 100;

            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int moveNum = 0;
        float x = event.getX();
        float y = event.getY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                for (int i = 0; i < timeBoxEntities.size(); i++) {
                    TimeBoxEntity timeBoxEntity = timeBoxEntities.get(i);
                    if (timeBoxEntity.left < x && x < timeBoxEntity.right && timeBoxEntity.top < y && y < timeBoxEntity.bottom) {
                        if (!timeBoxEntity.isAddView) {
                            timeBoxEntity.isDel = true;
                        } else {
                            timeBoxEntity.isAdd = true;
                        }
                    }
                }
                break;
            case MotionEvent.ACTION_MOVE:
                moveNum++;
                break;
            case MotionEvent.ACTION_UP:
                if (moveNum < 5) {
                    for (int i = 0; i < timeBoxEntities.size(); i++) {
                        TimeBoxEntity timeBoxEntity = timeBoxEntities.get(i);
                        if (timeBoxEntity.isDel) {
                            timeBoxEntities.remove(timeBoxEntity);
                            if (timeBoxEntities.size() == 5 && !timeBoxEntities.get(timeBoxEntities.size() - 1).isAddView) {
                                TimeBoxEntity addEntity = new TimeBoxEntity();
                                addEntity.time = "添加时间";
                                addEntity.isAddView = true;
                                timeBoxEntities.add(addEntity);
                            }
                            invalidate();
                            break;
                        }
                        if (timeBoxEntity.isAdd && timeBoxEntity.isAddView && timeBoxEntities.size() < 8) {
                            break;
                        }
                    }

                } else {
                    for (TimeBoxEntity entity : timeBoxEntities) {
                        entity.isAdd = false;
                        entity.isDel = false;
                    }
                }
                break;
        }
        return hasFocus;
    }



    private boolean checkTimeCur(Date startTime, Date endTime) {
        // TODO: 2017/8/21 (时间校对、校对开始时间、结束时间等正确性的校验)
        return true;
    }
}
