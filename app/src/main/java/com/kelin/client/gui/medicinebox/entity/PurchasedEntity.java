package com.kelin.client.gui.medicinebox.entity;

/**
 * Created by Administrator on 2017/8/8 0008.
 */

public class PurchasedEntity {

    public boolean isShowTop;
    public boolean isBottomTop;
    public String title;
    public String time;
    public String imgUrl;
    public String name;
    public String price;
    public String num;
    public int total;
    public float totalPrice;

}
