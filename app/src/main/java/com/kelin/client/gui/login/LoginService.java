package com.kelin.client.gui.login;

import com.kelin.client.gui.login.bean.User;
import com.monty.library.http.BaseCallModel;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by monty on 2017/7/22.
 */

public interface LoginService {
    /**
     * 注册时获取手机验证码
     *
     * @param phone
     * @return
     */
    @GET("login/getCode")
    Call<BaseCallModel<String>> getVerifyCode(@Query("phone") String phone);

    /**
     * 忘记密码／扫码领券时的验证码
     *
     * @param phone
     * @param type  类型，1医生端 其他为用户端 2为扫码领券
     * @return
     */
    @GET("login/getCodeByType")
    Call<BaseCallModel<String>> getCodeByType(@Query("phone") String phone, @Query("type") int type);

    /**
     * 用户注册
     *
     * @param phone
     * @param password
     * @param verifyCode 短信验证码
     * @return
     */
    @FormUrlEncoded
    @POST("login/userRegister")
    Call<BaseCallModel<User>> register(@Field("phone") String phone, @Field("password") String password, @Field("code") String verifyCode);

    /**
     * 用户登录
     *
     * @param phone
     * @param password
     * @return
     */
    @FormUrlEncoded
    @POST("login/userIndex")
    Call<BaseCallModel<User>> login(@Field("phone") String phone, @Field("password") String password);

    /**
     * 第三方账号登录
     *
     * @param openId 第三方账号唯一标识
     * @param flag   1-微信，2-QQ
     * @return
     */
    @FormUrlEncoded
    @POST("thirdAccount/thirdLogin")
    Call<BaseCallModel<User>> thirdLogin(@Field("openId") String openId, @Field("flag") int flag);

    /**
     * 第三方登录后绑定并注册
     *
     * @param phone
     * @param password
     * @param verifyCode 短信验证码
     * @param openId     第三方账号唯一标识
     * @param flag       1-微信，2-QQ
     * @param photoUrl
     * @return
     */
    @FormUrlEncoded
    @POST("thirdAccount/userBindPhone")
    Call<BaseCallModel<User>> userBind(@Field("phone") String phone, @Field("password") String password, @Field("code") String verifyCode, @Field("openId") String openId, @Field("flag") int flag, @Field("photo") String photoUrl);

    /**
     * 密码重置（忘记密码）
     *
     * @param phone
     * @param password
     * @param verifyCode 短信验证码
     * @return
     */
    @FormUrlEncoded
    @POST("login/userForgotPassword")
    Call<BaseCallModel<User>> userForgotPassword(@Field("phone") String phone, @Field("password") String password, @Field("code") String verifyCode);

    /**
     * 退出系统
     *
     * @param token
     * @return
     */
    @GET("logout")
    Call<BaseCallModel<String>> logout(@Query("token") String token);

    /**
     * 获取城市级联的接口
     *
     * @param token
     * @return
     */
    @GET("getProvinces")
    Call<BaseCallModel<Map<String,List<String>>>> getProvinces(@Query("token") String token);

    /**
     * 扫码领券获取验证码（最新）
     *
     * @param phone
     * @return
     */
    @GET("login/getCodeByScanCode")
    Call<BaseCallModel<String>> getLotteryCode(@Query("phone") String phone);


    /**
     * 扫码领券（最新）
     *
     * @param  手机号，奖券号，验证码，密码
     * @return
     */
    @FormUrlEncoded
    @POST("login/receive")
    Call<BaseCallModel<Object>> getLottery(@Field("phone") String phone,
                                           @Field("number") String number,
                                           @Field("code") String code,
                                           @Field("password") String password);
}
