package com.kelin.client.gui.coupons;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.WindowManager;
import android.widget.EditText;

import com.kelin.client.R;
import com.kelin.client.common.BaseActivity;
import com.kelin.client.gui.coupons.entity.CouponsEntity;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.kelin.client.util.ScreenUtil;
import com.kelin.client.util.ToastUtils;
import com.kelin.client.widget.BaseTitleBar;
import com.kelin.client.widget.dialog.ForTicktsDialog;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by guangjiqin on 2017/8/15.
 */

public class FortTicketsActivity extends BaseActivity {

    private final String TAG = getClass().getName();

    @BindView(R.id.titleBar)
    BaseTitleBar title;

    @BindView(R.id.et_ticket_num)
    EditText etTicket;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_for_tickets);

        ButterKnife.bind(this);

        initTitle();
    }

    private void initTitle() {
        title.showCenterText(getString(R.string.for_ticket), 0, 0);// TODO: 2017/8/15 (添加现金样式的图片S)
    }

    @OnClick(R.id.bt_exchange)
    public void forTicket() {
        RetrofitHelper.getInstance().createService(CouponsService.class).forTickets(MyselfInfo.getLoginUser().getToken(), etTicket.getText().toString()).enqueue(new BaseCallBack<BaseCallModel<List<CouponsEntity>>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<List<CouponsEntity>>> response) {
                Log.d(TAG, "兑换奖券成功！！");
                ToastUtils.showToast("兑换现金券成功！");
                FortTicketsActivity.this.finish();
                EventBus.getDefault().post("兑换成功");
            }

            @Override
            public void onFailure(String message) {
                Log.d(TAG, "兑换现金券失败！！");
                if(null == message){
                    message = getString(R.string.for_ticket_failed);
                }
                ForTicktsDialog dialog = new ForTicktsDialog(FortTicketsActivity.this,message);
                dialog.show();

                WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
                params.width = ScreenUtil.dip2px(FortTicketsActivity.this,350);
                params.height = ScreenUtil.dip2px(FortTicketsActivity.this,130);
                dialog.getWindow().setAttributes(params);
            }
        });
    }
}
