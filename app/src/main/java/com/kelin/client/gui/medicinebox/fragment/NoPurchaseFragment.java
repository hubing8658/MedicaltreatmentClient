package com.kelin.client.gui.medicinebox.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.kelin.client.R;
import com.kelin.client.gui.medicinebox.adapter.NoPurchaseExpListViewAdapter;
import com.kelin.client.gui.medicinebox.controller.PurchaseController;
import com.kelin.client.gui.medicinebox.entity.PurchasedBean;
import com.kelin.client.gui.pay.PayMentActivity;
import com.kelin.client.util.ToastUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;


/**
 * 待购买列表
 * Created by guangjiqin on 2017/8/8.
 */

public class NoPurchaseFragment extends Fragment implements View.OnClickListener {

    private View view;
    private List<String> dataKey;
    private Map<String, PurchasedBean> data;
    private NoPurchaseExpListViewAdapter adapter;
    private PtrClassicFrameLayout mPtrFrame;
    private ExpandableListView listView;
    private static NoPurchaseFragment noPurchaseFragment;

    private PurchaseController controller;


    private CheckBox cbSelectAll;
    private Button btCommit;
    private TextView tvPrice;
    //    private ArrayList<Integer>diagnosticIds;//治疗记录id集合

    public static HashMap<Integer, List<Integer>> diagnosticIds;

    public static NoPurchaseFragment createInstance() {
        if (null == noPurchaseFragment) {
            noPurchaseFragment = new NoPurchaseFragment();
        }
        return noPurchaseFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_no_purchase, null);

        initView();

        initPullRefresh();

        initController();

        setOnClick();

        initData();

        return view;
    }

    private void setOnClick() {
        cbSelectAll.setOnClickListener(this);
        btCommit.setOnClickListener(this);
    }

    private void initData() {
        mPtrFrame.postDelayed(new Runnable() {
            @Override
            public void run() {
                mPtrFrame.autoRefresh();
            }
        }, 100);
    }

    private void initController() {
        controller = new PurchaseController(getActivity()) {
            @Override
            public void getDataSuccess(Map<String, PurchasedBean> map) {
                data.clear();
                dataKey.clear();
                data.putAll(map);
                for (String key : map.keySet()) {
                    dataKey.add(key);
                }
                for (int i = 0; i < dataKey.size(); i++) {//默认全部展开
                    listView.expandGroup(i);
                }

                adapter.notifyDataSetChanged();
                mPtrFrame.refreshComplete();
            }

            @Override
            public void getDataFailrue(String msg) {
                mPtrFrame.refreshComplete();
            }
        };
    }


    private void initView() {
        diagnosticIds = new HashMap<>();

        cbSelectAll = (CheckBox) view.findViewById(R.id.cb_select_all);
        tvPrice = (TextView) view.findViewById(R.id.tv_price);
        btCommit = (Button) view.findViewById(R.id.bt_commit);

        mPtrFrame = (PtrClassicFrameLayout) view.findViewById(R.id.store_house_ptr_frame);
        listView = (ExpandableListView) view.findViewById(R.id.listview);
        listView.setGroupIndicator(null);
        dataKey = new ArrayList<>();
        data = new HashMap<>();
        adapter = new NoPurchaseExpListViewAdapter(getActivity(), dataKey, data) {
            @Override
            public void itemOnclick() {
                for (String key : data.keySet()) {
                    if (!data.get(key).isSelectAll) {
                        cbSelectAll.setChecked(false);
                        break;
                    }
                    cbSelectAll.setChecked(true);
                }

                diagnosticIds.clear();
                float price = 0;
                for (String key : data.keySet()) {//计算价格
                    for (PurchasedBean.DrugBean bean : data.get(key).drug) {
                        if (bean.isSelect) {
                            price = price + (bean.price * bean.count);


                            List<Integer> ids = diagnosticIds.get(bean.diagnosticId);
                            if (ids == null) {
                                ids = new ArrayList<>();
                                ids.add(bean.drugId);
                                diagnosticIds.put(bean.diagnosticId, ids);
                            } else {
                                ids.add(bean.drugId);
                            }
                        }
                    }
                }
                tvPrice.setText("" + price);
            }
        };
        listView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {//设置点击不收缩

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                return true;
            }
        });
        listView.setAdapter(adapter);
    }

    private void initPullRefresh() {
        mPtrFrame.setLastUpdateTimeRelateObject(this);
        mPtrFrame.setPtrHandler(new PtrHandler() {
            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
                return PtrDefaultHandler.checkContentCanBePulledDown(frame, listView, header);
            }

            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                controller.getPurchaseDataType(2);
            }
        });


        // the following are default settings
        mPtrFrame.setResistance(1.7f);
        mPtrFrame.setRatioOfHeaderHeightToRefresh(1.2f);
        mPtrFrame.setDurationToClose(200);
        mPtrFrame.setDurationToCloseHeader(1000);
        // default is false
        mPtrFrame.setPullToRefresh(false);
        // default is true
        mPtrFrame.setKeepHeaderWhenRefresh(true);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cb_select_all:
                float price = controller.selectAll(data, cbSelectAll.isChecked());
                tvPrice.setText("" + price);
                adapter.notifyDataSetChanged();
                break;
            case R.id.bt_commit:
                if (Double.valueOf(tvPrice.getText().toString()) <= 0) {
                    ToastUtils.showToast("商品不能为空");
                    return;
                }
                Intent intent = new Intent();
                intent.putExtra("price", Double.valueOf(tvPrice.getText().toString()));
                intent.setClass(getActivity(), PayMentActivity.class);
                startActivity(intent);
                break;
        }

    }
}
