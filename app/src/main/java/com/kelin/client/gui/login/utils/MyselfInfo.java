package com.kelin.client.gui.login.utils;

import android.content.Intent;
import android.text.TextUtils;

import com.kelin.client.MyApplication;
import com.kelin.client.db.UserDBManager;
import com.kelin.client.gui.login.bean.User;
import com.kelin.client.util.PreferencesManager;
import com.kelin.client.util.ToastUtils;
import com.kelin.client.util.VerificationUtil;
import com.monty.library.BaseApp;
import com.monty.library.live.Constants;


/**
 * Created by monty on 2017/7/22.
 */

public class MyselfInfo {
    /**
     * 校验手机号是否合法
     *
     * @param telPhone
     * @return
     */
    public static boolean checkTelPhone(String telPhone) {
        if (TextUtils.isEmpty(telPhone)) {
            ToastUtils.showToast("请输入手机号");
            return false;
        }
        if (!VerificationUtil.isValidTelNumber(telPhone)) {
            ToastUtils.showToast("请输入正确的手机号");
            return false;
        }
        return true;
    }

    /**
     * 校验验证码是否合法
     *
     * @param verifyCode
     * @return
     */
    public static boolean checkVerifyCode(String verifyCode) {
        if (TextUtils.isEmpty(verifyCode)) {
            ToastUtils.showToast("请输入验证码");
            return false;
        }
        return true;
    }

    /**
     * 校验密码是否合法
     *
     * @param password
     * @return
     */
    public static boolean checkPassword(String password) {
        if (TextUtils.isEmpty(password)) {
            ToastUtils.showToast("请输入密码");
            return false;
        }
        if (password.length() < 6) {
            ToastUtils.showToast("密码长度最少为6位");
            return false;
        }

        return true;
    }


    public static void startLoginActivity() {
        Intent intent = new Intent("com.ygbd198.hellodoctor.client.login");
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        MyApplication.getApplication().startActivity(intent);
    }

    private static User currentUser;

    private static int treatmentTypeId;

    public static User getLoginUser() {
        return currentUser == null ? new UserDBManager().getUser() : currentUser;
    }

    public static boolean saveLoginUser(User user) {
        if(TextUtils.isEmpty(user.getName())){
            user.setName(user.getPhone());
        }
        BaseApp.getInstance().setTime(user.getTime());
        currentUser = user;
        return new UserDBManager().saveUser(user);
    }


    public static boolean clearLoginUser(){
        currentUser = null;
        return new UserDBManager().deleteAll();
    }

    public static int getTreatmentTypeId() {
        MyselfInfo.treatmentTypeId = PreferencesManager.getInt(Constants.TREATMENTTYPEID,-1);
        return MyselfInfo.treatmentTypeId;
    }

    public static void setTreatmentTypeId(int treatmentTypeId) {
        PreferencesManager.putInt(Constants.TREATMENTTYPEID,treatmentTypeId);
        MyselfInfo.treatmentTypeId = treatmentTypeId;
    }
}
