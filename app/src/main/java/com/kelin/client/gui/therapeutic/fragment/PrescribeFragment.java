package com.kelin.client.gui.therapeutic.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.kelin.client.R;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.kelin.client.gui.therapeutic.TherapeuticService;
import com.kelin.client.gui.therapeutic.adapter.PrescribeExpandableListAdapter;
import com.kelin.client.gui.therapeutic.entity.PrescribeBean;
import com.kelin.client.util.ToastUtils;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;
import retrofit2.Response;

/**
 * 治疗方案-药方
 * Created by guangjiqin on 2017/8/18.
 */

public class PrescribeFragment extends BaseFragment implements View.OnClickListener {

    private final String TAG = getClass().getName();

    private List<String> group;
    private Map<String, PrescribeBean> childEntities;
    private PrescribeExpandableListAdapter adapter;
    private PtrClassicFrameLayout mPtrFrame;
    private ExpandableListView listView;
    private long diagnosticId;

    private View view;

    private TextView cbSelectAll;
    private Button btCommit;
    private TextView tvPrice;


    public static PrescribeFragment createInstance(long diagnosticId) {
//        if (null == prescribeFragment) {
        PrescribeFragment prescribeFragment = new PrescribeFragment();
        prescribeFragment.diagnosticId = diagnosticId;
//        }
        return prescribeFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_prescribe, null);

        initView();

        initPullRefresh();

        initController();

        return view;
    }


    private void initController() {
    }

    private void initPullRefresh() {
        mPtrFrame.setLastUpdateTimeRelateObject(this);
        mPtrFrame.setPtrHandler(new PtrHandler() {
            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
                return PtrDefaultHandler.checkContentCanBePulledDown(frame, listView, header);
            }

            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                requestData();
            }
        });


        // the following are default settings
        mPtrFrame.setResistance(1.7f);
        mPtrFrame.setRatioOfHeaderHeightToRefresh(1.2f);
        mPtrFrame.setDurationToClose(200);
        mPtrFrame.setDurationToCloseHeader(1000);
        // default is false
        mPtrFrame.setPullToRefresh(false);
        // default is true
        mPtrFrame.setKeepHeaderWhenRefresh(true);
        /*mPtrFrame.postDelayed(new Runnable() {
            @Override
            public void run() {
                mPtrFrame.autoRefresh();
            }
        }, 100);*/
    }

    private void initView() {

//        cbSelectAll = (TextView) view.findViewById(R.id.cb_select_all);
        tvPrice = (TextView) view.findViewById(R.id.tv_price);
        btCommit = (Button) view.findViewById(R.id.bt_commit);
//        cbSelectAll.setOnClickListener(this);
        btCommit.setOnClickListener(this);

        mPtrFrame = (PtrClassicFrameLayout) view.findViewById(R.id.store_house_ptr_frame);

        group = new ArrayList<>();
        childEntities = new HashMap<>();
        adapter = new PrescribeExpandableListAdapter(getActivity(), group, childEntities) {
            @Override
            public void itemOnclick() {
                for (String key : childEntities.keySet()) {//设置全选
                    if (!childEntities.get(key).isSelectAll) {
//                        cbSelectAll.setChecked(false);
                        break;
                    }
//                    cbSelectAll.setChecked(true);
                }

                float price = 0;
                for (String key : childEntities.keySet()) {//计算价格
                    for (PrescribeBean.DrugsBean bean : childEntities.get(key).drug) {
                       /* if (bean.isSelect) {
                            price = price + (bean.price * bean.count);
                        }*/
                    }
                }
                tvPrice.setText("¥" + price);
            }
        };
        listView = (ExpandableListView) view.findViewById(R.id.listview);
        listView.setGroupIndicator(null);
        listView.setAdapter(adapter);
        listView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {//设置点击不收缩

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                return true;
            }
        });
    }

    /**
     * 获取总金额
     *
     * @return
     */
    private double getPrice(Map<String, PrescribeBean> prescribeBeanMap) {
        double price = 0;
        for (PrescribeBean prescribe : prescribeBeanMap.values()) {
            for (PrescribeBean.DrugsBean drug : prescribe.drug) {
                price += drug.price;
            }
        }
        return price;
    }

    @Override
    protected void requestData() {
        RetrofitHelper.getInstance().createService(TherapeuticService.class).getDrug(MyselfInfo.getLoginUser().getToken(),
                diagnosticId).enqueue(new BaseCallBack<BaseCallModel<Map<String, PrescribeBean>>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<Map<String, PrescribeBean>>> response) {
                childEntities.clear();
                group.clear();
                childEntities.putAll(response.body().data);
                for (String key : childEntities.keySet()) {
                    group.add(key);
                }
                adapter.notifyDataSetChanged();
                mPtrFrame.refreshComplete();

                for (int i = 0; i < group.size(); i++) {//默认全部展开
                    listView.expandGroup(i);
                }

                tvPrice.setText("￥" + getPrice(childEntities));
            }

            @Override
            public void onFailure(String message) {
                ToastUtils.showToast(message);
                mPtrFrame.refreshComplete();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cb_select_all:
//                float price = selectAll(childEntities, cbSelectAll.isChecked());
//                tvPrice.setText("¥" + price);
//                adapter.notifyDataSetChanged();
                break;
            case R.id.bt_commit:
                break;
        }
    }

    private float selectAll(Map<String, PrescribeBean> data, boolean isSelect) {
        float price = 0;
        for (String key : data.keySet()) {
            data.get(key).isSelectAll = isSelect;
            for (PrescribeBean.DrugsBean bean : data.get(key).drug) {
//                bean.isSelect = isSelect;
//                price = price + (bean.price * bean.count);
            }
        }
        if (isSelect) {
            return price;
        } else {
            return 0;
        }

    }
}
