package com.kelin.client.gui.address.controller;

import com.kelin.client.gui.address.MyAddressService;
import com.kelin.client.gui.address.entity.MyAddressEntity;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by guangjiqin on 2017/8/14.
 */

public abstract class AddAddressController {
    public abstract void addAddressSuccess();

    public abstract void addAddressFailure(String message);

    public abstract void editAddressSuccess();

    public abstract void editAddressFailure(String message);

    private String token;

    public AddAddressController(String token) {
        this.token = token;
    }


    //添加添加地址
    public void addAddressData(String receiveName, String receivePhone, String area, String receiveAddress, boolean flag) {
        RetrofitHelper.getInstance().createService(MyAddressService.class).addAddressList(token, receiveName, receivePhone, area, receiveAddress, flag).enqueue(new BaseCallBack<BaseCallModel<MyAddressEntity>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<MyAddressEntity>> response) {
                addAddressSuccess();
            }

            @Override
            public void onFailure(String message) {
                addAddressFailure(message);
            }
        });
    }

    public void editAddressData(String receiveName, int addressId, String receivePhone, String area, String receiveAddress, boolean flag) {
        RetrofitHelper.getInstance().createService(MyAddressService.class).editAddressList(token, addressId, receiveName, receivePhone, area, receiveAddress, flag).enqueue(new BaseCallBack<BaseCallModel<List<MyAddressEntity>>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<List<MyAddressEntity>>> response) {
                editAddressSuccess();
            }

            @Override
            public void onFailure(String message) {
                editAddressFailure(message);
            }
        });

    }
}
