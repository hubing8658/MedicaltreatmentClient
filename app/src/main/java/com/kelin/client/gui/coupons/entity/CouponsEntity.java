package com.kelin.client.gui.coupons.entity;

import java.util.List;

/**
 * Created by guangjiqin on 2017/8/15.
 */

public class CouponsEntity {


    /**
     * id : 3
     * number : 8304aeb767c1437f8764a7bad88b6422
     * couponName : 现金券
     * couponType : 分享链接
     * money : 160
     * startTime : 2017-06-30 14:32:50
     * endTime : 2017-06-30 14:32:50
     * isReceive : true
     * userId : 2
     * receiveTime : 2017-06-30 14:32:50
     * isUse : false
     * consumeDetailId : null
     * useTime : null
     * createTime : 2017-06-30 14:32:50
     */

    public int id;
    public String number;
    public String couponName;
    public String couponType;
    public int money;
    public String startTime;
    public String endTime;
    public boolean isReceive;
    public int userId;
    public String receiveTime;
    public boolean isUse;
    public Object consumeDetailId;
    public Object useTime;
    public String createTime;


}
