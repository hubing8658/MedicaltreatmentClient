package com.kelin.client.gui.pay;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bigkoo.pickerview.OptionsPickerView;
import com.bigkoo.pickerview.listener.CustomListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.kelin.client.R;
import com.kelin.client.bean.ProvinceBean;
import com.kelin.client.common.BaseActivity;
import com.kelin.client.common.Constans;
import com.kelin.client.gui.address.MyAddressService;
import com.kelin.client.gui.address.entity.MyAddressEntity;
import com.kelin.client.gui.login.LoginService;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.kelin.client.gui.medicinebox.fragment.NoPurchaseFragment;
import com.kelin.client.util.ToastUtils;
import com.kelin.client.widget.BaseTitleBar;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.monty.library.okhttp.RequestManger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by guangjiqin on 2017/9/20.
 */

public class PayMentActivity extends BaseActivity {

    private MyAddressEntity addressEntity;

    @BindView(R.id.titleBar)
    BaseTitleBar titleBar;

    //地址
    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.et_phone_num)
    EditText etPhoneNum;
    @BindView(R.id.tv_region)
    TextView tvRegion;
    @BindView(R.id.et_address)
    EditText etAddress;

    //支付方式
    @BindView(R.id.lin_cash_on_delivery)
    LinearLayout linCashOnDelivery;
    @BindView(R.id.lin_online_pay)
    LinearLayout linOnlinePay;
    @BindView(R.id.imgbt_cash_on_delivery)
    CheckBox imgbtCashOnDelivery;
    @BindView(R.id.imgbt_online_pay)
    CheckBox imgbtOnlinePay;

    //支付
    @BindView(R.id.tv_private)
    TextView tvPrivate;
    @BindView(R.id.bt_commit)
    Button btCommit;

    private double price;
    private ArrayList<Integer> diagnosticIds;

    private String weixinJson = "";
    private String aLiJson = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        Log.d("gg", "onCreate===================================================");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_ment);
        ButterKnife.bind(this);

        getIntentData();

        iniTitle();

        setOnClick();

        getAddressListData();
    }

    private void getWeixinAliJosn() {

        RequestManger requestManger = RequestManger.getInstance(this);
        HashMap<String, Object> params = new HashMap<>();

        if (null == addressEntity) {
            ToastUtils.showToast("默认收货地址为空，请先设置默认收货地址");
            finish();
            return;
        }
        String urlWeixin = "/record/drugPay?token=" + MyselfInfo.getLoginUser().getToken() + "&payType=1&consumeType=0" + "&addressId=" + addressEntity.id;//微信

        for (Integer key : NoPurchaseFragment.diagnosticIds.keySet()) {
            List<Integer> idList = NoPurchaseFragment.diagnosticIds.get(key);
            Integer[] ids = new Integer[idList.size()];
            for (int i = 0; i < idList.size(); i++) {
                ids[i] = idList.get(i);
            }
            params.put(String.valueOf(key), ids);
        }

        requestManger.requestAsy(urlWeixin, RequestManger.TYPE_POST, params, new RequestManger.ReuCallBack() {
            @Override
            public void onReSuccess(String result, okhttp3.Response response) {
                try {
                    if (!TextUtils.isEmpty(result)) {
                        Gson gson = new GsonBuilder().create();
                        JsonObject jsonObject = gson.fromJson(result, JsonObject.class);
                        int code = jsonObject.get("code").getAsInt();
                        if (code == 200) {
                            weixinJson = result;
                        } else {
                            String msg = jsonObject.get("msg").getAsString();
                            ToastUtils.showToast(msg);
                        }
                    }
                } catch (Exception e) {
                    ToastUtils.showToast("购买异常");
                }
                Log.e("gg", "weix = " + weixinJson);

            }

            @Override
            public void onReFailed(String errorMsg) {
                ToastUtils.showToast(errorMsg);
            }
        });

        String urlAli = "/record/drugPay?token=" + MyselfInfo.getLoginUser().getToken() + "&payType=1&consumeType=1" + "&addressId=" + addressEntity.id;//支付宝

        params.clear();
        for (Integer key : NoPurchaseFragment.diagnosticIds.keySet()) {
            List<Integer> idList = NoPurchaseFragment.diagnosticIds.get(key);
            Integer[] ids = new Integer[idList.size()];
            for (int i = 0; i < idList.size(); i++) {
                ids[i] = idList.get(i);
            }
            params.put(String.valueOf(key), ids);
        }

        requestManger.requestAsy(urlAli, RequestManger.TYPE_POST, params, new RequestManger.ReuCallBack() {
            @Override
            public void onReSuccess(String result, okhttp3.Response response) {
//                aLiJson = result;
                try {
                    if (!TextUtils.isEmpty(result)) {
                        Gson gson = new GsonBuilder().create();
                        JsonObject jsonObject = gson.fromJson(result, JsonObject.class);
                        int code = jsonObject.get("code").getAsInt();
                        if (code == 200) {
                            aLiJson = result;
                        } else {
                            String msg = jsonObject.get("msg").getAsString();
                            ToastUtils.showToast(msg);
                        }
                    }
                } catch (Exception e) {
                    ToastUtils.showToast("购买异常");
                }
                Log.e("gg", "aLiJson = " + aLiJson);

            }

            @Override
            public void onReFailed(String errorMsg) {
                ToastUtils.showToast(errorMsg);
            }
        });

    }

    private void getPayJson(boolean isCashOnDelivery) {//获取支付需要的json传给后面的页面

        if (null == addressEntity) {
            ToastUtils.showToast("请先保存联系人地址");
            return;
        }

        editAddressData(etName.getText().toString(), addressEntity.id, etPhoneNum.getText().toString(), tvRegion.getText().toString(), etAddress.getText().toString(), true);//防止修改地址


        if (isCashOnDelivery) {//货到付款
            String url = "/record/drugPay?token=" + MyselfInfo.getLoginUser().getToken() + "&payType=0" + "&addressId=" + addressEntity.id;

            RequestManger requestManger = RequestManger.getInstance(this);
            HashMap<String, Object> params = new HashMap<>();
            for (Integer key : NoPurchaseFragment.diagnosticIds.keySet()) {
                List<Integer> idList = NoPurchaseFragment.diagnosticIds.get(key);
                Integer[] ids = new Integer[idList.size()];
                for (int i = 0; i < idList.size(); i++) {
                    ids[i] = idList.get(i);
                }
                params.put(String.valueOf(key), ids);
            }

            requestManger.requestAsy(url, RequestManger.TYPE_POST, params, new RequestManger.ReuCallBack() {
                @Override
                public void onReSuccess(String result, okhttp3.Response response) {
                    try {
                        if (!TextUtils.isEmpty(result)) {
                            Gson gson = new GsonBuilder().create();
                            JsonObject jsonObject = gson.fromJson(result, JsonObject.class);
                            int code = jsonObject.get("code").getAsInt();
                            if (code == 200) {
                                ToastUtils.showToast("订单提交成功");
                            } else {
                                String msg = jsonObject.get("msg").getAsString();
                                ToastUtils.showToast(msg);
                            }
                        }
                    } catch (Exception e) {
                        ToastUtils.showToast("购买异常");
                    }
                    PayMentActivity.this.finish();
                }

                @Override
                public void onReFailed(String errorMsg) {
                    ToastUtils.showToast(errorMsg);
                }
            });
        } else {    //在线支付
            DrugPayActivity.gotoPayActivityByDrug(PayMentActivity.this, weixinJson, aLiJson, price);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == 101) {
            finish();
        }
    }

    private void getIntentData() {
        price = getIntent().getDoubleExtra("price", 0.0);
        diagnosticIds = getIntent().getIntegerArrayListExtra("diagnosticIds");
        tvPrivate.setText("" + price);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_region:
//                getProvinces();//正式接入代码注释掉
                showCityChoies();
                break;
            case R.id.lin_cash_on_delivery:
                imgbtCashOnDelivery.setChecked(!imgbtCashOnDelivery.isChecked());
                imgbtOnlinePay.setChecked(!imgbtCashOnDelivery.isChecked());
                break;
            case R.id.lin_online_pay:
                imgbtOnlinePay.setChecked(!imgbtOnlinePay.isChecked());
                imgbtCashOnDelivery.setChecked(!imgbtOnlinePay.isChecked());
                break;
            case R.id.bt_commit:
                pay();
                break;
        }
    }

    private void pay() {
        if (null == etName.getText() || etName.getText().toString().trim().isEmpty()) {
            ToastUtils.showToast(PayMentActivity.this, R.string.reipt_name_null);
            return;
        }

        if (null == etPhoneNum.getText() || etPhoneNum.getText().toString().trim().isEmpty()) {
            ToastUtils.showToast(PayMentActivity.this, R.string.reipt_phone_null);
            return;
        }

        if (null == tvRegion.getText() || tvRegion.getText().toString().trim().isEmpty()) {
            ToastUtils.showToast(PayMentActivity.this, R.string.reipt_district_null);
            return;
        }

        if (null == etAddress.getText() || etAddress.getText().toString().trim().isEmpty()) {
            ToastUtils.showToast(PayMentActivity.this, R.string.reipt_address_null);
            return;
        }

        getPayJson(imgbtCashOnDelivery.isChecked());

//        if (imgbtCashOnDelivery.isChecked()) {//货到付款
//
//        } else {//在线支付
//            ToastUtils.showToast("支付");// TODO: 2017/9/20
//        DrugPayActivity.gotoPayActivity(this,0,,price);
//            DrugPayActivity.gotoPayActivityByDrug();
//        }
    }

    private void setOnClick() {
        tvRegion.setOnClickListener(this);
        linCashOnDelivery.setOnClickListener(this);
        linOnlinePay.setOnClickListener(this);
        btCommit.setOnClickListener(this);
    }

    private void iniTitle() {
        this.titleBar.showCenterText("PAYMENT","支付");
        this.titleBar.setRightText("保存", 0, 0, new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (null == etName.getText() || etName.getText().toString().trim().isEmpty()) {
                    ToastUtils.showToast(PayMentActivity.this, R.string.reipt_name_null);
                    return;
                }

                if (null == etPhoneNum.getText() || etPhoneNum.getText().toString().trim().isEmpty()) {
                    ToastUtils.showToast(PayMentActivity.this, R.string.reipt_phone_null);
                    return;
                }

                if (null == tvRegion.getText() || tvRegion.getText().toString().trim().isEmpty()) {
                    ToastUtils.showToast(PayMentActivity.this, R.string.reipt_district_null);
                    return;
                }

                if (null == etAddress.getText() || etAddress.getText().toString().trim().isEmpty()) {
                    ToastUtils.showToast(PayMentActivity.this, R.string.reipt_address_null);
                    return;
                }
                if (null == addressEntity) {
                    addAddressData(etName.getText().toString(), etPhoneNum.getText().toString(), tvRegion.getText().toString(), etAddress.getText().toString());
                } else {
                    editAddressData(etName.getText().toString(), addressEntity.id, etPhoneNum.getText().toString(), tvRegion.getText().toString(), etAddress.getText().toString(), true);

                }
            }
        });
    }


    //弹出选择框======
    private ArrayList<ProvinceBean> options1Items = new ArrayList<>();
    private ArrayList<ArrayList<String>> options2Items = new ArrayList<>();
    private OptionsPickerView pvOptions;

    private void showCityChoies() {
        options1Items.clear();
        options2Items.clear();

        for (String key : Constans.provinces.keySet()) {
            options1Items.add(new ProvinceBean(-1, key, "", ""));
            ArrayList<String> options2Items_01 = new ArrayList<>();
            for (String value : Constans.provinces.get(key)) {
                options2Items_01.add(value);
            }
            options2Items.add(options2Items_01);
        }

        pvOptions = new OptionsPickerView.Builder(this, new OptionsPickerView.OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int options2, int options3, View v) {
                //返回的分别是三个级别的选中位置
                String tx = options1Items.get(options1).getPickerViewText()
                        + options2Items.get(options1).get(options2)
                       /* + options3Items.get(options1).get(options2).get(options3).getPickerViewText()*/;

                //设置位置
                tvRegion.setText(tx);
                pvOptions.dismiss();
            }
        }).setLayoutRes(R.layout.dialog_city_choise, new CustomListener() {
            @Override
            public void customLayout(View v) {
                Button btnSubmit = (Button) v.findViewById(R.id.btnSubmit);
                btnSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        pvOptions.returnData();
                    }
                });

            }
        })
//                .setTitleText("城市选择")
                .setContentTextSize(16)//设置滚轮文字大小
                .setDividerColor(Color.parseColor("#0f345e"))//设置分割线的颜色
                .setSelectOptions(0, 1)//默认选中项
                .setTextColorCenter(Color.parseColor("#f5b8d3"))
                .setTextColorOut(Color.parseColor("#0f345e"))
                .isCenterLabel(false) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
//                .setLabels("省", "市", "区")
                .setBackgroundId(0x66000000) //设置外部遮罩颜色
                .build();

        pvOptions.setPicker(options1Items, options2Items);//二级选择器
        pvOptions.show();
    }

    //获取城市级联的接口(该方法接入正式代码中注释掉)
    private void getProvinces() {
        RetrofitHelper.getInstance().createService(LoginService.class).getProvinces(MyselfInfo.getLoginUser().getToken()).enqueue(new BaseCallBack<BaseCallModel<Map<String, List<String>>>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<Map<String, List<String>>>> response) {
                Constans.provinces = response.body().data;
            }

            @Override
            public void onFailure(String message) {

            }
        });
    }

    //添加添加地址
    public void addAddressData(String receiveName, String receivePhone, String area, String receiveAddress) {
        RetrofitHelper.getInstance().createService(MyAddressService.class).addAddressList(MyselfInfo.getLoginUser().getToken(), receiveName, receivePhone, area, receiveAddress, true).enqueue(new BaseCallBack<BaseCallModel<MyAddressEntity>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<MyAddressEntity>> response) {
                getAddressListData();
            }

            @Override
            public void onFailure(String message) {
                ToastUtils.showToast("保存成功");
            }
        });
    }


    //保存地址
    private void editAddressData(String receiveName, int addressId, String receivePhone, String area, String receiveAddress, boolean flag) {
        RetrofitHelper.getInstance().createService(MyAddressService.class).editAddressList(MyselfInfo.getLoginUser().getToken(), addressId, receiveName, receivePhone, area, receiveAddress, flag).enqueue(new BaseCallBack<BaseCallModel<List<MyAddressEntity>>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<List<MyAddressEntity>>> response) {
//                ToastUtils.showToast("修改成功");
            }

            @Override
            public void onFailure(String message) {
                ToastUtils.showToast(message);
            }
        });

    }


    //获取收货地址
    private void getAddressListData() {
        RetrofitHelper.getInstance().createService(MyAddressService.class).getAddressList(MyselfInfo.getLoginUser().getToken()).enqueue(new BaseCallBack<BaseCallModel<List<MyAddressEntity>>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<List<MyAddressEntity>>> response) {
                List<MyAddressEntity> treatmentType = response.body().data;
                for (MyAddressEntity myAddressEntity : treatmentType) {
                    if (myAddressEntity.isDefault) {
                        addressEntity = myAddressEntity;
                        break;
                    }
                }

                if (null != addressEntity) {
                    etName.setText(null == addressEntity.receiveName ? "" : addressEntity.receiveName);
                    etPhoneNum.setText(null == addressEntity.receivePhone ? "" : addressEntity.receivePhone);
                    tvRegion.setText(null == addressEntity.area ? "" : addressEntity.area);
                    etAddress.setText(null == addressEntity.receiveAddress ? "" : addressEntity.receiveAddress);
                    getWeixinAliJosn();
                }
//                getWeixinAliJosn();
            }

            @Override
            public void onFailure(String message) {
                ToastUtils.showToast(message);
            }
        });
    }
}

