package com.kelin.client.gui.mydoctor.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.kelin.client.R;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.kelin.client.gui.mydoctor.bean.TreatmentType;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by monty on 2017/8/1.
 */

public class GridAdapter extends BaseAdapter {
    private List<TreatmentType> treatmentTypeList;

    private int[] imgs = {R.drawable.home_icon_acne
            , R.drawable.home_icon_allergy
            , R.drawable.home_icon_barbiers
            , R.drawable.home_icon_freckle
            , R.drawable.home_icon_hairloss
            , R.drawable.home_icon_lichen
            , R.drawable.home_icon_odor
            , R.drawable.home_icon_tineaunguium
            , R.drawable.home_icon_pouch
            , R.drawable.home_icon_scar};

    private LayoutInflater inflater;
    private Context context;
    private int selectIndex = -1; // 当前选中的病症

    public GridAdapter(Context context, int selectIndex) {
        this.context = context;
        this.selectIndex = selectIndex;
        this.inflater = LayoutInflater.from(this.context);
        initTreatmentTyleList();
    }

    private void initTreatmentTyleList() {

    }

    @Override
    public int getCount() {
        return treatmentTypeList == null ? imgs.length : (treatmentTypeList.size() > 10 ? 10 : treatmentTypeList.size());
//        return treatmentTypeList.size();
    }

    @Override
    public Object getItem(int position) {
        return treatmentTypeList == null ? imgs[position] : treatmentTypeList.get(position);
//        return treatmentTypeList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.layout_disease_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if (selectIndex <= getCount() && treatmentTypeList != null && getCount() > 0 && selectIndex == treatmentTypeList.get(position).getId()) {
//            viewHolder.itemView.setBackgroundResource(R.color.pink);
            viewHolder.ivShadow.setVisibility(View.VISIBLE);
        } else {
//            viewHolder.itemView.setBackground(null);
            viewHolder.ivShadow.setVisibility(View.INVISIBLE);
        }
        if (treatmentTypeList == null) {
            Glide.with(context).load(imgs[position]).into(viewHolder.ivDiseaseIcon);
        } else {
            String url = treatmentTypeList.get(position).getPicUrl();
            Glide.with(context).load(url).into(viewHolder.ivDiseaseIcon);
        }
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectIndex = treatmentTypeList.get(position).getId();
                MyselfInfo.setTreatmentTypeId(treatmentTypeList.get(position).getId());
                notifyDataSetChanged();
                listener.onItemClick(position);
            }
        });
        return convertView;
    }

    public void setSelectIndex(int selectIndex) {
        this.selectIndex = selectIndex;
        notifyDataSetChanged();
    }

    private OnItemClickListener listener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void notifyDataSetChanged(List<TreatmentType> treatmentTypeList) {
        this.treatmentTypeList = treatmentTypeList;
        notifyDataSetChanged();
    }

    static class ViewHolder {
        @BindView(R.id.iv_disease_icon)
        ImageView ivDiseaseIcon;
        @BindView(R.id.iv_shadow)
        ImageView ivShadow;

        View itemView;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
            itemView = view;
        }
    }
}
