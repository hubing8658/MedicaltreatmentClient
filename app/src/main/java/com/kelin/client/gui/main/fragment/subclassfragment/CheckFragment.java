package com.kelin.client.gui.main.fragment.subclassfragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.kelin.client.R;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.kelin.client.gui.main.adapter.MedicalRecordListAdapter;
import com.kelin.client.gui.main.bean.MedicalRecordBean;
import com.kelin.client.gui.main.service.MainService;
import com.kelin.client.util.ToastUtils;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;

import java.util.ArrayList;
import java.util.List;

import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;
import retrofit2.Response;

/**
 * Created by guangjiqin on 2017/8/8.
 */

public class CheckFragment extends Fragment {

    private ListView listView;
    private View view;
    private List<MedicalRecordBean> medicalRecordEntities;
    private MedicalRecordListAdapter adapter;
    private PtrClassicFrameLayout mPtrFrame;
    private static CheckFragment checkFragment;

    public static CheckFragment createInstance() {
        if (null == checkFragment) {
            checkFragment = new CheckFragment();
        }
        return checkFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_check, null);

        initView();

        initPullRefresh();

//        initData();
//        mPtrFrame.autoRefresh();

        return view;

    }

    private void initData() {
        RetrofitHelper.getInstance().createService(MainService.class).getRevisit(MyselfInfo.getLoginUser().getToken()).enqueue(new BaseCallBack<BaseCallModel<List<MedicalRecordBean>>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<List<MedicalRecordBean>>> response) {
                mPtrFrame.refreshComplete();
                medicalRecordEntities.clear();
                medicalRecordEntities.addAll(response.body().data);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(String message) {
                mPtrFrame.refreshComplete();
                ToastUtils.showToast(message);
            }
        });
    }


    private void initView() {
        mPtrFrame = (PtrClassicFrameLayout) view.findViewById(R.id.store_house_ptr_frame);

        medicalRecordEntities = new ArrayList<>();
        listView = (ListView) view.findViewById(R.id.listview);
        adapter = new MedicalRecordListAdapter(getActivity(), medicalRecordEntities);
        listView.setAdapter(adapter);
    }

    private void initPullRefresh() {
        mPtrFrame.setLastUpdateTimeRelateObject(this);
        mPtrFrame.setPtrHandler(new PtrHandler() {
            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
                return PtrDefaultHandler.checkContentCanBePulledDown(frame, listView, header);
            }

            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                initData();
            }
        });


        // the following are default settings
        mPtrFrame.setResistance(1.7f);
        mPtrFrame.setRatioOfHeaderHeightToRefresh(1.2f);
        mPtrFrame.setDurationToClose(200);
        mPtrFrame.setDurationToCloseHeader(1000);
        // default is false
        mPtrFrame.setPullToRefresh(false);
        // default is true
        mPtrFrame.setKeepHeaderWhenRefresh(true);
        mPtrFrame.postDelayed(new Runnable() {
            @Override
            public void run() {
                mPtrFrame.autoRefresh();
            }
        }, 100);
    }
}