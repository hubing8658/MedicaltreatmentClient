package com.kelin.client.gui.live.controller;

import android.content.Context;


import com.kelin.client.gui.live.event.CurVisitorTimeBean;
import com.kelin.client.gui.live.model.MySelfInfo;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.kelin.client.gui.setting.MyCenterService;
import com.kelin.client.util.ToastUtils;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;

import retrofit2.Response;


/**
 * Created by guangjiqin on 2017/8/4.
 */

public abstract class GetVisitorTimeController {

    public abstract void getDataSuccess(CurVisitorTimeBean curVisitorTimeBean);
    public abstract void getDataFsailrue(String msg);

    private Context context;


    public GetVisitorTimeController(Context context) {
        this.context = context;
    }

    public void getData(String token){
        RetrofitHelper.getInstance().createService(MyCenterService.class).getCurVisitorTime(MyselfInfo.getLoginUser().getToken(),token).enqueue(new BaseCallBack<BaseCallModel<CurVisitorTimeBean>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<CurVisitorTimeBean>> response) {
                getDataSuccess(response.body().data);
            }

            @Override
            public void onFailure(String msg) {
                ToastUtils.showToast(msg);
                getDataFsailrue(msg);
            }
        });
    }
}
