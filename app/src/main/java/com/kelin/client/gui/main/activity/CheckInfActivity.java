package com.kelin.client.gui.main.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.kelin.client.R;
import com.kelin.client.common.BaseActivity;
import com.kelin.client.gui.live.LiveActivity;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.kelin.client.gui.main.bean.CheckInfEntity;
import com.kelin.client.gui.main.bean.MedicalRecordBean;
import com.kelin.client.gui.main.service.MainService;
import com.kelin.client.gui.mydoctor.bean.Doctor;
import com.kelin.client.gui.therapeutic.TherapeuticMainActivity;
import com.kelin.client.util.ToastUtils;
import com.kelin.client.util.imageloader.GlideImageHelper;
import com.kelin.client.widget.BaseTitleBar;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.monty.library.widget.dialog.NiceDialogFactory;
import com.othershe.nicedialog.BaseNiceDialog;
import com.othershe.nicedialog.NiceDialog;
import com.othershe.nicedialog.ViewConvertListener;
import com.othershe.nicedialog.ViewHolder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by guangjiqin on 2017/10/11.
 */

public class CheckInfActivity extends BaseActivity {

    @BindView(R.id.titleBar)
    BaseTitleBar title;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_type)
    TextView tvType;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.bt_inf)
    Button btInf;
    @BindView(R.id.img_doc)
    ImageView imgDoc;
    @BindView(R.id.bt_operation)
    Button btOperation;

    private MedicalRecordBean entity;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_inf);
        ButterKnife.bind(this);

        initTitle();

        getIntentData();

        initView();

        btInf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CheckInfActivity.this, TherapeuticMainActivity.class);
                intent.putExtra("diagnostic_id", entity.diagnosticId);
                startActivity(intent);
            }
        });

        btOperation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //判断是否过期 1过期  2未到时候  3时间刚好
                switch (checkTime()) {
                    case 1:
                        RetrofitHelper.getInstance().createService(MainService.class).delayRevisit(MyselfInfo.getLoginUser().getToken(), entity.diagnosticId).enqueue(new BaseCallBack<BaseCallModel<String>>() {

                            @Override
                            public void onSuccess(Response<BaseCallModel<String>> response) {
                                NiceDialogFactory.createComfirmDialog("为您匹配下一个复查时间", response.body().data, new NiceDialogFactory.OnPositiveClickListener() {
                                    @Override
                                    public void onPositiveClick(BaseNiceDialog baseNiceDialog) {
                                        baseNiceDialog.dismiss();
                                        btOperation.setTextColor(Color.parseColor("#ffffff"));
                                        btOperation.setBackgroundResource(R.drawable.button_deep_gray_bg);
                                        btOperation.setEnabled(false);
                                    }
                                }, getResources().getColor(R.color.pink_new)).setOutCancel(false).show(getSupportFragmentManager());
                            }

                            @Override
                            public void onFailure(String message) {
                                ToastUtils.showToast(message);
                            }
                        });
                        break;
                    case 2:
                        ToastUtils.showToast("未到约定复查时间");
                        btOperation.setTextColor(Color.parseColor("#ffffff"));
                        btOperation.setBackgroundResource(R.drawable.button_deep_gray_bg);
                        btOperation.setEnabled(false);
                        break;
                    case 3:
                        RetrofitHelper.getInstance().createService(MainService.class).revisit(MyselfInfo.getLoginUser().getToken(), entity.diagnosticId).enqueue(new BaseCallBack<BaseCallModel<CheckInfEntity>>() {
                            @Override
                            public void onSuccess(Response<BaseCallModel<CheckInfEntity>> response) {
                                switch (response.code()) {//-100医生已注销，-200医生不在房间中 ,200正常进入
                                    case 200:
                                        Doctor doctor = new Doctor();
                                        doctor.setName(entity.doctor.name);
                                        doctor.setLocalHospital(entity.doctor.localHospital);
                                        doctor.setLevel(entity.doctor.level);//该值目测没有
                                        doctor.setIntroduceUrl(entity.doctor.introduceUrl);
//                                        DoctorVideoActivity.startLiveActivity(CheckInfActivity.this,doctor);
                                        LiveActivity.startLiveActivity(CheckInfActivity.this,doctor,0);
                                        break;

                                }
                            }

                            @Override
                            public void onFailure(String message) {
                                if ("医生不在坐诊中".equals(message)) {
                                    ToastUtils.showToast(message);// 医生不在坐诊中
                                } else {
                                    showChangeDocDialog();
                                }
                            }
                        });
                        break;

                }
            }
        });

    }

    private int checkTime() {
        long entityTime = 0;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = simpleDateFormat.parse(entity.revisitDate);
            entityTime = date.getTime();
            Log.e("gg", "date = " + date.toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (System.currentTimeMillis() > (entityTime + (24 * 60 * 60 * 1000))) {//已过期
            return 1;
        } else if (System.currentTimeMillis() < entityTime) {//未到期
            return 2;
        } else {//时间刚好
            return 3;
        }
    }

    @Override
    protected void initView() {
        super.initView();
        tvName.setText(entity.doctor.name);
        tvDate.setText(entity.revisitDate);
        GlideImageHelper.showImage(this, entity.doctor.pic, R.drawable.bld105819, R.drawable.bld105819, imgDoc);

    }

    private void getIntentData() {
        entity = getIntent().getParcelableExtra("entity");
        Log.e("gg", "entity = " + entity.toString());
    }

    private void initTitle() {
        title.showCenterText("RECORDS","治疗记录");
    }


    private void showChangeDocDialog() {
        NiceDialog.init()
                .setLayoutId(R.layout.base_dilog_layout)
                .setConvertListener(new ViewConvertListener() {
                    @Override
                    protected void convertView(ViewHolder viewHolder, final BaseNiceDialog baseNiceDialog) {
                        ((TextView) viewHolder.getView(R.id.tv_title)).setText("医生已注销，为您升级更高级医生");
                        ((TextView) viewHolder.getView(R.id.tv_message)).setText(entity.doctor.name);
                        ((Button) viewHolder.getView(R.id.btn_positive)).setText("  确定");
                        viewHolder.getView(R.id.btn_positive).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Doctor doctor = new Doctor();
                                doctor.setName(entity.doctor.name);
                                doctor.setLocalHospital(entity.doctor.localHospital);
                                doctor.setLevel(entity.doctor.level);//该值目测没有
                                doctor.setIntroduceUrl(entity.doctor.introduceUrl);
//                                DoctorVideoActivity.startLiveActivity(CheckInfActivity.this,doctor);
                                LiveActivity.startLiveActivity(CheckInfActivity.this,doctor,0);
                            }
                        });
                        viewHolder.getView(R.id.btn_negative).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                closeLoadingDialog();
                            }
                        });
                    }
                })   //进行相关View操作的回调
                .setDimAmount(0.5f)     //调节灰色背景透明度[0-1]，默认0.5f
//                .setShowBottom(true)     //是否在底部显示dialog，默认flase
                .setMargin(30)     //dialog左右两边到屏幕边缘的距离（单位：dp），默认0dp
//                .setWidth()     //dialog宽度（单位：dp），默认为屏幕宽度
//                .setHeight()     //dialog高度（单位：dp），默认为WRAP_CONTENT
                .setOutCancel(false)     //点击dialog外是否可取消，默认true
                .setAnimStyle(android.R.style.Animation_Dialog).show(getSupportFragmentManager());
    }

}
