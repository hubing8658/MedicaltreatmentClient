package com.kelin.client.gui.pay;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kelin.client.R;
import com.kelin.client.common.BaseActivity;
import com.kelin.client.common.Constans;
import com.kelin.client.common.IntentConstants;
import com.kelin.client.gui.live.event.JumpToIMFragemtEvent;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.kelin.client.gui.pay.event.WxEvent;
import com.kelin.client.util.ToastUtils;
import com.kelin.client.widget.BaseTitleBar;
import com.monty.library.EventBusUtil;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.tsy.sdk.pay.alipay.Alipay;
import com.tsy.sdk.pay.weixin.WXPay;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * 诊金支付
 */
public class DiagnosePayActivity extends BaseActivity {

    @BindView(R.id.titleBar)
    BaseTitleBar titleBar;
    @BindView(R.id.tv_price)
    TextView tvPrice;
    @BindView(R.id.ll_alipay)
    LinearLayout llAlipay;
    @BindView(R.id.ll_wechat)
    LinearLayout llWechat;
    @BindView(R.id.btn_pay)
    Button btnPay;
    @BindView(R.id.iv_alipaySelect)
    ImageView ivAlipaySelect;
    @BindView(R.id.iv_wechatSelect)
    ImageView ivWechatSelect;
    @BindView(R.id.tv_deductionMoney)
    TextView tvDeductionMoney;
    @BindView(R.id.tv_money)
    TextView tvMoney;

    /**
     * 优惠券ids
     */
    private String couponIdss;

    private List<Integer> couponIds = new ArrayList<>();

    /**
     * 支付类型
     */
    private int payType = 0;


    /**
     * 诊断id
     */
    private long diagnosticId;

    /**
     * 微信支付SDK需要的参数
     */
    private String wxParams;

    /**
     * 支付宝支付SDK需要的参数
     */
    private String aliParams;

    /*诊金费用*/
    private double price = 0;
    /*抵扣金额*/
    private double deductionMoney;
    /*实际支付金额 （诊金费用 - 抵扣金额）*/
    private double money;

    /*扣款类型 0 - 使用优惠券支付(优惠券>=需支付的金额)
              1 - 使用现金支付(优惠券<需支付的金额)*/
    private int payMontyType;

    private String orderId; //订单号

    /**
     * 支付诊金入口
     *
     * @param context
     * @param diagnosticId 诊断记录Id
     */
    public static void GotoPayActivity(Context context, long diagnosticId) {
        Intent intent = new Intent(context, DiagnosePayActivity.class);
//        intent.putExtra(IntentConstants.KEY_COUPON_ID, couponId);
        intent.putExtra(IntentConstants.KEY_DIAGNOSTIC_ID, diagnosticId);
//        intent.putExtra(IntentConstants.KEY_PRICE, price);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.diagnose_pay);
        ButterKnife.bind(this);
        getIntentData();
        initView();
        initData();
        EventBusUtil.register(this);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBusUtil.unregister(this);
    }

    @Subscribe
    public void onWxEvent(WxEvent wxEvent) {
        switch (wxEvent.code) {
            case 0:
                ToastUtils.showToast("支付成功");
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                }, 100);
                break;
            case -1:
                ToastUtils.showToast("支付失败");
                cancelPay(orderId);
                break;
            case -2:
                ToastUtils.showToast("支付取消");
                cancelPay(orderId);
                break;
            default:
                ToastUtils.showToast("支付失败");
                cancelPay(orderId);
                break;
        }
    }

    public void getIntentData() {
        Intent intent = getIntent();
        if (intent != null) {
//            couponId = intent.getIntExtra(IntentConstants.KEY_COUPON_ID, 0);
            diagnosticId = intent.getLongExtra(IntentConstants.KEY_DIAGNOSTIC_ID, 0);
//            price = intent.getDoubleExtra(IntentConstants.KEY_PRICE, 0);
//            wxParams = intent.getStringExtra(IntentConstants.KEY_WXPAY_PARAMS);
//            aliParams = intent.getStringExtra(IntentConstants.KEY_ALIPAY_PARAMS);
        }
    }

    private DiagnosticInfo.Coupon coupon; // 要使用的优惠券

    @Override
    protected void initData() {
        showLoadingDialog("");
        RetrofitHelper.getInstance().createService(PayService.class).getDiagnostic(MyselfInfo.getLoginUser().getToken(), diagnosticId + "").enqueue(new BaseCallBack<BaseCallModel<DiagnosticInfo>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<DiagnosticInfo>> response) {
                DiagnosticInfo diagnosticInfo = response.body().data;
                tvPrice.setText("￥" + diagnosticInfo.getPrice());
                price = diagnosticInfo.getPrice();
                /*// 获取到优惠券列表,并取第一个优惠券
                List<DiagnosticInfo.Coupon> coupons = diagnosticInfo.getCoupon();
                if (coupons != null && coupons.size() > 0) {
                    DiagnosticInfo.Coupon coupon = coupons.get(0);
                    couponId = coupon.getId();
                    deductionMoney = coupon.getMoney();
                }

                if (deductionMoney >= price) { // 优惠券金额是否大于需支付的金额
                    payMontyType = 0; // 使用优惠券支付
                    money = 0; // 如果是优惠券支付则需要支付的现金为0
                } else {
                    payMontyType = 1; // 使用现金支付
                    money = price - deductionMoney;// 如果是现金支付则需要支付的现金为 诊金减现金券金额
                }*/
                List<DiagnosticInfo.Coupon> coupons = diagnosticInfo.getCoupon();


                for (int i = 0; i < coupons.size(); i++) {
                    if (deductionMoney < price) {
                        deductionMoney += coupons.get(i).getMoney();
                        couponIds.add(coupons.get(i).getId());
                    } else {
                        couponIds.add(coupons.get(i).getId());
                        break;
                    }
                }

                if (deductionMoney <= price) {
                    money = price - deductionMoney;
                } else {
                    money = 0;
                }

                tvDeductionMoney.setText("-" + deductionMoney);
                tvMoney.setText("￥" + money);
                closeLoadingDialog();
            }

            @Override
            public void onFailure(String message) {
                ToastUtils.showToast(message);
                closeLoadingDialog();
            }
        });
    }

    private void payDiagnosticByCoupon(final String couponIds) {
        showLoadingDialog("");
        RetrofitHelper.getInstance().createService(PayService.class).payDiagnosticByCoupon(MyselfInfo.getLoginUser().getToken(), diagnosticId, couponIds).enqueue(new BaseCallBack<BaseCallModel<AliPayEntity>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<AliPayEntity>> response) {
                ToastUtils.showToast("使用现金券支付成功");
                closeLoadingDialog();
                DiagnosePayActivity.this.finish();
                EventBusUtil.post(new JumpToIMFragemtEvent());
            }

            @Override
            public void onFailure(String message) {
                ToastUtils.showToast(message);
                closeLoadingDialog();
            }
        });
    }

    @Override
    protected void initView() {
        this.titleBar.showCenterText("PAYMENT","支付");
        this.titleBar.setBackBtnClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        selectPay(0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getPayResult(orderId);
    }

    public void aliPay() {
        RetrofitHelper.getInstance().createService(PayService.class).aliPayDiagnostic(MyselfInfo.getLoginUser().getToken(), 1, couponIdss, diagnosticId).enqueue(new BaseCallBack<BaseCallModel<AliPayEntity>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<AliPayEntity>> response) {
                closeLoadingDialog();
                AliPayEntity data = response.body().data;
                orderId = data.orderId;
                new Alipay(DiagnosePayActivity.this, data.content, new Alipay.AlipayResultCallBack() {
                    @Override
                    public void onSuccess() {
                        Toast.makeText(getApplication(), "支付成功", Toast.LENGTH_SHORT).show();
                        DiagnosePayActivity.this.finish();
                        EventBusUtil.post(new JumpToIMFragemtEvent());
                    }

                    @Override
                    public void onDealing() {
                        Toast.makeText(getApplication(), "支付处理中...", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(int error_code) {
                        switch (error_code) {
                            case Alipay.ERROR_RESULT:
                                Toast.makeText(getApplication(), "支付失败:支付结果解析错误", Toast.LENGTH_SHORT).show();
                                break;

                            case Alipay.ERROR_NETWORK:
                                Toast.makeText(getApplication(), "支付失败:网络连接错误", Toast.LENGTH_SHORT).show();
                                break;

                            case Alipay.ERROR_PAY:
                                Toast.makeText(getApplication(), "支付错误:支付码支付失败", Toast.LENGTH_SHORT).show();
                                break;

                            default:
                                Toast.makeText(getApplication(), "支付错误", Toast.LENGTH_SHORT).show();
                                break;
                        }
                        cancelPay(orderId);
                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(getApplication(), "支付取消", Toast.LENGTH_SHORT).show();
                        cancelPay(orderId);
                    }
                }).doPay();
            }

            @Override
            public void onFailure(String message) {
                ToastUtils.showToast(message);
                closeLoadingDialog();
            }
        });
    }

    public void wechatPay() {
        RetrofitHelper.getInstance().createService(PayService.class).wechatPayDiagnostic(MyselfInfo.getLoginUser().getToken(), 0, couponIdss, diagnosticId).enqueue(new BaseCallBack<BaseCallModel<WechatPayEntity>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<WechatPayEntity>> response) {
                WechatPayEntity data = response.body().data;
                orderId = data.getOrderId();
                Gson gson = new GsonBuilder().serializeNulls().create();
                String json = gson.toJson(data);
                if (!TextUtils.isEmpty(json)) {
                    json = json.replace("packageValue", "package");
                }
                closeLoadingDialog();
                WXPay.init(getApplicationContext(), Constans.APP_ID_WX_PAY);      //要在支付前调用
                WXPay.getInstance().doPay(json, new WXPay.WXPayResultCallBack() {
                    @Override
                    public void onSuccess() {
                        Toast.makeText(getApplication(), "支付成功", Toast.LENGTH_SHORT).show();
                        DiagnosePayActivity.this.finish();
                        EventBusUtil.post(new JumpToIMFragemtEvent());
                    }

                    @Override
                    public void onError(int error_code) {
                        switch (error_code) {
                            case WXPay.NO_OR_LOW_WX:
                                Toast.makeText(getApplication(), "未安装微信或微信版本过低", Toast.LENGTH_SHORT).show();
                                break;

                            case WXPay.ERROR_PAY_PARAM:
                                Toast.makeText(getApplication(), "参数错误", Toast.LENGTH_SHORT).show();
                                break;

                            case WXPay.ERROR_PAY:
                                Toast.makeText(getApplication(), "支付失败", Toast.LENGTH_SHORT).show();
                                break;
                        }
                        cancelPay(orderId);
                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(getApplication(), "支付取消", Toast.LENGTH_SHORT).show();
                        cancelPay(orderId);
                    }
                });
            }

            @Override
            public void onFailure(String message) {
                ToastUtils.showToast(message);
                closeLoadingDialog();
            }
        });
    }

    @OnClick({R.id.ll_alipay, R.id.ll_wechat, R.id.btn_pay})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_alipay:
                selectPay(0);
                break;
            case R.id.ll_wechat:
                selectPay(1);
                break;
            case R.id.btn_pay:
                Log.d("monty","诊金支付支付");
                if (couponIds != null && couponIds.size() > 0) {
                    StringBuilder couponIdStrBuilder = new StringBuilder();
                    for (int i : couponIds) {
                        couponIdStrBuilder.append("" + i);
                        couponIdStrBuilder.append(",");
                    }
                    String couponIdStr = couponIdStrBuilder.toString();
                    couponIdss = couponIdStr.substring(0, couponIdStr.length() - 1);
                }

                if (deductionMoney >= price) { // 现金券是否足够支付本次诊金，如果足够支付就弹出使用现金券支付的Dialog，否则就弹出现金支付的Dialog
                    payDiagnosticByCoupon(couponIdss);
                } else {
                    showLoadingDialog("正在请求支付信息");
                    if (payType == 0) { // 支付宝
                        aliPay();
                    } else {
                        wechatPay(); // 微信
                    }
                }
                break;
        }
    }

    /**
     * 选择支付方式： 0 - 支付宝；1 - 微信
     *
     * @param type
     */
    private void selectPay(int type) {
        if (type == 0) {
            ivAlipaySelect.setVisibility(View.VISIBLE);
            ivWechatSelect.setVisibility(View.GONE);
        } else {
            ivAlipaySelect.setVisibility(View.GONE);
            ivWechatSelect.setVisibility(View.VISIBLE);
        }
        payType = type;
    }

    private void getPayResult(String orderId) {
        // TODO: 2017/10/2 获取支付结果
        RetrofitHelper.getInstance().createService(PayService.class).getPayResult(MyselfInfo.getLoginUser().getToken(), orderId).enqueue(new BaseCallBack<BaseCallModel<PayResult>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<PayResult>> response) {
                ToastUtils.showToast("支付成功");
                DiagnosePayActivity.this.finish();
            }

            @Override
            public void onFailure(String message) {
                ToastUtils.showToast(message);
            }
        });
    }

    private void cancelPay(String orderId) {
        RetrofitHelper.getInstance().createService(PayService.class).cancelPay(MyselfInfo.getLoginUser().getToken(), orderId).enqueue(new BaseCallBack<BaseCallModel<PayResult>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<PayResult>> response) {
                ToastUtils.showToast("退回现金券");
            }

            @Override
            public void onFailure(String message) {
                ToastUtils.showToast("现金券退回失败，请联系客服");
            }
        });
    }

}
