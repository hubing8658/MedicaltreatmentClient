package com.kelin.client.gui.main.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by guangjiqin on 2017/8/8.
 */

public class MedicalRecordBean implements Parcelable {
    /**
     * doctor : {"sumTime":50726,"roleId":1,"localHospital":"1111","name":"宇智波佐助","count":1028,"id":4,"pic":"http://123.207.9.75:8080/upload-file/doctor/photo/83ff7e5d2b9147f68125d2fa88cb731e.jpg","job":"火影","introduceUrl":"http://123.207.9.75:8080/upload-file/doctor/introduce_url/c088cc56ff684d9790a1cc45c01675b5.mp4","introduction":"1111","token":"eh1231871583wt","treatmentNames":"痘痘,脸部皮炎,脚气,斑,脱发,鸡皮肤,腋臭,xiongmaoyan,疤痕,灰指甲"}
     * revisitStartTime : 03:35
     * revisitDate : 2017-10-09
     * diagnosticTime : 2017-10-06 00:27:05
     * diagnosticId : 750
     * revisitEndTime : 06:35
     * revisitContent : 痘痘
     * treatmentName : 痘痘
     */

    public DoctorBean doctor;
    public String revisitStartTime;
    public String revisitDate;
    public String diagnosticTime;
    public int diagnosticId;
    public String revisitEndTime;
    public String revisitContent;
    public String treatmentName;



    public static class DoctorBean implements Parcelable {
        /**
         * sumTime : 50726
         * roleId : 1
         * localHospital : 1111
         * name : 宇智波佐助
         * count : 1028
         * id : 4
         * pic : http://123.207.9.75:8080/upload-file/doctor/photo/83ff7e5d2b9147f68125d2fa88cb731e.jpg
         * job : 火影
         * introduceUrl : http://123.207.9.75:8080/upload-file/doctor/introduce_url/c088cc56ff684d9790a1cc45c01675b5.mp4
         * introduction : 1111
         * token : eh1231871583wt
         * treatmentNames : 痘痘,脸部皮炎,脚气,斑,脱发,鸡皮肤,腋臭,xiongmaoyan,疤痕,灰指甲
         */

        public int sumTime;
        public int roleId;
        public String localHospital;
        public String name;
        public int count;
        public int id;
        public String pic;
        public String job;
        public String introduceUrl;
        public String introduction;
        public String token;
        public String treatmentNames;

        public String level;//貌似没有


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.sumTime);
            dest.writeInt(this.roleId);
            dest.writeString(this.localHospital);
            dest.writeString(this.name);
            dest.writeInt(this.count);
            dest.writeInt(this.id);
            dest.writeString(this.pic);
            dest.writeString(this.job);
            dest.writeString(this.introduceUrl);
            dest.writeString(this.introduction);
            dest.writeString(this.token);
            dest.writeString(this.treatmentNames);
            dest.writeString(this.level);
        }

        public DoctorBean() {
        }

        protected DoctorBean(Parcel in) {
            this.sumTime = in.readInt();
            this.roleId = in.readInt();
            this.localHospital = in.readString();
            this.name = in.readString();
            this.count = in.readInt();
            this.id = in.readInt();
            this.pic = in.readString();
            this.job = in.readString();
            this.introduceUrl = in.readString();
            this.introduction = in.readString();
            this.token = in.readString();
            this.treatmentNames = in.readString();
            this.level = in.readString();

        }

        public static final Parcelable.Creator<DoctorBean> CREATOR = new Parcelable.Creator<DoctorBean>() {
            @Override
            public DoctorBean createFromParcel(Parcel source) {
                return new DoctorBean(source);
            }

            @Override
            public DoctorBean[] newArray(int size) {
                return new DoctorBean[size];
            }
        };
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.doctor, flags);
        dest.writeString(this.revisitStartTime);
        dest.writeString(this.revisitDate);
        dest.writeString(this.diagnosticTime);
        dest.writeInt(this.diagnosticId);
        dest.writeString(this.revisitEndTime);
        dest.writeString(this.revisitContent);
        dest.writeString(this.treatmentName);
    }

    public MedicalRecordBean() {
    }

    protected MedicalRecordBean(Parcel in) {
        this.doctor = in.readParcelable(DoctorBean.class.getClassLoader());
        this.revisitStartTime = in.readString();
        this.revisitDate = in.readString();
        this.diagnosticTime = in.readString();
        this.diagnosticId = in.readInt();
        this.revisitEndTime = in.readString();
        this.revisitContent = in.readString();
        this.treatmentName = in.readString();
    }

    public static final Parcelable.Creator<MedicalRecordBean> CREATOR = new Parcelable.Creator<MedicalRecordBean>() {
        @Override
        public MedicalRecordBean createFromParcel(Parcel source) {
            return new MedicalRecordBean(source);
        }

        @Override
        public MedicalRecordBean[] newArray(int size) {
            return new MedicalRecordBean[size];
        }
    };
}
