package com.kelin.client.gui.medicinebox.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kelin.client.R;
import com.kelin.client.gui.medicinebox.entity.PurchasedBean;
import com.kelin.client.util.imageloader.GlideImageHelper;

import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by guangjiqin on 2017/8/8.
 * 未购买列表适配器
 */

public abstract class NoPurchaseExpListViewAdapter extends BaseExpandableListAdapter {

    public abstract void itemOnclick();

    private Context context;
    private List<String> dataKey;
    private Map<String, PurchasedBean> data;

    public NoPurchaseExpListViewAdapter(Context context, List<String> dataKey, Map<String, PurchasedBean> data) {
        this.context = context;
        this.data = data;
        this.dataKey = dataKey;
    }

    @Override
    public int getGroupCount() {
        return data.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return data.get(dataKey.get(groupPosition)).drug.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return data.get(dataKey.get(groupPosition));
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return data.get(dataKey.get(groupPosition)).drug.get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        final ParentHolder holder;
        if (null == convertView) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_no_purchase_title, parent, false);
            holder = new ParentHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ParentHolder) convertView.getTag();
        }
        final PurchasedBean bean = data.get(dataKey.get(groupPosition));
        holder.imgBtSelectMany.setText(bean.treatmentName);
        holder.imgBtSelectMany.setChecked(bean.isSelectAll);
        holder.imgBtSelectMany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bean.isSelectAll = holder.imgBtSelectMany.isChecked();
                for (PurchasedBean.DrugBean drugBean : bean.drug) {
                    drugBean.isSelect = holder.imgBtSelectMany.isChecked();
                }
                itemOnclick();
                notifyDataSetChanged();
            }
        });
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final ChildHolder holder;
        if (null == convertView) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_no_purchase, parent, false);
            holder = new ChildHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ChildHolder) convertView.getTag();
        }
        final PurchasedBean fatherBean = data.get(dataKey.get(groupPosition));
        final PurchasedBean.DrugBean bean = fatherBean.drug.get(childPosition);
        holder.tvGoodsName.setText(bean.drugName);
        holder.tvDosage.setText("用量:"+bean.useInfo);
        holder.tvEffect.setText("功效:"+bean.effect);
        holder.tvPrice.setText("¥" + bean.price);
        holder.tvNum.setText("x"+bean.count);
        holder.imgBtSelectOne.setChecked(bean.isSelect);
        GlideImageHelper.showImage(context, bean.pic, R.drawable.ic_launcher, R.drawable.ic_launcher, holder.imgGoodsPhoto);
        holder.imgBtSelectOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bean.isSelect = holder.imgBtSelectOne.isChecked();
                for (PurchasedBean.DrugBean bean : fatherBean.drug) {
                    if (!bean.isSelect) {
                        fatherBean.isSelectAll = false;
                        break;
                    }
                    fatherBean.isSelectAll = true;
                }
                itemOnclick();
                notifyDataSetChanged();
            }
        });
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    class ParentHolder {
        @BindView(R.id.rel_top)
        RelativeLayout relTop;
        @BindView(R.id.imgbt_select_many)
        CheckBox imgBtSelectMany;

        View itemView;

        ParentHolder(View view) {
            ButterKnife.bind(this, view);
            itemView = view;
        }
    }

    class ChildHolder {
        @BindView(R.id.imgbt_select_one)
        CheckBox imgBtSelectOne;
        @BindView(R.id.img_goods_photo)
        ImageView imgGoodsPhoto;

        @BindView(R.id.tv_goods_name)
        TextView tvGoodsName;
        @BindView(R.id.tv_dosage)
        TextView tvDosage;
        @BindView(R.id.tv_effect)
        TextView tvEffect;
        @BindView(R.id.tv_price)
        TextView tvPrice;
        @BindView(R.id.tv_num)
        TextView tvNum;

        View itemView;

        ChildHolder(View view) {
            ButterKnife.bind(this, view);
            itemView = view;
        }
    }
}
