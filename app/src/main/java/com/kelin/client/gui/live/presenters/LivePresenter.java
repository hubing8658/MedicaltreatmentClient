package com.kelin.client.gui.live.presenters;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.kelin.client.gui.live.model.MySelfInfo;
import com.kelin.client.gui.live.model.UpMenber;
import com.kelin.client.gui.live.presenters.viewinterface.ILiveView;
import com.kelin.client.gui.login.presenters.Presenter;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.kelin.client.gui.mydoctor.bean.Doctor;
import com.kelin.client.util.SxbLog;
import com.kelin.client.util.ToastUtils;
import com.monty.library.live.Constants;
import com.monty.library.live.LiveConstants;
import com.monty.library.live.event.MessageEvent;
import com.tencent.av.sdk.AVRoomMulti;
import com.tencent.av.sdk.AVView;
import com.tencent.ilivesdk.ILiveCallBack;
import com.tencent.ilivesdk.ILiveConstants;
import com.tencent.ilivesdk.ILiveMemStatusLisenter;
import com.tencent.ilivesdk.core.ILiveCameraListener;
import com.tencent.ilivesdk.core.ILiveLog;
import com.tencent.livesdk.ILVChangeRoleRes;
import com.tencent.livesdk.ILVCustomCmd;
import com.tencent.livesdk.ILVLiveConfig;
import com.tencent.livesdk.ILVLiveConstants;
import com.tencent.livesdk.ILVLiveManager;
import com.tencent.livesdk.ILVLiveRoomOption;
import com.tencent.livesdk.ILVText;

import java.util.Arrays;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by monty on 2017/8/2.
 */

public class LivePresenter extends Presenter implements ILVLiveRoomOption.onRequestViewListener, ILVLiveRoomOption.onRoomDisconnectListener, ILVLiveRoomOption.onExceptionListener, Observer, ILiveMemStatusLisenter {
    private static final String TAG = "monty - " + LivePresenter.class.getSimpleName();
    private Context mContext;
    private ILiveView mLiveView;
    private Doctor mDoctor;
    private UserUpLoadModel userUpLoadModel;

    public LivePresenter(Context context, Doctor doctor) {
        this.mContext = context;
        this.mLiveView = (ILiveView) context;
        this.mDoctor = doctor;
        userUpLoadModel = new UserUpLoadModel(doctor);
        InitBusinessHelper.initLive();
        MessageEvent.getInstance().addObserver(this);
    }


    @Override
    public void onDestory() {
        mLiveView = null;
        mContext = null;
        cancelApplyConnect();
        userUpLoadModel.upLoadQuitRoom();
        userUpLoadModel = null;

        sendC2CCmd(ILVLiveConstants.ILVLIVE_CMD_LEAVE,"out room",mDoctor.getToken());
        ILVLiveManager.getInstance().quitRoom(null);
        MessageEvent.getInstance().deleteObserver(this);
        ILVLiveManager.getInstance().onDestory();
        // 置空直播的消息监听
        ILVLiveManager.getInstance().init(new ILVLiveConfig());
        InitBusinessHelper.clearMsgListener();
//        ILVLiveManager.getInstance().shutdown();
    }

    /**
     * 群发信令（进入房间、退出房间）
     */

    public int sendGroupCmd(int cmd, String param) {
        ILVCustomCmd customCmd = new ILVCustomCmd();
        customCmd.setCmd(cmd);
        customCmd.setParam(param);
        customCmd.setType(ILVText.ILVTextType.eGroupMsg);
        return sendCmd(customCmd);
    }

    /**
     * 发送点对点消息（申请排队，取消排队，同意连麦，拒绝连麦）
     *
     * @param cmd
     * @param param
     * @param destId
     * @return
     */
    public int sendC2CCmd(final int cmd, String param, String destId) {
        ILVCustomCmd customCmd = new ILVCustomCmd();
        customCmd.setDestId(destId);
        customCmd.setCmd(cmd);
        customCmd.setParam(param);
        customCmd.setType(ILVText.ILVTextType.eC2CMsg);
        return sendCmd(customCmd);
    }

    private int sendCmd(final ILVCustomCmd cmd) {
        return ILVLiveManager.getInstance().sendCustomCmd(cmd, new ILiveCallBack() {
            @Override
            public void onSuccess(Object data) {
                Log.i("monty", "sendCmd->success:" + cmd.getCmd() + "|" + cmd.getDestId());
            }

            @Override
            public void onError(String module, int errCode, String errMsg) {
                Log.e("monty", "sendCmd->failed:" + module + "|" + errCode + "|" + errMsg);
//                Toast.makeText(mContext, "sendCmd->failed:" + module + "|" + errCode + "|" + errMsg, Toast.LENGTH_SHORT).show();
            }
        });
    }


    /**
     * 用户申请连麦
     */
    public void applyConnect() {
        userUpLoadModel.upLoadApplyConnect(new UserUpLoadModel.OnUpLoadApplyConnectListener() {
            @Override
            public void onSuccess(UpMenber upMenber) {
                // 发消息给医生，进入队列
                int result = sendC2CCmd(LiveConstants.ILVLIVE_CMD_IN_QUEUE, MyselfInfo.getLoginUser().getToken(), mDoctor.getToken() + "");
                if (result == ILiveConstants.NO_ERR) { //发送成功
                    mLiveView.applyConnected(upMenber);
                }
            }

            @Override
            public void onFailure(String msg) {
                ToastUtils.showToast(msg);
                mLiveView.applyConnected(null);
            }
        });
    }

    /**
     * 用户取消排队
     */
    public void cancelApplyConnect() {
        userUpLoadModel.upLoadCancelConnect(new UserUpLoadModel.OnUploadReportListener() {
            @Override
            public void onSuccess() {
                // 发消息给医生，退出队列
//                ToastUtils.showToast("退出队列成功，您可以重新排队");
                sendC2CCmd(LiveConstants.ILVLIVE_CMD_OUT_QUEUE, "", mDoctor.getToken() + "");
            }

            @Override
            public void onFailure(String msg) {
                ToastUtils.showToast(msg);
            }
        });

    }

    /**
     * 用户上麦，开始连麦(上麦之前需要接受到医生端的上麦邀请（排队到自己了）)
     */
    public void upToVideoMember() {
        mLiveView.coverGuestView(false);
        ILVLiveManager.getInstance().upToVideoMember(Constants.VIDEO_MEMBER_ROLE, true, true, new ILiveCallBack<ILVChangeRoleRes>() {
            @Override
            public void onSuccess(ILVChangeRoleRes data) {
                SxbLog.d("monty", "upToVideoMember->success");
                mLiveView.upToVideoMemberComplete(true);
                sendC2CCmd(ILVLiveConstants.ILVLIVE_CMD_INTERACT_AGREE, MyselfInfo.getLoginUser().getToken(), mDoctor.getToken());
            }

            @Override
            public void onError(String module, int errCode, String errMsg) {
                mLiveView.upToVideoMemberComplete(false);
                SxbLog.e("monty", "upToVideoMember->failed:" + module + "|" + errCode + "|" + errMsg);
            }
        });
    }

    /**
     * 用户下麦，结束连麦（收到医生的下麦通知后，自己下麦）
     */
    private void downToNorMember() {
        ILVLiveManager.getInstance().downToNorMember(Constants.VIDEO_MEMBER_ROLE, new ILiveCallBack<ILVChangeRoleRes>() {
            @Override
            public void onSuccess(ILVChangeRoleRes data) {
                Log.d(TAG, "下麦成功 - camRes:" + data.getCamRes() + ",micRes:" + data.getMicRes());
                mLiveView.downToNorMemberComplete(true);
            }

            @Override
            public void onError(String module, int errCode, String errMsg) {
                Log.d(TAG, "下麦失败 - module:" + module + ",errCode:" + errCode + ",errMsg:" + errMsg);
                mLiveView.downToNorMemberComplete(false);
            }
        });
    }

    /**
     * 进入房间
     */
    public void joinRoom() {
//        userUpLoadModel.upLoadStartRoom();
        // 发消息通知房间中的用户本人已经上线

        sendC2CCmd(ILVLiveConstants.ILVLIVE_CMD_ENTER, "enter roon",mDoctor.getToken());

        ILVLiveRoomOption memberOption = new ILVLiveRoomOption(mDoctor.getId() + "")
                .autoCamera(false)
                .imGroupId(mDoctor.getId() + "")
                .roomDisconnectListener(this)
                .videoMode(ILiveConstants.VIDEOMODE_BSUPPORT)
                .controlRole(Constants.NORMAL_MEMBER_ROLE)
                .authBits(AVRoomMulti.AUTH_BITS_JOIN_ROOM | AVRoomMulti.AUTH_BITS_RECV_AUDIO | AVRoomMulti.AUTH_BITS_RECV_CAMERA_VIDEO | AVRoomMulti.AUTH_BITS_RECV_SCREEN_VIDEO)
                .videoRecvMode(AVRoomMulti.VIDEO_RECV_MODE_SEMI_AUTO_RECV_CAMERA_VIDEO)
                .autoMic(true)
                .exceptionListener(this)
                .setRequestViewLisenter(this)
                .cameraListener(new ILiveCameraListener() {
                    @Override
                    public void onCameraEnable(int cameraId) {
                        ILiveLog.d("ILiveLog monty", "onCameraEnable -> cameraId:" + cameraId);
                    }

                    @Override
                    public void onCameraDisable(int cameraId) {
                        ILiveLog.e("ILiveLog monty", "onCameraDisable -> cameraId:" + cameraId);
                    }

                    @Override
                    public void onCameraPreviewChanged(int cameraId) {
                        ILiveLog.d("ILiveLog monty", "onCameraPreviewChanged -> cameraId:" + cameraId);
                    }
                })
                .setRoomMemberStatusLisenter(this).roomDisconnectListener(this);


        int ret = ILVLiveManager.getInstance().joinRoom(mDoctor.getId(), memberOption, new ILiveCallBack() {
            @Override
            public void onSuccess(Object data) {
                ILiveLog.d("monty", "joinRoom -> join room sucess");
                if (null != mLiveView)
                    mLiveView.enterRoomComplete(true);
            }

            @Override
            public void onError(String module, int errCode, String errMsg) {
                mLiveView.enterRoomComplete(false);
                ILiveLog.d("monty", "joinRoom -> join room failed:" + module + "|" + errCode + "|" + errMsg);
                if (null != mLiveView) {
                    mLiveView.quiteRoomComplete(true, null);
                }
            }
        });
        checkEnterReturn(ret);
        SxbLog.i("monty", "joinLiveRoom joinRoom ");
    }

    /**
     * 用户退出房间
     */
    private void quitRoom() {
        userUpLoadModel.upLoadQuitRoom();
        ILVLiveManager.getInstance().quitRoom(new ILiveCallBack() {
            @Override
            public void onSuccess(Object data) {
                if (null != mLiveView) {
                    mLiveView.quiteRoomComplete(true, null);
                }
            }

            @Override
            public void onError(String module, int errCode, String errMsg) {
                if (null != mLiveView) {
                    mLiveView.quiteRoomComplete(true, null);
                }
            }
        });

    }

    /**
     * 检查用户上次是否正常退出房间，如果上次未正常退出房间就将用户退出房间
     *
     * @param iRet
     */
    private void checkEnterReturn(int iRet) {
        if (ILiveConstants.NO_ERR != iRet) {
            ILiveLog.d("monty", "ILVB-Suixinbo|checkEnterReturn->enter room failed:" + iRet);
            if (ILiveConstants.ERR_ALREADY_IN_ROOM == iRet) {     // 上次房间未退出处理做退出处理
                quitRoom();
            } else {
                if (null != mLiveView) {
                    mLiveView.quiteRoomComplete(true, null);
                }
            }
        }
    }

    @Override
    public void update(Observable observable, Object o) {
        Log.d("monty", "update" + o.toString());
        MessageEvent.MsgInfo info = (MessageEvent.MsgInfo) o;
        processCmdMsg(info);
    }

    /**
     * 解析自定义信令
     *
     * @param info
     */
    private void processCmdMsg(MessageEvent.MsgInfo info) {
        if (null == info.data || !(info.data instanceof ILVCustomCmd)) {
            Log.w("monty", "processCmdMsg->wrong object:" + info.data);
            return;
        }
        ILVCustomCmd cmd = (ILVCustomCmd) info.data;
        if (cmd.getType() == ILVText.ILVTextType.eGroupMsg
                && !(mDoctor.getId() + "").equals(cmd.getDestId())) { // 此处的chatId就是医生房间id
            Log.d("monty", "processCmdMsg->ingore message from: " + cmd.getDestId() + "/" + (mDoctor.getId() + ""));
            return;
        }

        String name = info.senderId;
        if (null != info.profile && !TextUtils.isEmpty(info.profile.getNickName())) {
            name = info.profile.getNickName();
        }

        handleCustomMsg(cmd.getCmd(), cmd.getParam(), info.senderId, name);
    }

    /**
     * 处理自定义信令消息
     *
     * @param action
     * @param param
     * @param identifier
     * @param nickname
     */
    private void handleCustomMsg(int action, String param, String identifier, String nickname) {
        Log.d(TAG, "handleCustomMsg->action: " + action + ",param-> " + param + ",identifier-> " + identifier + ",nickname-> " + nickname);
        if (null == mLiveView) {
            return;
        }
        switch (action) {
            case ILVLiveConstants.ILVLIVE_CMD_INVITE: // 医生邀请用户上麦（排队到自己了）
                SxbLog.d(TAG, Constants.LogConstants.ACTION_VIEWER_SHOW + Constants.LogConstants.DIV + MySelfInfo.getInstance().getId() + Constants.LogConstants.DIV + "receive invite message" +
                        Constants.LogConstants.DIV + "id " + identifier);
                Log.i("monty", "医生邀请上麦");
                mLiveView.doctorCalled();
//                upToVideoMember();
                break;
            case ILVLiveConstants.ILVLIVE_CMD_INVITE_CANCEL: // 医生取消用户上麦邀请
                Log.i("monty", "医生取消了上麦邀请");
                break;
            case ILVLiveConstants.ILVLIVE_CMD_INVITE_CLOSE: // 医生结束了连麦
                Log.i("monty", "医生关闭了上麦，结束连麦");
                downToNorMember();
                break;
            case ILVLiveConstants.ILVLIVE_CMD_ENTER: // 房间有其他用户加入到直播房间（群发消息）,接收到此消息后需要调用房间获取用户的接口，刷新用户列表
//                mLiveView.memberJoin(identifier, nickname);
                Log.i("monty", "有用户进入房间啦 -> " + nickname);

                break;
            case ILVLiveConstants.ILVLIVE_CMD_LEAVE: // 用户退出房间
                Log.i("monty", "有用户退出房间啦 -> " + nickname);
                break;

            case LiveConstants.ILVLIVE_CMD_BARRAGE: // 弹幕消息
                Log.i("monty", "弹幕消息 -> " + param);
                mLiveView.addBarrage(param);
                break;

            case Constants.AVIMCMD_HOST_LEAVE: // 主播离开
                Log.i("monty","主播离开了");
                mLiveView.onRoomDisconnect(0,"异常退出");
                break;
//            case Constants.AVIMCMD_HOST_BACK: // 主播回来
//                Log.i("monty","主播回来啦");
////                mLiveView.hostBack(identifier, nickname);

            default:
                break;
        }
    }

    //房间异常
    @Override
    public void onException(int exceptionId, int errCode, String errMsg) {
        ILiveLog.e("ILiveLog monty", "房间异常 onException -> exceptionId:" + exceptionId + ",errCode:" + errCode + ",errMsg:" + errMsg);
        if(mLiveView!=null){
            mLiveView.onException(exceptionId, errCode, errMsg);
        }
    }

    //请求画面回调
    @Override
    public void onComplete(String[] identifierList, AVView[] viewList, int count, int result, String errMsg) {
        ILiveLog.d("ILiveLog monty", "请求画面回调 onComplete -> identifierList:" + Arrays.asList(identifierList).toString() + ",viewList:" + viewList.toString() + ",count:" + count + ",result:" + result + ",errMsg:" + errMsg);
        if (viewList != null && viewList.length > 1) { //如果回调有两个视频视图，说明有人在连麦
            mLiveView.coverGuestView(true); //覆盖患者视屏
        }else{
            mLiveView.coverGuestView(false);
        }
        mLiveView.onComplete(identifierList, viewList, count, result, errMsg);
    }

    @Override
    public boolean onEndpointsUpdateInfo(int eventid, String[] updateList) {
        ILiveLog.d("ILiveLog monty", "房间状态回调 -> eventid:" + eventid + ",updateList:" + Arrays.asList(updateList).toString());
        return false;
    }

    //房间异常退出回调(一般为断网)
    @Override
    public void onRoomDisconnect(int errCode, String errMsg) {
        ILiveLog.d("ILiveLog monty", "房间异常退出回调 onRoomDisconnect -> errCode:" + errCode + ",errMsg:" + errMsg);
        mLiveView.onRoomDisconnect(errCode, errMsg);
    }
}
