package com.kelin.client.gui.therapeutic.entity;

import java.util.List;

/**
 * Created by guangjiqin on 2017/9/8.
 */

public class FootBean {
    /**
     * id : 1
     * adminId : 1
     * treatmentName : 痘痘
     * name : 红枣羹
     * ingredients : 红豆-0.5千克,绿豆-0.1克
     * effect : 能很好的修复皮肤
     * sideEffect : true
     * createTime : 2017-08-08 18:07:08
     * statu : true
     * lastUpdateTime : 2017-08-08 18:09:00
     * useInfo : [{"id":1,"dietsId":1,"useInfo":"步骤1的做法","pic":"图片路径","sort":1},{"id":2,"dietsId":1,"useInfo":"步骤2的做法","pic":"图片路径","sort":2},{"id":3,"dietsId":1,"useInfo":"步骤3的做法","pic":null,"sort":3}]
     */

    public int id;
    public int adminId;
    public String treatmentName;
    public String name;
    public String ingredients;
    public String effect;
    public boolean sideEffect;
    public String createTime;
    public boolean statu;
    public String lastUpdateTime;
    public String pic;
    public List<UseInfoBean> useInfo;

    //（方便界面适配）
    public String material="";//材料转化为如下格式 花生\n芝麻\n大豆
    public String dose="";//材料转化为如下格式 10克\n10克\n半斤

    public static class UseInfoBean {
        /**
         * id : 1
         * dietsId : 1
         * useInfo : 步骤1的做法
         * pic : 图片路径
         * sort : 1
         */

        public int id;
        public int dietsId;
        public String useInfo;
        public String pic;
        public int sort;
    }
}

