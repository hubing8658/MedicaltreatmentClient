package com.kelin.client.gui.mydoctor.adapter;

import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kelin.client.R;
import com.kelin.client.gui.live.DoctorVideoActivity;
import com.kelin.client.gui.mydoctor.bean.Doctor;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by monty on 2017/8/1.
 */

public class DoctorAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private List<Doctor> doctors;

    private int MAX_COUNT = 0;

    public DoctorAdapter(LayoutInflater inflater, List<Doctor> doctors, int maxCount) {
        this(inflater, doctors);
        MAX_COUNT = maxCount;
    }

    public DoctorAdapter(LayoutInflater inflater, List<Doctor> doctors) {
        this.inflater = inflater;
        this.doctors = doctors;
    }

    @Override
    public int getCount() {
        // 设置最多展示个数
        if (MAX_COUNT != 0 && doctors.size() >= MAX_COUNT) {
            return MAX_COUNT;
        }
        return doctors.size();
    }

    @Override
    public Object getItem(int position) {
        return doctors.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.layout_doctor_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final Doctor doctor = doctors.get(position);
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DoctorVideoActivity.startLiveActivity(inflater.getContext(), doctor);
            }
        });
        String photo = doctor.getSittingPhotos();
        Glide.with(convertView.getContext()).load(TextUtils.isEmpty(photo) ? R.drawable.doctor_photo2 : photo).centerCrop().into(viewHolder.ivDoctorPhoto);
        viewHolder.tvDoctorName.setText(doctor.getName());
        if (doctor.getDoctorTreatments().size() > 0) {
            viewHolder.tvTreatmentTypeName.setText(doctor.getTreatmentNames()[0]);
        }
        viewHolder.tvWaitingCount.setText(Html.fromHtml("<span style=\"color:#ffffff\">" + doctor.getWaitCount() + "</span>人等待"));
        viewHolder.tvWaitingDuration.setText("约" + Html.fromHtml("<span style=\"color:#ffffff\">" + 15 + "</span>") + "分钟");
        viewHolder.tvHistoryCount.setText(doctor.getUserCount() + "人");

        return convertView;
    }

    public void notifyDataSetChanged(List<Doctor> doctors) {
        this.doctors = doctors;
        notifyDataSetChanged();
    }
    public void addNotifyData(List<Doctor> doctors) {
        this.doctors.addAll(doctors);
        notifyDataSetChanged();
    }

    static class ViewHolder {
        @BindView(R.id.iv_doctorPhoto)
        ImageView ivDoctorPhoto;
        @BindView(R.id.tv_enter_room)
        TextView tvEnterRoom;
        @BindView(R.id.tv_doctorName)
        TextView tvDoctorName;
        @BindView(R.id.tv_treatmentTypeName)
        TextView tvTreatmentTypeName;
        @BindView(R.id.tv_waitingCount)
        TextView tvWaitingCount;
        @BindView(R.id.tv_waitingDuration)
        TextView tvWaitingDuration;
        @BindView(R.id.tv_historyCount)
        TextView tvHistoryCount;

        View itemView;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
            itemView = view;
        }
    }
}
