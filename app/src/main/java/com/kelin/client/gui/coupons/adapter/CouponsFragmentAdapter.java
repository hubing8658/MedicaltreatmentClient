package com.kelin.client.gui.coupons.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.kelin.client.gui.coupons.fragment.ExceedGiftCouponsFragement;
import com.kelin.client.gui.coupons.fragment.UnusedGiftCouponsFragement;
import com.kelin.client.gui.coupons.fragment.UsedGiftCouponsFragement;
import com.kelin.client.common.MyFragmentPagerAdapter;

/**
 * Created by guangjiqin on 2017/8/10.
 */

public class CouponsFragmentAdapter extends MyFragmentPagerAdapter {

    int fragmentNum = 3;


    public CouponsFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                UnusedGiftCouponsFragement unusedGiftCouponsFragement = UnusedGiftCouponsFragement.createInstance();
                return unusedGiftCouponsFragement;
            case 1:
                UsedGiftCouponsFragement usedGiftCouponsFragement = UsedGiftCouponsFragement.createInstance();
                return  usedGiftCouponsFragement;
            case 2:
                ExceedGiftCouponsFragement exceedGiftCouponsFragement = ExceedGiftCouponsFragement.createInstance();
                return exceedGiftCouponsFragement;

        }
        return new Fragment();
    }

    @Override
    public int getCount() {
        return fragmentNum;
    }
}
