package com.kelin.client.gui.main.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.kelin.client.R;
import com.kelin.client.common.BaseActivity;
import com.kelin.client.gui.main.bean.MyRecordBean;
import com.kelin.client.gui.therapeutic.TherapeuticMainActivity;
import com.kelin.client.util.imageloader.GlideImageHelper;
import com.kelin.client.widget.BaseTitleBar;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by guangjiqin on 2017/10/11.
 */

public class DiagnoseInfActivity extends BaseActivity {

    @BindView(R.id.titleBar)
    BaseTitleBar title;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_type)
    TextView tvType;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.img_doc)
    ImageView imgDoc;
    @BindView(R.id.bt_inf)
    Button btInf;

    private MyRecordBean.DiagnosticsBean entity;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diagnose_inf);
        ButterKnife.bind(this);

        initTitle();

        getIntentData();

        initView();

        btInf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DiagnoseInfActivity.this, TherapeuticMainActivity.class);
                intent.putExtra("diagnostic_id",entity.diagnosticId);
                startActivity(intent);
            }
        });

    }

    @Override
    protected void initView() {
        super.initView();
        tvName.setText(entity.doctorName);
        String [] date = entity.diagnosticTime.split(" ");
        tvDate.setText(date[0]);
        GlideImageHelper.showImage(this,entity.doctorPic,R.drawable.bld105819,R.drawable.bld105819,imgDoc);
    }

    private void getIntentData() {
        entity = getIntent().getParcelableExtra("entity");
        Log.e("gg", "entity = " + entity.toString());
    }

    private void initTitle() {
        title.showCenterText("RECORDS","治疗记录");
    }
}
