package com.kelin.client.gui.login.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.kelin.client.R;
import com.kelin.client.gui.login.presenters.LoginPresenter;
import com.kelin.client.gui.login.presenters.viewinterface.ILoginView;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.kelin.client.gui.main.MainActivity;
import com.kelin.client.util.PreferencesManager;
import com.kelin.client.util.ToastUtils;
import com.kelin.client.widget.BaseTitleBar;
import com.kelin.client.widget.PasswordEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.sharesdk.tencent.qq.QQ;
import cn.sharesdk.wechat.friends.Wechat;


/**
 * Created by monty on 2017/7/8.
 */
public class LoginFragment extends BaseLoadingFragment implements ILoginView {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.titleBar)
    BaseTitleBar titleBar;
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.cb_RememberPassword)
    CheckBox cbRememberPassword;
    @BindView(R.id.btn_ForgottenPassword)
    Button btnForgottenPassword;
    @BindView(R.id.btn_wxLogin)
    ImageButton btnWxLogin;
    @BindView(R.id.btn_qqLogin)
    ImageButton btnQqLogin;
    @BindView(R.id.cb_agree)
    CheckBox cbAgree;
    @BindView(R.id.tv_privacyPolicy)
    TextView tvPrivacyPolicy;
    @BindView(R.id.et_pwdLayout)
    PasswordEditText etPwdLayout;
    Unbinder unbinder;


    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private LoginPresenter loginPresenter;


    public LoginFragment() {
    }

    public static LoginFragment newInstance(String param1, String param2) {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        loginPresenter = new LoginPresenter(this);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View containerView = inflater.inflate(R.layout.fragment_login, container, false);
        unbinder = ButterKnife.bind(this, containerView);

        this.titleBar.setBackBtnVis(false);
        this.titleBar.showCenterText(R.string.login, 18);
        this.titleBar.setRightText(R.string.register, 15, getResources().getColor(R.color.white), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.startRegisterFragment();
            }
        });
        this.etPwdLayout.setHint("请输入6-20位密码");
//        this.tvPrivacyPolicy.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG); // 给TextView添加下划线并添加抗锯齿
        /*if (BuildConfig.DEBUG && TextUtils.isEmpty(PreferencesManager.getString("phoneNum"))) {
            //            this.etPhone.setText("18688827660");
//            this.etPwdLayout.setPassword("qqqqqq");

            this.etPhone.setText("13823227110");
            this.etPwdLayout.setPassword("123456");
        }*/

        if (PreferencesManager.getBoolean("isRememberPas")) {
            this.cbRememberPassword.setChecked(true);
            this.etPhone.setText(PreferencesManager.getString("phoneNum"));
            this.etPwdLayout.setPassword(PreferencesManager.getString("passWord"));
        } else {
            this.cbRememberPassword.setChecked(false);
        }

        if (!mParam1.isEmpty()) {
            this.etPhone.setText(mParam1);
            this.etPwdLayout.setPassword(mParam2);
            loginPresenter.login(mParam1, mParam2, false);
        }

        return containerView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnModifyPwdListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void startMainActivity() {
        Intent intent = new Intent(getContext(), MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void startBindPhoneNumFragment(String openid,int flag,String photo) {
        mListener.startBindPhoneNumFragment(openid,flag,photo);
    }

    @Override
    public void finish() {
        getActivity().finish();
    }

    @OnClick(R.id.btn_login)
    public void onBtnLoginClicked() {
        String telPhone = etPhone.getText().toString();
        if (!MyselfInfo.checkTelPhone(etPhone.getText().toString()) || !MyselfInfo.checkPassword(etPwdLayout.getPassword())) {
            return;
        }
        if(!cbAgree.isChecked()){
            ToastUtils.showToast("请先同意用户许可协议和隐私条款");
            return;
        }
        loginPresenter.login(telPhone, etPwdLayout.getPassword(), cbRememberPassword.isChecked());
    }

    @OnClick(R.id.btn_wxLogin)
    public void onBtnWxLoginClicked() {
        /*if (!wxapi.isWXAppInstalled()) {
            Toast.makeText(this.getContext(), "未安装微信客户端", Toast.LENGTH_SHORT).show();
            return;
        }*/

        loginPresenter.ThirdLogin(getActivity(), Wechat.NAME);
    }

    @OnClick(R.id.btn_qqLogin)
    public void onBtnQqLoginClicked() {
        loginPresenter.ThirdLogin(getActivity(), QQ.NAME);
    }

    @OnClick(R.id.tv_privacyPolicy)
    public void onBtnPrivacyPolicyClicked() {
    }

    @OnClick(R.id.btn_ForgottenPassword)
    public void onViewClicked() {
        mListener.startModifyPwdFragment();
    }

    @OnClick(R.id.cb_RememberPassword)
    public void onSavePassword() {
        if (!cbRememberPassword.isChecked()) {
            PreferencesManager.removeValue("phoneNum");
            PreferencesManager.removeValue("passWord");
            PreferencesManager.removeValue("isRememberPas");
        }
    }

    public interface OnFragmentInteractionListener {
        void startRegisterFragment();

        void startModifyPwdFragment();

        void startBindPhoneNumFragment(String openId,int flag,String photo);

    }
}
