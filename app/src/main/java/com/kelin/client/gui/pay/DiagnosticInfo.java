package com.kelin.client.gui.pay;

import java.util.List;

/**
 * Created by monty on 2017/10/1.
 */

public class DiagnosticInfo {


    /**
     * unit : 次
     * coupon : [{"id":7,"number":"946476c0f9214f21a9382a35507f2826","couponName":"代金券","treatmentType":"腋臭","money":103,"startTime":"2017-07-28","endTime":"2018-04-19","isReceive":true,"phone":null,"userId":2,"receiveTime":"2017-07-27 15:46:44","isUse":false,"diagnosticId":null,"useTime":null,"createTime":"2017-07-27 15:46:44"},{"id":27,"number":"c3a1f883ce61405194b241c39f78c01a","couponName":"现金券","treatmentType":"xiongmaoyan","money":137,"startTime":"2017-05-31","endTime":"2018-02-26","isReceive":true,"phone":null,"userId":2,"receiveTime":"2017-07-27 15:46:44","isUse":false,"diagnosticId":null,"useTime":null,"createTime":"2017-07-27 15:46:44"}]
     * diagnosticId : 8
     * price : 60
     */

    private String unit;
    private long diagnosticId;
    private double price;
    private List<Coupon> coupon;

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public long getDiagnosticId() {
        return diagnosticId;
    }

    public void setDiagnosticId(long diagnosticId) {
        this.diagnosticId = diagnosticId;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public List<Coupon> getCoupon() {
        return coupon;
    }

    public void setCoupon(List<Coupon> coupon) {
        this.coupon = coupon;
    }

    @Override
    public String toString() {
        return "DiagnosticInfo{" +
                "unit='" + unit + '\'' +
                ", diagnosticId=" + diagnosticId +
                ", price=" + price +
                ", coupon=" + coupon.toString() +
                '}';
    }

    public static class Coupon {
        /**
         * id : 7
         * number : 946476c0f9214f21a9382a35507f2826
         * couponName : 代金券
         * treatmentType : 腋臭
         * money : 103
         * startTime : 2017-07-28
         * endTime : 2018-04-19
         * isReceive : true
         * phone : null
         * userId : 2
         * receiveTime : 2017-07-27 15:46:44
         * isUse : false
         * diagnosticId : null
         * useTime : null
         * createTime : 2017-07-27 15:46:44
         */

        private int id;
        private String number;
        private String couponName;
        private String treatmentType;
        private double money;
        private String startTime;
        private String endTime;
        private boolean isReceive;
        private long phone;
        private long userId;
        private String receiveTime;
        private boolean isUse;
        private long diagnosticId;
        private String useTime;
        private String createTime;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getCouponName() {
            return couponName;
        }

        public void setCouponName(String couponName) {
            this.couponName = couponName;
        }

        public String getTreatmentType() {
            return treatmentType;
        }

        public void setTreatmentType(String treatmentType) {
            this.treatmentType = treatmentType;
        }

        public double getMoney() {
            return money;
        }

        public void setMoney(double money) {
            this.money = money;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        public boolean isReceive() {
            return isReceive;
        }

        public void setReceive(boolean isReceive) {
            this.isReceive = isReceive;
        }

        public long getPhone() {
            return phone;
        }

        public void setPhone(long phone) {
            this.phone = phone;
        }

        public long getUserId() {
            return userId;
        }

        public void setUserId(long userId) {
            this.userId = userId;
        }

        public String getReceiveTime() {
            return receiveTime;
        }

        public void setReceiveTime(String receiveTime) {
            this.receiveTime = receiveTime;
        }

        public boolean isUse() {
            return isUse;
        }

        public void setUse(boolean isUse) {
            this.isUse = isUse;
        }

        public long getDiagnosticId() {
            return diagnosticId;
        }

        public void setDiagnosticId(long diagnosticId) {
            this.diagnosticId = diagnosticId;
        }

        public String getUseTime() {
            return useTime;
        }

        public void setUseTime(String useTime) {
            this.useTime = useTime;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        @Override
        public String toString() {
            return "Coupon{" +
                    "id=" + id +
                    ", number='" + number + '\'' +
                    ", couponName='" + couponName + '\'' +
                    ", treatmentType='" + treatmentType + '\'' +
                    ", money=" + money +
                    ", startTime='" + startTime + '\'' +
                    ", endTime='" + endTime + '\'' +
                    ", isReceive=" + isReceive +
                    ", phone=" + phone +
                    ", userId=" + userId +
                    ", receiveTime='" + receiveTime + '\'' +
                    ", isUse=" + isUse +
                    ", diagnosticId=" + diagnosticId +
                    ", useTime='" + useTime + '\'' +
                    ", createTime='" + createTime + '\'' +
                    '}';
        }
    }
}
