package com.kelin.client.gui.therapeutic.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.kelin.client.common.MyFragmentPagerAdapter;
import com.kelin.client.gui.therapeutic.fragment.FootFragment;
import com.kelin.client.gui.therapeutic.fragment.LifeStyleFragment;
import com.kelin.client.gui.therapeutic.fragment.PrescribeFragment;
import com.kelin.client.gui.therapeutic.fragment.ReturnFragment;
import com.kelin.client.gui.therapeutic.fragment.TabooFragment;


/**
 * Created by guangjiqin on 2017/8/18.
 */

public class TherapeuticFragmentAdapter extends MyFragmentPagerAdapter {

    int fragnmentNUm = 5;
    long diagnosticId;

    public TherapeuticFragmentAdapter(FragmentManager fm, long diagnosticId) {
        super(fm);
        this.diagnosticId = diagnosticId;
    }

    @Override
    public int getCount() {
        return fragnmentNUm;
    }


    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return PrescribeFragment.createInstance(diagnosticId);
            case 1:
                return ReturnFragment.createInstance(diagnosticId);
            case 2:
                return FootFragment.createInstance(diagnosticId);
            case 3:
                return LifeStyleFragment.createInstance(diagnosticId);
            case 4:
                return TabooFragment.createInstance(diagnosticId);
            default:
                break;
        }
        return new Fragment();
    }
}
