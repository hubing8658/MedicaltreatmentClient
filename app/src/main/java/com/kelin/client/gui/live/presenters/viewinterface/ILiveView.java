package com.kelin.client.gui.live.presenters.viewinterface;

import com.kelin.client.gui.live.model.UpMenber;
import com.tencent.av.sdk.AVView;

/**
 * Created by monty on 2017/8/2.
 */

public interface ILiveView {

    void applyConnected(UpMenber upMenber);

    void enterRoomComplete(boolean succ);

    void quiteRoomComplete(boolean succ, Object liveinfo);

    void upToVideoMemberComplete(boolean succ);

    void downToNorMemberComplete(boolean succ);

    void doctorCalled();

    void addBarrage(String content);

    //房间异常
    void onException(int exceptionId, int errCode, String errMsg);

    //请求画面回调
    void onComplete(String[] identifierList, AVView[] viewList, int count, int result, String errMsg);

    //房间异常退出回调(一般为断网)
    void onRoomDisconnect(int errCode, String errMsg);

    /**
     * 覆盖患者视频画面
     * @param b
     */
    void coverGuestView(boolean b);
}
