package com.kelin.client.gui.mydoctor.presenters;

import android.util.Log;

import com.kelin.client.gui.login.presenters.Presenter;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.kelin.client.gui.mydoctor.DoctorService;
import com.kelin.client.gui.mydoctor.bean.BaseItem;
import com.kelin.client.gui.mydoctor.bean.Doctor;
import com.kelin.client.gui.mydoctor.bean.DoctorClassify;
import com.kelin.client.gui.mydoctor.bean.EmptyLayoutLeft;
import com.kelin.client.gui.mydoctor.bean.EmptyLayoutRight;
import com.kelin.client.gui.mydoctor.bean.Footer;
import com.kelin.client.gui.mydoctor.bean.TreatmentType;
import com.kelin.client.gui.mydoctor.presenters.viewinterface.IDoctorView;
import com.kelin.client.gui.mydoctor.presenters.viewinterface.IMoreDoctorsView;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

/**
 * Created by monty on 2017/8/1.
 */

public class DoctorPresenter extends Presenter {
    private IDoctorView doctorView;
    private IMoreDoctorsView moreDoctorsView;

    public DoctorPresenter(IDoctorView doctorView) {
        this.doctorView = doctorView;
    }

    public DoctorPresenter(IMoreDoctorsView moreDoctorsView) {
        this.moreDoctorsView = moreDoctorsView;
    }

    public void getAllTreatmentType(final String token) {
        RetrofitHelper.getInstance().createService(DoctorService.class).getAllTreatentType(token).enqueue(new BaseCallBack<BaseCallModel<List<TreatmentType>>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<List<TreatmentType>>> response) {
                List<TreatmentType> treatmentType = response.body().data;
                Log.d("monty", "getAllTreatemtType - onSuccess -> " + treatmentType.toString());
                if (MyselfInfo.getTreatmentTypeId() == -1 && treatmentType != null && treatmentType.size() > 0) {
                    MyselfInfo.setTreatmentTypeId(treatmentType.get(0).getId());
                }
                doctorView.getTreatmentTypeSuccess(treatmentType);
//                getRoomList(token,treatmentType.get(0).getId(),4);
            }

            @Override
            public void onFailure(String message) {
                Log.e("monty", "getAllTreatemtType - onFailure -> " + message);
//                doctorView.refreshComplete();
                doctorView.getTreatmentTypeFailure(message);
            }
        });
    }

    public void getRoomList(String token, int treatmentTypeId, int count) {
        RetrofitHelper.getInstance().createService(DoctorService.class).getRoomList(token, treatmentTypeId, count).enqueue(new BaseCallBack<BaseCallModel<List<DoctorClassify>>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<List<DoctorClassify>>> response) {
                List<DoctorClassify> doctorClassifies = response.body().data;
                Log.d("monty", "getRoomList - onSuccess -> " + doctorClassifies.toString());

                doctorView.getDoctorRoomSuccess(doctorClassifies);
//                doctorView.refreshComplete();
            }

            @Override
            public void onFailure(String message) {
                Log.e("monty", "getRoomList - onFailure -> " + message);
                doctorView.getDoctorRoomFailure(message);
//                doctorView.refreshComplete();
            }
        });
    }

    public void getFreeRoomList(String token, int roleId, String treatmentTypeName, int pageNo) {
        RetrofitHelper.getInstance().createService(DoctorService.class).getFreeRoomList(token, roleId, treatmentTypeName, pageNo).enqueue(new BaseCallBack<BaseCallModel<List<Doctor>>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<List<Doctor>>> response) {
                List<Doctor> doctors = response.body().data;
                Log.d("monty", "getMoreRoomList - onSuccess -> " + doctors.toString());
                moreDoctorsView.getDoctorRoomSuccess(doctors);

            }

            @Override
            public void onFailure(String message) {
                Log.e("monty", "getMoreRoomList - onFailure -> " + message);
            }

            @Override
            public void finish() {
                super.finish();
                moreDoctorsView.refreshComplete();
            }
        });
    }

    private List<BaseItem> convertDoctorClassify(List<DoctorClassify> list) {
        List<BaseItem> newList = new ArrayList<>();
        for (DoctorClassify doctorClassify : list) {
            newList.add(doctorClassify);
            for (int i = 0; i < 4; i++) {
                int size = doctorClassify.getDoctorAttachList().size();
                if (i < size) {
                    Doctor doctor = doctorClassify.getDoctorAttachList().get(i);
                    if (i % 2 == 0) {
                        doctor.setLeft(true);
                    } else {
                        doctor.setRight(true);
                    }
                    newList.add(doctor);
                } else {
                    if (i % 2 == 0) {
                        newList.add(new EmptyLayoutRight());
                    } else {
                        newList.add(new EmptyLayoutLeft());
                    }
                }
            }


            newList.add(new Footer());
        }
        return newList;
    }

    public void getMoreRoomList(String token, int roleId, int pageNo) {

        RetrofitHelper.getInstance().createService(DoctorService.class).getMoreRoomList(token, roleId, pageNo).enqueue(new BaseCallBack<BaseCallModel<List<Doctor>>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<List<Doctor>>> response) {
                List<Doctor> doctors = response.body().data;
                Log.d("monty", "getMoreRoomList - onSuccess -> " + doctors.toString());
                moreDoctorsView.getDoctorRoomSuccess(doctors);
            }

            @Override
            public void onFailure(String message) {
                Log.e("monty", "getMoreRoomList - onFailure -> " + message);
            }

            @Override
            public void finish() {

            }
        });
    }

    public void searchRoom(String token, String key, int pageNo) {

        RetrofitHelper.getInstance().createService(DoctorService.class).searchRoom(token, key, pageNo).enqueue(new BaseCallBack<BaseCallModel<List<Doctor>>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<List<Doctor>>> response) {
                List<Doctor> doctors = response.body().data;
                Log.d("monty", "getMoreRoomList - onSuccess -> " + doctors.toString());
                moreDoctorsView.getDoctorRoomSuccess(doctors);
            }

            @Override
            public void onFailure(String message) {
                Log.e("monty", "getMoreRoomList - onFailure -> " + message);
            }

            @Override
            public void finish() {

            }
        });
    }


    @Override
    public void onDestory() {

    }
}
