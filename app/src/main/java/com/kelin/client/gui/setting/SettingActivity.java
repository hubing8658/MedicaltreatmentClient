package com.kelin.client.gui.setting;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.kelin.client.R;
import com.kelin.client.common.BaseActivity;
import com.kelin.client.util.ToastUtils;
import com.kelin.client.widget.BaseTitleBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by guangjiqin on 2017/8/9.
 */

public class SettingActivity extends BaseActivity {


    @BindView(R.id.titleBar)
    BaseTitleBar title;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);

        initTitle();
    }

    private void initTitle() {
        title.showCenterText("SETTINGS","设置");
    }

    @OnClick(R.id.tv_change_ps)
    public void toChangePasswordActivity() {
        Intent intent = new Intent(SettingActivity.this, ResetPasswordsActivity.class);
        intent.putExtra("type","reSet");
        startActivity(intent);
    }

    @OnClick(R.id.tv_forget_ps)
    public void toResetPasswordActivity() {
        Intent intent = new Intent(SettingActivity.this, ResetPasswordsActivity.class);
        intent.putExtra("type","forget");
        startActivity(intent);
    }

    @OnClick(R.id.tv_wipe_cache)
    public void wipeCache() {
        ToastUtils.showToast(R.string.wipe_cached);
    }

    @OnClick(R.id.lin_about_us)
    public void toAboutUsActivity() {
        startActivity(new Intent(SettingActivity.this, AboutUsActivity.class));
    }
}
