package com.kelin.client.gui.setting;

import com.kelin.client.gui.live.event.CurVisitorTimeBean;
import com.monty.library.http.BaseCallModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by guangjiqin on 2017/8/27.
 */

public interface MyCenterService {

    /**
     * 获取成为医生的协议内容
     * @param token
     * @param type   1-医生注册协议、2-患者注册协议、3-功能介绍、4-如何视频问诊、5-视频操作指导、6-关于我们
     * @return
     */
    @GET("getContent")
    Call<BaseCallModel<AgreenDataBean>> getTermsContent(@Query("token") String token, @Query("type") int type);


    /**
     * 获取当前就诊时间接口
     *
     * @param
     * @return
     */
    @GET("sittingSet/getByUser")
    Call<BaseCallModel<CurVisitorTimeBean>> getCurVisitorTime(@Query("token") String token,@Query("doctorToken") String doctorToken);
}
