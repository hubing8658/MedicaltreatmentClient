package com.kelin.client.gui.setting;

/**
 * Created by guangjiqin on 2017/8/26.
 */

public class AgreenDetailBean {
    /**
     * id : 2
     * title : 一、定义
     * content : 1、服务平台：指Hello医生手机诊所平台及Hello医生平台与网络平台的合称。
     2、咨询医师：指同意并承诺遵守本协议规定使用Hello医生手机诊所平台及Hello医生平台向服务平台的用户提供医疗咨询解答服务的享有相应资质的医生。
     3、服务：指咨询医师通过本协议服务平台向服务平台用户提供医疗问题解答，医疗知识传播等。
     4、诊所账户：是指咨询医师在服务平台上，用于接收咨询的个人实名账户。
     5、账户：用于接收服务平台上服务费的个人实名银行账户。
     * sort : 1
     * type : 1
     */

    public int id;
    public String title;
    public String content;
    public String pic;
    public int sort;
    public int type;

    @Override
    public String toString() {
        return "AgreenDetailBean{" +
                "id=" + id +
                "pic=" + pic +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", sort=" + sort +
                ", type=" + type +
                '}';
    }
}
