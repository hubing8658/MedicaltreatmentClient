package com.kelin.client.gui.main.service;

import com.kelin.client.gui.main.bean.CheckInfEntity;
import com.kelin.client.gui.main.bean.MedicalRecordBean;
import com.kelin.client.gui.main.bean.MyRecordBean;
import com.monty.library.http.BaseCallModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by guangjiqin on 2017/9/7.
 */

public interface MainService {

    /**
     * 获取诊疗记录接口
     */
    @GET("record/getRevisitAll")
    Call<BaseCallModel<List<MedicalRecordBean>>> getRevisit(@Query("token") String token);

    /**
     * 获取诊疗记录接口
     */
    @GET("record/getAll")
    Call<BaseCallModel<MyRecordBean>> getAll(@Query("token") String token);


    /**
     * 开始复查
     * */
    @GET("record/revisit")
    Call<BaseCallModel<CheckInfEntity>> revisit(@Query("token") String token, @Query("diagnosticId") int diagnosticId);

    /**
     * 延期复查
     */
    @GET("record/delayRevisit")
    Call<BaseCallModel<String>> delayRevisit(@Query("token") String token, @Query("diagnosticId") int diagnosticId);

    /**
     * 升级医生
     * */

}
