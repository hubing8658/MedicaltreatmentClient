package com.kelin.client.gui.coupons;

import com.kelin.client.gui.address.entity.MyAddressEntity;
import com.kelin.client.gui.coupons.entity.CouponsEntity;
import com.monty.library.http.BaseCallModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by guangjiqin on 2017/8/15.
 */

public interface CouponsService {




    /**
     * 获取使用卷接口
     * 标示，1-未使用，2-已使用，3-已过期
     */
    @GET("coupon/getCoupons")
    Call<BaseCallModel<List<CouponsEntity>>> getCouponList(@Query("token") String token,
                                                           @Query("flag") int flag);


    /**
     * 兑换奖券接口
     *
     */
    @FormUrlEncoded
    @POST("coupon/exchange")
    Call<BaseCallModel<List<CouponsEntity>>> forTickets(@Field("token") String token,
                                                        @Field("number") String number);
}
