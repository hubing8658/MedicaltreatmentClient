package com.kelin.client.gui.live.event;

/**
 * Created by guangjiqin on 2017/8/29.
 */

public class CurVisitorTimeBean {
    /**
     * id : 1008
     * doctorId : 105
     * startDate : 2017-08-17
     * monday : 9:00-12:00,14:00-18:00
     * tuesday :
     * wednesday :
     * thursday :
     * friday :
     * saturday :
     * sunday :
     * applyTime : 2017-08-16 17:44:42
     * reason : 初次使用
     * statu : true
     * passTime : 2017-08-18 19:09:09
     */

    public int id;
    public int doctorId;
    public String startDate;
    public String monday;
    public String tuesday;
    public String wednesday;
    public String thursday;
    public String friday;
    public String saturday;
    public String sunday;
    public String applyTime;
    public String reason;
    public boolean statu;
    public String passTime;

    @Override
    public String toString() {
        return "CurVisitorTimeBean{" +
                "id=" + id +
                ", doctorId=" + doctorId +
                ", startDate='" + startDate + '\'' +
                ", monday='" + monday + '\'' +
                ", tuesday='" + tuesday + '\'' +
                ", wednesday='" + wednesday + '\'' +
                ", thursday='" + thursday + '\'' +
                ", friday='" + friday + '\'' +
                ", saturday='" + saturday + '\'' +
                ", sunday='" + sunday + '\'' +
                ", applyTime='" + applyTime + '\'' +
                ", reason='" + reason + '\'' +
                ", statu=" + statu +
                ", passTime='" + passTime + '\'' +
                '}';
    }
}
