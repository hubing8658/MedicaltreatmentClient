package com.kelin.client.gui.live;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


import com.kelin.client.R;
import com.kelin.client.common.BaseActivity;
import com.kelin.client.gui.live.controller.GetVisitorTimeController;
import com.kelin.client.gui.live.event.CurVisitorTimeBean;
import com.kelin.client.util.SerializableMap;
import com.kelin.client.widget.BaseTitleBar;
import com.kelin.client.widget.VisitorTimeView.SetVisitorTimeView;
import com.kelin.client.widget.VisitorTimeView.TimeBoxEntity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by guangjiqin on 2017/8/29.
 */

public class GetVisitorTimeActivity extends BaseActivity {


    private GetVisitorTimeController controller;


    @BindView(R.id.titleBar)
    BaseTitleBar titleBar;

    @BindView(R.id.timeview1)
    SetVisitorTimeView timeView1;
    @BindView(R.id.timeview2)
    SetVisitorTimeView timeView2;
    @BindView(R.id.timeview3)
    SetVisitorTimeView timeView3;
    @BindView(R.id.timeview4)
    SetVisitorTimeView timeView4;
    @BindView(R.id.timeview5)
    SetVisitorTimeView timeView5;
    @BindView(R.id.timeview6)
    SetVisitorTimeView timeView6;
    @BindView(R.id.timeview7)
    SetVisitorTimeView timeView7;


    @BindView(R.id.bt_next)
    Button btNext;

    private String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_visitor_time);

        ButterKnife.bind(this);

        initController();

        initTitle();

        getIntentData();

        initView();

        initData();

    }

    private void getIntentData() {
        token = getIntent().getStringExtra("docToken");
    }

    protected void initView(){
        timeView1.setHasFocus(false);
        timeView2.setHasFocus(false);
        timeView3.setHasFocus(false);
        timeView4.setHasFocus(false);
        timeView5.setHasFocus(false);
        timeView6.setHasFocus(false);
        timeView7.setHasFocus(false);

        btNext.setVisibility(View.GONE);
    }

    @Override
    protected void initData() {
        controller.getData(token);
    }

    private void initController() {
        controller = new GetVisitorTimeController(this) {
            @Override
            public void getDataSuccess(CurVisitorTimeBean curVisitorTimeBean) {
                timeView1.setTime(timeChange(curVisitorTimeBean.monday));
                timeView2.setTime(timeChange(curVisitorTimeBean.tuesday));
                timeView3.setTime(timeChange(curVisitorTimeBean.wednesday));
                timeView4.setTime(timeChange(curVisitorTimeBean.thursday));
                timeView5.setTime(timeChange(curVisitorTimeBean.friday));
                timeView6.setTime(timeChange(curVisitorTimeBean.saturday));
                timeView7.setTime(timeChange(curVisitorTimeBean.sunday));

            }

            @Override
            public void getDataFsailrue(String msg) {

            }
        };

    }


    private void initTitle() {
        titleBar.showCenterText("",R.drawable.current_time,0);
        titleBar.setBackBtnClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }




    //string时间字段转化为List集合方法
    private List<String> timeChange(String timeStr) {
        List<String> stringList = new ArrayList<>();
        if (null != timeStr && !timeStr.isEmpty()) {
            String[] strTemp = timeStr.split(",");
            for (String str : strTemp) {
                if (!str.isEmpty()) {
                    stringList.add(str);
                }
            }
        }
        return stringList;
    }


}
