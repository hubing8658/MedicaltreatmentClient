package com.kelin.client.gui.live;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.kelin.client.R;
import com.kelin.client.common.BaseActivity;
import com.kelin.client.gui.mydoctor.bean.Doctor;
import com.kelin.client.widget.BaseTitleBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * 医生详情
 * @author monty
 */
public class DoctorDetailsActivity extends BaseActivity {
    @BindView(R.id.titleBar)
    BaseTitleBar titleBar;
    @BindView(R.id.iv_doctorPhoto)
    ImageView ivDoctorPhoto;
    @BindView(R.id.tv_doctor_cnName)
    TextView tvDoctorCnName;
    @BindView(R.id.tv_doctor_enName)
    TextView tvDoctorEnName;
    @BindView(R.id.tv_hospitalName)
    TextView tvHospitalName;
    @BindView(R.id.tv_jobTitle)
    TextView tvJobTitle;
    @BindView(R.id.tv_waitingCount)
    TextView tvWaitingCount;
    @BindView(R.id.tv_waitingTime)
    TextView tvWaitingTime;
    @BindView(R.id.tv_doctorDescription)
    TextView tvDoctorDescription;
    @BindView(R.id.tv_remedyFlow)
    TextView tvRemedyFlow;
    @BindView(R.id.btn_confirm)
    LinearLayout btnConfirm;

    private Doctor doctor;

    public static void GotoDoctorDetailsActivity(Context context, Doctor doctor) {
        Intent intent = new Intent(context, DoctorDetailsActivity.class);
        intent.putExtra("doctor", doctor);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_details);
        doctor = getIntent().getParcelableExtra("doctor");
        ButterKnife.bind(this);

        initView();
    }

    @Override
    protected void initView() {
        this.titleBar.showCenterText("DOCTOR DETAILS","医生详情");
        this.titleBar.setRightIcon(R.drawable.details_icon_calender, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(DoctorDetailsActivity.this, GetVisitorTimeActivity.class);
                intent.putExtra("docToken", doctor.getToken());
                startActivity(intent);
            }
        });

        Glide.with(this).load(doctor.getSittingPhotos()).asBitmap().centerCrop().into(new BitmapImageViewTarget(ivDoctorPhoto) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                ivDoctorPhoto.setImageDrawable(circularBitmapDrawable);
            }
        });

        this.tvDoctorCnName.setText(doctor.getName());
        this.tvDoctorEnName.setText(doctor.getEnglishName());
        this.tvHospitalName.setText(doctor.getLocalHospital());
        this.tvJobTitle.setText(doctor.getJob());
        this.tvWaitingCount.setText("需等待\n" + doctor.getWaitCount() + "人");
        this.tvWaitingTime.setText("大约\n" + doctor.getWaitCount() * 5 + "分钟");
        this.tvDoctorDescription.setText(doctor.getIntroduction());
    }

    @OnClick(R.id.tv_waitingCount)
    public void onTvWaitingCountClicked() {
    }

    @OnClick(R.id.tv_waitingTime)
    public void onTvWaitingTimeClicked() {
    }

    @OnClick(R.id.tv_doctorDescription)
    public void onTvDoctorDescriptionClicked() {
    }

    @OnClick(R.id.tv_remedyFlow)
    public void onTvRemedyFlowClicked() {
    }

    @OnClick(R.id.btn_confirm)
    public void onBtnConfirmClicked() {
        // TODO: 2017/8/6 跳转到直播页面并进行排队
        LiveActivity.startLiveActivity(this, doctor, 0);
    }
}
