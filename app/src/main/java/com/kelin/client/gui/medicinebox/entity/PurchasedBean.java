package com.kelin.client.gui.medicinebox.entity;

import java.util.List;

/**
 * Created by guangjiqin on 2017/9/6.
 */

public class PurchasedBean  {
    /**
     * createTime : 2017-08-25 14:30:39
     * treatmentName : xiongmaoyan
     * drug : [{"id":2,"diagnosticId":7,"doctorId":105,"userId":2,"drugId":2,"drugName":"皮肤灵","useTime":"餐前","useInfo":"每日3次，每次2粒","useType":1,"price":40,"maxUnit":"瓶","drugstorePrice":0,"effect":"能很好的修复皮肤","count":2,"pic":null,"isPrompt":false,"promptTime":null,"isBuy":true,"buyTime":"2017-08-25 18:26:30"}]
     */

    public String createTime;
    public String treatmentName;
    public List<DrugBean> drug;
    public boolean isSelectAll; //是否全部选择

    public static class DrugBean {
        /**
         * id : 2
         * diagnosticId : 7
         * doctorId : 105
         * userId : 2
         * drugId : 2
         * drugName : 皮肤灵
         * useTime : 餐前
         * useInfo : 每日3次，每次2粒
         * useType : 1
         * price : 40
         * maxUnit : 瓶
         * drugstorePrice : 0
         * effect : 能很好的修复皮肤
         * count : 2
         * pic : null
         * isPrompt : false
         * promptTime : null
         * isBuy : true
         * buyTime : 2017-08-25 18:26:30
         */

        public int id;
        public int diagnosticId;
        public int doctorId;
        public int userId;
        public int drugId;
        public String drugName;
        public String useTime;
        public String useInfo;
        public int useType;
        public Float price;
        public String maxUnit;
        public Float drugstorePrice;
        public String effect;
        public int count;
        public String pic;
        public boolean isPrompt;
        public Object promptTime;
        public boolean isBuy;
        public String buyTime;

        public boolean isSelect;//是否选中

        @Override
        public String toString() {
            return "DrugBean{" +
                    "id=" + id +
                    ", diagnosticId=" + diagnosticId +
                    ", doctorId=" + doctorId +
                    ", userId=" + userId +
                    ", drugId=" + drugId +
                    ", drugName='" + drugName + '\'' +
                    ", useTime='" + useTime + '\'' +
                    ", useInfo='" + useInfo + '\'' +
                    ", useType=" + useType +
                    ", price=" + price +
                    ", maxUnit='" + maxUnit + '\'' +
                    ", drugstorePrice=" + drugstorePrice +
                    ", effect='" + effect + '\'' +
                    ", count=" + count +
                    ", pic='" + pic + '\'' +
                    ", isPrompt=" + isPrompt +
                    ", promptTime=" + promptTime +
                    ", isBuy=" + isBuy +
                    ", buyTime='" + buyTime + '\'' +
                    ", isSelect=" + isSelect +
                    '}';
        }
    }
}
