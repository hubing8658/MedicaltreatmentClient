package com.kelin.client.gui.coupons.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.kelin.client.R;
import com.kelin.client.gui.coupons.adapter.GiftCouponsListAdapter;
import com.kelin.client.gui.coupons.controller.CouponsController;
import com.kelin.client.gui.coupons.entity.CouponsEntity;
import com.kelin.client.gui.login.utils.MyselfInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by guangjiqin on 2017/8/10.
 */

public class UsedGiftCouponsFragement extends Fragment {

    private View view;
    private List<CouponsEntity> couponsEntities;
    private GiftCouponsListAdapter adapter;

    private ListView listView;
    private CouponsController controller;

    public static UsedGiftCouponsFragement createInstance() {

        UsedGiftCouponsFragement fragment = new UsedGiftCouponsFragement();

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_used_gift_coupons, null);

        initView();

        initListView();

        initController();

        initData();

        return view;
    }


    private void initController() {
        controller = new CouponsController(MyselfInfo.getLoginUser().getToken()) {
            @Override
            public void getCouponsListSuccess(List<CouponsEntity> treatmentType) {
                couponsEntities.clear();
                couponsEntities.addAll(treatmentType);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void getCouponsListFailure(String message) {

            }
        };
    }

    private void initView() {
        listView = (ListView) view.findViewById(R.id.listview);
    }

    private void initListView() {
        couponsEntities = new ArrayList<>();
        adapter = new GiftCouponsListAdapter(getActivity(), couponsEntities, false);
        listView.setAdapter(adapter);
    }

    private void initData() {
        controller.getCouponListData(2);
    }
}
