package com.kelin.client.gui.therapeutic.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kelin.client.R;
import com.kelin.client.common.MyBaseAdapter;
import com.kelin.client.gui.therapeutic.entity.LifeStyleBean;
import com.kelin.client.util.imageloader.GlideImageHelper;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by guangjiqin on 2017/9/8.
 */

public class LifeStyleAdapter extends MyBaseAdapter {

    public LifeStyleAdapter(Context context,List<LifeStyleBean> data) {
        super(context);
        mData = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Holder holder;
        if (null == convertView) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_life_style, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        LifeStyleBean bean = (LifeStyleBean) mData.get(position);
        holder.tvSubjectContent.setText(bean.treatmentName);
        holder.tvExplain.setText(bean.describe);
        holder.tvEffect.setText(bean.effect);
        GlideImageHelper.showImage(mContext, bean.pic, R.drawable.ic_launcher, R.drawable.ic_launcher, holder.imgExplain);
        if(position == 0){
            holder.imgTop.setVisibility(View.VISIBLE);
        }else{
            holder.imgTop.setVisibility(View.GONE);

        }
        return convertView;
    }

    class Holder {
        @BindView(R.id.tv_subject_content)
        TextView tvSubjectContent;
        @BindView(R.id.tv_explain)
        TextView tvExplain;
        @BindView(R.id.tv_effect)
        TextView tvEffect;
        @BindView(R.id.img_explain)
        ImageView imgExplain;
        @BindView(R.id.img_top)
        ImageView imgTop;

        View itemView;

        Holder(View view) {
            ButterKnife.bind(this, view);
            itemView = view;
        }
    }
}
