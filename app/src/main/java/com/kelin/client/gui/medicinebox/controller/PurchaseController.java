package com.kelin.client.gui.medicinebox.controller;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.kelin.client.gui.medicinebox.MedicicineBoxService;
import com.kelin.client.gui.medicinebox.entity.PurchasedBean;
import com.kelin.client.util.ToastUtils;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;


import java.util.Map;

import retrofit2.Response;

/**
 * Created by guangjiqin on 2017/9/6.
 */

public abstract class PurchaseController {

    public abstract void getDataSuccess(Map<String, PurchasedBean> map);

    public abstract void getDataFailrue(String msg);

    public Context context;

    public PurchaseController(Context context) {
        this.context = context;
    }

    public void getPurchaseDataType(int type) {
        RetrofitHelper.getInstance().createService(MedicicineBoxService.class).getMedicalBox(MyselfInfo.getLoginUser().getToken(), type).enqueue(new BaseCallBack<BaseCallModel<Map<String, PurchasedBean>>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<Map<String, PurchasedBean>>> response) {
                Map<String, PurchasedBean> map = response.body().data;
                getDataSuccess(map);
            }

            @Override
            public void onFailure(String message) {
                ToastUtils.showToast(message);
            }
        });
    }

    public float selectAll(Map<String, PurchasedBean> data, boolean isSelect) {
        float price = 0;
        for (String key : data.keySet()) {
            data.get(key).isSelectAll = isSelect;
            for (PurchasedBean.DrugBean bean : data.get(key).drug) {
                bean.isSelect = isSelect;
                price = price + (bean.price * bean.count);
            }
        }
        if (isSelect) {
            return price;
        } else {
            return 0;
        }

    }
}
