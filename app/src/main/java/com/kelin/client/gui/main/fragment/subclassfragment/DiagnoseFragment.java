package com.kelin.client.gui.main.fragment.subclassfragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ExpandableListView;
import android.widget.ImageView;

import com.kelin.client.R;
import com.kelin.client.gui.live.LiveActivity;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.kelin.client.gui.main.adapter.DiagnoseListAdapter;
import com.kelin.client.gui.main.bean.MedicalRecordEntity;
import com.kelin.client.gui.main.bean.MyRecordBean;
import com.kelin.client.gui.main.service.MainService;
import com.kelin.client.gui.mydoctor.bean.Doctor;
import com.kelin.client.util.ToastUtils;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;
import retrofit2.Response;

/**
 * Created by guangjiqin on 2017/8/8.
 */

public class DiagnoseFragment extends Fragment {

    private PtrClassicFrameLayout mPtrFrame;
    private ExpandableListView listView;
    private View view;

    private List<MedicalRecordEntity> treatRecordEntities;
    private String[] parent = {"继续诊断", "诊疗记录"};
    private Map<String, List<MyRecordBean.DiagnosticsBean>> data;

    private Doctor doctor; // 继续诊断的医生信息

    private DiagnoseListAdapter adapter;
    private static DiagnoseFragment diagnoseFragment;

    public static DiagnoseFragment createInstance() {
        if (null == diagnoseFragment) {
            diagnoseFragment = new DiagnoseFragment();
        }
        return diagnoseFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_diagnose, null);

        initView();

        initPullRefresh();

//        initData();

        return view;

    }

    private void initData() {
        RetrofitHelper.getInstance().createService(MainService.class).getAll(MyselfInfo.getLoginUser().getToken()).enqueue(new BaseCallBack<BaseCallModel<MyRecordBean>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<MyRecordBean>> response) {
                mPtrFrame.refreshComplete();
                data.get("诊疗记录").clear();
                data.get("诊疗记录").addAll(response.body().data.diagnostics);
                doctor = response.body().data.exceptionRecord;
                adapter.notifyDataSetChanged();

                //默认不展开
//                for (int i = 0; i < parent.length; i++) {
//                    listView.expandGroup(i);
//                }
            }

            @Override
            public void onFailure(String message) {
                mPtrFrame.refreshComplete();
                ToastUtils.showToast(message);
            }
        });
    }


    private void initView() {
        final Animation anBegin = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_begin);
        final Animation anEnd = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_end);

        mPtrFrame = (PtrClassicFrameLayout) view.findViewById(R.id.store_house_ptr_frame);
        listView = (ExpandableListView) view.findViewById(R.id.listview);
        listView.setGroupIndicator(null);
        data = new HashMap<>();
        data.put("继续诊断", new ArrayList<MyRecordBean.DiagnosticsBean>());
        data.put("诊疗记录", new ArrayList<MyRecordBean.DiagnosticsBean>());
        adapter = new DiagnoseListAdapter(getActivity(), data, parent);
        listView.setAdapter(adapter);
        listView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {//设置点击不收缩

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                if (0 == groupPosition) {
                    if (doctor != null) {
//                        DoctorVideoActivity.startLiveActivity(getContext(), doctor);
                        LiveActivity.startLiveActivity(getActivity(), doctor,0);
                    } else {
                        ToastUtils.showToast("无医生信息");
                    }
                    return true;
                }
                ImageView imgMore = (ImageView) v.findViewById(R.id.img_more);
                if (parent.isGroupExpanded(groupPosition)) {
                    imgMore.startAnimation(anEnd);
                } else {
                    imgMore.startAnimation(anBegin);
                }
                return false;
            }
        });

//        listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
//            @Override
//            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
//                Intent intent  = new Intent();
//                intent.setClass(getActivity(), DiagnoseInfActivity.class);
//                intent.putExtra("entity",data.get("诊疗记录").get(childPosition));
//                getActivity().startActivity(intent);
//                return false;
//            }
//        });
    }

    private void initPullRefresh() {
        mPtrFrame.setLastUpdateTimeRelateObject(this);
        mPtrFrame.setPtrHandler(new PtrHandler() {
            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
                return PtrDefaultHandler.checkContentCanBePulledDown(frame, listView, header);
            }

            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                initData();
                /*mPtrFrame.postDelayed(new Runnable() {//测试
                    @Override
                    public void run() {
                        mPtrFrame.refreshComplete();
                    }
                }, 100);*/
            }
        });


        // the following are default settings
        mPtrFrame.setResistance(1.7f);
        mPtrFrame.setRatioOfHeaderHeightToRefresh(1.2f);
        mPtrFrame.setDurationToClose(200);
        mPtrFrame.setDurationToCloseHeader(1000);
        // default is false
        mPtrFrame.setPullToRefresh(false);
        // default is true
        mPtrFrame.setKeepHeaderWhenRefresh(true);
        mPtrFrame.postDelayed(new Runnable() {
            @Override
            public void run() {
                mPtrFrame.autoRefresh();
            }
        }, 100);
    }
}