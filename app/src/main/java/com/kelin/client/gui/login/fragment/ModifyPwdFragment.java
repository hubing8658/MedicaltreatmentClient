package com.kelin.client.gui.login.fragment;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.kelin.client.R;
import com.kelin.client.gui.login.LoginService;
import com.kelin.client.gui.login.bean.User;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.kelin.client.util.ToastUtils;
import com.kelin.client.widget.BaseTitleBar;
import com.kelin.client.widget.PasswordEditText;
import com.kelin.client.widget.VerifyCodeEditText;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Response;


/**
 * Created by monty on 2017/7/8.
 */
public class ModifyPwdFragment extends BaseLoadingFragment {
    private static final String ARG_OPERATE = "operateCode";
    @BindView(R.id.titleBar)
    BaseTitleBar titleBar;
    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.et_verifyCodeLayout)
    VerifyCodeEditText etVerifyCodeLayout;
    @BindView(R.id.et_pwdLayout)
    PasswordEditText etPwdLayout;
    @BindView(R.id.et_verifyPwdLayout)
    PasswordEditText etVerifyPwdLayout;
    @BindView(R.id.btn_confirm)
    Button btnConfirm;
    Unbinder unbinder;

    public static final int OPERATE_FORGOT = 0x11;
    public static final int OPERATE_MODIFY = 0x12;


    private int operateCode;

    private OnModifyPwdListener mListener;

    public ModifyPwdFragment() {
    }

    public static ModifyPwdFragment newInstance(int operateCode) {
        ModifyPwdFragment fragment = new ModifyPwdFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_OPERATE, operateCode);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            operateCode = getArguments().getInt(ARG_OPERATE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View containerView = inflater.inflate(R.layout.fragment_modifypwd, container, false);
        unbinder = ButterKnife.bind(this, containerView);
        this.titleBar = (BaseTitleBar) containerView.findViewById(R.id.titleBar);
        this.titleBar.setBackBtnVis(true);
        this.titleBar.setBackBtnClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });

        if (operateCode == OPERATE_FORGOT) {
            this.titleBar.showCenterText(R.string.forgot_password, 18);
        } else if (operateCode == OPERATE_MODIFY) {
            this.titleBar.showCenterText(R.string.modify_password, 18);
        }

        etPwdLayout.setHint("请输入新密码");
        etVerifyPwdLayout.setHint("再次输入新密码");

        this.etVerifyCodeLayout.setOnVerifyCodeButtonClickListener(new VerifyCodeEditText.OnVerifyCodeButtonClickListener() {
            @Override
            public boolean onClick(View v) {
                String telPhone = etPhone.getText().toString();
                if (!MyselfInfo.checkTelPhone(telPhone)) {
                    return false;
                }
                RetrofitHelper.getInstance().createService(LoginService.class).getCodeByType(telPhone,0).enqueue(new BaseCallBack<BaseCallModel<String>>() {

                    @Override
                    public void onSuccess(Response<BaseCallModel<String>> response) {
                        Log.d("monty", "response->" + response.body().data);
                        ToastUtils.showToast(R.string.getverify_success);
                    }

                    @Override
                    public void onFailure(String message) {
                        ToastUtils.showToast(message);
                        Log.d("monty", "Throwable->" + message);
                    }
                });
                return true;
            }

        });

        return containerView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnModifyPwdListener) {
            mListener = (OnModifyPwdListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnModifyPwdListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.btn_confirm)
    public void onViewClicked() {
        showLoadingDialog("正在修改密码");
        String telPhone = etPhone.getText().toString();
        if (!MyselfInfo.checkTelPhone(telPhone)) return;

        if (!MyselfInfo.checkVerifyCode(etVerifyCodeLayout.getVerifyCode())) return;

        if (!MyselfInfo.checkPassword(etPwdLayout.getPassword())) return;

        if (!MyselfInfo.checkPassword(etVerifyPwdLayout.getPassword())) return;

        if(!etPwdLayout.getPassword().equals(etVerifyPwdLayout.getPassword())){
            ToastUtils.showToast("两次输入的密码不一致");
            return;
        }

        RetrofitHelper.getInstance().createService(LoginService.class).userForgotPassword(telPhone,etVerifyPwdLayout.getPassword(),etVerifyCodeLayout.getVerifyCode()).enqueue(new BaseCallBack<BaseCallModel<User>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<User>> response) {
                ToastUtils.showToast("密码修改成功，请重新登录");
//                Log.d("monty","modify password - onResponse -> "+response.body().data.toString());
                mListener.startLiginFragment();
                closeLoadingDialog();
            }

            @Override
            public void onFailure(String message) {
                ToastUtils.showToast(message);
                closeLoadingDialog();
            }
        });

    }

    public interface OnModifyPwdListener {
//        void modify(String phoneNum, String verifyCode, String password);
        void startLiginFragment();
    }
}
