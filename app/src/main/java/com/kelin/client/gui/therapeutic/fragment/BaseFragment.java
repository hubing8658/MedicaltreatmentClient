package com.kelin.client.gui.therapeutic.fragment;

import android.support.v4.app.Fragment;
import android.util.Log;

/**
 * Created by guangjiqin on 2017/8/18.
 */

public abstract class BaseFragment extends Fragment {

    protected  abstract  void requestData();
    protected  boolean isFirst = true;//第一次获取数据的标记



    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isFirst) {
            isFirst = false;
            Log.e("gg","setUserVisibleHint");
            requestData();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isFirst = true;
    }
}
