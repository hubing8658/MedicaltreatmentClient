package com.kelin.client.gui.mydoctor;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.kelin.client.R;
import com.kelin.client.common.BaseActivity;
import com.kelin.client.gui.live.ComplainActivity;
import com.kelin.client.gui.live.LiveActivity;
import com.kelin.client.gui.live.event.JumpToIMFragemtEvent;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.kelin.client.gui.mydoctor.adapter.DoctorAdapter2;
import com.kelin.client.gui.mydoctor.bean.Doctor;
import com.kelin.client.gui.mydoctor.bean.DoctorClassify;
import com.kelin.client.gui.mydoctor.bean.TreatmentType;
import com.kelin.client.gui.mydoctor.presenters.DoctorPresenter;
import com.kelin.client.gui.mydoctor.presenters.viewinterface.IDoctorView;
import com.kelin.client.gui.pay.AliPayEntity;
import com.kelin.client.gui.pay.DiagnosticInfo;
import com.kelin.client.gui.pay.PayService;
import com.kelin.client.util.ToastUtils;
import com.kelin.client.widget.BaseTitleBar;
import com.kelin.client.widget.FullyLinearLayoutManager;
import com.monty.library.EventBusUtil;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.monty.library.widget.dialog.NiceDialogFactory;
import com.othershe.nicedialog.BaseNiceDialog;
import com.othershe.nicedialog.NiceDialog;
import com.othershe.nicedialog.ViewConvertListener;
import com.othershe.nicedialog.ViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrFrameLayout;
import retrofit2.Response;

/**
 * Created by monty on 2017/8/9.
 * 更多医生
 */
public class MoreDoctorsActivity extends BaseActivity implements IDoctorView, DoctorAdapter2.OnItemClickListener {

    @BindView(R.id.store_house_ptr_frame)
    PtrClassicFrameLayout mPtrFrame;
    @BindView(R.id.iv_ad)
    ImageView ivAd;
    @BindView(R.id.tv_broadcast)
    TextView tvBroadcast;
    @BindView(R.id.rcv_doctors)
    RecyclerView rcvDoctors;

    private DoctorPresenter doctorPresenter;
    private long diagnosticId; // 诊断记录Id

    public static void goActivity(Context context, TreatmentType treatmentType) {
        Intent intent = new Intent(context, MoreDoctorsActivity.class);
        intent.putExtra("treatmentType", treatmentType);
        context.startActivity(intent);
    }

    @BindView(R.id.titleBar)
    BaseTitleBar titleBar;

    private DoctorAdapter2 doctorAdapter;

    private TreatmentType treatmentType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_doctors);
        ButterKnife.bind(this);
        doctorPresenter = new DoctorPresenter(this);
        treatmentType = getIntent().getParcelableExtra("treatmentType");
        initView();
        initData();

        this.titleBar.showCenterText("My Dr.","颜值医生");
        titleBar.setCenterLeftTextSize(26);
        this.titleBar.setBackBtnClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mPtrFrame.postDelayed(new Runnable() {
            @Override
            public void run() {
                mPtrFrame.autoRefresh();
            }
        }, 100);
    }

    private void initPullRefresh() {
        mPtrFrame.setLastUpdateTimeRelateObject(this);
        mPtrFrame.setPtrHandler(new in.srain.cube.views.ptr.PtrDefaultHandler() {
            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                doctorPresenter.getRoomList(MyselfInfo.getLoginUser().getToken(), treatmentType.getId(), 4);
            }
        });

        // the following are default settings
        mPtrFrame.setResistance(1.7f);
        mPtrFrame.setRatioOfHeaderHeightToRefresh(1.2f);
        mPtrFrame.setDurationToClose(200);
        mPtrFrame.setDurationToCloseHeader(1000);
        // default is false
        mPtrFrame.setPullToRefresh(false);
        // default is true
        mPtrFrame.setKeepHeaderWhenRefresh(true);
    }

    @Override
    protected void initView() {
        initPullRefresh();
    }

    @Override
    protected void initData() {
        Log.d("monty","treatmentType.getOtherPicture() -> "+treatmentType.getOtherPicture());

        tvBroadcast.setText("已匹配治疗"+treatmentType.getTypeName()+"的医生");
        Glide.with(this).load(treatmentType.getOtherPicture()).centerCrop().into(ivAd);

        doctorAdapter = new DoctorAdapter2(this, new ArrayList<Doctor>(),rcvDoctors);
        doctorAdapter.setOnItemClickListener(this);
        FullyLinearLayoutManager linearLayoutManager = new FullyLinearLayoutManager(this);
        rcvDoctors.setNestedScrollingEnabled(false);
        rcvDoctors.setLayoutManager(linearLayoutManager);
        rcvDoctors.setAdapter(doctorAdapter);
        rcvDoctors.setAdapter(doctorAdapter);
    }

    @Override
    protected void onActivityResult(final int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == LiveActivity.REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                NiceDialogFactory.createStarDialog("评价", new NiceDialogFactory.OnStarDialogListener() {
                    @Override
                    public void onNegativeClick(BaseNiceDialog baseNiceDialog) {
//                        ToastUtils.showToast("投诉");
                        baseNiceDialog.dismiss();
                        ComplainActivity.GoToComplainActivity(MoreDoctorsActivity.this);

//                        getDiagnostic();
                    }

                    @Override
                    public void onPositiveClick(BaseNiceDialog baseNiceDialog, AppCompatRatingBar ratingBar) {
                        int rating = (int) ratingBar.getRating();
                        baseNiceDialog.dismiss();
                        RetrofitHelper.getInstance().createService(DoctorService.class).evaluateSave(MyselfInfo.getLoginUser().getToken(), String.valueOf(rating)).enqueue(new BaseCallBack<BaseCallModel<String>>() {
                            @Override
                            public void onSuccess(Response<BaseCallModel<String>> response) {
                                ToastUtils.showToast("评分成功");
                            }

                            @Override
                            public void onFailure(String message) {
                                ToastUtils.showToast("评分失败");
                            }
                        });
                        getDiagnostic();

                    }
                }).setOutCancel(false).show(getSupportFragmentManager());
            }
        } else if (requestCode == ComplainActivity.REQUEST_CODE) {
            getDiagnostic();
        }

    }
    /**
     * 获取诊断信息
     */
    private void getDiagnostic() {
        showLoadingDialog("");
        RetrofitHelper.getInstance().createService(PayService.class).getDiagnostic(MyselfInfo.getLoginUser().getToken()).enqueue(new BaseCallBack<BaseCallModel<DiagnosticInfo>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<DiagnosticInfo>> response) {
                closeLoadingDialog();
                DiagnosticInfo diagnosticInfo = response.body().data;
                diagnosticId = diagnosticInfo.getDiagnosticId();
                double price = diagnosticInfo.getPrice();
                if (diagnosticInfo != null && diagnosticInfo.getCoupon() != null && diagnosticInfo.getCoupon().size() > 0) {
                    List<DiagnosticInfo.Coupon> coupon = diagnosticInfo.getCoupon();
                    double money = 0;
                    List<Integer> couponIds = new ArrayList<>();
                    for (int i = 0; i < coupon.size(); i++) {
                        if (money < price) {
                            money += coupon.get(i).getMoney();
                            couponIds.add(coupon.get(i).getId());
                        } else {
                            couponIds.add(coupon.get(i).getId());
                            break;
                        }
                    }
                    final double finalMoney = money;
                    if (money >= price) { // 现金券是否足够支付本次诊金，如果足够支付就弹出使用现金券支付的Dialog，否则就弹出现金支付的Dialog
                        StringBuilder couponIdStrBuilder = new StringBuilder();
                        for (int i : couponIds) {
                            couponIdStrBuilder.append("" + i);
                            couponIdStrBuilder.append(",");
                        }
                        String couponIdStr = couponIdStrBuilder.toString();
                        couponIdStr = couponIdStr.substring(0, couponIdStr.length() - 1);

                        showCouponDialog(finalMoney, couponIdStr);
                    } else {
                        showGotoPayDialog(diagnosticId);
                    }
                } else {
                    showGotoPayDialog(diagnosticId);
                }
            }

            @Override
            public void onFailure(String message) {
                closeLoadingDialog();
                ToastUtils.showToast("请求诊断信息失败");
                showGotoPayDialog(-1);
            }
        });
    }

    private void showCouponDialog(final double finalMoney, final String couponIds) {
        NiceDialog.init()
                .setLayoutId(R.layout.base_dilog_layout)
                .setConvertListener(new ViewConvertListener() {
                    @Override
                    protected void convertView(ViewHolder viewHolder, final BaseNiceDialog baseNiceDialog) {
                        ((TextView) viewHolder.getView(R.id.tv_title)).setVisibility(View.GONE);
                        ((TextView) viewHolder.getView(R.id.tv_message)).setText("使用现金券" + finalMoney + "元\n(使用期限：3个月)");
                        ((Button) viewHolder.getView(R.id.btn_positive)).setText("立即使用");
                        viewHolder.getView(R.id.btn_positive).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                baseNiceDialog.dismiss();
                                showLoadingDialog("");
                                RetrofitHelper.getInstance().createService(PayService.class).payDiagnosticByCoupon(MyselfInfo.getLoginUser().getToken(), diagnosticId, couponIds).enqueue(new BaseCallBack<BaseCallModel<AliPayEntity>>() {
                                    @Override
                                    public void onSuccess(Response<BaseCallModel<AliPayEntity>> response) {
                                        closeLoadingDialog();
                                        new AlertDialog.Builder(MoreDoctorsActivity.this).setMessage("支付成功,是否前往我的护士查看治疗方案?").setPositiveButton("前往查看", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
//                                                DoctorVideoActivity.this.finish();
                                                EventBusUtil.post(new JumpToIMFragemtEvent());
                                            }
                                        }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        }).show();
                                    }

                                    @Override
                                    public void onFailure(String message) {
                                        ToastUtils.showToast("现金券支付失败");
                                        closeLoadingDialog();
                                    }
                                });
                            }
                        });
                        viewHolder.getView(R.id.btn_negative).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                baseNiceDialog.dismiss();
                            }
                        });
                    }
                })   //进行相关View操作的回调
                .setDimAmount(0.5f)     //调节灰色背景透明度[0-1]，默认0.5f
//                .setShowBottom(true)     //是否在底部显示dialog，默认flase
                .setMargin(30)     //dialog左右两边到屏幕边缘的距离（单位：dp），默认0dp
//                .setWidth()     //dialog宽度（单位：dp），默认为屏幕宽度
//                .setHeight()     //dialog高度（单位：dp），默认为WRAP_CONTENT
                .setOutCancel(false)     //点击dialog外是否可取消，默认true
                .setAnimStyle(android.R.style.Animation_Dialog).show(getSupportFragmentManager());
    }

    private void showGotoPayDialog(final long diagnosticId) {
        /*new AlertDialog.Builder(DoctorVideoActivity.this).setMessage("诊疗已结束,请支付诊金(可用现金券),护士会发送治疗方案给您!").setPositiveButton("前往支付", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DiagnosePayActivity.GotoPayActivity(DoctorVideoActivity.this, diagnosticId);
                dialog.dismiss();
            }
        }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).show();*/

        NiceDialog.init()
                .setLayoutId(R.layout.pay_dialog_layout)
                .setConvertListener(new ViewConvertListener() {
                    @Override
                    protected void convertView(ViewHolder viewHolder, final BaseNiceDialog baseNiceDialog) {
                        ((TextView) viewHolder.getView(R.id.tv_title)).setVisibility(View.GONE);
                        ((TextView) viewHolder.getView(R.id.tv_message)).setText("诊疗已结束,请支付诊金(可用现金券),护士会发送治疗方案给您!");
                        ((Button) viewHolder.getView(R.id.btn_positive)).setText("前往支付");
                        viewHolder.getView(R.id.btn_positive).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                baseNiceDialog.dismiss();
                                finish();
                                EventBusUtil.post(new JumpToIMFragemtEvent());
                            }
                        });
                    }
                })   //进行相关View操作的回调
                .setDimAmount(0.5f)     //调节灰色背景透明度[0-1]，默认0.5f
//                .setShowBottom(true)     //是否在底部显示dialog，默认flase
                .setMargin(30)     //dialog左右两边到屏幕边缘的距离（单位：dp），默认0dp
//                .setWidth()     //dialog宽度（单位：dp），默认为屏幕宽度
//                .setHeight()     //dialog高度（单位：dp），默认为WRAP_CONTENT
                .setOutCancel(false)     //点击dialog外是否可取消，默认true
                .setAnimStyle(android.R.style.Animation_Dialog).show(getSupportFragmentManager());
    }
    @Override
    public void getTreatmentTypeSuccess(List<TreatmentType> treatmentTypeList) {

    }

    @Override
    public void getDoctorRoomSuccess(List<DoctorClassify> doctorClassifies) {

        /*if (doctors != null && doctors.size() != 0) {
            doctorAdapter.notifyDataSetChanged(doctors);
        }*/
        List<Doctor> doctors = new ArrayList<>();
        for (DoctorClassify classify : doctorClassifies) {
            if (classify.getDoctorAttachList() != null && classify.getDoctorAttachList().size() > 0) {
                Doctor doctor = classify.getDoctorAttachList().get(0);
                doctor.setPrice(classify.getPrice());
                doctor.setRole(classify.getRole());
                doctor.setEnglishName(classify.getEnglishName());
                doctors.add(doctor);
                /*
                调试用
                doctors.add(doctor);
                doctors.add(doctor);*/
            }
        }
        doctorAdapter.notifyDataSetChanged(doctors);
        mPtrFrame.refreshComplete();
    }

    @Override
    public void getTreatmentTypeFailure(String message) {

    }

    @Override
    public void getDoctorRoomFailure(String message) {
        Toast.makeText(this, "获取医生列表失败", Toast.LENGTH_SHORT).show();
        mPtrFrame.refreshComplete();
    }

    @Override
    public void onItemClick(View view, Doctor doctor, int position) {
//        DoctorVideoActivity.startLiveActivity(this, doctor);
        LiveActivity.startLiveActivity(this, doctor, LiveActivity.REQUEST_CODE);
    }

   /* @Override
    public void getDoctorRoomFailure(String errorMsg) {
        ToastUtils.showToast("请求失败:" + errorMsg);
        mPtrFrame.refreshComplete();
    }

    @Override
    public void refreshComplete() {

    }*/
}
