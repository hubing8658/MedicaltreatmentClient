package com.kelin.client.gui.medicinebox;

import com.kelin.client.gui.medicinebox.entity.PurchasedBean;
import com.monty.library.http.BaseCallModel;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by guangjiqin on 2017/9/6.
 */

public interface MedicicineBoxService {

//    /**
//     * 积分兑换接口
//     *
//     * @param
//     * @return
//     */
//    @FormUrlEncoded
//    @POST("integral/takeMoney")
//    Call<BaseCallModel<Object>> takeMoney(@Field("token") String token,
//                                          @Field("code") String money,
//                                          @Field("integral") long integral);

    /**
     * 获取当前就诊时间接口
     *
     * @param
     * @return
     */
    @GET("record/myDrug")
    Call<BaseCallModel<Map<String, PurchasedBean>>> getMedicalBox(@Query("token") String token, @Query("type") int type);
}
