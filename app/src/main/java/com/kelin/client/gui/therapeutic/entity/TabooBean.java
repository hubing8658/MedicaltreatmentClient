package com.kelin.client.gui.therapeutic.entity;

/**
 * Created by guangjiqin on 2017/9/8.
 */

public class TabooBean {
    /**
     * id : 1
     * adminId : null
     * doctorId : 105
     * treatmentName : 脱发
     * programName : 留短发
     * describe : 具有很好的功效
     * pic :
     * sideEffect : false
     * applyTime : 2017-08-18 19:02:32
     * statu : null
     * passTime : null
     */

    public int id;
    public Object adminId;
    public int doctorId;
    public String treatmentName;
    public String programName;
    public String describe;
    public String pic;
    public boolean sideEffect;
    public String applyTime;
    public Object statu;
    public Object passTime;
}
