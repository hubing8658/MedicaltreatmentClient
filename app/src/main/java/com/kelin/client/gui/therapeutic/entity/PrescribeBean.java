package com.kelin.client.gui.therapeutic.entity;

import java.util.List;

/**
 * Created by Administrator on 2017/8/8 0008.
 */

public class PrescribeBean {

    /**
     * createTime : 2017-08-25 14:30:39
     * drugs : [{"id":2,"diagnosticId":7,"doctorId":105,"userId":2,"drugId":2,"drugType":"美白祛斑综合治疗法","drugName":"皮肤灵","useTime":"餐前","useInfo":"每日3次，每次2粒","useType":1,"price":40,"maxUnit":"瓶","drugstorePrice":0,"effect":"能很好的修复皮肤","count":2,"pic":null,"promptTime":null,"isBuy":true,"buyTime":"2017-08-25 19:40:28"}]
     * treatmentName : xiongmaoyan
     */

    public String createTime;
    public String treatmentName;
    public List<DrugsBean> drug;

    public boolean isSelectAll;

    public static class DrugsBean {
        /**
         * id : 2
         * diagnosticId : 7
         * doctorId : 105
         * userId : 2
         * drugId : 2
         * drugType : 美白祛斑综合治疗法
         * drugName : 皮肤灵
         * useTime : 餐前
         * useInfo : 每日3次，每次2粒
         * useType : 1
         * price : 40
         * maxUnit : 瓶
         * drugstorePrice : 0
         * effect : 能很好的修复皮肤
         * count : 2
         * pic : null
         * promptTime : null
         * isBuy : true
         * buyTime : 2017-08-25 19:40:28
         */

        public int id;
        public int diagnosticId;
        public int doctorId;
        public int userId;
        public int drugId;
        public String drugType;
        public String drugName;
        public String useTime;
        public String useInfo;
        public int useType;
        public double price;
        public String maxUnit;
        public double drugstorePrice;
        public String effect;
        public int count;
        public String pic;
        public Object promptTime;
        public boolean isBuy;
        public String buyTime;
        public String component;
        public String shape;
        public String applicableScope;
        public String badEffect;
        public String taboo;
        public String matters;
        public String effectiveTime;
        public String storage;
        public String standardNumber;
        public String drugStandard;
        public String producers;



        public boolean isOpen;
    }
}

