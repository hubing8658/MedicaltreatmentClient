package com.kelin.client.gui;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.kelin.client.R;
import com.kelin.client.common.BaseActivity;
import com.kelin.client.gui.login.LoginActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

public class GuiActivity extends BaseActivity {
    private ViewPager viewPager;
    private GuiAdapter adapter;

    private int currentPosition = 0;
    private int[] guis = {R.drawable.gui1,
            R.drawable.gui2,
            R.drawable.gui3,
            R.drawable.gui4};

    private List<View> imageViews;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gui);
        viewPager = ButterKnife.findById(this, R.id.viewpager);
        imageViews = new ArrayList<>();
        for (int i = 0; i < guis.length; i++) {
            ImageView view = new ImageView(this);
            view.setBackgroundColor(Color.WHITE);
            Glide.with(this).load(guis[i]).centerCrop().into(view);
//            view.setImageResource(guis[i]);
            view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            if (i == guis.length - 1) {
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
//                        startActivity(new Intent(GuiActivity.this, ScanTkeLotteryActivity.class));
                        startActivity(new Intent(GuiActivity.this, LoginActivity.class));
                    }
                });
            }
            imageViews.add(view);
        }

        adapter = new GuiAdapter(imageViews);
        viewPager.setAdapter(adapter);
        /*viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position == adapter.getCount()-1){
                    viewPager.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            finish();
                            startActivity(new Intent(GuiActivity.this,ScanTkeLotteryActivity.class));
                        }
                    },500);
                }
                Log.d("monty", "onPageSelected -> position -> " + position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });*/
    }

}
