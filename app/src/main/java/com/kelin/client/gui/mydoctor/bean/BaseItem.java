package com.kelin.client.gui.mydoctor.bean;

/**
 * 注释
 *
 * @e-mail mwu@szrlzz.com
 * Created by monty on 2017/8/18.
 */

public interface BaseItem {
    int DOCTOR = 0;
    int DOCTOR_CLASSIFY = 1;
    int DOCTOR_LEFT = 2;
    int DOCTOR_RIGHT = 3;
    int FOOTER = 4;
    int EMPTYLEFT = 5;
    int EMPTYRIGHT = 6;

    int getType();

}
