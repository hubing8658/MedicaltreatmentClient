package com.kelin.client.gui.therapeutic.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kelin.client.R;
import com.kelin.client.gui.therapeutic.entity.PrescribeBean;
import com.kelin.client.util.imageloader.GlideImageHelper;

import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by guangjiqin on 2017/8/18.
 */

public abstract class PrescribeExpandableListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<String> strKrys;
    private Map<String, PrescribeBean> entityChilds;

    public abstract void itemOnclick();

    public PrescribeExpandableListAdapter(Context context, List<String> strKrys, Map<String, PrescribeBean> entityChilds) {
        this.context = context;
        this.strKrys = strKrys;
        this.entityChilds = entityChilds;
    }

    @Override
    public int getGroupCount() {
        return strKrys.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return entityChilds.get(strKrys.get(groupPosition)).drug.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return entityChilds.get(strKrys.get(groupPosition));
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return entityChilds.get(strKrys.get(groupPosition)).drug.get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        final ParentHolder holder;
        if (null == convertView) {
            holder = new ParentHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.item_prescribe_title, parent, false);
            holder.cbSelectAllChild = (TextView) convertView.findViewById(R.id.cb_select_all);
            convertView.setTag(holder);
        } else {
            holder = (ParentHolder) convertView.getTag();
        }

        final PrescribeBean bean = entityChilds.get(strKrys.get(groupPosition));
        holder.cbSelectAllChild.setText(bean.treatmentName);
//        holder.cbSelectAllChild.setChecked(bean.isSelectAll);

        holder.cbSelectAllChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                bean.isSelectAll = holder.cbSelectAllChild.isChecked();
//                for (PrescribeBean.DrugsBean drugBean : bean.drug) {
//                    drugBean.isSelect = holder.cbSelectAllChild.isChecked();
//                }
                itemOnclick();
                notifyDataSetChanged();
            }
        });
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final ChildHolder holder;
        if (null == convertView) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_prescribe_content, parent, false);
            holder = new ChildHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ChildHolder) convertView.getTag();
        }

        final PrescribeBean fatherBean = entityChilds.get(strKrys.get(groupPosition));
        final PrescribeBean.DrugsBean entity = entityChilds.get(strKrys.get(groupPosition)).drug.get(childPosition);

        holder.tvGoodsName.setText(entity.drugName);
        holder.tvDosage.setText("使用方法：" + entity.drugType+"/"+entity.useInfo+"/"+entity.useTime);
        holder.tvEffect.setText("功效：" + entity.effect);
        holder.tvPrice.setText(String.valueOf("¥" + entity.price+"/"+entity.maxUnit));
        holder.tvNum.setText(String.valueOf("x" + entity.count));
        GlideImageHelper.showImage(context, entity.pic, R.drawable.ic_launcher, R.drawable.ic_launcher, holder.imgGoodsPhoto);

        holder.gonell.setVisibility(entity.isOpen ? View.VISIBLE : View.GONE);
        holder.ivPulldown.setImageResource(entity.isOpen ? R.drawable.icon_pullup : R.drawable.icon_pulldown);
        holder.ivPulldown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                entity.isOpen = !entity.isOpen;
                notifyDataSetChanged();
            }
        });

        holder.tvChengfen.setText("成分:" + entity.component);
        holder.tvZhengzhuang.setText("症状:" + entity.shape);
        holder.tvShiyongfanwei.setText("适用范围:" + entity.applicableScope);
        holder.tvBuliangfanying.setText("不良反应:" + entity.badEffect);
        holder.tvJinji.setText("禁忌:" + entity.taboo);
        holder.tvZhuyishixiang.setText("注意事项:" + entity.matters);
        holder.tvYouxiaoqi.setText("有效期:" + entity.effectiveTime);
        holder.tvChucangfangshi.setText("贮藏方式:" + entity.storage);
        holder.tvShengchanchangshang.setText("生产厂商:" + entity.producers);
        holder.tvGuige.setText("规格:" + entity.drugStandard);
        holder.tvChanpinbiaozhunhao.setText("产品标准号:" + entity.standardNumber);


        /*holder.cbSelect.setChecked(entity.isSelect);
        holder.cbSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                entity.isSelect = holder.cbSelect.isChecked();
                for (PrescribeBean.DrugsBean bean : fatherBean.drug) {
                    if (!bean.isSelect) {
                        fatherBean.isSelectAll = false;
                        break;
                    }
                    fatherBean.isSelectAll = true;
                }
                itemOnclick();
                notifyDataSetChanged();
            }
        });*/

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    class ParentHolder {
        TextView cbSelectAllChild;
    }

    /*class ChildHolder {
        CheckBox cbSelect;
        ImageView imgGoods;
        TextView tvName;
        TextView tvDosage;
        TextView tvEffect;
        TextView tvPrice;
        TextView tvNum;

    }*/

    static class ChildHolder {
        @BindView(R.id.img_goods_photo)
        ImageView imgGoodsPhoto;
        @BindView(R.id.tv_goods_name)
        TextView tvGoodsName;
        @BindView(R.id.iv_pulldown)
        ImageView ivPulldown;
        @BindView(R.id.tv_dosage)
        TextView tvDosage;
        @BindView(R.id.tv_effect)
        TextView tvEffect;
        @BindView(R.id.tv_chengfen)
        TextView tvChengfen;
        @BindView(R.id.tv_zhengzhuang)
        TextView tvZhengzhuang;
        @BindView(R.id.tv_guige)
        TextView tvGuige;
        @BindView(R.id.tv_shiyongfanwei)
        TextView tvShiyongfanwei;
        @BindView(R.id.tv_buliangfanying)
        TextView tvBuliangfanying;
        @BindView(R.id.tv_jinji)
        TextView tvJinji;
        @BindView(R.id.tv_zhuyishixiang)
        TextView tvZhuyishixiang;
        @BindView(R.id.tv_youxiaoqi)
        TextView tvYouxiaoqi;
        @BindView(R.id.tv_chucangfangshi)
        TextView tvChucangfangshi;
        @BindView(R.id.tv_shengchanchangshang)
        TextView tvShengchanchangshang;
        @BindView(R.id.tv_chanpinbiaozhunhao)
        TextView tvChanpinbiaozhunhao;
        @BindView(R.id.gonell)
        LinearLayout gonell;
        @BindView(R.id.tv_price)
        TextView tvPrice;
        @BindView(R.id.tv_num)
        TextView tvNum;

        ChildHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
