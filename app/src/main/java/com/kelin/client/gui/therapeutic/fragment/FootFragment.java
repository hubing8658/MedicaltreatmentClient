package com.kelin.client.gui.therapeutic.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.kelin.client.R;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.kelin.client.gui.therapeutic.TherapeuticService;
import com.kelin.client.gui.therapeutic.adapter.FootExpandanbleListAdapter;
import com.kelin.client.gui.therapeutic.entity.FootBean;
import com.kelin.client.util.ToastUtils;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;

import java.util.ArrayList;
import java.util.List;

import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;
import retrofit2.Response;

/**
 * 治疗方案-饮食
 * Created by guangjiqin on 2017/8/18.
 */

public class FootFragment extends BaseFragment {

    private final String TAG = getClass().getName();
    public static FootFragment footFragment;

    private View view;
    private long diagnosticId;

    private PtrClassicFrameLayout mPtrFrame;
    private ExpandableListView listView;
    private FootExpandanbleListAdapter adapter;
    private List<FootBean> mData;

    public static FootFragment createInstance(long diagnosticId) {
//        if (null == footFragment) {
            FootFragment footFragment = new FootFragment();
            footFragment.diagnosticId = diagnosticId;
//        }
        return footFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_foot, null);

        initView();

        initPullRefresh();

        return view;
    }

    private void initPullRefresh() {
        mPtrFrame = (PtrClassicFrameLayout) view.findViewById(R.id.store_house_ptr_frame);
        mPtrFrame.setLastUpdateTimeRelateObject(this);
        mPtrFrame.setPtrHandler(new PtrHandler() {
            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
                return PtrDefaultHandler.checkContentCanBePulledDown(frame, listView, header);
            }

            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                requestData();
            }
        });


        // the following are default settings
        mPtrFrame.setResistance(1.7f);
        mPtrFrame.setRatioOfHeaderHeightToRefresh(1.2f);
        mPtrFrame.setDurationToClose(200);
        mPtrFrame.setDurationToCloseHeader(1000);
        // default is false
        mPtrFrame.setPullToRefresh(false);
        // default is true
        mPtrFrame.setKeepHeaderWhenRefresh(true);
        mPtrFrame.postDelayed(new Runnable() {
            @Override
            public void run() {
                mPtrFrame.autoRefresh();
            }
        }, 100);
    }

    private void initView() {
        mData = new ArrayList<>();
        adapter = new FootExpandanbleListAdapter(mData, getActivity());
        listView = (ExpandableListView) view.findViewById(R.id.listview);
        listView.setGroupIndicator(null);
        listView.setAdapter(adapter);

    }

    @Override
    protected void requestData() {
        RetrofitHelper.getInstance().createService(TherapeuticService.class).getDiet(MyselfInfo.getLoginUser().getToken(),
                diagnosticId).enqueue(new BaseCallBack<BaseCallModel<List<FootBean>>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<List<FootBean>>> response) {
                mPtrFrame.refreshComplete();
                mData.clear();
                mData.addAll(response.body().data);
                dealData(mData);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(String message) {
                mPtrFrame.refreshComplete();
                ToastUtils.showToast(message);
            }
        });
    }

    private void dealData(List<FootBean> data) {
        for (FootBean bean : data) {
            //ingredients : 红豆-0.5千克,绿豆-0.1克
            String[] TempFood = bean.ingredients.split(",");
            if (TempFood.length > 0 && !TempFood[0].isEmpty()) {
                for (String string : TempFood) {
                    bean.material = bean.material + string.substring(0, string.lastIndexOf("-")) + "\n";
                    bean.dose = bean.dose + string.substring(string.lastIndexOf("-") + 1, string.length()) + "\n";
                }
            }

        }
    }
}
