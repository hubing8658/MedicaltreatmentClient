package com.kelin.client.gui.mydoctor.presenters.viewinterface;


import com.kelin.client.gui.mydoctor.bean.DoctorClassify;
import com.kelin.client.gui.mydoctor.bean.TreatmentType;

import java.util.List;

/**
 * Created by monty on 2017/8/1.
 */

public interface IDoctorView {
    void getTreatmentTypeSuccess(List<TreatmentType> treatmentTypeList);
    void getDoctorRoomSuccess(List<DoctorClassify> doctorClassifies);


    void getTreatmentTypeFailure(String message);

    void getDoctorRoomFailure(String message);

//    void refreshComplete();
}
