package com.kelin.client.gui.live.presenters;

import android.util.Log;

import com.kelin.client.gui.live.LiveService;
import com.kelin.client.gui.live.model.UpMenber;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.kelin.client.gui.mydoctor.bean.Doctor;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;

import retrofit2.Response;

/**
 * 用户上报状态到后台
 * Created by monty on 2017/8/13.
 */

public class UserUpLoadModel {
    private Doctor mDoctor;
    public UserUpLoadModel(Doctor doctor){
        this.mDoctor = doctor;
    }
    /**
     * 用户进入房间上报
     * 取消此接口
     */
    @Deprecated
    public void upLoadStartRoom(){
        RetrofitHelper.getInstance().createService(LiveService.class).enterRoom(MyselfInfo.getLoginUser().getToken(),mDoctor.getId()).enqueue(new BaseCallBack<BaseCallModel<String>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<String>> response) {
                Log.d("monty","upLoadStartRoom - onSuccess");
            }

            @Override
            public void onFailure(String message) {
                Log.e("monty","upLoadStartRoom - onFailure");
            }
        });
    }

    /**
     * 用户退出房间上报
     */
    public void upLoadQuitRoom(){
        /*RetrofitHelper.getInstance().createService(LiveService.class).outRoom(MyselfInfo.getLoginUser().getToken()).enqueue(new BaseCallBack<BaseCallModel<String>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<String>> response) {
                Log.d("monty","upLoadQuitRoom - onSuccess");
            }

            @Override
            public void onFailure(String message) {
                Log.e("monty","upLoadQuitRoom - onFailure");
            }
        });*/
    }

    /**
     * 用户申请连麦上报
     */
    public void upLoadApplyConnect(final OnUpLoadApplyConnectListener listener){
        RetrofitHelper.getInstance().createService(LiveService.class).applyConnect(MyselfInfo.getLoginUser().getToken(),mDoctor.getId()).enqueue(new BaseCallBack<BaseCallModel<UpMenber>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<UpMenber>> response) {
                Log.d("monty","upLoadApplyConnect - onSuccess");
                listener.onSuccess(response.body().data);

            }

            @Override
            public void onFailure(String message) {
                listener.onFailure(message);
                Log.e("monty","upLoadApplyConnect - onFailure");
            }
        });
    }

    /**
     * 用户退出队列上报
     */
    public void upLoadCancelConnect(final OnUploadReportListener listener){
        RetrofitHelper.getInstance().createService(LiveService.class).cancelApply(MyselfInfo.getLoginUser().getToken()).enqueue(new BaseCallBack<BaseCallModel<String>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<String>> response) {
                Log.d("monty","upLoadCancelConnect - onSuccess");
                listener.onSuccess();
            }

            @Override
            public void onFailure(String message) {
                Log.e("monty","upLoadCancelConnect - onFailure");
                listener.onFailure(message);
            }
        });
    }

    public void upLoadUserTimer(){
        RetrofitHelper.getInstance().createService(LiveService.class).userTimer(MyselfInfo.getLoginUser().getToken()).enqueue(new BaseCallBack<BaseCallModel<String>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<String>> response) {
                Log.d("monty","upLoadUserTimer - onSuccess");
            }

            @Override
            public void onFailure(String message) {
                Log.e("monty","upLoadUserTimer - onFailure");
            }
        });
    }

    interface OnUploadReportListener{
        void onSuccess();
        void onFailure(String msg);
    }

    interface OnUpLoadApplyConnectListener{
        void onSuccess(UpMenber upMenber);
        void onFailure(String msg);
    }

}
