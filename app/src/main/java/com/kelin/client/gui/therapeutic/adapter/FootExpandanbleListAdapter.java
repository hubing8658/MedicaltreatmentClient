package com.kelin.client.gui.therapeutic.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kelin.client.R;
import com.kelin.client.gui.therapeutic.entity.FootBean;
import com.kelin.client.util.imageloader.GlideImageHelper;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by guangjiqin on 2017/9/8.
 */

public class FootExpandanbleListAdapter extends BaseExpandableListAdapter {

    private List<FootBean> data;
    private Context context;

    public FootExpandanbleListAdapter(List<FootBean> data, Context context) {
        this.data = data;
        this.context = context;

    }

    @Override
    public int getGroupCount() {
        return data.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return data.get(groupPosition).useInfo.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return data.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return data.get(groupPosition).useInfo.get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        final ParentHolder holder;
        if (null == convertView) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_foot_title, parent, false);
            holder = new ParentHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ParentHolder) convertView.getTag();
        }
        final FootBean bean = data.get(groupPosition);
        holder.tvName.setText(bean.name);
        if(isExpanded){
            holder.jiantou.setImageResource(R.drawable.icon_pullup);
        }else{
            holder.jiantou.setImageResource(R.drawable.icon_pulldown);
        }
        GlideImageHelper.showImage(context, bean.pic, R.drawable.ic_launcher, R.drawable.ic_launcher, holder.imgFoot);
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final ChildHolder holder;
        if (null == convertView) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_foot, parent, false);
            holder = new ChildHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ChildHolder) convertView.getTag();
        }
        final FootBean fatherBean = data.get(groupPosition);
        final FootBean.UseInfoBean bean = fatherBean.useInfo.get(childPosition);

        //控制可见部分
        if (0 == childPosition) {
            holder.relTop.setVisibility(View.VISIBLE);
        }else{
            holder.relTop.setVisibility(View.GONE);
        }
        if (0 != childPosition && fatherBean.useInfo.size() - 1 != childPosition) {
            holder.relTop.setVisibility(View.GONE);
            holder.relBottom.setVisibility(View.GONE);
        }
        if (fatherBean.useInfo.size() - 1 == childPosition) {
            holder.relBottom.setVisibility(View.VISIBLE);
        }else{
            holder.relBottom.setVisibility(View.GONE);
        }
        //设置数据
        holder.tvMaterials.setText(fatherBean.material);
        holder.tvDose.setText(fatherBean.dose);

        holder.tvMethod.setText(bean.useInfo);
        if(bean.pic.isEmpty()){
            holder.imgMethod.setVisibility(View.GONE);
        }else{
            holder.imgMethod.setVisibility(View.VISIBLE);
            GlideImageHelper.showImage(context, bean.pic, R.drawable.ic_launcher, R.drawable.ic_launcher, holder.imgMethod);

        }
//        holder.imgMethod.setImageResource();// TODO: 2017/9/8

        holder.tvEffect.setText(fatherBean.effect);
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }


    class ParentHolder {
        @BindView(R.id.img_foot_photo)
        ImageView imgFoot;
        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.jiantou)
        ImageView jiantou;

        View itemView;

        ParentHolder(View view) {
            ButterKnife.bind(this, view);
            itemView = view;
        }
    }

    class ChildHolder {
        @BindView(R.id.lin_top)
        LinearLayout relTop;
        @BindView(R.id.tv_materials)
        TextView tvMaterials;
        @BindView(R.id.tv_dose)
        TextView tvDose;


        @BindView(R.id.lin_center)
        LinearLayout relCeter;
        @BindView(R.id.tv_method)
        TextView tvMethod;
        @BindView(R.id.img_method)
        ImageView imgMethod;


        @BindView(R.id.lin_bottom)
        LinearLayout relBottom;
        @BindView(R.id.tv_effect)
        TextView tvEffect;

        View itemView;

        ChildHolder(View view) {
            ButterKnife.bind(this, view);
            itemView = view;
        }
    }
}
