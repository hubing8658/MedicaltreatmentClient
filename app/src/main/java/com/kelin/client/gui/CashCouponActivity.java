package com.kelin.client.gui;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.kelin.client.R;
import com.kelin.client.common.BaseActivity;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.kelin.client.widget.BaseTitleBar;
import com.kelin.client.widget.PasswordEditText;
import com.kelin.client.widget.VerifyCodeEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 领取现金券
 */
public class CashCouponActivity extends BaseActivity {

    @BindView(R.id.titleBar)
    BaseTitleBar titleBar;
    @BindView(R.id.et_cashCoupon)
    EditText etCashCoupon;
    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.et_verifyCodeLayout)
    VerifyCodeEditText etVerifyCodeLayout;
    @BindView(R.id.et_pwdLayout)
    PasswordEditText etPwdLayout;
    @BindView(R.id.btn_confirm)
    Button btnConfirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cash_coupon);
        ButterKnife.bind(this);
        this.titleBar.showCenterText(R.string.scan_cashCoupon, R.drawable.scan_code, 0);
        this.titleBar.setBackBtnVis(false);
        this.titleBar.setRightText("跳过", 0, 0, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                MyselfInfo.startLoginActivity();
            }
        });

        this.etVerifyCodeLayout.setDrawableLeft(R.drawable.scan_icon_safe_blue);
        this.etPwdLayout.setDrawableLeft(R.drawable.reg_icon_password);

    }

    @OnClick(R.id.btn_confirm)
    public void onViewClicked() {
    }
}
