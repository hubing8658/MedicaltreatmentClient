package com.kelin.client.gui.address;


import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.bigkoo.pickerview.OptionsPickerView;
import com.bigkoo.pickerview.listener.CustomListener;
import com.kelin.client.R;
import com.kelin.client.bean.ProvinceBean;
import com.kelin.client.common.BaseActivity;
import com.kelin.client.common.Constans;
import com.kelin.client.gui.address.controller.AddAddressController;
import com.kelin.client.gui.address.entity.MyAddressEntity;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.kelin.client.util.ToastUtils;
import com.kelin.client.widget.BaseTitleBar;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by guangjiqin on 2017/8/9.
 */

public class EditAddressActivity extends BaseActivity {

    private final String TAG = getClass().getName();

    private boolean isAddAddress;

    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.et_phone_num)
    EditText etPhoneNum;
    @BindView(R.id.tv_region)
    TextView tvRegion;
    @BindView(R.id.et_address)
    EditText etAddress;
    @BindView(R.id.titleBar)
    BaseTitleBar title;

    private MyAddressEntity addressEntity;
    private AddAddressController controller;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_edit_address);

        ButterKnife.bind(this);

        initController();

        getIntentData();

        initTitle();

        iniView();
    }

    private void initController() {
        controller = new AddAddressController(MyselfInfo.getLoginUser().getToken()) {
            @Override
            public void addAddressSuccess() {
                Log.i(TAG, "添加收获地址成功！！");
                setResult(1);
                EditAddressActivity.this.finish();
            }

            @Override
            public void addAddressFailure(String message) {
                Log.i(TAG, "添加收获地址失败！！" + message);

            }

            @Override
            public void editAddressSuccess() {
                Log.i(TAG, "编辑收获地址成功！！");
                setResult(1);
                EditAddressActivity.this.finish();
            }

            @Override
            public void editAddressFailure(String message) {
                Log.i(TAG, "编辑收获地址失败！！" + message);

            }
        };
    }

    private void getIntentData() {
        addressEntity = getIntent().getParcelableExtra("addressEntity");
        if (null == addressEntity) {
            isAddAddress = true;
            etName.setText("");
            etPhoneNum.setText("");
            tvRegion.setText("");
            etAddress.setText("");
        } else {
            isAddAddress = false;
            etName.setText(addressEntity.receiveName);
            etPhoneNum.setText(addressEntity.receivePhone);
            tvRegion.setText(addressEntity.area);
            etAddress.setText(addressEntity.receiveAddress);
        }
    }

    private void initTitle() {
        title.showCenterText("MY ADDRESS","我的地址");
    }

    private void iniView() {

    }

    @OnClick(R.id.tv_region)
    public void changeRegion() {
        //隐藏键盘
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(etPhoneNum.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(etName.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(etAddress.getWindowToken(), 0);

        showCityChoies();
    }

    @OnClick(R.id.pvOptions)
    public void commitAddress() {
        if(null == etName.getText() || etName.getText().toString().trim().isEmpty()){
            ToastUtils.showToast(EditAddressActivity.this,R.string.reipt_name_null);
            return;
        }

        if(null == etPhoneNum.getText() || etPhoneNum.getText().toString().trim().isEmpty()){
            ToastUtils.showToast(EditAddressActivity.this,R.string.reipt_phone_null);
            return;
        }

        if(null == tvRegion.getText() || tvRegion.getText().toString().trim().isEmpty()){
            ToastUtils.showToast(EditAddressActivity.this,R.string.reipt_district_null);
            return;
        }

        if(null == etAddress.getText() || etAddress.getText().toString().trim().isEmpty()){
            ToastUtils.showToast(EditAddressActivity.this,R.string.reipt_address_null);
            return;
        }

        if (isAddAddress) {
            controller.addAddressData(etName.getText().toString(), etPhoneNum.getText().toString(), tvRegion.getText().toString(), etAddress.getText().toString(), false);
        } else {
            controller.editAddressData(etName.getText().toString(), addressEntity.id, etPhoneNum.getText().toString(), tvRegion.getText().toString(), etAddress.getText().toString(), false);
        }
    }


    //弹出选择框======
    private ArrayList<ProvinceBean> options1Items = new ArrayList<>();
    private ArrayList<ArrayList<String>> options2Items = new ArrayList<>();
    private OptionsPickerView pvOptions;

    private void showCityChoies() {
        options1Items.clear();
        options2Items.clear();

        for (String key: Constans.provinces.keySet()) {
            options1Items.add(new ProvinceBean(-1, key, "", ""));
            ArrayList<String> options2Items_01 = new ArrayList<>();
            for(String value:Constans.provinces.get(key)){
                options2Items_01.add(value);
            }
            options2Items.add(options2Items_01);
        }

        pvOptions = new OptionsPickerView.Builder(this, new OptionsPickerView.OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int options2, int options3, View v) {
                //返回的分别是三个级别的选中位置
                String tx = options1Items.get(options1).getPickerViewText()
                        + options2Items.get(options1).get(options2)
                       /* + options3Items.get(options1).get(options2).get(options3).getPickerViewText()*/;

                //设置位置
                tvRegion.setText(tx);
                pvOptions.dismiss();
            }
        }).setLayoutRes(R.layout.dialog_city_choise, new CustomListener() {
            @Override
            public void customLayout(View v) {
                Button btnSubmit = (Button) v.findViewById(R.id.btnSubmit);
                btnSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        pvOptions.returnData();
                    }
                });

            }
        })
//                .setTitleText("城市选择")
                .setContentTextSize(16)//设置滚轮文字大小
                .setDividerColor(Color.parseColor("#0f345e"))//设置分割线的颜色
                .setSelectOptions(0, 1)//默认选中项
                .setTextColorCenter(Color.parseColor("#f5b8d3"))
                .setTextColorOut(Color.parseColor("#0f345e"))
                .isCenterLabel(false) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
//                .setLabels("省", "市", "区")
                .setBackgroundId(0x66000000) //设置外部遮罩颜色
                .build();

        pvOptions.setPicker(options1Items, options2Items);//二级选择器
        pvOptions.show();
    }

}
