package com.kelin.client.gui.personalcenter.entity;

/**
 * Created by guangjiqin on 2017/8/15.
 */

public class PersonalCenterEntity {
    /**
     * id : 2
     * name : lyj
     * phone : 13332965191
     * password : 5701117D3416
     * photo :
     * gender : 男
     * age : 22
     * area : 湖北省荆州市
     * messageReminder : true
     * createTime : 1501034257000
     * enable : true
     * token : ac8dc909ac6c4a
     * openIdWX : null
     * openIdQQ : null
     * sig : null
     */

    public int id;
    public String name;
    public long phone;
    public String password;
    public String photo;
    public String gender;
    public int age;
    public String area;
    public boolean messageReminder;
    public String createTime;
    public boolean enable;
    public String token;
    public Object openIdWX;
    public Object openIdQQ;
    public Object sig;
}
