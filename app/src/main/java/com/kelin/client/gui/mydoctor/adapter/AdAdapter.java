package com.kelin.client.gui.mydoctor.adapter;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.jude.rollviewpager.RollPagerView;
import com.jude.rollviewpager.adapter.LoopPagerAdapter;
import com.kelin.client.R;

/**
 * Created by monty on 2017/7/31.
 */

public class AdAdapter extends LoopPagerAdapter {
    private int[] imgs = {
            R.drawable.banner1,
            R.drawable.banner2,
            R.drawable.banner3
    };
    public AdAdapter(RollPagerView viewPager) {
        super(viewPager);
    }

    @Override
    public View getView(ViewGroup container, int position) {
        View layout = LayoutInflater.from(container.getContext()).inflate(R.layout.layout_ad_item, container, false);
        ViewGroup.LayoutParams layoutParams = layout.getLayoutParams();
        layoutParams.height = container.getMeasuredHeight();
        layoutParams.width = container.getMeasuredWidth();
        layout.setLayoutParams(layoutParams);
        ImageView imageView = (ImageView) layout.findViewById(R.id.iv_adview);
        Glide.with(container.getContext()).load(imgs[position]).centerCrop().into(imageView);
        return layout;
    }

    @Override
    public int getRealCount() {
        return imgs.length;
    }

}
