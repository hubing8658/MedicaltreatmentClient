package com.kelin.client.gui.therapeutic.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kelin.client.R;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.kelin.client.gui.therapeutic.TherapeuticService;
import com.kelin.client.gui.therapeutic.entity.ReCheckBean;
import com.kelin.client.util.ToastUtils;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;

import retrofit2.Response;

/**
 * 治疗方案复诊
 * Created by guangjiqin on 2017/8/18.
 */

public class ReturnFragment extends BaseFragment {

    private final String TAG = getClass().getName();
    private long diagnosticId;

    private TextView tvReCheeckDate;
    private TextView tvContent;

    private View view;

    public static ReturnFragment createInstance(long diagnosticId) {
//        if (null == returnFragment) {
        ReturnFragment returnFragment = new ReturnFragment();
        returnFragment.diagnosticId = diagnosticId;
//        }
        return returnFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_return, null);

        initView();

        return view;
    }

    private void initView() {
        tvReCheeckDate = (TextView) view.findViewById(R.id.tv_recheck_date);
        tvContent = (TextView) view.findViewById(R.id.tv_content);

    }

    @Override
    protected void requestData() {
        RetrofitHelper.getInstance().createService(TherapeuticService.class).getRevisit(MyselfInfo.getLoginUser().getToken(),
                diagnosticId).enqueue(new BaseCallBack<BaseCallModel<ReCheckBean>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<ReCheckBean>> response) {
                ReCheckBean bean = response.body().data;
                tvReCheeckDate.setText(bean.date + " " + bean.startTime + "-" + bean.endTime);
                tvContent.setText(bean.content);
            }

            @Override
            public void onFailure(String message) {
                ToastUtils.showToast(message);
            }
        });
    }
}
