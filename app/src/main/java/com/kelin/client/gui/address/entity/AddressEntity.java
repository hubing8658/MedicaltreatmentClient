package com.kelin.client.gui.address.entity;


import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by guangjiqin on 2017/8/9.
 */

public class AddressEntity implements Parcelable {
    public String name;
    public String phoneNum;
    public String region;
    public String address;

    public AddressEntity() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.phoneNum);
        dest.writeString(this.region);
        dest.writeString(this.address);
    }

    protected AddressEntity(Parcel in) {
        this.name = in.readString();
        this.phoneNum = in.readString();
        this.region = in.readString();
        this.address = in.readString();
    }

    public static final Creator<AddressEntity> CREATOR = new Creator<AddressEntity>() {
        @Override
        public AddressEntity createFromParcel(Parcel source) {
            return new AddressEntity(source);
        }

        @Override
        public AddressEntity[] newArray(int size) {
            return new AddressEntity[size];
        }
    };
}


