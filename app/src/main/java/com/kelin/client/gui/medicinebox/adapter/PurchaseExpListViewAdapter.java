package com.kelin.client.gui.medicinebox.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.ExpandableListAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kelin.client.R;
import com.kelin.client.common.MyBaseAdapter;
import com.kelin.client.gui.medicinebox.entity.PurchasedBean;
import com.kelin.client.gui.medicinebox.entity.PurchasedEntity;
import com.kelin.client.util.imageloader.GlideImageHelper;

import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by guangjiqin on 2017/8/8.
 * 已购买列表适配器
 */

public class PurchaseExpListViewAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<String> dataKey;
    private Map<String, PurchasedBean> data;

    public PurchaseExpListViewAdapter(Context context, List<String> dataKey, Map<String, PurchasedBean> data) {
        this.context = context;
        this.data = data;
        this.dataKey = dataKey;
    }


    @Override
    public int getGroupCount() {
        return data.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return data.get(dataKey.get(groupPosition)).drug.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return data.get(dataKey.get(groupPosition));
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return data.get(dataKey.get(groupPosition)).drug.get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        ParentHolder holder;
        if (null == convertView) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_purchase_title, parent, false);
            holder = new ParentHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ParentHolder) convertView.getTag();
        }
        PurchasedBean bean = data.get(dataKey.get(groupPosition));
        holder.tvTutle.setText(bean.treatmentName);
        holder.tvTime.setText(bean.createTime);
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ChildHolder holder;
        if (null == convertView) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_purchase, parent, false);
            holder = new ChildHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ChildHolder) convertView.getTag();
        }
        PurchasedBean.DrugBean bean = data.get(dataKey.get(groupPosition)).drug.get(childPosition);
        holder.tvGoodsName.setText(bean.drugName);
        holder.tvPrice.setText("¥" + bean.price);
        holder.tvNum.setText("x" + bean.count);
        GlideImageHelper.showImage(context, bean.pic, R.drawable.ic_launcher, R.drawable.ic_launcher, holder.imgGoodsPhoto);

        if (childPosition == data.get(dataKey.get(groupPosition)).drug.size() - 1) {
            holder.relBottom.setVisibility(View.VISIBLE);
            float money = 0;
            for (PurchasedBean.DrugBean drugBean : data.get(dataKey.get(groupPosition)).drug) {
                money = money + (drugBean.price*drugBean.count);
            }
            holder.tvToal.setText(money + "元");
            holder.textToal.setText("共" + data.get(dataKey.get(groupPosition)).drug.size() + "件商品  合计：");
        } else {
            holder.relBottom.setVisibility(View.GONE);
        }

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    class ParentHolder {
        @BindView(R.id.rel_top)
        RelativeLayout relTop;
        @BindView(R.id.tv_title)
        TextView tvTutle;
        @BindView(R.id.tv_time)
        TextView tvTime;

        View itemView;

        ParentHolder(View view) {
            ButterKnife.bind(this, view);
            itemView = view;
        }
    }

    class ChildHolder {
        @BindView(R.id.img_goods_photo)
        ImageView imgGoodsPhoto;
        @BindView(R.id.tv_goods_name)
        TextView tvGoodsName;
        @BindView(R.id.tv_price)
        TextView tvPrice;
        @BindView(R.id.tv_num)
        TextView tvNum;
        @BindView(R.id.rel_bottom)
        RelativeLayout relBottom;
        @BindView(R.id.text_total)
        TextView textToal;
        @BindView(R.id.tv_total)
        TextView tvToal;

        View itemView;

        ChildHolder(View view) {
            ButterKnife.bind(this, view);
            itemView = view;
        }
    }
}
