package com.kelin.client.gui.login.presenters;

import android.content.Context;
import android.util.Log;

import com.kelin.client.common.Constans;
import com.kelin.client.gui.live.model.MySelfInfo;
import com.kelin.client.gui.login.LoginService;
import com.kelin.client.gui.login.bean.User;
import com.kelin.client.gui.login.presenters.viewinterface.ILoginView;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.kelin.client.util.PreferencesManager;
import com.kelin.client.util.ToastUtils;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.tencent.TIMManager;
import com.tencent.ilivesdk.ILiveCallBack;
import com.tencent.ilivesdk.core.ILiveLoginManager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.tencent.qq.QQ;
import cn.sharesdk.wechat.friends.Wechat;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by monty on 2017/7/31.
 */

public class LoginPresenter extends Presenter {
    private ILoginView loginView;

    public LoginPresenter(ILoginView loginView) {
        this.loginView = loginView;
    }

    public void login(final String phone, final String password, final boolean isRememberPas) {
        loginView.showLoadingDialog("");
        RetrofitHelper.getInstance().createService(LoginService.class).login(phone, password).enqueue(new BaseCallBack<BaseCallModel<User>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<User>> response) {

                if(isRememberPas){
                    PreferencesManager.putString("phoneNum",phone);
                    PreferencesManager.putString("passWord",password);
                    PreferencesManager.putBoolean("isRememberPas",true);
                }

                User user = response.body().data;
                MySelfInfo.getInstance().setNurse(user.getNurse());
                MyselfInfo.saveLoginUser(user);
                loginView.showLoadingDialog("正在登录直播平台");
                iLiveLogin(user.getToken(), user.getSig());

                getProvinces();

            }

            @Override
            public void onFailure(String message) {
                ToastUtils.showToast("登录失败," + message);
                loginView.closeLoadingDialog();
            }
        });
    }

    public void ThirdLogin(Context context, final String name) {
        loginView.showLoadingDialog("正在登录");
        ShareSDK.initSDK(context);
        Platform platForm = ShareSDK.getPlatform(name);
        platForm.removeAccount(true);
        platForm.setPlatformActionListener(new PlatformActionListener() {
            @Override
            public void onComplete(Platform platform, int i, HashMap<String, Object> hashMap) {
                Log.d("monty", name + " authorize —> " + hashMap.toString());
                String openid = "";
                if (Wechat.NAME.equals(name)) {
                    openid = (String) hashMap.get("openid");
                    String photo = (String) hashMap.get("headimgurl");
                    thirdLogin(openid,1,photo);
                } else if (QQ.NAME.equals(name)) {
                    openid = platform.getDb().getUserId();
                    platform.getDb().getUserIcon();
                    String photo = (String) hashMap.get("figureurl");
                    thirdLogin(openid,2,photo);
                }
            }

            @Override
            public void onError(Platform platform, int i, Throwable throwable) {
                Log.e("monty", name + " onError -> " + throwable.getMessage());
            }

            @Override
            public void onCancel(Platform platform, int i) {
                Log.e("monty", name + " onCancel -> ");
            }
        });
//        platForm.authorize();
        platForm.showUser(null);
    }

    public void thirdLogin(final String openid , final int flag,final String photo) {
        RetrofitHelper.getInstance().createService(LoginService.class).thirdLogin(openid, flag).enqueue(new Callback<BaseCallModel<User>>() {
            @Override
            public void onResponse(Call<BaseCallModel<User>> call, Response<BaseCallModel<User>> response) {
                loginView.closeLoadingDialog();
                if (response.body().code == 200) { // 获取用户数据成功
                    User user = response.body().data;
                    MyselfInfo.saveLoginUser(user);
                    Log.d("monty", "thirdLogin - onResponse -> " + user.toString());
                    loginView.showLoadingDialog("正在登录直播平台");
                    iLiveLogin(user.getToken(), user.getSig());
                } else if (response.body().code == 1) { // 该openid未绑定用户，待注册
                    loginView.startBindPhoneNumFragment(openid,flag,photo);
                } else {
                    onFailure(call, new Throwable("登录失败"));
                }

            }

            @Override
            public void onFailure(Call<BaseCallModel<User>> call, Throwable t) {
                ToastUtils.showToast(t.getMessage());
                loginView.closeLoadingDialog();
            }
        });
    }

    public void iLiveLogin(String id, String sig) {
        //登录
        ILiveLoginManager.getInstance().iLiveLogin(id, sig, new ILiveCallBack() {
            @Override
            public void onSuccess(Object data) {
                Log.d("monty", "iLiveLogin->env: " + TIMManager.getInstance().getEnv());
                loginView.startMainActivity();
                loginView.finish();
                loginView.closeLoadingDialog();
            }

            @Override
            public void onError(String module, int errCode, String errMsg) {
                Log.e("monty", "iLiveLogin - onError: " + errMsg);
                ToastUtils.showToast(errMsg);
                loginView.closeLoadingDialog();
            }
        });
    }

    //获取城市级联的接口
    private void getProvinces() {
        RetrofitHelper.getInstance().createService(LoginService.class).getProvinces(MyselfInfo.getLoginUser().getToken()).enqueue(new BaseCallBack<BaseCallModel<Map<String,List<String>>>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<Map<String,List<String>>>> response) {
                Constans.provinces = response.body().data;
            }

            @Override
            public void onFailure(String message) {

            }
        });
    }

    @Override
    public void onDestory() {

    }
}
