package com.kelin.client.gui.coupons.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import com.kelin.client.R;
import com.kelin.client.gui.coupons.FortTicketsActivity;
import com.kelin.client.gui.coupons.adapter.GiftCouponsListAdapter;
import com.kelin.client.gui.coupons.controller.CouponsController;
import com.kelin.client.gui.coupons.entity.CouponsEntity;
import com.kelin.client.gui.login.utils.MyselfInfo;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;



/**
 * Created by guangjiqin on 2017/8/10.
 */

public class UnusedGiftCouponsFragement extends Fragment {

    private View view;
    private List<CouponsEntity> couponsEntities;
    private GiftCouponsListAdapter adapter;

    private ListView listView;
    private Button btExchange;

    private CouponsController controller;


    public static UnusedGiftCouponsFragement createInstance() {

        UnusedGiftCouponsFragement fragment = new UnusedGiftCouponsFragement();

        return fragment;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_unused_gift_coupons, null);

//        testData();//测试数据
        EventBus.getDefault().register(this);


        initView();

        initListView();

        initController();

        initData();

        return view;
    }


    @Subscribe
    public void forTicketSuccess(String str) {
        if ("兑换成功".equals(str)) {
            initData();
        }
    }


    private void initController() {
        controller = new CouponsController(MyselfInfo.getLoginUser().getToken()) {
            @Override
            public void getCouponsListSuccess(List<CouponsEntity> treatmentType) {
                couponsEntities.clear();
                couponsEntities.addAll(treatmentType);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void getCouponsListFailure(String message) {

            }
        };
    }

    private void initView() {
        listView = (ListView) view.findViewById(R.id.listview);
        btExchange = (Button) view.findViewById(R.id.bt_exchange);
        btExchange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ToastUtils.showToast(getActivity(), "兑换金卷");
                startActivity(new Intent(getActivity(), FortTicketsActivity.class));
            }
        });
    }

    private void initListView() {
        couponsEntities = new ArrayList<>();
        adapter = new GiftCouponsListAdapter(getActivity(), couponsEntities, false);
        listView.setAdapter(adapter);
    }


    private void initData() {
        controller.getCouponListData(1);
    }
}

