package com.kelin.client.gui.main.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kelin.client.R;
import com.kelin.client.gui.main.activity.DiagnoseInfActivity;
import com.kelin.client.gui.main.bean.MyRecordBean;
import com.kelin.client.gui.therapeutic.TherapeuticMainActivity;
import com.kelin.client.util.imageloader.GlideImageHelper;

import java.util.List;
import java.util.Map;

/**
 * Created by guangjiqin on 2017/8/8.
 * 诊疗记录的二级listview
 */

public class DiagnoseListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private String[] strParent;
    private Map<String, List<MyRecordBean.DiagnosticsBean>> data;

    public DiagnoseListAdapter(Context context, Map<String, List<MyRecordBean.DiagnosticsBean>> data, String[] strParent) {
        this.context = context;
        this.data = data;
        this.strParent = strParent;

    }

    @Override
    public int getGroupCount() {
        return data.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return data.get(strParent[groupPosition]).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return data.get(strParent[groupPosition]);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return data.get(strParent[groupPosition]).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        ParentHolder holder;
        if (null == convertView) {
            holder = new ParentHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.item_diagnose_parent, parent, false);
            holder.tvTitle = (TextView) convertView.findViewById(R.id.tv_title);
            holder.imgMore = (ImageView) convertView.findViewById(R.id.img_more);
            convertView.setTag(holder);
        } else {
            holder = (DiagnoseListAdapter.ParentHolder) convertView.getTag();
        }
        holder.tvTitle.setText(strParent[groupPosition]);
        if (isExpanded) {
            holder.tvTitle.setTextColor(context.getResources().getColor(R.color.pink_new));
            holder.imgMore.setImageResource(R.drawable.down2);
        } else {
            holder.tvTitle.setTextColor(Color.WHITE);
            holder.imgMore.setImageResource(R.drawable.right);
        }
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ChildHolder holder;
        if (null == convertView) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_treatment_record, parent, false);
            holder = new DiagnoseListAdapter.ChildHolder();
            holder.imgPhoto = (ImageView) convertView.findViewById(R.id.img_user_photo);
            holder.tvTime = (TextView) convertView.findViewById(R.id.tv_time);
            holder.tvName = (TextView) convertView.findViewById(R.id.tv_name);
            holder.tvTreatmentTime = (TextView) convertView.findViewById(R.id.tv_treatment_time);
            holder.btReCheck = (Button) convertView.findViewById(R.id.bt_re_check);
            holder.relItem = (RelativeLayout) convertView.findViewById(R.id.rel_item);
            convertView.setTag(holder);
        } else {
            holder = (ChildHolder) convertView.getTag();
        }
        final MyRecordBean.DiagnosticsBean entity = data.get(strParent[groupPosition]).get(childPosition);
        holder.tvTime.setText(entity.diagnosticTime.substring(0, entity.diagnosticTime.lastIndexOf(" ")));
        holder.tvName.setText(entity.doctorName);
        holder.tvTreatmentTime.setText("诊疗时间：" + entity.diagnosticTime);
        GlideImageHelper.showImage(context, entity.doctorPic, R.drawable.ic_launcher, R.drawable.ic_launcher, holder.imgPhoto);
        holder.btReCheck.setText(R.string.therapeutic_schedule);
        holder.btReCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, TherapeuticMainActivity.class);
                intent.putExtra(TherapeuticMainActivity.KEY_DIAGNOSTIC_ID, entity.diagnosticId);
                context.startActivity(intent);
            }
        });

        holder.relItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(context, DiagnoseInfActivity.class);
                intent.putExtra("entity",entity);
                context.startActivity(intent);
            }
        });
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    class ParentHolder {
        TextView tvTitle;
        ImageView imgMore;
    }

    class ChildHolder {
        ImageView imgPhoto;
        TextView tvTime;
        TextView tvName;
        TextView tvTreatmentTime;
        Button btReCheck;
        RelativeLayout relItem;
    }

}
