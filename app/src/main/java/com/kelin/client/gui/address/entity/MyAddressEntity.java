package com.kelin.client.gui.address.entity;


import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by guangjiqin on 2017/8/13.
 */

public class MyAddressEntity implements Parcelable {

    public int id = -1;
    public int userId = -1;
    public String receiveName = "";
    public String receivePhone = "";
    public String area = "";
    public String receiveAddress = "";
    public boolean isDefault;
    public String createTime = "";
    public String lastupdateTime = "";
    public boolean statu;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.userId);
        dest.writeString(this.receiveName);
        dest.writeString(this.receivePhone);
        dest.writeString(this.area);
        dest.writeString(this.receiveAddress);
        dest.writeByte(this.isDefault ? (byte) 1 : (byte) 0);
        dest.writeString(this.createTime);
        dest.writeString(this.lastupdateTime);
        dest.writeByte(this.statu ? (byte) 1 : (byte) 0);
    }

    public MyAddressEntity() {
    }

    protected MyAddressEntity(Parcel in) {
        this.id = in.readInt();
        this.userId = in.readInt();
        this.receiveName = in.readString();
        this.receivePhone = in.readString();
        this.area = in.readString();
        this.receiveAddress = in.readString();
        this.isDefault = in.readByte() != 0;
        this.createTime = in.readString();
        this.lastupdateTime = in.readString();
        this.statu = in.readByte() != 0;
    }

    public static final Parcelable.Creator<MyAddressEntity> CREATOR = new Parcelable.Creator<MyAddressEntity>() {
        @Override
        public MyAddressEntity createFromParcel(Parcel source) {
            return new MyAddressEntity(source);
        }

        @Override
        public MyAddressEntity[] newArray(int size) {
            return new MyAddressEntity[size];
        }
    };
}
