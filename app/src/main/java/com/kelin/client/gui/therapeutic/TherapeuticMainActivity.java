package com.kelin.client.gui.therapeutic;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kelin.client.R;
import com.kelin.client.common.BaseActivity;
import com.kelin.client.gui.therapeutic.adapter.TherapeuticFragmentAdapter;
import com.kelin.client.widget.BaseTitleBar;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by guangjiqin on 2017/8/18.
 * 患者-治疗方案汇总
 */

public class TherapeuticMainActivity extends BaseActivity implements View.OnClickListener {

    /** 诊断id */
    public static final String KEY_DIAGNOSTIC_ID = "diagnostic_id";

    @BindView(R.id.viewpage)
    ViewPager viewPager;
    @BindView(R.id.titleBar)
    BaseTitleBar title;

    @BindView(R.id.tab1)
    FrameLayout tab1;
    @BindView(R.id.tab2)
    RelativeLayout tab2;
    @BindView(R.id.tab3)
    RelativeLayout tab3;
    @BindView(R.id.tab4)
    RelativeLayout tab4;
    @BindView(R.id.tab5)
    RelativeLayout tab5;


    @BindView(R.id.tv1)
    TextView tv1;
    @BindView(R.id.tv2)
    TextView tv2;
    @BindView(R.id.tv3)
    TextView tv3;
    @BindView(R.id.tv4)
    TextView tv4;
    @BindView(R.id.tv5)
    TextView tv5;

    @BindView(R.id.line_1)
    View line1;
    @BindView(R.id.line_2)
    View line2;
    @BindView(R.id.line_3)
    View line3;
    @BindView(R.id.line_4)
    View line4;
    @BindView(R.id.line_5)
    View line5;


    private TherapeuticFragmentAdapter adapter;

    private long diagnosticId = -1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_therapeutic);

        ButterKnife.bind(this);

        getIntentData();

        initTitle();

        initViewPage();


    }

    private void getIntentData() {
        diagnosticId = getIntent().getIntExtra(KEY_DIAGNOSTIC_ID, -1);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tab1:
                setSelect(0);
                break;
            case R.id.tab2:
                setSelect(1);
                break;
            case R.id.tab3:
                setSelect(2);
                break;
            case R.id.tab4:
                setSelect(3);
                break;
            case R.id.tab5:
                setSelect(4);
                break;
            default:
                break;
        }

    }

    private void initViewPage() {

        adapter = new TherapeuticFragmentAdapter(getSupportFragmentManager(),diagnosticId);
        viewPager.setAdapter(adapter);
        viewPager.setPageMargin(5);

        setSelect(0);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                setSelect(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        tab1.setOnClickListener(this);
        tab2.setOnClickListener(this);
        tab3.setOnClickListener(this);
        tab4.setOnClickListener(this);
        tab5.setOnClickListener(this);
    }

    private void initTitle() {
        title.showCenterText("TREANTMENTPLAN", "治疗方案");
    }


    private void setSelect(int position) {
        reSetAll();
        switch (position) {
            case 0:
                tv1.setTextColor(getResources().getColor(R.color.pink_ff7fb9));
                line1.setVisibility(View.VISIBLE);
                break;
            case 1:
                tv2.setTextColor(getResources().getColor(R.color.pink_ff7fb9));
                line2.setVisibility(View.VISIBLE);
                break;
            case 2:
                tv3.setTextColor(getResources().getColor(R.color.pink_ff7fb9));
                line3.setVisibility(View.VISIBLE);
                break;
            case 3:
                tv4.setTextColor(getResources().getColor(R.color.pink_ff7fb9));
                line4.setVisibility(View.VISIBLE);
                break;
            case 4:
                tv5.setTextColor(getResources().getColor(R.color.pink_ff7fb9));
                line5.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
        viewPager.setCurrentItem(position);
    }

    private void reSetAll() {
        tv1.setTextColor(Color.WHITE);
        tv2.setTextColor(Color.WHITE);
        tv3.setTextColor(Color.WHITE);
        tv4.setTextColor(Color.WHITE);
        tv5.setTextColor(Color.WHITE);
        line1.setVisibility(View.GONE);
        line2.setVisibility(View.GONE);
        line3.setVisibility(View.GONE);
        line4.setVisibility(View.GONE);
        line5.setVisibility(View.GONE);
    }
}
