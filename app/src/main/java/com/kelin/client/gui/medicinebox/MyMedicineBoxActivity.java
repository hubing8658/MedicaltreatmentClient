package com.kelin.client.gui.medicinebox;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kelin.client.R;
import com.kelin.client.common.BaseActivity;
import com.kelin.client.gui.medicinebox.adapter.MedicineBoxFragmentAdapter;
import com.kelin.client.widget.BaseTitleBar;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by guangjiqin on 2017/8/8.
 * //我的药箱页面
 */

public class MyMedicineBoxActivity extends BaseActivity {

    @BindView(R.id.viewPage)
    ViewPager viewPager;
    @BindView(R.id.titleBar)
    BaseTitleBar title;
    @BindView(R.id.tab1)
    RelativeLayout tab1;
    @BindView(R.id.tab2)
    RelativeLayout tab2;
    @BindView(R.id.tv1)
    TextView tv1;
    @BindView(R.id.tv2)
    TextView tv2;

    @BindView(R.id.line_1)
    View line1;
    @BindView(R.id.line_2)
    View line2;

    private MedicineBoxFragmentAdapter adapter;

    private boolean isDrugPlay;

    /**
     * 打开药箱页面
     * @param isDrugPlay 是否是买药，如果是买药则默认展示待购买Fragment
     */
    public static void GotoMyMedicineBoxActivity(Context context, boolean isDrugPlay){
        Intent intent = new Intent(context,MyMedicineBoxActivity.class);
        intent.putExtra("isDrugPlay",isDrugPlay);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medicine_box);
        ButterKnife.bind(this);

        isDrugPlay = getIntent().getBooleanExtra("isDrugPlay",false);

        initTitle();

        initViewPage();
    }

    private void initTitle() {
        title.showCenterText(getString(R.string.my_pillbox),R.drawable.my_pillbox,0);
    }

    private void initViewPage() {
        adapter = new MedicineBoxFragmentAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);


        setSelect(isDrugPlay?1:0);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                setSelect(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        tab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelect(0);
            }
        });
        tab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelect(1);
            }
        });
    }

    private void setSelect(int position) {
        reSetAll();
        switch (position) {
            case 0:
                tv1.setTextColor(getResources().getColor(R.color.pink_ff7fb9));
                line1.setVisibility(View.VISIBLE);
                break;
            case 1:
                tv2.setTextColor(getResources().getColor(R.color.pink_ff7fb9));
                line2.setVisibility(View.VISIBLE);
                break;
        }
        viewPager.setCurrentItem(position);
    }

    private void reSetAll() {
        tv1.setTextColor(getResources().getColor(R.color.blue_0b325a));
        tv2.setTextColor(getResources().getColor(R.color.blue_0b325a));
        line1.setVisibility(View.GONE);
        line2.setVisibility(View.GONE);
    }
}
