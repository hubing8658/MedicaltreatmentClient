package com.kelin.client.gui.main.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.kelin.client.gui.mydoctor.bean.Doctor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by guangjiqin on 2017/9/7.
 */

public class MyRecordBean implements Parcelable {

    /**
     * diagnostics : [{"doctorPic":"http://123.207.9.75:8080/upload-file/doctor/photo/a9605ed43989434e8bcf77d8205dde32.jpg","doctorName":"天边一朵云","diagnosticTime":"2017-10-10 19:09:41","diagnosticId":897},{"doctorPic":"http://123.207.9.75:8080/upload-file/doctor/photo/a9605ed43989434e8bcf77d8205dde32.jpg","doctorName":"天边一朵云","diagnosticTime":"2017-10-10 01:38:31","diagnosticId":890},{"doctorPic":"http://123.207.9.75:8080/upload-file/doctor/photo/a9605ed43989434e8bcf77d8205dde32.jpg","doctorName":"天边一朵云","diagnosticTime":"2017-10-10 01:28:05","diagnosticId":889},{"doctorPic":"http://123.207.9.75:8080/upload-file/doctor/photo/a9605ed43989434e8bcf77d8205dde32.jpg","doctorName":"天边一朵云","diagnosticTime":"2017-10-09 22:25:05","diagnosticId":879},{"doctorPic":"http://123.207.9.75:8080/upload-file/doctor/photo/a9605ed43989434e8bcf77d8205dde32.jpg","doctorName":"天边一朵云","diagnosticTime":"2017-10-09 17:50:23","diagnosticId":871},{"doctorPic":"http://123.207.9.75:8080/upload-file/doctor/photo/a9605ed43989434e8bcf77d8205dde32.jpg","doctorName":"天边一朵云","diagnosticTime":"2017-10-08 23:41:26","diagnosticId":836},{"doctorPic":"http://123.207.9.75:8080/upload-file/doctor/photo/83ff7e5d2b9147f68125d2fa88cb731e.jpg","doctorName":"宇智波佐助","diagnosticTime":"2017-10-08 02:36:28","diagnosticId":833},{"doctorPic":"http://123.207.9.75:8080/upload-file/doctor/photo/a9605ed43989434e8bcf77d8205dde32.jpg","doctorName":"天边一朵云","diagnosticTime":"2017-10-08 02:30:30","diagnosticId":832},{"doctorPic":"http://123.207.9.75:8080/upload-file/doctor/photo/a9605ed43989434e8bcf77d8205dde32.jpg","doctorName":"天边一朵云","diagnosticTime":"2017-10-08 02:24:10","diagnosticId":828},{"doctorPic":"http://123.207.9.75:8080/upload-file/doctor/photo/a9605ed43989434e8bcf77d8205dde32.jpg","doctorName":"天边一朵云","diagnosticTime":"2017-10-08 02:20:21","diagnosticId":827}]
     * exceptionRecord : {"sumTime":50726,"roleId":1,"count":1028,"sittingPhotos":"http://123.207.9.75:8080/upload-file/doctor/photo/83ff7e5d2b9147f68125d2fa88cb731e.jpg","introduceUrl":"http://123.207.9.75:8080/upload-file/doctor/introduce_url/7e7c00a75a5d4a2caf1ac72a0d708db9.mp4","token":"eh1231871583wt","localHospital":"1111","name":"宇智波佐助","id":4,"job":"火影","waitCount":null,"introduction":"1111","treatmentNames":"痘痘,脸部皮炎,脚气,斑,脱发,鸡皮肤,腋臭,xiongmaoyan,疤痕,灰指甲"}
     */

    public Doctor exceptionRecord;
    public List<DiagnosticsBean> diagnostics;

    @Override
    public String toString() {
        return "MyRecordBean{" +
                "exceptionRecord=" + exceptionRecord +
                ", diagnostics=" + diagnostics +
                '}';
    }

    public static class ExceptionRecordBean implements Parcelable {
        /**
         * sumTime : 50726
         * roleId : 1
         * count : 1028
         * sittingPhotos : http://123.207.9.75:8080/upload-file/doctor/photo/83ff7e5d2b9147f68125d2fa88cb731e.jpg
         * introduceUrl : http://123.207.9.75:8080/upload-file/doctor/introduce_url/7e7c00a75a5d4a2caf1ac72a0d708db9.mp4
         * token : eh1231871583wt
         * localHospital : 1111
         * name : 宇智波佐助
         * id : 4
         * job : 火影
         * waitCount : null
         * introduction : 1111
         * treatmentNames : 痘痘,脸部皮炎,脚气,斑,脱发,鸡皮肤,腋臭,xiongmaoyan,疤痕,灰指甲
         */

        public int sumTime;
        public int roleId;
        public int count;
        public String sittingPhotos;
        public String introduceUrl;
        public String token;
        public String localHospital;
        public String name;
        public int id;
        public String job;
        public int waitCount;
        public String introduction;
        public String treatmentNames;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.sumTime);
            dest.writeInt(this.roleId);
            dest.writeInt(this.count);
            dest.writeString(this.sittingPhotos);
            dest.writeString(this.introduceUrl);
            dest.writeString(this.token);
            dest.writeString(this.localHospital);
            dest.writeString(this.name);
            dest.writeInt(this.id);
            dest.writeString(this.job);
            dest.writeInt(this.waitCount);
            dest.writeString(this.introduction);
            dest.writeString(this.treatmentNames);
        }

        public ExceptionRecordBean() {
        }

        protected ExceptionRecordBean(Parcel in) {
            this.sumTime = in.readInt();
            this.roleId = in.readInt();
            this.count = in.readInt();
            this.sittingPhotos = in.readString();
            this.introduceUrl = in.readString();
            this.token = in.readString();
            this.localHospital = in.readString();
            this.name = in.readString();
            this.id = in.readInt();
            this.job = in.readString();
            this.waitCount = in.readInt();
            this.introduction = in.readString();
            this.treatmentNames = in.readString();
        }

        public static final Creator<ExceptionRecordBean> CREATOR = new Creator<ExceptionRecordBean>() {
            @Override
            public ExceptionRecordBean createFromParcel(Parcel source) {
                return new ExceptionRecordBean(source);
            }

            @Override
            public ExceptionRecordBean[] newArray(int size) {
                return new ExceptionRecordBean[size];
            }
        };

        @Override
        public String toString() {
            return "ExceptionRecordBean{" +
                    "sumTime=" + sumTime +
                    ", roleId=" + roleId +
                    ", count=" + count +
                    ", sittingPhotos='" + sittingPhotos + '\'' +
                    ", introduceUrl='" + introduceUrl + '\'' +
                    ", token='" + token + '\'' +
                    ", localHospital='" + localHospital + '\'' +
                    ", name='" + name + '\'' +
                    ", id=" + id +
                    ", job='" + job + '\'' +
                    ", waitCount=" + waitCount +
                    ", introduction='" + introduction + '\'' +
                    ", treatmentNames='" + treatmentNames + '\'' +
                    '}';
        }
    }

    public static class DiagnosticsBean implements Parcelable {
        /**
         * doctorPic : http://123.207.9.75:8080/upload-file/doctor/photo/a9605ed43989434e8bcf77d8205dde32.jpg
         * doctorName : 天边一朵云
         * diagnosticTime : 2017-10-10 19:09:41
         * diagnosticId : 897
         */

        public String doctorPic;
        public String doctorName;
        public String diagnosticTime;
        public int diagnosticId;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.doctorPic);
            dest.writeString(this.doctorName);
            dest.writeString(this.diagnosticTime);
            dest.writeInt(this.diagnosticId);
        }

        public DiagnosticsBean() {
        }

        protected DiagnosticsBean(Parcel in) {
            this.doctorPic = in.readString();
            this.doctorName = in.readString();
            this.diagnosticTime = in.readString();
            this.diagnosticId = in.readInt();
        }

        public static final Creator<DiagnosticsBean> CREATOR = new Creator<DiagnosticsBean>() {
            @Override
            public DiagnosticsBean createFromParcel(Parcel source) {
                return new DiagnosticsBean(source);
            }

            @Override
            public DiagnosticsBean[] newArray(int size) {
                return new DiagnosticsBean[size];
            }
        };

        @Override
        public String toString() {
            return "DiagnosticsBean{" +
                    "doctorPic='" + doctorPic + '\'' +
                    ", doctorName='" + doctorName + '\'' +
                    ", diagnosticTime='" + diagnosticTime + '\'' +
                    ", diagnosticId=" + diagnosticId +
                    '}';
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.exceptionRecord, flags);
        dest.writeList(this.diagnostics);
    }

    public MyRecordBean() {
    }

    protected MyRecordBean(Parcel in) {
        this.exceptionRecord = in.readParcelable(ExceptionRecordBean.class.getClassLoader());
        this.diagnostics = new ArrayList<DiagnosticsBean>();
        in.readList(this.diagnostics, DiagnosticsBean.class.getClassLoader());
    }

    public static final Parcelable.Creator<MyRecordBean> CREATOR = new Parcelable.Creator<MyRecordBean>() {
        @Override
        public MyRecordBean createFromParcel(Parcel source) {
            return new MyRecordBean(source);
        }

        @Override
        public MyRecordBean[] newArray(int size) {
            return new MyRecordBean[size];
        }
    };
}


