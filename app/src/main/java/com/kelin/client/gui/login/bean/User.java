package com.kelin.client.gui.login.bean;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by monty on 2017/7/22.
 */
@Entity
public class User {
    private String sig;
    private String createTime;
    private boolean enable;
    @Id
    private long id;
    private boolean messageReminder;
    private String password;
    private String phone;
    private String token;
    private String name;
    private String openIdWX;
    private String openIdQQ;
    private String photo;
    private String gender;
    private String age;
    private String area;
    private String time;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    /** 用户IM系统id */
    private String nurse;

    /**
     * 设置用户IM系统id
     * @param nurse 用户IM系统id
     */
    public void setNurse(String nurse) {
        this.nurse = nurse;
    }

    /**
     * 获取用户IM系统id
     * @return 用户IM系统id
     */
    public String getNurse() {
        return nurse;
    }


    @Generated(hash = 290365746)
    public User(String sig, String createTime, boolean enable, long id,
            boolean messageReminder, String password, String phone, String token,
            String name, String openIdWX, String openIdQQ, String photo,
            String gender, String age, String area, String time, String nurse) {
        this.sig = sig;
        this.createTime = createTime;
        this.enable = enable;
        this.id = id;
        this.messageReminder = messageReminder;
        this.password = password;
        this.phone = phone;
        this.token = token;
        this.name = name;
        this.openIdWX = openIdWX;
        this.openIdQQ = openIdQQ;
        this.photo = photo;
        this.gender = gender;
        this.age = age;
        this.area = area;
        this.time = time;
        this.nurse = nurse;
    }

    @Generated(hash = 586692638)
    public User() {
    }


    @Override
    public String toString() {
        return "User{" +
                "sig='" + sig + '\'' +
                ", createTime='" + createTime + '\'' +
                ", enable=" + enable +
                ", id=" + id +
                ", messageReminder=" + messageReminder +
                ", password='" + password + '\'' +
                ", phone=" + phone +
                ", token='" + token + '\'' +
                ", name='" + name + '\'' +
                ", openIdWX='" + openIdWX + '\'' +
                ", openIdQQ='" + openIdQQ + '\'' +
                ", photo='" + photo + '\'' +
                ", gender='" + gender + '\'' +
                ", age='" + age + '\'' +
                ", area='" + area + '\'' +
                '}';
    }


    public String getArea() {
        if(null == area){
            area = "";
        }
        return this.area;
    }


    public void setArea(String area) {
        this.area = area;
    }


    public String getAge() {
        if(null == age){
            age = "";
        }
        return this.age;
    }


    public void setAge(String age) {
        this.age = age;
    }


    public String getGender() {
        if(null == gender){
            gender = "";
        }
        return this.gender;
    }


    public void setGender(String gender) {
        this.gender = gender;
    }


    public String getPhoto() {
        if(null == photo){
            photo = "";
        }
        return this.photo;
    }


    public void setPhoto(String photo) {
        this.photo = photo;
    }


    public String getOpenIdQQ() {
        if(null == openIdQQ){
            openIdQQ = "";
        }
        return this.openIdQQ;
    }


    public void setOpenIdQQ(String openIdQQ) {
        this.openIdQQ = openIdQQ;
    }


    public String getOpenIdWX() {
        if(null == openIdWX){
            openIdWX = "";
        }
        return this.openIdWX;
    }


    public void setOpenIdWX(String openIdWX) {
        this.openIdWX = openIdWX;
    }


    public String getName() {
        if(null == name){
            name = "";
        }
        return this.name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public String getToken() {
        if(null == token){
            token = "";
        }
        return this.token;
    }


    public void setToken(String token) {
        this.token = token;
    }


    public String getPhone() {
        if(null == phone){
            phone = "";
        }
        return this.phone;
    }


    public void setPhone(String phone) {
        this.phone = phone;
    }


    public String getPassword() {
        if(null == password){
            password = "";
        }
        return this.password;
    }


    public void setPassword(String password) {
        this.password = password;
    }


    public boolean getMessageReminder() {
        return this.messageReminder;
    }


    public void setMessageReminder(boolean messageReminder) {
        this.messageReminder = messageReminder;
    }


    public long getId() {
        return this.id;
    }


    public void setId(long id) {
        this.id = id;
    }


    public boolean getEnable() {
        return this.enable;
    }


    public void setEnable(boolean enable) {
        this.enable = enable;
    }


    public String getCreateTime() {
        if(null == createTime){
            createTime = "";
        }
        return this.createTime;
    }


    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }


    public String getSig() {
        if(null == sig){
            sig = "";
        }
        return this.sig;
    }


    public void setSig(String sig) {
        this.sig = sig;
    }
}
