package com.kelin.client.services;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by kelin on 2017/6/9.
 */

public interface TestService {
    @GET("users/list")
    Call<List<String>> list();



}
