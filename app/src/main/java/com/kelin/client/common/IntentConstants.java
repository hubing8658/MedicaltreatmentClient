
package com.kelin.client.common;

/**
 * Intent中传递参数常量类
 *
 * @author hubing
 * @version [1.0.0.0, 2017-09-18]
 */
public class IntentConstants {

    private IntentConstants() {
    }

    /** 传递诊断id key */
    public static final String KEY_DIAGNOSTIC_ID = "diagnostic_id";

    /** 传递优惠券id key */
    public static final String KEY_COUPON_ID = "coupon_id";

    /** 传递价格 key */
    public static final String KEY_PRICE = "price";

    /** 支付方式 :1-在线支付，0-货到付款 */
    public static final String KEY_PAY_TYPE = "payType";

    /** 收货地址id */
    public static final String KEY_ADDRESS_ID = "addressId";

    /** 微信支付SDK需要的参数 */
    public static final String KEY_WXPAY_PARAMS = "wxpay_params";
    /** 支付宝支付SDK需要的参数 */
    public static final String KEY_ALIPAY_PARAMS = "alipay_params";

}
