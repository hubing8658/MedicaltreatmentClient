package com.kelin.client.common;

import android.content.Context;
import android.view.LayoutInflater;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wangbin on 2017/7/9.
 */

public abstract class MyBaseAdapter<T> extends android.widget.BaseAdapter {
    protected Context mContext;
    protected List<T> mData = new ArrayList<>();
    protected LayoutInflater mLayoutInflater;

    public MyBaseAdapter(Context context) {
        this.mContext = context;
        this.mLayoutInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public T getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setData(List<T> data, boolean isClear) {
        if (isClear) {
            mData.clear();
        }
        if (null != data) {
            mData.addAll(data);

        }
        notifyDataSetChanged();

    }

    public void addData(List<T> data) {
        if (null != data && data.size() > 0) {
            mData.addAll(data);
            notifyDataSetChanged();
        }

    }

}
