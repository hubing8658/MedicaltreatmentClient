package com.kelin.client.util;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Vibrator;

import com.kelin.client.MyApplication;
import com.kelin.client.R;

import java.io.IOException;

import static android.content.Context.VIBRATOR_SERVICE;

/**
 * ${Description}
 *
 * @author monty
 * @date 2017/12/6
 */

public class SoundManager {
    private MediaPlayer mediaPlayer;
    private Vibrator vibrator;

    /**
     * 播放铃声和震动
     */
    public void playRingtoneAndVibrator() {
        try {
            playRingtone();
        } catch (IOException e) {
            e.printStackTrace();
        }
        startVibrator();
    }

    /**
     * 停止铃声和震动
     */
    public void stopRingtoneAndVibrator() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
        if (vibrator != null) {
            vibrator.cancel();
            vibrator = null;
        }
    }


    /**
     * 播放铃声
     *
     * @throws IOException
     */
    private void playRingtone() throws IOException {
        mediaPlayer = new MediaPlayer();
        Uri uri = Uri.parse("android.resource://" + MyApplication.getApplication().getPackageName() + "/raw/" + R.raw.call);
        mediaPlayer.setDataSource(MyApplication.getApplication(), uri);
        mediaPlayer.prepare();
        mediaPlayer.start();
        mediaPlayer.setLooping(true); //循环播放
    }

    /**
     * 震动
     */
    private void startVibrator() {
        vibrator = (Vibrator) MyApplication.getApplication().getSystemService(VIBRATOR_SERVICE);
        //按照指定的模式去震动。
        //vibrator.vibrate(1000);
        //数组参数意义：第一个参数为等待指定时间后开始震动，震动时间为第二个参数。后边的参数依次为等待震动和震动的时间
        //第二个参数为重复次数，-1为不重复，0为一直震动
        vibrator.vibrate(new long[]{1000, 1000}, 0);
    }
}
