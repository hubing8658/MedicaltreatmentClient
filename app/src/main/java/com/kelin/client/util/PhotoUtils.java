package com.kelin.client.util;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;

import com.kelin.client.MyApplication;
import com.yanzhenjie.permission.AndPermission;

import java.io.File;

/**
 * Created by monty on 2017/7/26.
 */

public class PhotoUtils {
    private static String picturesDir = MyApplication.getApplication().getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath();

    public static final int REQUEST_CODE_TAKE_PHOTO = 0x01;
    public static final int REQUEST_CODE_OPEN_ALBUM = 0x02;

    public static String takePhoto(final Activity context) {
        if (AndPermission.hasPermission(context, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            String photoPath = picturesDir + File.separator + System.currentTimeMillis() + ".jpg";
            Uri photoUri = Uri.fromFile(new File(photoPath));
            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE"); //照相
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri); //指定图片输出地址
            try {
                context.startActivityForResult(intent, REQUEST_CODE_TAKE_PHOTO); //启动照相
            } catch (Exception e) {
                e.printStackTrace();
                ToastUtils.showToast("相机打开失败，请检查您的权限设置");
            }
            return photoPath;
        } else {
            AndPermission.with(context).permission(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE).start();
        }
        return null;
    }

    public static void openAlbum(Activity activity){
        Intent intent = new Intent();
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/*");
        if (Build.VERSION.SDK_INT <19) {
            intent.setAction(Intent.ACTION_GET_CONTENT);
        }else {
            intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
        }
        activity.startActivityForResult(intent, REQUEST_CODE_OPEN_ALBUM);
    }

}

