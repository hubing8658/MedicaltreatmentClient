package com.kelin.client.util;

import android.util.Log;

import java.util.List;

/**
 * Created by kelin on 2017/6/9.
 * Log打印
 */

public class MyLog {

    public static boolean isLogable = false;

    /**
     * 打印info信息
     *
     * @param tag
     * @param msg
     */
    public static void i(String tag, String msg) {
        if (isLogable) {
            Log.i(tag, "" + msg);
        }
    }

    public static void i(String tag, String msg, Throwable tr) {
        if (isLogable) {
            Log.i(tag, msg, tr);
        }
    }

    /**
     * 打印error信息
     *
     * @param msg
     */
    public static void e(String tag, String msg) {
        if (isLogable) {
            Log.e(tag, "" + msg);
        }
    }

    /**
     * 打印error信息
     *
     * @param msg
     */
    public static void e(String tag, String msg, Throwable tr) {
        if (isLogable) {
            Log.e(tag, msg, tr);
        }
    }

    /**
     * 打印debug信息
     *
     * @param tag
     * @param msg
     */
    public static void d(String tag, String msg) {
        if (isLogable) {
            Log.d(tag, "" + msg);
        }
    }

    public static void d(String tag, String msg, Throwable tr) {
        if (isLogable) {
            Log.d(tag, msg, tr);
        }
    }

    /**
     * 打印warn错误
     *
     * @param tag
     * @param msg
     */
    public static void w(String tag, String msg) {
        if (isLogable) {
            Log.w(tag, "" + msg);
        }
    }

    /**
     * System.out打印
     *
     * @param obj
     */
    public static void print(Object obj) {
        if (isLogable) {
            System.out.println(obj);
        }
    }

    /**
     * 打印list的info信息
     *
     * @param tag
     * @param prefix
     * @param list
     */
    public static void infoList(String tag, String prefix, List<?> list) {
        StringBuilder sb = new StringBuilder();
        if (isLogable && list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                sb.append(list.get(i).toString());
            }
        }
        i(tag, prefix + " : " + sb.toString());
    }
}
