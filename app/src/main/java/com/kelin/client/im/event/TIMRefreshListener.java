package com.kelin.client.im.event;

import com.tencent.TIMConversation;

import java.util.List;

/**
 * Created by Administrator on 2017/8/6 0006.
 */

public interface TIMRefreshListener {
    void onRefresh();

    void onRefreshConversation(List<TIMConversation> var1);
}
