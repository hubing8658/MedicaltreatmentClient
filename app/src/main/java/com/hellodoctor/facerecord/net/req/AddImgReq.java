package com.hellodoctor.facerecord.net.req;

/**
 * Created by soon on 2018/3/18.
 */

public class AddImgReq extends BaseReq {
    private String photoUrl;

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}
