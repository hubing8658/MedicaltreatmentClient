package com.hellodoctor.facerecord.net.req;

/**
 * Created by soon on 2018/3/18.
 */

public class GenerateVideoReq extends BaseReq {
    private int days;
    private String title;
    private String videoUrl;
    private int isContinuous;

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public int getIsContinuous() {
        return isContinuous;
    }

    public void setIsContinuous(int isContinuous) {
        this.isContinuous = isContinuous;
    }
}
