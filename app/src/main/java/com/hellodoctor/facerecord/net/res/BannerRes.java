
package com.hellodoctor.facerecord.net.res;

import com.hellodoctor.facerecord.entity.BannerEnt;
import com.hellodoctor.facerecord.entity.DocumentaryEnt;

import java.util.ArrayList;

/**
 * <一句话功能简述> <功能详细描述>
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-18]
 */
public class BannerRes {

    public ArrayList<BannerEnt> banner;

    public DocumentaryEnt documental;

}
