package com.hellodoctor.facerecord.entity;

import com.hellodoctor.facerecord.common.entity.BaseEntity;

/**
 * <一句话功能简述>
 * <功能详细描述>
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-18]
 */
public class BannerEnt extends BaseEntity {

    public String id;

    public String title;
    public String imageUrl;
    public String content;

    /** 排序（数字越大越靠前） */
    public int sort;
    public String isPublish;
    public String createBy;
    public String createDate;
    public String updateBy;
    public String updateDate;
    public String remarks;
    public String delFlag;

}
