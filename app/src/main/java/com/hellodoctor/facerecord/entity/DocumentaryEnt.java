package com.hellodoctor.facerecord.entity;

import com.hellodoctor.facerecord.common.entity.BaseEntity;

/**
 * <一句话功能简述>
 * <功能详细描述>
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-28]
 */
public class DocumentaryEnt extends BaseEntity {

    /** 编号 */
    public int id;
    /** 用户id */
    public int userId;
    /** 标题 */
    public String title;
    /** 视频路径 */
    public String videoUrl;
    /** 视频天数 */
    public int videoDays;
    /** 是否连续   1 是  2  否 */
    public int isContinuous;
    /** 生成时间 */
    public String generateTime;
    /** 价值 */
    public double worth;
    /** 状态  gen生成  aud审核  ref拒绝  rel发布  del删除 */
    public String status;
    /** 审核人 */
    public String auditName;
    /** 审核时间 */
    public String auditTime;
    /** 拒绝原因 */
    public String refuseReason;
    /** 发布时间 */
    public String releaseTime;
    /** 收藏数量 */
    public int collectAmount;
    /** 点赞数量 */
    public int praiseAmount;
    /** 点赞数更新时间 */
    public String praiseTime;
    /** 视频预览图 */
    public String previewUrl;
    /** 视频最后一张 */
    public String preFinalUrl;
    /** 是否收藏（1=是 0=否） */
    public int isCollect;
    /** 是否点赞（1=是 0=否） */
    public int isPraise;

}
