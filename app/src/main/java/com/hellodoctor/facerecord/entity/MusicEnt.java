package com.hellodoctor.facerecord.entity;

import com.hellodoctor.facerecord.common.entity.BaseEntity;

/**
 * <一句话功能简述>
 * <功能详细描述>
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-21]
 */
public class MusicEnt extends BaseEntity {

    /** 编号 */
    public String id;

    /** 音乐名 */
    public String musicName;

    /** 音乐路径 */
    public String musicUrl;

    /** 排序（数字越大越靠前） */
    public int sort;

    public String isPublish;
    public String createBy;
    public String createDate;
    public String updateBy;
    public String updateDate;
    public String remarks;
    public String delFlag;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MusicEnt musicEnt = (MusicEnt) o;

        return id != null ? id.equals(musicEnt.id) : musicEnt.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
