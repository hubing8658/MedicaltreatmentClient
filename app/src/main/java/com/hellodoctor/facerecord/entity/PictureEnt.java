
package com.hellodoctor.facerecord.entity;

import com.hellodoctor.facerecord.common.entity.BaseEntity;

/**
 * <一句话功能简述> <功能详细描述>
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-17]
 */
public class PictureEnt extends BaseEntity {

    /** 照片编号 */
    public int id;

    /** 用户编号 */
    public int userId;

    /** 照片远程地址 */
    public String photo;

    /** 照片日期(服务器返回时间，格式：yyyy-MM-dd HH:mm:ss) */
    public String photoTime;

    /** 照片日期毫秒值，本地使用 */
    public long photoTimeMs;

    /** 年月，本地展示使用 */
    public String yearMonth;

    /** 年月日，本地展示使用 */
    public String yearMonthDay;

    /** 照片本地路径 */
    public String localPath;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PictureEnt that = (PictureEnt) o;

        return id == that.id;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
