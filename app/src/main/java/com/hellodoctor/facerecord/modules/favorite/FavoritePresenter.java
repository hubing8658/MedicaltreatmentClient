package com.hellodoctor.facerecord.modules.favorite;

import android.content.Context;

import com.hellodoctor.facerecord.common.constants.AppCommonCons;
import com.hellodoctor.facerecord.common.constants.LogTag;
import com.hellodoctor.facerecord.entity.FavoriteEnt;
import com.hellodoctor.facerecord.modules.favorite.FavoriteContract.IFavoritePresenter;
import com.hellodoctor.facerecord.modules.favorite.FavoriteContract.IFavoriteView;
import com.hellodoctor.facerecord.net.RecordApiService;
import com.isoftstone.mis.mmsdk.common.architecture.presenter.BasePresenter;
import com.isoftstone.mis.mmsdk.common.utils.log.MMLog;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;

/**
 * <一句话功能简述>
 * <功能详细描述>
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-24]
 */
public class FavoritePresenter extends BasePresenter<IFavoriteView> implements IFavoritePresenter{

    public FavoritePresenter(Context context, IFavoriteView view) {
        super(context, view);
    }

    @Override
    public void loadFavorites(int pageNo) {
        RecordApiService service = RetrofitHelper.getInstance().createService(RecordApiService.class);
        String token = MyselfInfo.getLoginUser().getToken();

        Call<BaseCallModel<ArrayList<FavoriteEnt>>> serviceFavorites = service.getFavorites(pageNo,
                AppCommonCons.PAGE_SIZE, token);
        serviceFavorites.enqueue(new BaseCallBack<BaseCallModel<ArrayList<FavoriteEnt>>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<ArrayList<FavoriteEnt>>> response) {
                if (response == null) {
                    getView().loadFavoritesFinish(false, null);
                    return;
                }
                BaseCallModel<ArrayList<FavoriteEnt>> body = response.body();
                if (body == null) {
                    getView().loadFavoritesFinish(false, null);
                    return;
                }
                getView().loadFavoritesFinish(true, body.data);
            }

            @Override
            public void onFailure(String message) {
                MMLog.et(LogTag.FACE_RECORD, "获取收藏记录片失败：" + message);
                getView().loadFavoritesFinish(false, null);
            }
        });
    }

    @Override
    protected void onRelease() {

    }
}
