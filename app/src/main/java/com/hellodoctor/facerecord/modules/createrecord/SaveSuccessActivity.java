
package com.hellodoctor.facerecord.modules.createrecord;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.hellodoctor.facerecord.common.base.CommonBaseActivity;
import com.hellodoctor.facerecord.common.constants.VideoConstants;
import com.hellodoctor.facerecord.common.utils.MoneyUtils;
import com.hellodoctor.facerecord.entity.FaceRecordEnt;
import com.hellodoctor.facerecord.modules.createrecord.SaveSuccessContract.ISaveSuccessPresenter;
import com.hellodoctor.facerecord.modules.videoplayer.VideoActivity;
import com.kelin.client.R;

import java.io.Serializable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * <一句话功能简述> <功能详细描述>
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-22]
 */
public class SaveSuccessActivity extends CommonBaseActivity<ISaveSuccessPresenter>{

    Unbinder unbinder;

    /** 预估价值 */
    @BindView(R.id.tv_discreet_value)
    TextView discreetValueTv;

    private FaceRecordEnt faceRecordEnt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.save_success_activity);
    }

    @Override
    public void initViews() {
        unbinder = ButterKnife.bind(this);
        setTitleBg(R.drawable.save_success_title);
    }

    @Override
    public void initListener() {

    }

    @Override
    public void loadData() {
        Intent intent = getIntent();
        Serializable object = intent.getSerializableExtra(VideoConstants.KEY_VIDEO_RECORD);
        if (object instanceof FaceRecordEnt) {
            faceRecordEnt = (FaceRecordEnt) object;
        }

        if (faceRecordEnt != null) {
            discreetValueTv.setText(getString(R.string.money_value, MoneyUtils.formatMoney(faceRecordEnt.worth)));
        }
    }

    @Override
    public ISaveSuccessPresenter createPresenter() {
        return null;
    }

    @OnClick(R.id.btn_look_video)
    public void lookVideo(View v) {
        if (faceRecordEnt != null) {
            VideoActivity.gotoVideoActivity(this, faceRecordEnt);
        }
    }

    @Override
    protected void onDestroy() {
        if (unbinder != null) {
            unbinder.unbind();
        }
        super.onDestroy();
    }
}
