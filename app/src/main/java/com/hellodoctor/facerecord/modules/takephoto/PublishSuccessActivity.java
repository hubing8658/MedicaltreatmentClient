
package com.hellodoctor.facerecord.modules.takephoto;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.hellodoctor.facerecord.common.base.CommonBaseActivity;
import com.hellodoctor.facerecord.modules.createrecord.AlbumActivity;
import com.isoftstone.mis.mmsdk.common.architecture.presenter.IBasePresenter;
import com.isoftstone.mis.mmsdk.common.architecture.view.IBaseView;
import com.kelin.client.R;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * 发布照片成功
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-25]
 */
public class PublishSuccessActivity extends CommonBaseActivity<IBasePresenter<IBaseView>> {

    Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.publish_success_activity);
    }

    @Override
    public IBasePresenter<IBaseView> createPresenter() {
        return null;
    }

    @Override
    public void initViews() {
        unbinder = ButterKnife.bind(this);
        setTitleBg(R.drawable.publish_success_title);
    }

    @Override
    public void initListener() {

    }

    @Override
    public void loadData() {

    }

    @OnClick(R.id.btn_look_photos)
    public void lookVideo(View v) {
        startActivity(new Intent(this, AlbumActivity.class));
        finish();
    }

    @Override
    protected void onDestroy() {
        if (unbinder != null) {
            unbinder.unbind();
        }
        super.onDestroy();
    }
}
