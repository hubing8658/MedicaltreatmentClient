
package com.hellodoctor.facerecord.modules.myrecord;

import com.hellodoctor.facerecord.entity.FaceRecordEnt;
import com.isoftstone.mis.mmsdk.common.architecture.presenter.IBasePresenter;
import com.isoftstone.mis.mmsdk.common.architecture.view.IBaseLoadingView;

import java.util.List;

/**
 * 我的记录片界面Presenter接口和View接口定义。
 *
 * @author hubing
 * @version [1.0.0.0, 2017-05-03]
 */
class MyRecordContract {

    /**
     * 我的记录片界面presenter接口
     *
     * @author hubing
     * @version [1.0.0.0, 2016-12-29]
     */
    interface IMyRecordPresenter extends IBasePresenter<IMyRecordView> {

        void loadRecord(int pageNo);

        void modifyVideoStatus(FaceRecordEnt faceRecordEnt);

        void addPraise(FaceRecordEnt recordEnt);

        void cancelPraise(FaceRecordEnt recordEnt);

        void addFavorite(FaceRecordEnt recordEnt);

        void cancelFavorite(FaceRecordEnt recordEnt);

    }

    /**
     * 我的记录片界面view接口
     *
     * @author hubing
     * @version [1.0.0.0, 2016-12-29]
     */
    interface IMyRecordView extends IBaseLoadingView {

        /**
         * 加载完成
         * @param result 加载结果
         * @param faceRecordEnts 我的记录片列表
         */
        void loadRecordFinish(boolean result, List<FaceRecordEnt> faceRecordEnts);

        /**
         * 修改状态成功
         * @param faceRecordEnt
         */
        void modifyVideoStatusSuccess(FaceRecordEnt faceRecordEnt);

        /**
         * 修改状态失败
         */
        void modifyVideoStatusFailure();

        void opPraiseSuccess(FaceRecordEnt recordEnt);

        void opFavoriteSuccess(FaceRecordEnt recordEnt);

        void operationFailure();

    }

}
