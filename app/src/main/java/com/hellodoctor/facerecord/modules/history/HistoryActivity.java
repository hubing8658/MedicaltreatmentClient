package com.hellodoctor.facerecord.modules.history;

import android.os.Bundle;
import android.widget.ListView;

import com.canyinghao.canrefresh.CanRefreshLayout;
import com.hellodoctor.facerecord.common.base.CommonBaseActivity;
import com.hellodoctor.facerecord.common.utils.DateParserUtils;
import com.hellodoctor.facerecord.common.utils.ShareUtils;
import com.hellodoctor.facerecord.entity.FaceRecordEnt;
import com.hellodoctor.facerecord.entity.HistoryEnt;
import com.hellodoctor.facerecord.modules.history.HistoryContract.IHistoryPresenter;
import com.hellodoctor.facerecord.modules.history.HistoryContract.IHistoryView;
import com.hellodoctor.facerecord.modules.videoplayer.VideoActivity;
import com.isoftstone.mis.mmsdk.common.widget.refreshview.MMClassicRefreshView;
import com.kelin.client.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * <一句话功能简述>
 * <功能详细描述>
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-24]
 */
public class HistoryActivity extends CommonBaseActivity<IHistoryPresenter> implements IHistoryView,
        CanRefreshLayout.OnRefreshListener, CanRefreshLayout.OnLoadMoreListener, HistoryAdapter.OnOperationListener {

    Unbinder unbinder;

    @BindView(R.id.can_content_view)
    ListView favoritesLv;

    @BindView(R.id.crl_refresh)
    CanRefreshLayout refreshCrl;

    @BindView(R.id.can_refresh_header)
    MMClassicRefreshView refreshHeaderView;

    private HistoryAdapter adapter;

    /** 当前加载的页码数 */
    private int currentPage = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.favorite_activity);
    }

    @Override
    public IHistoryPresenter createPresenter() {
        return new HistoryPresenter(this, this);
    }

    @Override
    public void initViews() {
        unbinder = ButterKnife.bind(this);
        setTitleBg(R.drawable.history_title);

        adapter = new HistoryAdapter(this);
        favoritesLv.setAdapter(adapter);
    }

    @Override
    public void initListener() {
        // 设置上拉、下拉监听
        refreshCrl.setOnRefreshListener(this);
        refreshCrl.setOnLoadMoreListener(this);
        adapter.setOnOperationListener(this);
    }

    @Override
    public void loadData() {
        presenter.loadHistories(currentPage);
    }

    @Override
    public void onRefresh() {
        currentPage = 1;
        presenter.loadHistories(currentPage);
    }

    @Override
    public void onLoadMore() {
        presenter.loadHistories(currentPage);
    }

    @Override
    public void loadHistoriesFinish(boolean result, List<HistoryEnt> historyEnts) {
        refreshCrl.refreshComplete();
        refreshCrl.loadMoreComplete();
        if (!result) {
            return;
        }

        if (currentPage == 1) {
            // 刷新完成
            // 刷新成功，设置刷新时间
            currentPage++;
            refreshHeaderView.setHeadRefreshTime(DateParserUtils.parseDateToString(System.currentTimeMillis()));
            adapter.setData(historyEnts);
        } else {
            // 加载更多完成
            if (historyEnts == null || historyEnts.size() == 0) {
                showToast(R.string.no_more_data);
                return;
            }
            // 加载更多成功，页码加1
            currentPage++;
            adapter.addAll(historyEnts);
        }
    }

    @Override
    protected void onDestroy() {
        if (unbinder != null) {
            unbinder.unbind();
        }
        super.onDestroy();
    }

    @Override
    public void onPlay(HistoryEnt historyEnt) {
        FaceRecordEnt recordEnt = new FaceRecordEnt();
        recordEnt.id = historyEnt.did;
        recordEnt.title = historyEnt.title;
        recordEnt.videoUrl = historyEnt.video_url;
        recordEnt.preFinalUrl = historyEnt.pre_final_url;
        recordEnt.worth = historyEnt.worth;
        recordEnt.collectAmount = historyEnt.collect_amount;
        recordEnt.praiseAmount = historyEnt.praise_amount;

        VideoActivity.gotoVideoActivity(this, recordEnt);
    }

    @Override
    public void onShare(HistoryEnt historyEnt) {
        ShareUtils.share(this, historyEnt.title, historyEnt.video_url, historyEnt.pre_final_url, historyEnt.title);
    }
}
