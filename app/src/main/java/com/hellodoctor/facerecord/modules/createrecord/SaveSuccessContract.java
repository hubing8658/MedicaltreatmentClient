
package com.hellodoctor.facerecord.modules.createrecord;

import com.isoftstone.mis.mmsdk.common.architecture.presenter.IBasePresenter;
import com.isoftstone.mis.mmsdk.common.architecture.view.IBaseView;

/**
 * 生成视频成功界面Presenter接口和View接口定义。
 *
 * @author hubing
 * @version [1.0.0.0, 2017-05-03]
 */
class SaveSuccessContract {

    /**
     * 生成视频成功界面presenter接口
     *
     * @author hubing
     * @version [1.0.0.0, 2016-12-29]
     */
    interface ISaveSuccessPresenter extends IBasePresenter<ISaveSuccessView> {

    }

    /**
     * 生成视频成功界面view接口
     *
     * @author hubing
     * @version [1.0.0.0, 2016-12-29]
     */
    interface ISaveSuccessView extends IBaseView {

    }

}
