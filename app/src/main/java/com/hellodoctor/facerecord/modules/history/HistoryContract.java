
package com.hellodoctor.facerecord.modules.history;

import com.hellodoctor.facerecord.entity.HistoryEnt;
import com.isoftstone.mis.mmsdk.common.architecture.presenter.IBasePresenter;
import com.isoftstone.mis.mmsdk.common.architecture.view.IBaseLoadingView;

import java.util.List;

/**
 * 历史记录界面Presenter接口和View接口定义。
 *
 * @author hubing
 * @version [1.0.0.0, 2017-05-03]
 */
class HistoryContract {

    /**
     * 历史记录界面presenter接口
     *
     * @author hubing
     * @version [1.0.0.0, 2016-12-29]
     */
    interface IHistoryPresenter extends IBasePresenter<IHistoryView> {

        void loadHistories(int pageNo);

    }

    /**
     * 历史记录界面view接口
     *
     * @author hubing
     * @version [1.0.0.0, 2016-12-29]
     */
    interface IHistoryView extends IBaseLoadingView {

        /**
         * 加载完成
         * @param result 加载结果
         * @param historyEnts 历史记录列表
         */
        void loadHistoriesFinish(boolean result, List<HistoryEnt> historyEnts);

    }

}
