package com.hellodoctor.facerecord.modules.search;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.bumptech.glide.Glide;
import com.hellodoctor.facerecord.common.base.CommonBaseActivity;
import com.hellodoctor.facerecord.common.utils.ShareUtils;
import com.hellodoctor.facerecord.net.req.SearchReq;
import com.hellodoctor.facerecord.net.res.SearchRes;
import com.kelin.client.R;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.liaoinstan.springview.container.DefaultHeader;
import com.liaoinstan.springview.widget.SpringView;
import com.monty.library.http.BaseCallModel;
import com.monty.library.widget.CommonAdapter;
import com.monty.library.widget.ViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchDetailActivity extends CommonBaseActivity<SearchContract.ISearchPresenter> implements SearchContract.ISeasrchView {


    @BindView(R.id.spring_view)
    SpringView mSpringView;
    @BindView(R.id.list_view)
    ListView mListView;
    @BindView(R.id.ll_no_data)
    LinearLayout llNoData;

    private CommonAdapter<SearchRes> mAdapter;
    private List<SearchRes> resList;
    private SearchReq req;
    private final static String SEARCH_INPUT = "search_input";
    private String searchInput;


    public static void lunch(Activity activity, String search) {
        Intent intent = new Intent(activity, SearchDetailActivity.class);
        intent.putExtra(SEARCH_INPUT, search);
        activity.startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        searchInput = getIntent().getStringExtra(SEARCH_INPUT);
        setContentView(R.layout.activity_search);
    }

    @Override
    public SearchContract.ISearchPresenter createPresenter() {
        return new SearchPresenter(this);
    }

    @Override
    public void initViews() {
        ButterKnife.bind(this);
        setTitleBg(R.drawable.ic_seasrch_result_title_bg);
        mSpringView.setHeader(new DefaultHeader(this));
        mSpringView.setFooter(new DefaultHeader(this));
        mAdapter = new CommonAdapter<SearchRes>(this, resList, R.layout.search_list_item) {

            @Override
            public void convert(ViewHolder helper, final SearchRes item) {
                ImageView ivVideo = helper.getView(R.id.iv_img);
                ivVideo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        playVideo(item.getPreviewUrl());
                    }
                });
                Glide.with(SearchDetailActivity.this).load(item.getPreviewUrl()).into(ivVideo);
                helper.setText(R.id.tv_name, item.getTitle());
                helper.setText(R.id.tv_date, "发布于"+item.getReleaseTime());
                helper.setText(R.id.tv_star, "" + item.getPraiseAmount());
                helper.setText(R.id.tv_fire, "" + item.getCollectAmount());
                helper.getView(R.id.iv_share).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        share(item);
                    }
                });
            }
        };
        mListView.setAdapter(mAdapter);
    }

    private void share(SearchRes res) {
        ShareUtils.share(this, res.getTitle(), res.getVideoUrl(), res.getPreFinalUrl(), res.getTitle());
    }

    private void playVideo(String url) {
        Uri uri = Uri.parse(url);
        // 调用系统自带的播放器来播放流媒体视频
        Intent intent = new Intent(Intent.ACTION_VIEW);
        Log.v("URI:::::::::", uri.toString());
        intent.setDataAndType(uri, "video/mp4");
        startActivity(intent);
    }

    public void initListener() {
        mSpringView.setListener(new SpringView.OnFreshListener() {
            @Override
            public void onRefresh() {
                refresh();
            }

            @Override
            public void onLoadmore() {
                loadMore();
            }
        });

    }

    public void loadData() {
        req = new SearchReq();
        req.setToken(MyselfInfo.getLoginUser().getToken());
        req.setPageNo(1);
        req.setPageSize(8);
        req.setKeyword(searchInput);
       /* if (BuildConfig.DEBUG) {
            List<SearchRes> list = new ArrayList<>();
            SearchRes res = new SearchRes();
            res.setReleaseTime("2017-12-12");
            res.setPraiseAmount(222);
            res.setVideoUrl("http://123.207.9.75:8080/upload-file/user/photo/80d900a6a39645e28bda46d55c6177e3.mp4");
            res.setCollectAmount(122);
            res.setPreviewUrl("http://www.fzlqqqm.com/uploads/allimg/20150806/201508062253342606.jpg");
            list.add(res);
            list.add(res);
            mAdapter.update(list);
            showNoDataLayout();
        }else {*/
            presenter.search(req);
//        }
    }

    @Override
    public void success(BaseCallModel<List<SearchRes>> callBack) {
        mSpringView.onFinishFreshAndLoad();
        if(callBack.code == 200){
            if(req.getPageNo() == 1) {
                resList = callBack.data;
            }else {
                if(resList == null) {
                    resList = new ArrayList<>();
                }
                resList.addAll(callBack.data);
            }
            mAdapter.update(resList);
        }else {
            error(1, TextUtils.isEmpty(callBack.msg) ? getNoDataToastMsg() : callBack.msg);
        }
        showNoDataLayout();
    }

    private void showNoDataLayout() {
        if(resList==null||resList.size()==0) {
            llNoData.setVisibility(View.VISIBLE);
        }else {
            llNoData.setVisibility(View.GONE);
        }
    }

    private String getNoDataToastMsg() {
        if(req.getPageNo() == 1) {
            return "没有获取到数据";
        }else {
            return "没有获取到更多数据";
        }
    }

    @Override
    public void error(int code, String msg) {
        if (!TextUtils.isEmpty(msg)) {
            showToast(msg);
        }
        if(code == 1) {
            mSpringView.onFinishFreshAndLoad();
            showNoDataLayout();
        }
    }

    @Override
    public void refresh() {
        req.setPageNo(1);
        presenter.search(req);
    }

    @Override
    public void loadMore() {
        req.setPageNo(req.getPageNo() + 1);
        presenter.search(req);
    }
}
