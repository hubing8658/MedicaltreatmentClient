
package com.hellodoctor.facerecord.modules.takephoto;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.stetho.common.LogUtil;
import com.hellodoctor.facerecord.common.base.CommonBaseActivity;
import com.hellodoctor.facerecord.common.constants.LogTag;
import com.hellodoctor.facerecord.common.constants.PhotoConstants;
import com.hellodoctor.facerecord.common.intf.dialog.ConfirmDialog;
import com.hellodoctor.facerecord.common.utils.DateUtils;
import com.hellodoctor.facerecord.modules.takephoto.TakePhotoContract.ITakePhotoPresenter;
import com.hellodoctor.facerecord.modules.takephoto.TakePhotoContract.ITakePhotoView;
import com.isoftstone.mis.mmsdk.common.utils.log.MMLog;
import com.kelin.client.R;
import com.yanzhenjie.permission.AndPermission;
import com.yanzhenjie.permission.PermissionListener;
import com.yanzhenjie.permission.Rationale;
import com.yanzhenjie.permission.RationaleListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * 每日一拍
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-13]
 */
public class TakePhotoActivity extends CommonBaseActivity<ITakePhotoPresenter> implements ITakePhotoView {

    Unbinder unbinder;

    /** 拍照 */
    @BindView(R.id.iv_take_photo)
    ImageView takePhotoIv;

    /** 拍照布局 */
    @BindView(R.id.rl_take_photo)
    RelativeLayout takePhotoRl;
    
    /** 拍照完成预览布局 */
    @BindView(R.id.rl_preview)
    RelativeLayout previewRl;
    
    /** 拍照日期 */
    @BindView(R.id.tv_photo_date)
    TextView photoDateTv;

    /** 根布局 */
    @BindView(R.id.rl)
    RelativeLayout rlParent;

    /** 发布 */
    @BindView(R.id.iv_preview_publish)
    Button previewPublishIv;

    /** 拍照之后本地存放地址 */
    private String photoPath;

    private boolean isShowDialog = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.take_photo_activity);
    }

    @Override
    public void initViews() {
        unbinder = ButterKnife.bind(this);
        setTitleBg(R.drawable.take_photo_title);
    }

    private void openCamare() {
        // 摄像头展示区域
        if (!(rlParent.getChildAt(0) instanceof SurfaceView)) {
            final SurfaceView surfaceView = new SurfaceView(this);
            rlParent.addView(surfaceView, 0);
            presenter.startPreview(surfaceView);
            surfaceView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    surfaceView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    int[] locs = new int[2];
                    surfaceView.getLocationOnScreen(locs);
                    MMLog.dt(LogTag.TAKE_PHOTO, "surfaceView x=" + locs[0] + ",y=" + locs[1]);

                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                    params.topMargin = - locs[1];
                    surfaceView.setLayoutParams(params);
                }
            });
            rlParent.post(new Runnable() {
                @Override
                public void run() {
                    MMLog.dt(LogTag.TAKE_PHOTO, "surfaceView width=" + surfaceView.getWidth() + ",height=" + surfaceView.getHeight());
                }
            });
        }
    }

    @Override
    public void initListener() {
    }

    @Override
    protected void onResume() {
        super.onResume();
        initCamera();
    }

    private void initCamera() {
        if(!isShowDialog) {
            List<String> permissions = new ArrayList<>(3);
            if (!AndPermission.hasPermission(this, Manifest.permission.CAMERA)) {
                permissions.add(Manifest.permission.CAMERA);
            }
            if (!AndPermission.hasPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
            if (!AndPermission.hasPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }
            if (permissions.size() > 0) {
                AndPermission
                        .with(this)
                        .permission(permissions.toArray(new String[]{}))
                        .requestCode(200)
                        .rationale(new RationaleListener() {
                            @Override
                            public void showRequestPermissionRationale(int requestCode, Rationale rationale) {
                                LogUtil.d("showRequestPermissionRationale code = " + requestCode);
                                showPermissionDenyDialog();
                            }
                        })
                        .callback(listener)
                        .start();
            } else {
                onTitleRightClick(null);
                openCamare();
            }
        }
    }

    @Override
    public void loadData() {
        // 未使用
    }

    private PermissionListener listener = new PermissionListener() {
        @Override
        public void onSucceed(int requestCode, List<String> grantedPermissions) {
            // 权限申请成功回调。

            // 这里的requestCode就是申请时设置的requestCode。
            // 和onActivityResult()的requestCode一样，用来区分多个不同的请求。
            if(requestCode == 200) {
                // TODO ...
                openCamare();
            }
        }

        @Override
        public void onFailed(int requestCode, List<String> deniedPermissions) {
            // 权限申请失败回调。
            if(requestCode == 200) {
                // TODO ...
                showPermissionDenyDialog();
            }
        }
    };

    private void showPermissionDenyDialog() {
        isShowDialog = true;
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setCancelable(false);
        dialog.setTitle("提示！").setMessage("拍照需要使用相应的权限，请予以通过，否则将不能使用这个功能")
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        isShowDialog = false;
                        startActivity(getAppDetailSettingIntent());
                    }
                })
                .setNegativeButton("退出", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                        isShowDialog = false;
                    }
                })
                .create().show();
    }

    /**
     * 获取应用详情页面intent
     *
     * @return
     */
    private Intent getAppDetailSettingIntent() {
        Intent localIntent = new Intent();
        localIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (Build.VERSION.SDK_INT >= 9) {
            localIntent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            localIntent.setData(Uri.fromParts("package", getPackageName(), null));
        } else if (Build.VERSION.SDK_INT <= 8) {
            localIntent.setAction(Intent.ACTION_VIEW);
            localIntent.setClassName("com.android.settings", "com.android.settings.InstalledAppDetails");
            localIntent.putExtra("com.android.settings.ApplicationPkgName", getPackageName());
        }
        return localIntent;
    }

    @Override
    public ITakePhotoPresenter createPresenter() {
        return new TakePhotoPresenter(this, this);
    }

    @OnClick(R.id.iv_take_photo)
    public void takePhoto(View v) {
        presenter.takePhoto();
    }

    /**
     * 发布
     * @param v
     */
    @OnClick(R.id.iv_preview_publish)
    public void publish(View v) {
        presenter.publishPhoto(photoPath);
    }

    @Override
    public void publishSuccess() {
        Intent intent = new Intent(this, PublishSuccessActivity.class);
        startActivity(intent);
    }

    @Override
    public void publishFailure() {
        showToast(R.string.publish_photo_failure);
    }

    @Override
    public void takePhotoSuccess(String photoPath) {
        this.photoPath = photoPath;
        setRightTitle(R.string.rephotograph);

        takePhotoRl.setVisibility(View.GONE);
        previewRl.setVisibility(View.VISIBLE);
        previewPublishIv.setVisibility(View.GONE);

        photoDateTv.setText(DateUtils.getCurrDate(PhotoConstants.PHOTO_PREVIEW_PATTERN));
    }

    @Override
    public void canPublish() {
        previewPublishIv.setVisibility(View.VISIBLE);
    }

    @Override
    public void tooFar() {
        ConfirmDialog dialog = new ConfirmDialog(this);
        dialog.setMessage(R.string.too_far);
        dialog.setCancelVisiblity(false);
        dialog.setConfirmText(R.string.try_agin);
        dialog.setOnConfirmListener(new ConfirmDialog.OnConfirmListener() {
            @Override
            public void onConfirm(ConfirmDialog dialog) {
                onTitleRightClick(null);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void tooClose() {
        ConfirmDialog dialog = new ConfirmDialog(this);
        dialog.setMessage(R.string.too_close);
        dialog.setCancelVisiblity(false);
        dialog.setConfirmText(R.string.try_agin);
        dialog.setOnConfirmListener(new ConfirmDialog.OnConfirmListener() {
            @Override
            public void onConfirm(ConfirmDialog dialog) {
                onTitleRightClick(null);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void faceNotFound() {
        ConfirmDialog dialog = new ConfirmDialog(this);
        dialog.setMessage(R.string.face_not_found);
        dialog.setCancelVisiblity(false);
        dialog.setConfirmText(R.string.try_agin);
        dialog.setOnConfirmListener(new ConfirmDialog.OnConfirmListener() {
            @Override
            public void onConfirm(ConfirmDialog dialog) {
                onTitleRightClick(null);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    protected void onTitleRightClick(View v) {
        this.photoPath = null;
        setRightTitle("");
        presenter.rephotograph();
        takePhotoRl.setVisibility(View.VISIBLE);
        previewRl.setVisibility(View.GONE);
    }
}
