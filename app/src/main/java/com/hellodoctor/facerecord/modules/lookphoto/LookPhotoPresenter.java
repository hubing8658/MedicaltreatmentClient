
package com.hellodoctor.facerecord.modules.lookphoto;

import android.content.Context;

import com.hellodoctor.facerecord.common.constants.LogTag;
import com.hellodoctor.facerecord.entity.PictureEnt;
import com.hellodoctor.facerecord.modules.lookphoto.LookPhotoContract.ILookPhotoPresenter;
import com.hellodoctor.facerecord.modules.lookphoto.LookPhotoContract.ILookPhotoView;
import com.hellodoctor.facerecord.net.RecordApiService;
import com.isoftstone.mis.mmsdk.common.architecture.presenter.BasePresenter;
import com.isoftstone.mis.mmsdk.common.utils.log.MMLog;
import com.kelin.client.R;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;

import retrofit2.Call;
import retrofit2.Response;

/**
 * <一句话功能简述> <功能详细描述>
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-25]
 */
public class LookPhotoPresenter extends BasePresenter<ILookPhotoView> implements ILookPhotoPresenter{

    public LookPhotoPresenter(Context context, ILookPhotoView view) {
        super(context, view);
    }

    @Override
    public void deletePhoto(PictureEnt pictureEnt) {
        getView().showLoading(context.getString(R.string.deleting_wait));
        if (pictureEnt == null) {
            getView().deleteFailure();
            return;
        }

        RecordApiService service = RetrofitHelper.getInstance().createService(RecordApiService.class);
        String token = MyselfInfo.getLoginUser().getToken();

        Call<BaseCallModel> deleteImage = service.deleteImage(pictureEnt.id, token);
        deleteImage.enqueue(new BaseCallBack<BaseCallModel>() {
            @Override
            public void onSuccess(Response<BaseCallModel> response) {
                getView().hideLoading();
                if (response == null || response.code() != 200) {
                    getView().deleteFailure();
                    return;
                }
                BaseCallModel body = response.body();
                if (body == null) {
                    getView().deleteFailure();
                    return;
                }
                getView().deleteSuccess();
            }

            @Override
            public void onFailure(String message) {
                MMLog.et(LogTag.TAKE_PHOTO, "删除照片失败:" + message);
                getView().hideLoading();
                getView().deleteFailure();
            }
        });
    }

    @Override
    protected void onRelease() {
    }
}
