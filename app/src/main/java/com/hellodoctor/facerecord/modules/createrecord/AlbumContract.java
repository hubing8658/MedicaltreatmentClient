
package com.hellodoctor.facerecord.modules.createrecord;

import com.hellodoctor.facerecord.entity.PictureEnt;
import com.isoftstone.mis.mmsdk.common.architecture.presenter.IBasePresenter;
import com.isoftstone.mis.mmsdk.common.architecture.view.IBaseView;

import java.util.List;

/**
 * 相册界面Presenter接口和View接口定义。
 *
 * @author hubing
 * @version [1.0.0.0, 2017-05-03]
 */
class AlbumContract {

    /**
     * 相册界面presenter接口
     *
     * @author hubing
     * @version [1.0.0.0, 2016-12-29]
     */
    interface IAlbumPresenter extends IBasePresenter<IAlbumView> {

        /**
         * 获取我的相册
         * @param pageNo 页码
         */
        void getAlbums(int pageNo);

    }

    /**
     * 相册界面view接口
     *
     * @author hubing
     * @version [1.0.0.0, 2016-12-29]
     */
    interface IAlbumView extends IBaseView {

        /**
         * 加载完成
         * @param result 加载结果
         * @param pictureEnts 相册列表
         */
        void getAlbumsFinish(boolean result, List<PictureEnt> pictureEnts);

    }

}
