package com.hellodoctor.facerecord.modules.videoplayer;

import android.content.Context;

import com.hellodoctor.facerecord.biz.RecordOperationBiz;
import com.hellodoctor.facerecord.common.constants.LogTag;
import com.hellodoctor.facerecord.entity.FaceRecordEnt;
import com.hellodoctor.facerecord.modules.videoplayer.RecordVideoContract.IRecordVideoPresenter;
import com.hellodoctor.facerecord.modules.videoplayer.RecordVideoContract.IRecordVideoView;
import com.isoftstone.mis.mmsdk.common.architecture.presenter.BasePresenter;
import com.isoftstone.mis.mmsdk.common.utils.log.MMLog;
import com.kelin.client.gui.login.utils.MyselfInfo;

/**
 * <一句话功能简述>
 * <功能详细描述>
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-29]
 */
public class RecordVideoPresenter extends BasePresenter<IRecordVideoView> implements IRecordVideoPresenter {

    public RecordVideoPresenter(Context context, IRecordVideoView view) {
        super(context, view);
    }

    @Override
    public void addFavorite(FaceRecordEnt recordEnt) {
        String token = MyselfInfo.getLoginUser().getToken();
        final FaceRecordEnt resultRecordEnt = recordEnt.clone();
        new RecordOperationBiz().addFavorite(recordEnt.id, token, new RecordOperationBiz.OperationResultListener() {
            @Override
            public void onResult(boolean isSuccess) {
                if (isSuccess) {
                    resultRecordEnt.isCollect = 1;
                    resultRecordEnt.collectAmount = resultRecordEnt.collectAmount + 1;
                    getView().opFavoriteSuccess(resultRecordEnt);
                } else {
                    getView().operationFailure();
                }
                MMLog.it(LogTag.RANKING, "添加收藏结果：" + isSuccess);
            }
        });
    }

    @Override
    public void cancelFavorite(FaceRecordEnt recordEnt) {
        String token = MyselfInfo.getLoginUser().getToken();
        final FaceRecordEnt resultRecordEnt = recordEnt.clone();
        new RecordOperationBiz().deleteFavorite(recordEnt.id, token, new RecordOperationBiz.OperationResultListener() {
            @Override
            public void onResult(boolean isSuccess) {
                if (isSuccess) {
                    resultRecordEnt.isCollect = 0;
                    resultRecordEnt.collectAmount = resultRecordEnt.collectAmount - 1;
                    getView().opFavoriteSuccess(resultRecordEnt);
                } else {
                    getView().operationFailure();
                }
                MMLog.it(LogTag.RANKING, "取消收藏结果：" + isSuccess);
            }
        });
    }

    @Override
    public void addPraise(FaceRecordEnt recordEnt) {
        String token = MyselfInfo.getLoginUser().getToken();
        final FaceRecordEnt resultRecordEnt = recordEnt.clone();
        new RecordOperationBiz().addPraise(recordEnt.id, token, new RecordOperationBiz.OperationResultListener() {
            @Override
            public void onResult(boolean isSuccess) {
                if (isSuccess) {
                    resultRecordEnt.isPraise = 1;
                    resultRecordEnt.praiseAmount = resultRecordEnt.praiseAmount + 1;
                    getView().opPraiseSuccess(resultRecordEnt);
                } else {
                    getView().operationFailure();
                }
                MMLog.it(LogTag.RANKING, "点赞结果：" + isSuccess);
            }
        });
    }

    @Override
    public void cancelPraise(FaceRecordEnt recordEnt) {
        String token = MyselfInfo.getLoginUser().getToken();
        final FaceRecordEnt resultRecordEnt = recordEnt.clone();
        new RecordOperationBiz().deletePraise(recordEnt.id, token, new RecordOperationBiz.OperationResultListener() {
            @Override
            public void onResult(boolean isSuccess) {
                if (isSuccess) {
                    resultRecordEnt.isPraise = 0;
                    resultRecordEnt.praiseAmount = resultRecordEnt.praiseAmount - 1;
                    getView().opPraiseSuccess(resultRecordEnt);
                } else {
                    getView().operationFailure();
                }
                MMLog.it(LogTag.RANKING, "取消点赞结果：" + isSuccess);
            }
        });
    }

    @Override
    protected void onRelease() {

    }

}
