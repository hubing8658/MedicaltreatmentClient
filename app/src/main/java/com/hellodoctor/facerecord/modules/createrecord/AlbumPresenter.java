package com.hellodoctor.facerecord.modules.createrecord;

import android.content.Context;

import com.hellodoctor.facerecord.common.constants.AppCommonCons;
import com.hellodoctor.facerecord.common.constants.LogTag;
import com.hellodoctor.facerecord.common.constants.PhotoConstants;
import com.hellodoctor.facerecord.common.utils.DateUtils;
import com.hellodoctor.facerecord.entity.PictureEnt;
import com.hellodoctor.facerecord.modules.createrecord.AlbumContract.IAlbumPresenter;
import com.hellodoctor.facerecord.modules.createrecord.AlbumContract.IAlbumView;
import com.hellodoctor.facerecord.net.RecordApiService;
import com.isoftstone.mis.mmsdk.common.architecture.presenter.BasePresenter;
import com.isoftstone.mis.mmsdk.common.intf.UiTask;
import com.isoftstone.mis.mmsdk.common.utils.log.MMLog;
import com.kelin.client.gui.login.utils.MyselfInfo;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;

/**
 * <一句话功能简述>
 * <功能详细描述>
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-21]
 */
public class AlbumPresenter extends BasePresenter<IAlbumView> implements IAlbumPresenter {

    public AlbumPresenter(Context context, IAlbumView view) {
        super(context, view);
    }

    @Override
    public void getAlbums(int pageNo) {
        new UiTask<Integer, Void, ArrayList<PictureEnt>>() {
            @Override
            protected ArrayList<PictureEnt> doInBackground(Integer... integers) {
                try {
                    int pageNo = integers[0];
                    RecordApiService service = RetrofitHelper.getInstance().createService(RecordApiService.class);
                    String token = MyselfInfo.getLoginUser().getToken();

                    Call<BaseCallModel<ArrayList<PictureEnt>>> serviceRankings = service.getAlbums(pageNo,
                            AppCommonCons.PAGE_SIZE, token);
                    Response<BaseCallModel<ArrayList<PictureEnt>>> response = serviceRankings.execute();
                    if (response == null || response.code() != 200) {
                        return null;
                    }
                    BaseCallModel<ArrayList<PictureEnt>> body = response.body();
                    if (body == null) {
                        return null;
                    }
                    
                    // 解析展示时间，加快UI显示速度
                    ArrayList<PictureEnt> pictureEnts = body.data;
                    if (pictureEnts != null) {
                        for (PictureEnt pictureEnt : pictureEnts) {
                            long time = DateUtils.parseDate(PhotoConstants.PHOTOS_SERVER_TITME_PATTERN, pictureEnt.photoTime);
                            pictureEnt.photoTimeMs = time;
                            pictureEnt.yearMonth = DateUtils.format(PhotoConstants.PHOTOS_MONTH_PATTERN, time);
                            pictureEnt.yearMonthDay = DateUtils.format(PhotoConstants.PHOTOS_DATE_PATTERN, time);
                        }
                    }
                    return pictureEnts;
                } catch (Exception e) {
                    MMLog.et(LogTag.COMPOSE_VIDEO, e, "获取相册失败");
                }
                return null;
            }

            @Override
            protected void onPostExecute(ArrayList<PictureEnt> pictureEnts) {
                getView().getAlbumsFinish(pictureEnts != null, pictureEnts);
            }
        }.start(pageNo);
    }

    @Override
    protected void onRelease() {

    }
}
