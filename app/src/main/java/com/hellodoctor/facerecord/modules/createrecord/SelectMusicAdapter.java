
package com.hellodoctor.facerecord.modules.createrecord;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hellodoctor.facerecord.entity.MusicEnt;
import com.isoftstone.mis.mmsdk.common.intf.GeneralAdapterV2;
import com.kelin.client.R;

/**
 * <一句话功能简述> <功能详细描述>
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-21]
 */
public class SelectMusicAdapter extends GeneralAdapterV2<MusicEnt> {

    /** 选中的音乐 */
    private MusicEnt selectMusic;

    /**
     * 构造函数
     *
     * @param ctx  上下文
     */
    public SelectMusicAdapter(Context ctx) {
        super(ctx, null);
    }

    @Override
    public int getItemLayoutId(int itemViewType) {
        return R.layout.select_music_item;
    }

    @Override
    public void convert(ViewHolder holder, final MusicEnt item, int position, int itemViewType) {
        // 音乐名称
        TextView musicNameTv = holder.getView(R.id.tv_music_name);
        musicNameTv.setText(item.musicName);

        // 选中状态
        ImageView musicSelectedIv = holder.getView(R.id.iv_music_selected);

        Resources resources = getContext().getResources();
        if (selectMusic != null && selectMusic.equals(item)) {
            musicNameTv.setTextColor(resources.getColor(R.color.color_f5b8cf));
            musicSelectedIv.setVisibility(View.VISIBLE);
        } else {
            musicSelectedIv.setVisibility(View.GONE);
            musicNameTv.setTextColor(resources.getColor(R.color.white));
        }

        holder.getConvertView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectMusic = item;
                notifyDataSetChanged();
            }
        });
    }

    public MusicEnt getSelectMusic() {
        return selectMusic;
    }
}
