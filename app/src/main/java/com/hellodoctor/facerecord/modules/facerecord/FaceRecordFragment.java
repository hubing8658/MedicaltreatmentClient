
package com.hellodoctor.facerecord.modules.facerecord;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.GridView;
import android.widget.RelativeLayout;

import com.hellodoctor.facerecord.common.base.WebViewActivity;
import com.hellodoctor.facerecord.common.entity.BaseEntity;
import com.hellodoctor.facerecord.common.intf.DefaultTransformer;
import com.hellodoctor.facerecord.entity.BannerEnt;
import com.hellodoctor.facerecord.entity.DocumentaryEnt;
import com.hellodoctor.facerecord.entity.FaceRecordEnt;
import com.hellodoctor.facerecord.entity.RecordRankingEnt;
import com.hellodoctor.facerecord.modules.createrecord.AlbumActivity;
import com.hellodoctor.facerecord.modules.facerecord.FaceRecordContract.IFaceRecordPresenter;
import com.hellodoctor.facerecord.modules.myrecord.MyRecordActivity;
import com.hellodoctor.facerecord.modules.search.SearchInputActivity;
import com.hellodoctor.facerecord.modules.takephoto.TakePhotoActivity;
import com.hellodoctor.facerecord.modules.videoplayer.VideoActivity;
import com.isoftstone.mis.mmsdk.common.architecture.ui.BaseFragmentV4;
import com.isoftstone.mis.mmsdk.common.component.convenientbanner.MMConvenientBanner;
import com.isoftstone.mis.mmsdk.common.component.convenientbanner.holder.MMCBViewHolderCreator;
import com.isoftstone.mis.mmsdk.common.component.convenientbanner.listener.MMOnCBItemClickListener;
import com.isoftstone.mis.mmsdk.common.intf.UITemplate;
import com.kelin.client.R;
import com.kelin.client.widget.BaseTitleBar;

import java.util.List;

/**
 * 颜值记录界面
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-06]
 */
public class FaceRecordFragment extends BaseFragmentV4<IFaceRecordPresenter>
        implements View.OnClickListener, UITemplate, FaceRankedAdapter.OnOperationListener, FaceRecordContract.IFaceRecordView {

    /** 头像单击事件 */
    private OnHeadClickListener mOnHeadClickListener;

//    /** 头像 */
//    private ImageView headIv;

    /** 搜索 */
    private RelativeLayout searchRl;
    /** 标题栏 */
    private BaseTitleBar titleBar;



    /** 每日一拍 */
    private RelativeLayout onedayOnePhotoRl;
    /** 生成记录片 */
    private RelativeLayout createRecordVideoRl;
    /** 我的记录片 */
    private RelativeLayout faceRecordRl;
    /** 轮播图 */
    private MMConvenientBanner banner;

    /** 记录榜列表 */
    private GridView recordRankedGv;

    private FaceRankedAdapter rankedAdapter;

    public static FaceRecordFragment newInstance() {
        return new FaceRecordFragment();
    }

    @Override
    protected int getRootViewResid() {
        return R.layout.face_record_fragment;
    }

    @Override
    public IFaceRecordPresenter createPresenter() {
        return new FaceRecordPresenter(getActivity(), this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
        initListener();
        loadData();
    }

    @Override
    public void initViews() {
//        headIv = findViewById(R.id.iv_face_record_head);
        titleBar = findViewById(R.id.titleBar);
        titleBar.showCenterText("color record.","颜值记录");
        titleBar.setCenterLeftTextSize(26);
        titleBar.setBackBtnImg(R.drawable.home_icon_personal);
        titleBar.setBackBtnClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnHeadClickListener != null) {
                    mOnHeadClickListener.onClick();
                }
            }
        });

        searchRl = findViewById(R.id.rl_face_record_search);
        onedayOnePhotoRl = findViewById(R.id.rl_oneday_one_photo);
        createRecordVideoRl = findViewById(R.id.rl_create_record_video);
        faceRecordRl = findViewById(R.id.rl_my_face_record);

        banner = findViewById(R.id.cb_main_banner);
        // 设置两个点图片作为翻页指示器，不设置则没有指示器，可以根据自己需求自行配合自己的指示器,不需要圆点指示器可用不设【必需】
        banner.setPageIndicator(new int[] {
                R.drawable.banner_page_indicator, R.drawable.banner_page_indicator_focused
        });
        // 设置指示器的方向
        banner.setPageIndicatorAlign(MMConvenientBanner.PageIndicatorAlign.CENTER_HORIZONTAL);
        // 设置底部指示器是否可见 默认是true
        banner.setPointViewVisible(true);
        // 设置自定义翻页动画效果 默认是DefaultTransformer
        banner.setPageTransformer(new DefaultTransformer());

        recordRankedGv = findViewById(R.id.gv_record_ranked);
        rankedAdapter = new FaceRankedAdapter(getActivity());
        recordRankedGv.setAdapter(rankedAdapter);
    }

    @Override
    public void initListener() {
//        headIv.setOnClickListener(this);

        searchRl.setOnClickListener(this);
        onedayOnePhotoRl.setOnClickListener(this);
        createRecordVideoRl.setOnClickListener(this);
        faceRecordRl.setOnClickListener(this);

        rankedAdapter.setOnOperationListener(this);

        // 设置轮播图监听器
        banner.setOnCBItemClickListener(new MMOnCBItemClickListener() {
            @Override
            public void onItemClick(int position) {
                BaseEntity baseEnt = (BaseEntity) banner.getItem(position);
                if (baseEnt instanceof BannerEnt) {
                    BannerEnt bannerEnt = (BannerEnt) baseEnt;
                    WebViewActivity.lunch(getActivity(), null, null, bannerEnt.content, R.drawable.activities);
                } else if (baseEnt instanceof DocumentaryEnt) {
                    DocumentaryEnt documentaryEnt = (DocumentaryEnt) baseEnt;
                    FaceRecordEnt recordEnt = new FaceRecordEnt();
                    recordEnt.id = documentaryEnt.id;
                    recordEnt.title = documentaryEnt.title;
                    recordEnt.videoUrl = documentaryEnt.videoUrl;
                    recordEnt.preFinalUrl = documentaryEnt.preFinalUrl;
                    recordEnt.worth = documentaryEnt.worth;
                    recordEnt.collectAmount = documentaryEnt.collectAmount;
                    recordEnt.praiseAmount = documentaryEnt.praiseAmount;
                    recordEnt.isPraise = documentaryEnt.isPraise;
                    recordEnt.isCollect = documentaryEnt.isCollect;

                    VideoActivity.gotoVideoActivity(getActivity(), recordEnt);
                }
            }
        });
    }

    @Override
    public void loadData() {
        presenter.getBanners();
    }

    // 开始自动翻页
    @Override
    public void onResume() {
        super.onResume();
        // 开始自动翻页
        banner.startTurning(5000);
        presenter.getRankings(1);
    }

    // 停止自动翻页
    @Override
    public void onPause() {
        super.onPause();
        // 停止翻页
        banner.stopTurning();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /*case R.id.iv_face_record_head:
                if (mOnHeadClickListener != null) {
                    mOnHeadClickListener.onClick();
                }
                break;*/
            case R.id.rl_face_record_search:
                startActivity(new Intent(getActivity(), SearchInputActivity.class));
                break;
            case R.id.rl_oneday_one_photo:
                Intent takePhotoIntent = new Intent(getActivity(), TakePhotoActivity.class);
                startActivity(takePhotoIntent);
                break;
            case R.id.rl_create_record_video:
                startActivity(new Intent(getActivity(), AlbumActivity.class));
                break;
            case R.id.rl_my_face_record:
                Intent intent = new Intent(getActivity(), MyRecordActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onLookFaceRecord(RecordRankingEnt rankingEnt) {
        if (rankingEnt == null) {
            return;
        }
        FaceRecordEnt recordEnt = new FaceRecordEnt();
        recordEnt.id = rankingEnt.id;
        recordEnt.title = rankingEnt.title;
        recordEnt.videoUrl = rankingEnt.videoUrl;
        recordEnt.preFinalUrl = rankingEnt.preFinalUrl;
        recordEnt.worth = rankingEnt.worth;
        recordEnt.collectAmount = rankingEnt.collectAmount;
        recordEnt.praiseAmount = rankingEnt.praiseAmount;
        recordEnt.isPraise = rankingEnt.isPraise;
        recordEnt.isCollect = rankingEnt.isCollect;

        VideoActivity.gotoVideoActivity(getActivity(), recordEnt);
    }

    public void setOnHeadClickListener(OnHeadClickListener listener) {
        this.mOnHeadClickListener = listener;
    }

    public interface OnHeadClickListener {

        void onClick();

    }

    @Override
    public void getBannerSuccess(List<BaseEntity> banners) {
        if (banners == null || banners.size() == 0) {
            return;
        }
        banner.setPages(new MMCBViewHolderCreator<NetworkImageHolderView>() {
            @Override
            public NetworkImageHolderView createHolder() {
                return new NetworkImageHolderView();
            }
        }, banners);
    }

    @Override
    public void getRankingSuccess(List<RecordRankingEnt> recordRankingEnts) {
        rankedAdapter.setData(recordRankingEnts);
    }

    @Override
    public void onFavorite(RecordRankingEnt rankingEnt) {
        if (rankingEnt.isCollect == 0) {
            presenter.addFavorite(rankingEnt);
        } else {
            presenter.cancelFavorite(rankingEnt);
        }
    }

    @Override
    public void opFavoriteSuccess(RecordRankingEnt rankingEnt) {
        showToast(R.string.operation_success);
        updateRankingList(rankingEnt);
    }

    @Override
    public void onPraise(RecordRankingEnt rankingEnt) {
        if (rankingEnt.isPraise == 0) {
            presenter.addPraise(rankingEnt);
        } else {
            presenter.cancelPraise(rankingEnt);
        }
    }

    @Override
    public void opPraiseSuccess(RecordRankingEnt rankingEnt) {
        showToast(R.string.operation_success);
        updateRankingList(rankingEnt);
    }

    /**
     * 更新排名列表
     * @param rankingEnt
     */
    private void updateRankingList(RecordRankingEnt rankingEnt) {
        int position = rankedAdapter.getPosition(rankingEnt);
        if (position >= 0 && position < rankedAdapter.getCount()) {
            rankedAdapter.remove(rankingEnt);
            rankedAdapter.insert(rankingEnt, position);
        }
    }

    @Override
    public void operationFailure() {
        showToast(R.string.operation_failure);
    }
}
