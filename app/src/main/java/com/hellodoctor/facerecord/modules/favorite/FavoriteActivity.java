
package com.hellodoctor.facerecord.modules.favorite;

import android.os.Bundle;
import android.widget.ListView;

import com.canyinghao.canrefresh.CanRefreshLayout;
import com.hellodoctor.facerecord.common.base.CommonBaseActivity;
import com.hellodoctor.facerecord.common.utils.DateParserUtils;
import com.hellodoctor.facerecord.common.utils.ShareUtils;
import com.hellodoctor.facerecord.entity.FaceRecordEnt;
import com.hellodoctor.facerecord.entity.FavoriteEnt;
import com.hellodoctor.facerecord.modules.favorite.FavoriteContract.IFavoritePresenter;
import com.hellodoctor.facerecord.modules.favorite.FavoriteContract.IFavoriteView;
import com.hellodoctor.facerecord.modules.videoplayer.VideoActivity;
import com.isoftstone.mis.mmsdk.common.widget.refreshview.MMClassicRefreshView;
import com.kelin.client.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * <一句话功能简述> <功能详细描述>
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-24]
 */
public class FavoriteActivity extends CommonBaseActivity<IFavoritePresenter> implements IFavoriteView,
        CanRefreshLayout.OnRefreshListener, CanRefreshLayout.OnLoadMoreListener, FavoriteAdapter.OnOperationListener {

    Unbinder unbinder;

    @BindView(R.id.can_content_view)
    ListView favoritesLv;

    @BindView(R.id.crl_refresh)
    CanRefreshLayout refreshCrl;

    @BindView(R.id.can_refresh_header)
    MMClassicRefreshView refreshHeaderView;

    private FavoriteAdapter adapter;

    /** 当前加载的页码数 */
    private int currentPage = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.favorite_activity);
    }

    @Override
    public IFavoritePresenter createPresenter() {
        return new FavoritePresenter(this, this);
    }

    @Override
    public void initViews() {
        unbinder = ButterKnife.bind(this);
        setTitleBg(R.drawable.favorite_title);

        adapter = new FavoriteAdapter(this);
        favoritesLv.setAdapter(adapter);
    }

    @Override
    public void initListener() {
        // 设置上拉、下拉监听
        refreshCrl.setOnRefreshListener(this);
        refreshCrl.setOnLoadMoreListener(this);
        adapter.setOnOperationListener(this);
    }

    @Override
    public void loadData() {
        presenter.loadFavorites(currentPage);
    }

    @Override
    public void onRefresh() {
        currentPage = 1;
        presenter.loadFavorites(currentPage);
    }

    @Override
    public void onLoadMore() {
        presenter.loadFavorites(currentPage);
    }

    @Override
    public void loadFavoritesFinish(boolean result, List<FavoriteEnt> favoriteEnts) {
        refreshCrl.refreshComplete();
        refreshCrl.loadMoreComplete();
        if (!result) {
            return;
        }

        if (currentPage == 1) {
            // 刷新完成
            // 刷新成功，设置刷新时间
            currentPage++;
            refreshHeaderView.setHeadRefreshTime(DateParserUtils.parseDateToString(System.currentTimeMillis()));
            adapter.setData(favoriteEnts);
        } else {
            // 加载更多完成
            if (favoriteEnts == null || favoriteEnts.size() == 0) {
                showToast(R.string.no_more_data);
                return;
            }
            // 加载更多成功，页码加1
            currentPage++;
            adapter.addAll(favoriteEnts);
        }
    }

    @Override
    protected void onDestroy() {
        if (unbinder != null) {
            unbinder.unbind();
        }
        super.onDestroy();
    }

    @Override
    public void onPlay(FavoriteEnt favoriteEnt) {
        FaceRecordEnt recordEnt = new FaceRecordEnt();
        recordEnt.id = favoriteEnt.did;
        recordEnt.title = favoriteEnt.title;
        recordEnt.videoUrl = favoriteEnt.video_url;
        recordEnt.preFinalUrl = favoriteEnt.pre_final_url;
        recordEnt.worth = favoriteEnt.worth;
        recordEnt.collectAmount = favoriteEnt.collect_amount;
        recordEnt.praiseAmount = favoriteEnt.praise_amount;

        VideoActivity.gotoVideoActivity(this, recordEnt);
    }

    @Override
    public void onShare(FavoriteEnt favoriteEnt) {
        ShareUtils.share(this, favoriteEnt.title, favoriteEnt.video_url, favoriteEnt.pre_final_url, favoriteEnt.title);
    }
}
