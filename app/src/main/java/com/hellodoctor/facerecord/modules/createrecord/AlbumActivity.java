
package com.hellodoctor.facerecord.modules.createrecord;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.GridView;
import android.widget.RelativeLayout;

import com.canyinghao.canrefresh.CanRefreshLayout;
import com.hellodoctor.facerecord.common.base.CommonBaseActivity;
import com.hellodoctor.facerecord.common.constants.AppCommonCons;
import com.hellodoctor.facerecord.common.constants.PhotoConstants;
import com.hellodoctor.facerecord.common.constants.VideoConstants;
import com.hellodoctor.facerecord.common.utils.DateParserUtils;
import com.hellodoctor.facerecord.entity.PictureEnt;
import com.hellodoctor.facerecord.modules.createrecord.AlbumContract.IAlbumPresenter;
import com.hellodoctor.facerecord.modules.lookphoto.LookPhotoActivity;
import com.isoftstone.mis.mmsdk.common.widget.refreshview.MMClassicRefreshView;
import com.kelin.client.R;

import java.io.Serializable;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * <一句话功能简述> <功能详细描述>
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-19]
 */
public class AlbumActivity extends CommonBaseActivity<IAlbumPresenter> implements AlbumContract.IAlbumView,
        CompoundButton.OnCheckedChangeListener, CanRefreshLayout.OnRefreshListener,
        CanRefreshLayout.OnLoadMoreListener {

    Unbinder unbinder;

    @BindView(R.id.can_content_view)
    GridView albumGv;

    /** 选择照片时底部布局 */
    @BindView(R.id.rl_album_bottom)
    RelativeLayout albumBottomRl;

    /** 生成纪录片 */
    @BindView(R.id.btn_create_record_video)
    Button createRecordVideoBtn;

    /** 全选 */
    @BindView(R.id.cb_select_all)
    CheckBox selectAllCb;

    private AlbumAdapter albumAdapter;

    @BindView(R.id.crl_refresh)
    CanRefreshLayout refreshCrl;

    @BindView(R.id.can_refresh_header)
    MMClassicRefreshView refreshHeaderView;

    /** 当前加载的页码数 */
    private int currentPage = 1;

    private static final int REQUEST_LOOK_PICTURE = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.album_activity);
    }

    @Override
    public void initViews() {
        unbinder = ButterKnife.bind(this);

        setTitleBg(R.drawable.album_title);
        setRightTitle(R.string.select_photo);

        albumBottomRl.setVisibility(View.GONE);

        albumAdapter = new AlbumAdapter(this);
        albumGv.setAdapter(albumAdapter);
    }

    @Override
    public void initListener() {
        // 设置上拉、下拉监听
        refreshCrl.setOnRefreshListener(this);
        refreshCrl.setOnLoadMoreListener(this);

        selectAllCb.setOnCheckedChangeListener(this);
        albumAdapter.setOnSelectedAllListener(new AlbumAdapter.OnSelectedAllListener() {
            @Override
            public void onSelectedAll(boolean isSelectAll) {
                selectAllCb.setOnCheckedChangeListener(null);
                selectAllCb.setChecked(isSelectAll);
                selectAllCb.setOnCheckedChangeListener(AlbumActivity.this);
            }

            @Override
            public void onLookBigPicture(PictureEnt pictureEnt) {
                lookBigPicture(pictureEnt);
            }
        });
    }

    private void lookBigPicture(PictureEnt pictureEnt) {
        Intent intent = new Intent(this, LookPhotoActivity.class);
        intent.putExtra(PhotoConstants.KEY_PICTURE, pictureEnt);
        startActivityForResult(intent, REQUEST_LOOK_PICTURE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_LOOK_PICTURE && resultCode == RESULT_OK) {
            Serializable object = data.getSerializableExtra(PhotoConstants.KEY_PICTURE);
            if (!(object instanceof PictureEnt)) {
                return;
            }
            PictureEnt pictureEnt = (PictureEnt) object;
            albumAdapter.remove(pictureEnt);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        albumAdapter.selectAll(isChecked);
    }

    @Override
    public void loadData() {
        presenter.getAlbums(currentPage);
    }

    @Override
    public void onRefresh() {
        currentPage = 1;
        presenter.getAlbums(currentPage);
    }

    @Override
    public void onLoadMore() {
        presenter.getAlbums(currentPage);
    }

    @Override
    public void getAlbumsFinish(boolean result, List<PictureEnt> pictureEnts) {
        refreshCrl.refreshComplete();
        refreshCrl.loadMoreComplete();
        if (!result) {
            return;
        }

        if (currentPage == 1) {
            // 刷新完成
            // 刷新成功，设置刷新时间
            currentPage++;
            refreshHeaderView.setHeadRefreshTime(DateParserUtils.parseDateToString(System.currentTimeMillis()));
            albumAdapter.setData(pictureEnts);
            if (albumAdapter.getCount() < AppCommonCons.PAGE_SIZE) {
                refreshCrl.setLoadMoreEnabled(false);
            } else {
                refreshCrl.setLoadMoreEnabled(true);
            }
        } else {
            // 加载更多完成
            if (pictureEnts == null || pictureEnts.size() == 0) {
                showToast(R.string.no_more_data);
                return;
            }
            // 加载更多成功，页码加1
            currentPage++;
            albumAdapter.addAll(pictureEnts);
        }
    }

    @Override
    public IAlbumPresenter createPresenter() {
        return new AlbumPresenter(this, this);
    }

    /** 是否是选择照片模式 */
    private boolean isSelectPhotoMode = false;

    @Override
    protected void onTitleRightClick(View v) {
        isSelectPhotoMode = !isSelectPhotoMode;
        if (isSelectPhotoMode) {
            albumBottomRl.setVisibility(View.VISIBLE);
            setRightTitle(R.string.cancel);

            refreshCrl.setRefreshEnabled(false);
            refreshCrl.setLoadMoreEnabled(false);
        } else {
            albumBottomRl.setVisibility(View.GONE);
            selectAllCb.setChecked(false);
            setRightTitle(R.string.select_photo);

            refreshCrl.setRefreshEnabled(true);
            refreshCrl.setLoadMoreEnabled(true);
        }

        albumAdapter.setEditable(isSelectPhotoMode);
    }

    @OnClick(R.id.btn_create_record_video)
    public void createRecordVideoBtn(View v) {
        if (albumAdapter.getSelectedPictures().size() < 2) {
            showToast(R.string.min_selected_photo);
        } else {
            Intent intent = new Intent(this, SelectMusicActivity.class);
            intent.putExtra(VideoConstants.KEY_PICTURES, albumAdapter.getSelectedPictures());
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        if (unbinder != null) {
            unbinder.unbind();
        }
        super.onDestroy();
    }
}
