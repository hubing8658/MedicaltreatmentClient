package com.hellodoctor.facerecord.modules.search;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.RelativeLayout;

import com.facebook.stetho.common.LogUtil;
import com.isoftstone.mis.mmsdk.common.architecture.presenter.IBasePresenter;
import com.isoftstone.mis.mmsdk.common.architecture.ui.BaseActivity;
import com.kelin.client.R;
import com.monty.library.PreferencesUtils;
import com.monty.library.ScreemUtil;
import com.monty.library.widget.CommonAdapter;
import com.monty.library.widget.ViewHolder;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchInputActivity extends BaseActivity {

    @BindView(R.id.et_face_record_search)
    EditText etInput;
    @BindView(R.id.gv_recorder)
    GridView gvRecorder;
    @BindView(R.id.gv_want)
    GridView gvWant;

    private CommonAdapter<String> mRecorderAdapter;
    private CommonAdapter<String> mWantAdapter;
    private List<String> listRecorder;
    private List<String> listWant;
    private final static String SEARCH_RECORDER = "search_record";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);
        ButterKnife.bind(this);
        initData();
        initView();
        initListener();
    }

    private void initData() {
        String strRecord = PreferencesUtils.getString(this, SEARCH_RECORDER);
        if (!TextUtils.isEmpty(strRecord) && strRecord.contains(";")) {
            String[] strs = strRecord.split(";");
            if (strs != null) {
                LogUtil.d("record", "num of record is " + strs.length);
                listRecorder = Arrays.asList(strs);
            }
        }
        String[] strsWant = {"如何快速瘦脸", "祛斑祛痘护肤", "一个月丑女逆袭", "北大校花护肤"};
        listWant = Arrays.asList(strsWant);
    }

    private void initListener() {
        gvRecorder.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showSelText(listRecorder.get(position));
            }
        });
        gvWant.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showSelText(listWant.get(position));
            }
        });
    }

    private void showSelText(String text) {
        etInput.setText(text);
    }

    private void initView() {
        mRecorderAdapter = new CommonAdapter<String>(this, listRecorder, R.layout.gv_search_item) {
            @Override
            public void convert(ViewHolder helper, String item) {
                helper.setText(R.id.tv_content, item);
            }
        };
        gvRecorder.setAdapter(mRecorderAdapter);
        mWantAdapter = new CommonAdapter<String>(this, listWant, R.layout.gv_search_item) {
            @Override
            public void convert(ViewHolder helper, String item) {
                helper.setText(R.id.tv_content, item);
                View vBotLine = helper.getView(R.id.v_bot_line);
                View vRightLine = helper.getView(R.id.v_right_line);
                int position = helper.getPosition();
                vRightLine.setVisibility(isRightCol(position)?View.GONE:View.VISIBLE);
                vBotLine.setVisibility(isLastRow(position) ? View.GONE : View.VISIBLE);
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) vBotLine.getLayoutParams();
                int margin = ScreemUtil.dip2px(SearchInputActivity.this, 10);
                if (vBotLine.getVisibility() == View.VISIBLE && position % gvWant.getNumColumns() == 0) {
                    params.leftMargin = margin;
                    params.rightMargin = 0;
                }else {
                    params.leftMargin = 0;
                    params.rightMargin = margin;
                }
                vBotLine.setLayoutParams(params);
            }
        };
        gvWant.setAdapter(mWantAdapter);
    }

    private boolean isRightCol(int position) {
        return position != 0 && (position+1) % gvWant.getNumColumns() == 0;
    }
    private boolean isLastRow(int position) {
        position++;
        int col = gvWant.getNumColumns();
        int nowPostionLine = position / col + (position % col > 0 ? 1 : 0);
        int lastLine = mWantAdapter.getCount() / col + (mWantAdapter.getCount() % col > 0 ? 1 : 0);
        return nowPostionLine == lastLine;
    }

    @Override
    public IBasePresenter createPresenter() {
        return null;
    }


    @OnClick({R.id.iv_back, R.id.tv_search})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_search:
                attempToSearchDetailActivity();
                break;
        }
    }

    private void attempToSearchDetailActivity() {
        String input = etInput.getText().toString().trim();
        if(TextUtils.isEmpty(input)) {
            showToast("请输入搜索内容！");
            return;
        }
        SearchDetailActivity.lunch(this, input);
        finish();
    }
}
