package com.hellodoctor.facerecord.biz;

import com.hellodoctor.facerecord.common.constants.LogTag;
import com.hellodoctor.facerecord.net.RecordApiService;
import com.isoftstone.mis.mmsdk.common.utils.log.MMLog;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;

import retrofit2.Call;
import retrofit2.Response;

/**
 * <一句话功能简述>
 * <功能详细描述>
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-29]
 */
public class RecordOperationBiz {

    public interface OperationResultListener {

        /**
         * 操作结果
         * @param isSuccess true表示成功
         */
        void onResult(boolean isSuccess);

    }

    /**
     * 点赞
     * @param did 视频id
     */
    public void addPraise(int did, String token, final OperationResultListener listener) {
        if (listener == null) {
            return;
        }
        RecordApiService service = RetrofitHelper.getInstance().createService(RecordApiService.class);
        Call<BaseCallModel> addFavorite = service.addFavorite(did, token);
        addFavorite.enqueue(new BaseCallBack<BaseCallModel>() {
            @Override
            public void onSuccess(Response<BaseCallModel> response) {
                if (response == null) {
                    listener.onResult(false);
                    return;
                }
                BaseCallModel body = response.body();
                if (body == null) {
                    listener.onResult(false);
                    return;
                }
                listener.onResult(true);
            }

            @Override
            public void onFailure(String message) {
                MMLog.et(LogTag.RANKING, "点赞失败：" + message);
                listener.onResult(false);
            }
        });
    }

    /**
     * 取消点赞
     * @param did 视频id
     */
    public void deletePraise(int did, String token, final OperationResultListener listener) {
        if (listener == null) {
            return;
        }
        RecordApiService service = RetrofitHelper.getInstance().createService(RecordApiService.class);
        Call<BaseCallModel> addFavorite = service.deletePraise(did, token);
        addFavorite.enqueue(new BaseCallBack<BaseCallModel>() {
            @Override
            public void onSuccess(Response<BaseCallModel> response) {
                if (response == null) {
                    listener.onResult(false);
                    return;
                }
                BaseCallModel body = response.body();
                if (body == null) {
                    listener.onResult(false);
                    return;
                }
                listener.onResult(true);
            }

            @Override
            public void onFailure(String message) {
                MMLog.et(LogTag.RANKING, "取消点赞失败：" + message);
                listener.onResult(false);
            }
        });
    }

    /**
     * 收藏
     * @param did 视频id
     */
    public void addFavorite(int did, String token, final OperationResultListener listener) {
        if (listener == null) {
            return;
        }
        RecordApiService service = RetrofitHelper.getInstance().createService(RecordApiService.class);
        Call<BaseCallModel> addFavorite = service.addFavorite(did, token);
        addFavorite.enqueue(new BaseCallBack<BaseCallModel>() {
            @Override
            public void onSuccess(Response<BaseCallModel> response) {
                if (response == null) {
                    listener.onResult(false);
                    return;
                }
                BaseCallModel body = response.body();
                if (body == null) {
                    listener.onResult(false);
                    return;
                }
                listener.onResult(true);
            }

            @Override
            public void onFailure(String message) {
                MMLog.et(LogTag.RANKING, "添加收藏失败：" + message);
                listener.onResult(false);
            }
        });
    }

    /**
     * 取消收藏
     * @param did 视频id
     */
    public void deleteFavorite(int did, String token, final OperationResultListener listener) {
        if (listener == null) {
            return;
        }
        RecordApiService service = RetrofitHelper.getInstance().createService(RecordApiService.class);
        Call<BaseCallModel> addFavorite = service.deleteFavorite(did, token);
        addFavorite.enqueue(new BaseCallBack<BaseCallModel>() {
            @Override
            public void onSuccess(Response<BaseCallModel> response) {
                if (response == null) {
                    listener.onResult(false);
                    return;
                }
                BaseCallModel body = response.body();
                if (body == null) {
                    listener.onResult(false);
                    return;
                }
                listener.onResult(true);
            }

            @Override
            public void onFailure(String message) {
                MMLog.et(LogTag.RANKING, "取消收藏失败：" + message);
                listener.onResult(false);
            }
        });
    }

}
