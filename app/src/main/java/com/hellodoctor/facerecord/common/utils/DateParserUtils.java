
package com.hellodoctor.facerecord.common.utils;

import com.isoftstone.mis.mmsdk.common.utils.log.MMLog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * 日期时间解析工具类
 *
 * @author hubing
 * @version [1.0.0.0, 2017-01-12]
 */
public class DateParserUtils {

    private static final String TAG = "DateParserUtils";

    /** 默认日期格式化模板 yyyy-MM-dd HH:mm:ss */
    public static final String DEFAULT_TEMPLATE = "yyyy-MM-dd HH:mm:ss";

    /** 日期格式化模板 yyyy-MM-dd HH-mm-ss */
    public static final String TEMPLATE1 = "yyyy-MM-dd HH-mm-ss";

    /** 日期格式化模板 yyyy-MM-dd-HH-mm-ss */
    public static final String TEMPLATE2 = "yyyy-MM-dd-HH-mm-ss";

    /**
     * 使用默认模板将long型的日期转换成指定格式的字符串
     *
     * @param time 时间
     * @return 格式化之后的时间字符串
     */
    public static String parseDateToString(long time) {
        return parseDateToString(time, DEFAULT_TEMPLATE);
    }

    /**
     * 将long型的日期转换成指定格式的字符串
     *
     * @param time 时间
     * @param template 格式化的模板
     * @return 格式化之后的时间字符串
     */
    public static String parseDateToString(long time, String template) {
        return new SimpleDateFormat(template, Locale.getDefault()).format(new Date(time));
    }

    /**
     * 解析日期时间为毫秒值，时间格式为：yyyy-MM-dd HH-mm-ss
     *
     * @param time 日期时间
     * @return 解析出来的时间毫秒值
     */
    public static long parseDate(String time) {
        return parseDate(time, TEMPLATE1);
    }

    /**
     * 解析日期时间为毫秒值
     * 
     * @param time 日期时间
     * @param template 解析模板
     * @return 解析出来的时间毫秒值
     */
    public static long parseDate(String time, String template) {
        try {
            return new SimpleDateFormat(template, Locale.getDefault()).parse(time).getTime();
        } catch (ParseException e) {
            MMLog.et(TAG, e, "解析时间失败：", time);
        }
        return System.currentTimeMillis();
    }

//    /**
//     * 格式化日期成月份显示（与当前月份在同一月的，显示本月，与当前年同一年的，显示月数，其他年份显示年月）
//     * @param context 上下文
//     * @param time 日期时间
//     * @return 格式化之后月份字符串
//     */
//    public static String formatDateToMonth(Context context, long time) {
//        // 当前时间
//        Calendar curCal = Calendar.getInstance();
//        // 待格式化时间
//        Calendar formatCal = Calendar.getInstance();
//        formatCal.setTimeInMillis(time);
//        int curYear = curCal.get(Calendar.YEAR);
//        int curMonth = curCal.get(Calendar.MONTH);
//        int formatYear = formatCal.get(Calendar.YEAR);
//        int formatMonth = formatCal.get(Calendar.MONTH);
//
//        // 判断日期区间
//        if (formatYear >= curYear && formatMonth >= curMonth) {
//            return context.getString(R.string.current_month);
//        } else if (formatYear >= curYear) {
//            return context.getString(R.string.group_month, formatMonth + 1);
//        } else {
//            return context.getString(R.string.group_year_month, formatYear, formatMonth + 1);
//        }
//    }

}
