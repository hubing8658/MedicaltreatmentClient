
package com.hellodoctor.facerecord.common.fileupload;

import com.hellodoctor.facerecord.common.constants.LogTag;
import com.hellodoctor.facerecord.net.RecordApiService;
import com.isoftstone.mis.mmsdk.common.utils.log.MMLog;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;

import java.io.File;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * <一句话功能简述> <功能详细描述>
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-23]
 */
public class HttpFileUpload {

    /**
     * 视频文件上传
     * @param file 要上传的文件
     * @return 返回上传成功之后服务器地址
     */
    public String uploadVideoFile(File file) {
        MultipartBody.Builder builder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("", "");

        RequestBody imageBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        //后台接收文件流的参数名
        builder.addFormDataPart("file", file.getName(), imageBody);
        List<MultipartBody.Part> parts = builder.build().parts();

        MMLog.it(LogTag.FILE_UPLOAD, "开始上传视频文件");

        RecordApiService service = RetrofitHelper.getInstance().createService(RecordApiService.class);
        Call<BaseCallModel<List<String>>> uploadVideoFileCall = service.uploadVideoFile(parts);
        try {
            Response<BaseCallModel<List<String>>> response = uploadVideoFileCall.execute();
            if (response == null || response.code() != 200) {
                return null;
            }
            BaseCallModel<List<String>> body = response.body();
            if (body == null || body.data == null || body.data.size() == 0) {
                return null;
            }
            MMLog.it(LogTag.FILE_UPLOAD, "上传视频文件成功");
            return body.data.get(0);
        } catch (Exception e) {
            MMLog.et(LogTag.FILE_UPLOAD, e);
        }
        return null;
    }

    /**
     * 上传图片文件
     * @param token 用户登录token
     * @param type 1-头像,2-投诉截图
     * @param file 要上传的文件
     * @return 返回上传成功之后服务器地址
     */
    public String uploadImage(String token, String type, File file) {
        MultipartBody.Builder builder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)//表单类型
                .addFormDataPart("token", token)
                .addFormDataPart("type", type);

        RequestBody imageBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        //后台接收文件流的参数名
        builder.addFormDataPart("file", file.getName(), imageBody);
        List<MultipartBody.Part> parts = builder.build().parts();

        MMLog.it(LogTag.FILE_UPLOAD, "开始上传图片文件");

        RecordApiService service = RetrofitHelper.getInstance().createService(RecordApiService.class);
        Call<BaseCallModel<List<String>>> uploadImageCall = service.uploadImageFile(parts);
        try {
            Response<BaseCallModel<List<String>>> response = uploadImageCall.execute();
            if (response == null || response.code() != 200) {
                return null;
            }
            BaseCallModel<List<String>> body = response.body();
            if (body == null || body.data == null || body.data.size() == 0) {
                return null;
            }
            MMLog.it(LogTag.FILE_UPLOAD, "上传图片文件成功");
            return body.data.get(0);
        } catch (Exception e) {
            MMLog.et(LogTag.FILE_UPLOAD, e);
        }
        return null;
    }

}
