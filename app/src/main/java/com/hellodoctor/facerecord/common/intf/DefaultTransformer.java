package com.hellodoctor.facerecord.common.intf;

import android.view.View;

/**
 * <一句话功能简述>
 * <功能详细描述>
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-18]
 */
public class DefaultTransformer extends ABaseTransformer {

    @Override
    protected void onTransform(View view, float position) {
    }

    @Override
    public boolean isPagingEnabled() {
        return true;
    }

}
