
package com.hellodoctor.facerecord.common.intf.dialog;

import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.TextView;

import com.kelin.client.R;


/**
 * 自定义确认/取消对话框
 *
 * @author hubing
 * @version [1.0.0.0, 2017-01-16]
 */
public class ConfirmDialog extends Dialog {

    private TextView tvMessage;

    /** 确认按钮 */
    private Button btnConfirm;

    /** 取消按钮 */
    private Button btnCancel;

    public ConfirmDialog(Context context) {
        super(context, R.style.TranslucentDialog);
        initDialog(context);
    }

    public ConfirmDialog(Context context, int theme) {
        super(context, theme);
        initDialog(context);
    }

    /**
     * 初始化对话框
     *
     * @param context 上下文
     */
    private void initDialog(Context context) {
        View deleteView = View.inflate(context, R.layout.common_confirm_dialog, null);
        int width = context.getResources().getDimensionPixelSize(R.dimen.common_dialog_width);
        LayoutParams params = new LayoutParams(width, LayoutParams.WRAP_CONTENT);

        // 对话框的提示信息
        tvMessage = (TextView) deleteView.findViewById(R.id.tv_comfirm_message);

        // 取消按钮
        btnCancel = (Button) deleteView.findViewById(R.id.btn_dialog_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        // 确认按钮
        btnConfirm = (Button) deleteView.findViewById(R.id.btn_dialog_confirm);
        btnConfirm.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mOnConfirmListener != null) {
                    mOnConfirmListener.onConfirm(ConfirmDialog.this);
                }
            }

        });

        addContentView(deleteView, params);
        setCancelable(false);
    }

    /**
     * 设置是否显示取消按钮
     * 
     * @param isShowCancel true表示要显示取消按钮
     */
    public void setCancelVisiblity(boolean isShowCancel) {
        if (isShowCancel) {
            btnCancel.setVisibility(View.VISIBLE);
        } else {
            btnCancel.setVisibility(View.GONE);
        }
    }

    /**
     * 确认监听器
     *
     * @author hubing
     * @version 1.0.0 2015-10-12
     */
    public interface OnConfirmListener {

        /**
         * 对话框上按下确认删除，回调此方法
         *
         * @param dialog
         */
        void onConfirm(ConfirmDialog dialog);

    }

    private OnConfirmListener mOnConfirmListener;

    /**
     * 设置取消按钮监听
     *
     * @param l
     */
    public void setOnConfirmListener(OnConfirmListener l) {
        this.mOnConfirmListener = l;
    }

    /**
     * 设置提示信息
     *
     * @param msg
     */
    public void setMessage(String msg) {
        if (!TextUtils.isEmpty(msg)) {
            tvMessage.setText(msg);
        }
    }

    /**
     * 设置提示信息
     *
     * @param resId 资源id
     */
    public void setMessage(int resId) {
        tvMessage.setText(resId);
    }

    /**
     * 设置确认按钮显示文字
     *
     * @param resId 资源id
     */
    public void setConfirmText(int resId) {
        btnConfirm.setText(resId);
    }

    /**
     * 设置取消按钮显示文字
     *
     * @param resId 资源id
     */
    public void setCancelText(int resId) {
        btnCancel.setText(resId);
    }

}
