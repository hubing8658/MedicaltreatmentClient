
package com.hellodoctor.facerecord.common.utils;

import com.hellodoctor.facerecord.entity.PictureEnt;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;

/**
 * <一句话功能简述> <功能详细描述>
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-22]
 */
public class PictureUtils {

    /**
     * 对照片按时间进行排序
     * @param pictureEnts
     */
    public static void sort(ArrayList<PictureEnt> pictureEnts) {
        if (pictureEnts == null || pictureEnts.size() <= 1) {
            return;
        }
        Collections.sort(pictureEnts, new Comparator<PictureEnt>() {
            @Override
            public int compare(PictureEnt o1, PictureEnt o2) {
                if (o1 == null || o2 == null) {
                    return 0;
                }
                if (o1.photoTimeMs == o2.photoTimeMs) {
                    return 0;
                }
                return o1.photoTimeMs - o2.photoTimeMs > 0 ? 1 : -1;
            }
        });
    }

    /**
     * 判断列表照片是否连续日期
     * @param pictureEnts
     * @return true表示连续
     */
    public static boolean isContinuous(ArrayList<PictureEnt> pictureEnts) {
        if (pictureEnts == null || pictureEnts.size() <= 1) {
            return false;
        }
        PictureEnt firstPic = pictureEnts.get(0);
        Calendar calendarFirst = Calendar.getInstance();
        calendarFirst.setTimeInMillis(firstPic.photoTimeMs);
        int firstDay = calendarFirst.get(Calendar.DAY_OF_YEAR);

        PictureEnt lastPic = pictureEnts.get(pictureEnts.size() - 1);
        Calendar calendarLast = Calendar.getInstance();
        calendarLast.setTimeInMillis(lastPic.photoTimeMs);
        int lastDay = calendarLast.get(Calendar.DAY_OF_YEAR);
        return (lastDay - firstDay) == (pictureEnts.size() - 1);
    }

}
