
package com.hellodoctor.facerecord.common.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;

import com.hellodoctor.facerecord.common.constants.LogTag;
import com.isoftstone.mis.mmsdk.common.utils.log.MMLog;

import java.io.FileOutputStream;
import java.io.OutputStream;

/**
 * <一句话功能简述> <功能详细描述>
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-14]
 */
public class BitmapUtils {

    /**
     * 旋转图片
     */
    public static Bitmap rotateBitmapByDegree(Bitmap bm, int degree) {
        Bitmap returnBm = null;
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        try {
            returnBm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
        } catch (OutOfMemoryError e) {
        }
        if (returnBm == null) {
            returnBm = bm;
        }
        if (bm != returnBm) {
            bm.recycle();
        }
        return returnBm;
    }

    /**
     * 将两张照片拼接成一张
     * @param firstPicPath
     * @param secondPicPath
     * @param outputPath
     * @return
     */
    public static boolean splicingPic(String firstPicPath, String secondPicPath, String outputPath) {
        try {
            Bitmap firstPic = BitmapFactory.decodeFile(firstPicPath);
            Bitmap secondPic = BitmapFactory.decodeFile(secondPicPath);
            int newPicWidth = firstPic.getWidth() + secondPic.getWidth();
            int newPicHeight = firstPic.getHeight() > secondPic.getHeight() ? secondPic.getHeight() : firstPic.getHeight();
            Bitmap newPic = Bitmap.createBitmap(newPicWidth, newPicHeight, Bitmap.Config.RGB_565);
            Canvas canvas = new Canvas(newPic);
            Rect src = new Rect(0, 0, firstPic.getWidth(), firstPic.getHeight());
            Rect dst = new Rect(0, 0, firstPic.getWidth(), newPicHeight);
            canvas.drawBitmap(firstPic, src, dst, null);
            firstPic.recycle();

            src.set(0, 0, secondPic.getWidth(), secondPic.getHeight());
            dst.set(firstPic.getWidth(), 0, newPicWidth, newPicHeight);
            canvas.drawBitmap(secondPic, src, dst, null);
            secondPic.recycle();

            OutputStream os = new FileOutputStream(outputPath);
            newPic.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.close();
            newPic.recycle();
            return true;
        } catch (Exception e) {
            MMLog.et(LogTag.COMPOSE_VIDEO, e);
        }
        return false;
    }

}
