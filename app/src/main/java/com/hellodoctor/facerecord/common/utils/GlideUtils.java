
package com.hellodoctor.facerecord.common.utils;

import android.content.Context;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.Target;
import com.hellodoctor.facerecord.common.constants.LogTag;
import com.isoftstone.mis.mmsdk.common.utils.log.MMLog;

import java.io.File;

/**
 * <一句话功能简述> <功能详细描述>
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-22]
 */
public class GlideUtils {

    /**
     * 下载图片
     *
     * @param context
     * @param url 图片下载地址
     */
    public static File downloadOnly(Context context, String url) {
        try {
            return Glide.with(context)
                    .load(url)
                    .downloadOnly(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                    .get();
        } catch (Exception e) {
            MMLog.et(LogTag.COMPOSE_VIDEO, e);
        }
        return null;
    }

}
