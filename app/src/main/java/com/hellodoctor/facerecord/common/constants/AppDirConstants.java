
package com.hellodoctor.facerecord.common.constants;

import android.os.Environment;
import android.text.TextUtils;

import java.io.File;

/**
 * 应用目录常量类
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-13]
 */
public class AppDirConstants {

    private AppDirConstants() {
    }

    /** 根目录 */
    public static final String ROOT_DIR = Environment.getExternalStorageDirectory() + File.separator + "HelloDoctor";

    /** 背景音乐保存目录 */
    public static final String MUSICS_DIR = ROOT_DIR + File.separator + "musics";

    /** 视频保存目录 */
    private static final String VIDEO_DIR = File.separator + "videos";

    /** 拍照保存目录 */
    private static final String PHOTOS_DIR = File.separator + "photos";

    /**
     * 保存照片的目录
     * @param account 当前登录账号
     * @return
     */
    public static String getPhotosDir(String account) {
        if (TextUtils.isEmpty(account)) {
            return ROOT_DIR + PHOTOS_DIR;
        }
        return ROOT_DIR + File.separator + account + PHOTOS_DIR;
    }

    /**
     * 保存视频的目录
     * @param account 当前登录账号
     * @return
     */
    public static String getVideoDir(String account) {
        if (TextUtils.isEmpty(account)) {
            return ROOT_DIR + VIDEO_DIR;
        }
        return ROOT_DIR + File.separator + account + VIDEO_DIR;
    }

}
