package com.hellodoctor.facerecord.common.base;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.kelin.client.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 通用webview Activity
 */

public class WebViewActivity extends AppCompatActivity {

    @BindView(R.id.activity_web_view)
    WebView mWebView;
    @BindView(R.id.tv_common_title)
    TextView tvTitle;

    private String mTitle;
    private String mUrl;
    private String mContent;
    private int mTitleBgRes;
    private static final String TITLE = "title";
    private static final String URL = "url";
    private static final String CONTENT = "content";
    private static final String TITLE_BG = "title_bg";

    /**
     *
     * @param activity
     * @param title  标题
     * @param url 链接
     * @param titleBgRes 标题背景，如果有标题则不用使用背景
     */
    public static void lunch(Activity activity, String title, String url, String content, Integer titleBgRes) {
        Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra(TITLE, title);
        intent.putExtra(URL, url);
        intent.putExtra(CONTENT, content);
        intent.putExtra(TITLE_BG, titleBgRes);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        ButterKnife.bind(this);
        initData();
        initView();
    }

    private void initView() {
        if (!TextUtils.isEmpty(mTitle)) {
            tvTitle.setText(mTitle);
        } else {
            tvTitle.setBackgroundResource(mTitleBgRes);
        }
        if (TextUtils.isEmpty(mUrl) && !TextUtils.isEmpty(mContent)) {
            mContent = translation(mContent);
            mContent = translation(mContent);
            mContent = HTML_FORMAT + mContent + HTML_SUFFIX;
            mWebView.loadData(mContent, "text/html", "utf-8");
        } else {
            mWebView.loadUrl(mUrl);
        }
        initWebSetting();
    }

    static final String HTML_FORMAT = "<html> \n" + "<head> \n" + "<meta name=\"viewport\" content=\"initial-scale=1.0, maximum-scale=1.0, user-scalable=no\" /> \n"
            + "<style type=\"text/css\"> \n" + "body {font-size:15px;}\n" + "</style> \n" + "</head> \n" + "<body>" + "<script type='text/javascript'>"
            + "window.onload = function(){\n" + "var $img = document.getElementsByTagName('img');\n" + "for(var p in  $img){\n" + " $img[p].style.width = '100%%';\n"
            + "$img[p].style.height ='auto'\n" + "}\n" + "}" + "</script>";
    static final String HTML_SUFFIX = "</body></html>";
    
    private String translation(String content) {
        String replace = content.replace("&lt;", "<");
        String replace1 = replace.replace("&gt;", ">");
        String replace2 = replace1.replace("&amp;", "&");
        String replace3 = replace2.replace("&quot;", "\"");
        return replace3.replace("&copy;", "©");
    }

    private void initWebSetting() {
        WebViewClient mWebViewClient = new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                super.shouldOverrideUrlLoading(view, request);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    String path = request.getUrl().toString();
                    if (!WebViewHelper.dealSpecialUrl(path)) {
                        view.loadUrl(path);
                    }
                }
                return true;
            }
        };
        mWebView.getSettings().setDefaultTextEncodingName("utf-8") ;
        mWebView.getSettings().setSupportZoom(false);
        mWebView.setVerticalScrollBarEnabled(false);
        mWebView.setWebViewClient(mWebViewClient);
    }

    private void initData() {
        Intent intent = getIntent();
        mTitle = intent.getStringExtra(TITLE);
        mUrl = intent.getStringExtra(URL);
        mContent = intent.getStringExtra(CONTENT);
        mTitleBgRes = intent.getIntExtra(TITLE_BG, -1);
    }

    @OnClick(R.id.rl_common_title_left)
    public void click(View v) {
        switch (v.getId()) {
            case R.id.rl_common_title_left:
                finish();
                break;
        }
    }

    @Override
    protected void onDestroy() {
        WebViewHelper.clearWebView(mWebView);
        super.onDestroy();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (mWebView.canGoBack()) {
                mWebView.goBack();
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            if (mWebView != null) {
                mWebView.onPause();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            if (mWebView != null) {
                mWebView.onResume();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
