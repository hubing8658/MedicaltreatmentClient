package com.hellodoctor.facerecord.common.base;

import android.content.Intent;
import android.net.Uri;
import android.provider.Browser;
import android.view.ViewGroup;
import android.webkit.DownloadListener;
import android.webkit.WebView;
import com.kelin.client.MyApplication;

/**
 * @author wangzhengfang
 * @date 2018/3/13.
 * @Mail:wangzhengfang@xunlei.com
 * @Description:
 */

public class WebViewHelper {
    public static final void clearWebView(WebView webView) {
        if (webView != null) {
            webView.loadDataWithBaseURL(null, "", "text/html", "utf-8", null);
            webView.clearHistory();
            webView.clearCache(true);
            if (webView.getParent() != null && webView.getParent() instanceof ViewGroup) {
                ((ViewGroup) webView.getParent()).removeView(webView);
            }
            webView.destroy();
            webView = null;
        }
    }

    public static boolean dealSpecialUrl(String url){
        if(url == null){
            return false;
        }
        if (url.startsWith("weixin://") //微信
                || url.startsWith("alipays://") || url.startsWith("alipay") //支付宝
                || url.startsWith("mqqapi://")//qq
                || url.startsWith("tel://")//电话
                || url.startsWith("mailto://")//邮箱
                || url.startsWith("smsto://") || url.startsWith("sms://")//短信
                )
        {
            try {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                intent.putExtra(Browser.EXTRA_APPLICATION_ID, MyApplication.getInstance().getPackageName());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                MyApplication.getInstance().startActivity(intent);
                return true;
            } catch (Exception e) {
            }
        }
        return false;
    }

    public static void setDownloadListener(WebView webView){
        webView.setDownloadListener(new DownloadListener() {
            @Override
            public void onDownloadStart(String arg0, String arg1, String arg2,
                                        String arg3, long arg4) {
                try {
                    Uri uri = Uri.parse(arg0);
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    MyApplication.getInstance().startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
