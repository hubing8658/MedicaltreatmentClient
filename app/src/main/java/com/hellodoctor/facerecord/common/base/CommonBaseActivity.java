
package com.hellodoctor.facerecord.common.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.isoftstone.mis.mmsdk.common.architecture.presenter.IBasePresenter;
import com.isoftstone.mis.mmsdk.common.architecture.ui.BaseActivity;
import com.isoftstone.mis.mmsdk.common.architecture.view.IBaseLoadingView;
import com.isoftstone.mis.mmsdk.common.intf.UITemplate;
import com.kelin.client.R;

/**
 * 模块Activity基类
 *
 * @author hubing
 * @version [1.0.0.0, 2016-12-30]
 */
public abstract class CommonBaseActivity<P extends IBasePresenter> extends BaseActivity<P> implements UITemplate, IBaseLoadingView {

    /** 标题 */
    private TextView tvTitle;

    /** 标题栏左侧按钮 */
    private TextView tvTitleLeft;

    /** 标题栏右侧按钮 */
    private TextView tvTitleRight;

    /** 标题栏 */
    private RelativeLayout commonMainTitleRl;

    /** 内容容器控件 */
    private LinearLayout llContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.layout_common_main);
        init();
    }

    /**
     * 初始化基类views和监听器
     */
    private void init() {
        // 标题栏左边按钮
        RelativeLayout commonTitleLeft = (RelativeLayout) findViewById(R.id.rl_common_title_left);
        // 标题栏右边按钮
        RelativeLayout commonTitleRight = (RelativeLayout) findViewById(R.id.rl_common_title_right);
        // 设置单击监听器
        commonTitleLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onTitleLeftClick(v);
            }
        });
        commonTitleRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onTitleRightClick(v);
            }
        });

        // 标题栏控件
        tvTitle = (TextView) findViewById(R.id.tv_common_title);
        tvTitleLeft = (TextView) findViewById(R.id.tv_common_title_left);
        tvTitleRight = (TextView) findViewById(R.id.tv_common_title_right);

        // 标题栏
        commonMainTitleRl = (RelativeLayout) findViewById(R.id.im_common_main_title);

        // 内容容器控件
        llContent = (LinearLayout) findViewById(R.id.ll_common_content);
    }

    /**
     * 获取标题栏高度
     *
     * @return 返回标题栏高度
     */
    public int getTitleBarHeight() {
        return tvTitle.getHeight();
    }

    /**
     * 关闭标题栏
     */
    public void setNoTitleBar() {
        commonMainTitleRl.setVisibility(View.GONE);
    }

    /**
     * 设置标题栏Title文字
     *
     * @param titleId 标题文本资源id
     */
    @Override
    public void setTitle(int titleId) {
        tvTitle.setText(titleId);
    }

    /**
     * 设置标题栏Title文字
     *
     * @param title 标题文本内容
     */
    public void setTitle(String title) {
        tvTitle.setText(title);
    }

    /**
     * 设置标题栏Title背景
     * @param titleBgId 标题文本背景资源id
     */
    public void setTitleBg(int titleBgId) {
        tvTitle.setBackgroundResource(titleBgId);
    }

    /**
     * 设置标题栏左侧Title文字
     * 
     * @param titleId 文本资源id
     */
    public void setLeftTitle(int titleId) {
        tvTitleLeft.setText(titleId);
    }

    /**
     * 设置标题栏左侧Title文字
     * 
     * @param title 文本内容
     */
    public void setLeftTitle(String title) {
        tvTitleLeft.setText(title);
    }

    /**
     * 设置标题栏左侧Title背景
     * 
     * @param resid 背景资源id
     */
    public void setLeftBackground(int resid) {
        tvTitleLeft.setBackgroundResource(resid);
    }

    /**
     * 设置标题栏左侧Title背景
     * 
     * @param background The Drawable to use as the background, or null to remove the background
     */
    public void setLeftBackground(Drawable background) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            tvTitleLeft.setBackground(background);
        } else {
            tvTitleLeft.setBackgroundDrawable(background);
        }
    }

    /**
     * 设置标题栏左侧Drawables
     * 
     * @param left 文字左边Drawable资源id
     * @param right 文字右边Drawable资源id
     */
    public void setLeftCompoundDrawables(int left, int right) {
        tvTitleLeft.setCompoundDrawablesWithIntrinsicBounds(left, 0, right, 0);
    }

    /**
     * 设置标题栏左侧Drawables
     *
     * @param left 文字左边Drawable资源
     * @param right 文字右边Drawable资源
     */
    public void setLeftCompoundDrawables(Drawable left, Drawable right) {
        tvTitleLeft.setCompoundDrawablesWithIntrinsicBounds(left, null, right, null);
    }

    /**
     * 设置标题栏右侧Title文字
     *
     * @param titleId 文本资源id
     */
    public void setRightTitle(int titleId) {
        tvTitleRight.setText(titleId);
    }

    /**
     * 设置标题栏右侧Title文字
     *
     * @param title 文本内容
     */
    public void setRightTitle(String title) {
        tvTitleRight.setText(title);
    }

    /**
     * 设置标题栏右侧Title背景
     *
     * @param resid 背景资源id
     */
    public void setRightBackground(int resid) {
        tvTitleRight.setBackgroundResource(resid);
    }

    /**
     * 设置标题栏右侧Title背景
     *
     * @param background The Drawable to use as the background, or null to remove the background
     */
    public void setRightBackground(Drawable background) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            tvTitleRight.setBackground(background);
        } else {
            tvTitleRight.setBackgroundDrawable(background);
        }
    }

    /**
     * 设置标题栏右侧Drawables
     *
     * @param left 文字左边Drawable资源id
     * @param right 文字右边Drawable资源id
     */
    public void setRightCompoundDrawables(int left, int right) {
        tvTitleRight.setCompoundDrawablesWithIntrinsicBounds(left, 0, right, 0);
    }

    /**
     * 设置标题栏右侧Drawables
     *
     * @param left 文字左边Drawable资源
     * @param right 文字右边Drawable资源
     */
    public void setRightCompoundDrawables(Drawable left, Drawable right) {
        tvTitleRight.setCompoundDrawablesWithIntrinsicBounds(left, null, right, null);
    }

    @Override
    public void setContentView(int layoutResID) {
        View view = LayoutInflater.from(this).inflate(layoutResID, llContent, false);
        setContentView(view);
    }

    @Override
    public void setContentView(View view) {
        llContent.addView(view);
        initViews();
        initListener();
        loadData();
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        llContent.addView(view, params);
        initViews();
        initListener();
        loadData();
    }

    /**
     * 顶部左侧按钮点击事件
     *
     * @param v 标题栏左边被单击的view
     */
    protected void onTitleLeftClick(View v) {
        finish();
    }

    /**
     * 顶部右侧按钮点击事件
     *
     * @param v 标题栏右边被单击的view
     */
    protected void onTitleRightClick(View v) {
    }

    private ProgressDialog mProgressDialog;

//    /**
//     * 显示提示loading
//     *
//     * @param title 标题
//     * @param message 提示信息
//     */
//    public void showLoading(String title, String message) {
//        mProgressDialog = ProgressDialog.show(this, title, message, false, true);
//    }

    @Override
    public void showLoading(String message) {
        mProgressDialog = ProgressDialog.show(this, null, message);
        mProgressDialog.show();
    }

    /**
     * 隐藏提示loading
     */
    @Override
    public void hideLoading() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        int[] ids = getHideSoftEditViewIds();
        if (ids == null || ids.length == 0) {
            return super.dispatchTouchEvent(ev);
        }
        // 过滤不需要隐藏的控件
        if (isTouchFilterView(getFilterViews(), ev)) {
            return super.dispatchTouchEvent(ev);
        }

        // 点击编辑框外部区域，隐藏软键盘
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (isFocusEditText(v, ids)) {
                // 如果焦点在EditText上，隐藏软键盘
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                // 清除焦点
                ((ViewGroup)v.getParent()).setFocusableInTouchMode(true);
                v.clearFocus();
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    /**
     * 是否触摸到了需要过滤不隐藏软键盘的View
     * 
     * @param filterViews 需要过滤不隐藏软键盘的View
     * @param ev 触摸事件
     * @return 返回true，表示不需要隐藏键盘
     */
    private boolean isTouchFilterView(View[] filterViews, MotionEvent ev) {
        if (filterViews == null || filterViews.length == 0) {
            return false;
        }
        int[] location = new int[2];
        for (View view : filterViews) {
            view.getLocationOnScreen(location);
            int x = location[0];
            int y = location[1];
            if (ev.getX() > x && ev.getX() < (x + view.getWidth()) && ev.getY() > y && ev.getY() < (y + view.getHeight())) {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断当前焦点是否在编辑框上
     * 
     * @param focusView 当前焦点view
     * @param ids 编辑框id数组
     * @return 返回true，表示焦点在编辑框上
     */
    private boolean isFocusEditText(View focusView, int[] ids) {
        if (focusView == null) {
            return false;
        }
        int focusViewId = focusView.getId();
        for (int id : ids) {
            if (focusViewId == id) {
                return true;
            }
        }
        return false;
    }

    /**
     * 获取需要过滤不隐藏软键盘的View数组
     * 
     * @return 需要过滤不隐藏软键盘的View数组
     */
    protected View[] getFilterViews() {
        return null;
    }

    /**
     * 获取需要点击外部区域隐藏键盘的编辑框控件id数组
     * 
     * @return 编辑框控件id数组
     */
    protected int[] getHideSoftEditViewIds() {
        return null;
    }

}
