
package com.ys.test.testffmpeg;

/**
 * FFmpeg命令执行管理类
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-17]
 */
public class FFmpegManager {

    static {
        System.loadLibrary("avcodec-57");
        System.loadLibrary("avdevice-57");
        System.loadLibrary("avfilter-6");
        System.loadLibrary("avformat-57");
        System.loadLibrary("avutil-55");

        System.loadLibrary("postproc-54");
        System.loadLibrary("swresample-2");
        System.loadLibrary("swscale-4");

        System.loadLibrary("ffmepgmain");
    }

    public static native int ffmpegCore(int argc, String[] argv);

    /** 执行成功返回码 */
    public static final int EXECUTE_SUCCESS = 0;

}
