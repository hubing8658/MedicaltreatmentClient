package com.isoftstone.mis.mmsdk.common.component.convenientbanner;

import android.content.Context;
import android.view.animation.Interpolator;
import android.widget.Scroller;

/**
 *  viewpager滑动组件
 */
public class MMViewPagerScroller extends Scroller {
	/**滑动速度,值越大滑动越慢，滑动太快会使3d效果不明显*/
	private int mScrollDuration = 800;

	/**是否为0*/
	private boolean zero;

	public MMViewPagerScroller(Context context) {
		super(context);
	}

	public MMViewPagerScroller(Context context, Interpolator interpolator) {
		super(context, interpolator);
	}

	public MMViewPagerScroller(Context context, Interpolator interpolator,
							   boolean flywheel) {
		super(context, interpolator, flywheel);
	}

	@Override
	public void startScroll(int startX, int startY, int dx, int dy, int duration) {
		super.startScroll(startX, startY, dx, dy, zero ? 0 : mScrollDuration);
	}

	@Override
	public void startScroll(int startX, int startY, int dx, int dy) {
		super.startScroll(startX, startY, dx, dy, zero ? 0 : mScrollDuration);
	}

	/**
	 * 获取滑动速度
	 * @return 滑动速度
     */
	public int getScrollDuration() {
		return mScrollDuration;
	}

	/**
	 * 设置滑动速度
	 * @param scrollDuration 滑动速度
     */
	public void setScrollDuration(int scrollDuration) {
		this.mScrollDuration = scrollDuration;
	}

	/**
	 * 获取滑动速度是否为0
	 * @return true-是0，false-不是0
     */
	public boolean isZero() {
		return zero;
	}

	/**
	 * 设置滑动速度是否为0
	 * @param zero  true-是0，false-不是0
     */
	public void setZero(boolean zero) {
		this.zero = zero;
	}
}