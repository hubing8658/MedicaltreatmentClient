
package com.isoftstone.mis.mmsdk.common.intf;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * 文本变更监听适配器
 * <p>
 * 空实现所有文本变更方法，用于解决每次都需要实现所有方法的问题
 *
 * @author hubing
 * @version [1.0.0.0, 2016-07-14]
 */
public abstract class TextWatcherAdapter implements TextWatcher {

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
    }

}
