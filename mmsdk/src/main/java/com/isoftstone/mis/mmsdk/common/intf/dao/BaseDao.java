
package com.isoftstone.mis.mmsdk.common.intf.dao;

import java.util.List;

/**
 * Dao数据层接口
 * 
 * @author hubing
 * @version 1.0.0 2015-11-26
 */
public interface BaseDao<E> {

    /**
     * 查询所有
     * 
     * @return 返回当前数据表中所有数据列表
     */
    List<E> queryAll();

    /**
     * 查询表中内容
     * 
     * @param e 查询的条件
     * @return 查询出来的数据
     */
    E query(E e);

    /**
     * 新增数据
     * 
     * @param e 要新增的数据对象
     * @return 新增结果，true表示成功，false表示失败
     */
    boolean insert(E e);

    /**
     * 新增多条数据
     * 
     * @param es 要新增的数据列表
     * @return 新增结果，true表示成功，false表示失败
     */
    boolean insert(List<E> es);

    /**
     * 修改数据
     * 
     * @param e 要修改的数据对象
     * @return 修改结果，true表示成功，false表示失败
     */
    boolean update(E e);

    /**
     * 删除数据
     * 
     * @param e 要删除的数据对象
     * @return 删除结果，true表示成功，false表示失败
     */
    boolean delete(E e);

}
