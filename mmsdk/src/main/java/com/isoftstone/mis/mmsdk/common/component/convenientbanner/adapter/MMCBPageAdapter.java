package com.isoftstone.mis.mmsdk.common.component.convenientbanner.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.isoftstone.mis.mmsdk.R;
import com.isoftstone.mis.mmsdk.common.component.convenientbanner.holder.MMCBViewHolderCreator;
import com.isoftstone.mis.mmsdk.common.component.convenientbanner.holder.MMCBViewHolder;
import com.isoftstone.mis.mmsdk.common.component.convenientbanner.view.MMCBLoopViewPager;

import java.util.List;

/**
 * 翻页界面适配器
 */
public class MMCBPageAdapter<T> extends PagerAdapter {
    protected List<T> mDatas;
    protected MMCBViewHolderCreator holderCreator;
//    private View.OnClickListener onItemClickListener;
    private boolean canLoop = true;
    private MMCBLoopViewPager viewPager;
    private final int MULTIPLE_COUNT = 300;

    public int toRealPosition(int position) {
        int realCount = getRealCount();
        if (realCount == 0)
            return 0;
        int realPosition = position % realCount;
        return realPosition;
    }

    @Override
    public int getCount() {
        return canLoop ? getRealCount()*MULTIPLE_COUNT : getRealCount();
    }

    public int getRealCount() {
        return mDatas == null ? 0 : mDatas.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        int realPosition = toRealPosition(position);

        View view = getView(realPosition, null, container);
//        if(onItemClickListener != null) view.setOnClickListener(onItemClickListener);
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
    }

    @Override
    public void finishUpdate(ViewGroup container) {
        int position = viewPager.getCurrentItem();
        if (position == 0) {
            position = viewPager.getFristItem();
        } else if (position == getCount() - 1) {
            position = viewPager.getLastItem();
        }
        try {
            viewPager.setCurrentItem(position, false);
        }catch (IllegalStateException e){}
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    public void setCanLoop(boolean canLoop) {
        this.canLoop = canLoop;
    }

    public void setViewPager(MMCBLoopViewPager viewPager) {
        this.viewPager = viewPager;
    }

    public MMCBPageAdapter(MMCBViewHolderCreator holderCreator, List<T> datas) {
        this.holderCreator = holderCreator;
        this.mDatas = datas;
    }

    public View getView(int position, View view, ViewGroup container) {
        MMCBViewHolder holder = null;
        if (view == null) {
            holder = (MMCBViewHolder) holderCreator.createHolder();
            view = holder.createView(container.getContext());
            view.setTag(R.id.cb_item_tag, holder);
        } else {
            holder = (MMCBViewHolder<T>) view.getTag(R.id.cb_item_tag);
        }
        if (mDatas != null && !mDatas.isEmpty())
            holder.updateUI(container.getContext(), position, mDatas.get(position));
        return view;
    }

//    public void setOnCBItemClickListener(View.OnClickListener onItemClickListener) {
//        this.onItemClickListener = onItemClickListener;
//    }
}
