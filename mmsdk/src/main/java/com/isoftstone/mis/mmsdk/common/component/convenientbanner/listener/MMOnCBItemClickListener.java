package com.isoftstone.mis.mmsdk.common.component.convenientbanner.listener;

/**
 * item点击监听
 */
public interface MMOnCBItemClickListener {
     void onItemClick(int position);
}
