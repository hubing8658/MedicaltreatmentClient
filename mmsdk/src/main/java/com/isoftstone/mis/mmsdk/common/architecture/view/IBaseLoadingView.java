
package com.isoftstone.mis.mmsdk.common.architecture.view;

/**
 * 包含loading方法定义的view基类接口
 *
 * @author hubing
 * @version [1.0.0.0, 2016-11-08]
 */
public interface IBaseLoadingView extends IBaseView {

    /**
     * 显示提示loading
     * 
     * @param message 提示语字符串
     */
    void showLoading(String message);

    /**
     * 隐藏提示loading
     */
    void hideLoading();

}
