package com.isoftstone.mis.mmsdk.common.component.convenientbanner.holder;

import android.content.Context;
import android.view.View;
/**
 * 广告页面View创建器接口
 * @param <T> 任何你指定的对象
 */
public interface MMCBViewHolder<T>{
    /**创建所需的view*/
    View createView(Context context);

    /**根据传入数据更新界面*/
    void updateUI(Context context, int position, T data);
}