
package com.isoftstone.mis.mmsdk.common.utils.encryption;

import com.isoftstone.mis.mmsdk.common.utils.log.MMLog;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

/**
 * 对字符串或文件生成MD5
 * 
 * @author hubing
 * @version 1.0.0 2015-9-21
 */
public class MD5 {

	private static final String TAG = "MD5";

	public static String getMD5Str(String strSrc) {
		MessageDigest md = null;
		String md5Str = null;

		byte[] bt = strSrc.getBytes();
		try {
			md = MessageDigest.getInstance("MD5");
			md.update(bt);
			md5Str = HexString.bytes2Hex(md.digest()); // to HexString
		} catch (NoSuchAlgorithmException e) {
			MMLog.et(TAG, e, e.getMessage());
		}
		// 转为小写输出
		return md5Str == null ? md5Str : md5Str.toLowerCase(Locale.getDefault());
	}

	/**
	 * 
	 * 获取16位的md5字符串值
	 * 
	 * @param strSrc
	 * @return
	 */
	public String get16MD5Str(String strSrc) {
		String md5Str32 = getMD5Str(strSrc);

		if (null != md5Str32) {
			return md5Str32.substring(16);
		}

		return null;
	}

	
	protected static char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6',
			'7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

	protected static MessageDigest messagedigest = null;

	static {
		try {
			messagedigest = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException nsaex) {
			MMLog.et(TAG, nsaex, "初始化失败，MessageDigest不支持MD5Util");
		}
	}

	/**
	 * 生成文件MD5
	 * 
	 * @param filePath
	 * @return
	 */
	public static String getFileMD5String(String filePath) {
		return getFileMD5String(new File(filePath));
	}
	
	/**
	 * 生成文件MD5
	 * 
	 * @param file
	 * @return
	 */
	public static String getFileMD5String(File file) {
		FileInputStream in = null;
		try {
			in = new FileInputStream(file);
			FileChannel ch = in.getChannel();
			MappedByteBuffer byteBuffer = ch.map(FileChannel.MapMode.READ_ONLY, 0, file.length());
			messagedigest.update(byteBuffer);
		} catch (IOException e) {
			MMLog.et(TAG, e, "生成文件md5失败");
		} finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return bufferToHex(messagedigest.digest());
	}

	public static String getMD5String(byte[] bytes) {
		messagedigest.update(bytes);
		return bufferToHex(messagedigest.digest());
	}

	private static String bufferToHex(byte bytes[]) {
		return bufferToHex(bytes, 0, bytes.length);
	}

	private static String bufferToHex(byte bytes[], int m, int n) {
		StringBuffer stringbuffer = new StringBuffer(2 * n);
		int k = m + n;
		for (int l = m; l < k; l++) {
			appendHexPair(bytes[l], stringbuffer);
		}
		return stringbuffer.toString();
	}

	private static void appendHexPair(byte bt, StringBuffer stringbuffer) {
		char c0 = hexDigits[(bt & 0xf0) >> 4];
		char c1 = hexDigits[bt & 0xf];
		stringbuffer.append(c0);
		stringbuffer.append(c1);
	}

}
