
package com.isoftstone.mis.mmsdk.common.intf;

/**
 * 通用界面初始化模板方法
 *
 * @author hubing
 * @version [1.0.0.0, 2018-03-07]
 */
public interface UITemplate {

    /**
     * 初始化View
     */
    void initViews();

    /**
     * 初始化监听器
     */
    void initListener();

    /**
     * 加载数据
     */
    void loadData();

}
