package com.isoftstone.mis.mmsdk.common.component.convenientbanner.holder;

/**
 * 广告页面View创建器加工厂接口
 * @param <MMCBViewHolder> 持有者
 */
public interface MMCBViewHolderCreator<MMCBViewHolder> {
	public MMCBViewHolder createHolder();
}
