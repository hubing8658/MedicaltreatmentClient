
package com.isoftstone.mis.mmsdk.common.widget.refreshview;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.canyinghao.canrefresh.CanRefresh;
import com.isoftstone.mis.mmsdk.R;

import java.text.DateFormat;
import java.util.Date;

/**
 * 经典的上拉加载、下拉刷新方式
 *
 * @author hubing
 * @version [1.0.0.0, 2016-08-30]
 */
public class MMClassicRefreshView extends FrameLayout implements CanRefresh {

    /** 是否是头部 */
    private boolean isHead;

    /** 头部箭头 */
    private ImageView ivHeadArrow;

    /** 上下拉动显示的提示信息控件 */
    private TextView tvRefreshHint;

    /** 头部刷新时间 */
    private TextView tvClassicHeaderTime;

    /** 头部刷新时间布局 */
    private LinearLayout llClassicHeaderTime;

    /** 刷新时显示的转进度控件 */
    private ProgressBar progressBar;

    /** 向上旋转的动画 */
    private Animation rotateUp;

    /** 向下旋转的动画 */
    private Animation rotateDown;

    /** 是否需要旋转 */
    private boolean rotated = false;

    /** 正在加载 */
    private String refreshingHint;

    /** 下拉刷新 */
    private String headerHintNormal;

    /** 松开刷新数据 */
    private String headerHintReady;

    /** 上拉加载更多 */
    private String footerHintNormal;

    /** 松开载入更多 */
    private String footerHintReady;

    /** 是否外部设置了时间 */
    private boolean isOutSetRefreshTime = false;

    public MMClassicRefreshView(Context context) {
        this(context, null);
    }

    public MMClassicRefreshView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MMClassicRefreshView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        // 加载箭头动画
        rotateUp = AnimationUtils.loadAnimation(context, com.canyinghao.canrefresh.R.anim.rotate_up);
        rotateDown = AnimationUtils.loadAnimation(context, com.canyinghao.canrefresh.R.anim.rotate_down);

        // 将头部/底部布局加载进来
        View v = LayoutInflater.from(getContext()).inflate(R.layout.ua_layout_classic_refresh, this, false);
        addView(v);

        // 初始化提示信息
        Resources resources = getResources();
        refreshingHint = resources.getString(R.string.ua_cr_classic_header_hint_loading);
        headerHintNormal = resources.getString(R.string.ua_cr_classic_header_hint_normal);
        headerHintReady = resources.getString(R.string.ua_cr_classic_header_hint_ready);
        footerHintNormal = resources.getString(R.string.ua_cr_classic_footer_hint_normal);
        footerHintReady = resources.getString(R.string.ua_cr_classic_footer_hint_ready);
    }

    /**
     * 设置头部正常显示的提示
     * 
     * @param headerHintNormal 头部正常显示的提示语
     */
    public void setHeaderHintNormal(String headerHintNormal) {
        this.headerHintNormal = headerHintNormal;
    }

    /**
     * 设置头部准备好刷新的提示
     * 
     * @param headerHintReady 头部准备好刷新的提示语
     */
    public void setHeaderHintReady(String headerHintReady) {
        this.headerHintReady = headerHintReady;
    }

    /**
     * 设置底部正常显示的提示
     * 
     * @param footerHintNormal 底部正常显示的提示语
     */
    public void setFooterHintNormal(String footerHintNormal) {
        this.footerHintNormal = footerHintNormal;
    }

    /**
     * 设置底部准备好刷新的提示
     *
     * @param footerHintReady 底部准备好刷新的提示语
     */
    public void setFooterHintReady(String footerHintReady) {
        this.footerHintReady = footerHintReady;
    }

    /**
     * 设置正在加载的提示
     * 
     * @param refreshingHint 正在加载的提示语
     */
    public void setRefreshingHint(String refreshingHint) {
        this.refreshingHint = refreshingHint;
    }

    /**
     * 设置头部刷新时间
     * 
     * @param refreshTime 刷新时间
     */
    public void setHeadRefreshTime(String refreshTime) {
        setHeadRefreshTime(refreshTime, true);
    }

    /**
     * 设置头部刷新时间
     *
     * @param date 刷新时间日期对象
     */
    public void setHeadRefreshTime(Date date) {
        setHeadRefreshTime(DateFormat.getDateTimeInstance().format(date));
    }

    /**
     * 设置头部刷新时间(内部调用)
     * 
     * @param refreshTime 刷新时间
     * @param isOutSetRefreshTime 是否外部设置刷新时间
     */
    private void setHeadRefreshTime(String refreshTime, boolean isOutSetRefreshTime) {
        if (isHead) {
            // 如果是头部才设置时间
            tvClassicHeaderTime.setText(refreshTime);
            this.isOutSetRefreshTime = isOutSetRefreshTime;
        }
    }

    /**
     * 设置头部刷新时间(内部调用)
     *
     * @param date 刷新时间日期对象
     * @param isOutSetRefreshTime 是否外部设置刷新时间
     */
    private void setHeadRefreshTime(Date date, boolean isOutSetRefreshTime) {
        setHeadRefreshTime(DateFormat.getDateTimeInstance().format(date), isOutSetRefreshTime);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        // 查找控件
        tvRefreshHint = (TextView) findViewById(R.id.ua_cr_classic_header_hint_textview);
        tvClassicHeaderTime = (TextView) findViewById(R.id.ua_cr_classic_header_time);
        ivHeadArrow = (ImageView) findViewById(R.id.ua_cr_classic_header_arrow);
        progressBar = (ProgressBar) findViewById(R.id.ua_cr_classic_header_progressbar);
        llClassicHeaderTime = (LinearLayout) findViewById(R.id.ua_cr_classic_refresh_time);
    }

    @Override
    public void onReset() {
        // 设置不需要旋转
        rotated = false;
        // 清除箭头上的动画
        ivHeadArrow.clearAnimation();
        // 隐藏箭头
        ivHeadArrow.setVisibility(GONE);
        // 隐藏进度条
        progressBar.setVisibility(GONE);
    }

    @Override
    public void onPrepare() {
    }

    @Override
    public void onComplete() {
        // 设置不需要旋转
        rotated = false;
        // 清除箭头上的动画
        ivHeadArrow.clearAnimation();
        // 隐藏箭头
        ivHeadArrow.setVisibility(GONE);
        // 隐藏进度条
        progressBar.setVisibility(GONE);
        // 设置默认刷新时间
        if (!isOutSetRefreshTime) {
            setHeadRefreshTime(new Date(), false);
        }
    }

    @Override
    public void onRelease() {
        // 设置不需要旋转
        rotated = false;
        // 清除箭头上的动画
        ivHeadArrow.clearAnimation();
        // 隐藏箭头
        ivHeadArrow.setVisibility(GONE);
        // 显示正在加载的进度条
        progressBar.setVisibility(VISIBLE);
        // 设置正在加载的提示
        tvRefreshHint.setText(refreshingHint);
    }

    @Override
    public void onPositionChange(float currentPercent) {
        progressBar.setVisibility(GONE);

        // 判断是头部还是底部
        if (isHead) {
            // 头部
            // 头部显示箭头
            ivHeadArrow.setVisibility(View.VISIBLE);
            // 判断下拉的距离
            if (currentPercent < 1) {
                if (rotated) {
                    ivHeadArrow.clearAnimation();
                    ivHeadArrow.startAnimation(rotateDown);
                    rotated = false;
                }
                // 设置下拉的提示语
                tvRefreshHint.setText(headerHintNormal);
            } else {
                tvRefreshHint.setText(headerHintReady);
                if (!rotated) {
                    ivHeadArrow.clearAnimation();
                    ivHeadArrow.startAnimation(rotateUp);
                    rotated = true;
                }
            }
        } else {
            // 底部
            // 判断上拉的距离
            if (currentPercent < 1) {
                // 设置上拉的提示语
                tvRefreshHint.setText(footerHintNormal);
            } else {
                tvRefreshHint.setText(footerHintReady);
            }
        }
    }

    @Override
    public void setIsHeaderOrFooter(boolean isHead) {
        this.isHead = isHead;
        // 判断是否是头部
        if (isHead) {
            // 头部显示箭头
            ivHeadArrow.setVisibility(View.VISIBLE);
            // 头部显示刷新时间布局
            llClassicHeaderTime.setVisibility(View.VISIBLE);

            // 设置初始的刷新时间
            if (!isOutSetRefreshTime) {
                setHeadRefreshTime(new Date(), false);
            }
        } else {
            ivHeadArrow.setVisibility(View.GONE);
            llClassicHeaderTime.setVisibility(View.GONE);
        }
    }

}
