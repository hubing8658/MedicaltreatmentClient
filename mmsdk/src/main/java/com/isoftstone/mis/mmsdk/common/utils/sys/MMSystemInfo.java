
package com.isoftstone.mis.mmsdk.common.utils.sys;

import android.content.Context;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import com.isoftstone.mis.mmsdk.common.utils.log.MMLog;

import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Locale;

/**
 * 手机系统相关的工具类
 *
 * @author yansu
 */
public class MMSystemInfo {

    /** tag */
    private static final String TAG = "MMSystemInfo";

    /**
     * 获取当前终端系统语言
     *
     * @return String：系统默认语言。例如：当前设置的是“中文-中国”，则返回“zh-CN”
     * @author yansu
     */
    public static String getDefaultLanguage() {
        return Locale.getDefault().getLanguage();
    }

    /**
     * 获取当前系统上的语言列表
     *
     * @return 系统支持的所有语言列表
     * @author qjcheng
     */
    public static ArrayList<String> getAvailableLanguageList() {
        Locale[] locales = getAvailableLocaleList();
        ArrayList<String> languageList = new ArrayList<>();
        for (Locale locale : locales) {
            if (locale != null) {
                String tempLan = locale.getLanguage();
                if (!TextUtils.isEmpty(tempLan)) {
                    languageList.add(tempLan);
                }
            }
        }
        return languageList;
    }

    /**
     * 获取当前系统上可用的Locale列表
     *
     * @return 系统可用的Locale列表
     * @author yansu
     */
    public static Locale[] getAvailableLocaleList() {
        return Locale.getAvailableLocales();
    }

    /**
     * 获取当前终端系统SDK版本号
     *
     * @return 系统SDK版本号
     * @author qjcheng
     */
    public static int getSDKVersion() {
        return android.os.Build.VERSION.SDK_INT;
    }

    /**
     * 获取当前手机系统版本号
     *
     * @return 系统版本号
     * @author yansu
     */
    public static String getSystemVersion() {
        return android.os.Build.VERSION.RELEASE;
    }

    /**
     * 获取终端设备名称
     *
     * @return 终端设备名称
     * @author qjcheng
     */
    public static String getDeviceName() {
        return android.os.Build.DEVICE;
    }

    /**
     * 获取终端型号
     *
     * @return 终端型号
     * @author yansu
     */
    public static String getModel() {
        return android.os.Build.MODEL;
    }

    /**
     * 获取终端设备厂商
     *
     * @return 终端设备厂商
     * @author yansu
     */
    public static String getDeviceBrand() {
        return android.os.Build.BRAND;
    }

    /**
     * 获取设备ID（IMEI/MEID）<br/>
     * 需要“android.permission.READ_PHONE_STATE”权限
     *
     * @param ctx 上下文
     * @return 设备ID数据，GSM网络返回IMEI，CDMA网络返回MEID
     * @author yansu
     */
    public static String getDeviceId(Context ctx) {
        TelephonyManager tm = (TelephonyManager) ctx.getSystemService(Context.TELEPHONY_SERVICE);
        if (tm != null) {
            return tm.getDeviceId();
        }
        return null;
    }

    /**
     * 获取手机wifi mac地址
     * 
     * @param context 上下文
     * @return 返回手机wifi mac地址
     */
    public static String getMacAddress(Context context) {
        try {
            Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
            while (interfaces.hasMoreElements()) {
                NetworkInterface networkInterface = interfaces.nextElement();
                if (networkInterface != null) {
                    byte[] macBytes = networkInterface.getHardwareAddress();
                    if (macBytes != null) {
                        StringBuilder sb = new StringBuilder();
                        for (int i = 0; i < macBytes.length; i++) {
                            if (i != 0) {
                                sb.append(":");
                            }
                            String sTemp = Integer.toHexString(0xFF & macBytes[i]);
                            if (sTemp.length() == 1) {
                                sb.append("0");
                            }
                            sb.append(sTemp);
                        }
                        return sb.toString();
                    }
                }
            }
        } catch (Exception e) {
            MMLog.et(TAG, e);
        }
        return null;
    }

    /**
     * 获取设备的SN号
     *
     * @param ctx 上下文
     * @return 设备的SN号
     */
    public static String getSerialNumber(Context ctx) {
        return Settings.Secure.getString(ctx.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    /**
     * 获取设备的SN号
     *
     * @return 设备的SN号
     */
    public static String getSerialNumber() {
        return android.os.Build.SERIAL;
    }

}
