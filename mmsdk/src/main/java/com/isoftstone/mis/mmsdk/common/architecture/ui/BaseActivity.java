
package com.isoftstone.mis.mmsdk.common.architecture.ui;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

import com.isoftstone.mis.mmsdk.common.architecture.presenter.IBasePresenter;
import com.isoftstone.mis.mmsdk.common.architecture.view.IBaseView;

/**
 * MVP模式Activity基类
 *
 * @author hubing
 * @version [1.0.0.0, 2016-07-12]
 */
public abstract class BaseActivity<P extends IBasePresenter> extends FragmentActivity implements IBaseView {

    /** Presenter对象 */
    protected P presenter;

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = createPresenter();
    }

    /**
     * 创建presenter类对象
     * 
     * @return presenter类对象
     */
    public abstract P createPresenter();

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onDestroy() {
        if (presenter != null) {
            presenter.onDetachView();
        }
        super.onDestroy();
    }

    /**
     * 展示给定资源的Toast提示
     *
     * @param resId The resource id of the string resource to use. Can be formatted text.
     */
    public void showToast(int resId) {
        Toast.makeText(this, resId, Toast.LENGTH_SHORT).show();
    }

    /**
     * 展示给定字符串的Toast提示
     *
     * @param text The text to show. Can be formatted text.
     */
    public void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

}
