
package cn.sharesdk.onekeyshare.entity;

import java.io.Serializable;

/**
 * 分享实体对象
 *
 * @author hubing
 * @version [1.0.0.0, 2017-06-05]
 */
public class ShareEntity implements Serializable {

    /** 标题的网络链接，仅在人人网和QQ空间使用，否则可以不提供 */
    public String titleUrl;

    /** 图片的网络路径，新浪微博、人人网、QQ空间和Linked-In支持此字段 */
    public String imageUrl;

    /** imageUrl是图片的网络路径，新浪微博、人人网、QQ空间和Linked-In支持此字段 */
    public String title;

    /** 分享文本描述，所有平台都需要这个字段 */
    public String description;

    /** url在微信（包括好友、朋友圈收藏）和易信（包括好友和朋友圈）中使用 */
    public String url;

    /** site是分享此内容的网站名称，仅在QQ空间使用，否则可以不提供 */
    public String site;

    /** 分享此内容的网站地址，仅在QQ空间使用，否则可以不提供 */
    public String siteUrl;

}
