package com.kelin.client.im.model;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.kelin.client.im.ChatAdapter;
import com.kelin.client.im.utils.EmoticonUtil;
import com.tencent.TIMElem;
import com.tencent.TIMElemType;
import com.tencent.TIMFaceElem;
import com.tencent.TIMMessage;
import com.tencent.TIMTextElem;
import com.ygbd198.hellodoctor.R;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * 文本消息数据
 */
public class TextMessage extends Message {

    /** 文本消息体 */
    private TextBody textBody;

    /**
     * 文本消息体对象
     */
    public static class TextBody {

        /** 消息类型 */
        public String type;

        /** 诊断id */
        public int diagnosticId = -1;

    }

    public TextMessage(TIMMessage message){
        this.message = message;

        // 解析文本内容
        /** 如果是纯文本，则尝试解析是否是{@link CustomMessage#REMINDER_PAY}类型消息 */
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < message.getElementCount(); ++i){
            TIMElem timElem = message.getElement(i);
            if (timElem.getType() != TIMElemType.Text){
                return;
            }
            TIMTextElem textElem = (TIMTextElem) timElem;
            sb.append(textElem.getText());
        }

        // 处理消息中单引号问题
        String textMsg = sb.toString();
        textMsg = textMsg.replace("\'", "\"");
        Gson gson = new Gson();
        try {
            textBody = gson.fromJson(textMsg, TextBody.class);
        } catch (JsonSyntaxException e) {
            // Ignore
        }
    }

    public TextMessage(String s){
        message = new TIMMessage();
        TIMTextElem elem = new TIMTextElem();
        elem.setText(s);
        message.addElement(elem);
    }

    private List<ImageSpan> sortByIndex(final Editable editInput, ImageSpan[]array){
        ArrayList<ImageSpan> sortList = new ArrayList<>(Arrays.asList(array));
        Collections.sort(sortList, new Comparator<ImageSpan>() {
            @Override
            public int compare(ImageSpan lhs, ImageSpan rhs) {
                return editInput.getSpanStart(lhs) - editInput.getSpanStart(rhs);
            }
        });

        return sortList;
    }

    public TextMessage(Editable s){
        message = new TIMMessage();
        ImageSpan[] spans = s.getSpans(0, s.length(), ImageSpan.class);
        List<ImageSpan> listSpans = sortByIndex(s, spans);
        int currentIndex = 0;
        for (ImageSpan span : listSpans){
            int startIndex = s.getSpanStart(span);
            int endIndex = s.getSpanEnd(span);
            if (currentIndex < startIndex){
                TIMTextElem textElem = new TIMTextElem();
                textElem.setText(s.subSequence(currentIndex, startIndex).toString());
                message.addElement(textElem);
            }
            TIMFaceElem faceElem = new TIMFaceElem();
            int index = Integer.parseInt(s.subSequence(startIndex, endIndex).toString());
            faceElem.setIndex(index);
            if (index < EmoticonUtil.emoticonData.length){
                faceElem.setData(EmoticonUtil.emoticonData[index].getBytes(Charset.forName("UTF-8")));
            }
            message.addElement(faceElem);
            currentIndex = endIndex;
        }
        if (currentIndex < s.length()){
            TIMTextElem textElem = new TIMTextElem();
            textElem.setText(s.subSequence(currentIndex, s.length()).toString());
            message.addElement(textElem);
        }
    }

    /**
     * 在聊天界面显示消息
     *
     * @param viewHolder 界面样式
     * @param context 显示消息的上下文
     */
    @Override
    public void showMessage(ChatAdapter.ViewHolder viewHolder, final Context context) {
        clearView(viewHolder);
        // 判断消息类型
        if (textBody != null && CustomMessage.REMINDER_PAY.equals(textBody.type))
        {
            View customView = View.inflate(context, R.layout.item_custom_pay_message, null);
            TextView tv_tips = (TextView) customView.findViewById(R.id.tv_tips);
            Button btn_pay = (Button) customView.findViewById(R.id.btn_pay);
            btn_pay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
            getBubbleView(viewHolder).addView(customView);
        } else {
            boolean hasText = false;
            TextView tv = new TextView(context);
            tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            tv.setTextColor(context.getResources().getColor(isSelf() ? R.color.white : R.color.black));
            List<TIMElem> elems = new ArrayList<>();
            for (int i = 0; i < message.getElementCount(); ++i){
                elems.add(message.getElement(i));
                if (message.getElement(i).getType() == TIMElemType.Text){
                    hasText = true;
                }
            }
            SpannableStringBuilder stringBuilder = getString(elems, context);
            if (!hasText){
                stringBuilder.insert(0," ");
            }
            tv.setText(stringBuilder);
            getBubbleView(viewHolder).addView(tv);
            showStatus(viewHolder);
        }
    }

    @Override
    public String getSummary() {
        return null;
    }

    /**
     * 保存消息或消息文件
     */
    @Override
    public void save() {

    }

    private static int getNumLength(int n){
        return String.valueOf(n).length();
    }


    public static SpannableStringBuilder getString(List<TIMElem> elems, Context context){
        SpannableStringBuilder stringBuilder = new SpannableStringBuilder();
        for (int i = 0; i<elems.size(); ++i){
            switch (elems.get(i).getType()){
                case Face:
                    TIMFaceElem faceElem = (TIMFaceElem) elems.get(i);
                    int startIndex = stringBuilder.length();
                    try{
                        AssetManager am = context.getAssets();
                        InputStream is = am.open(String.format("emoticon/%d.png", faceElem.getIndex()));
                        if (is == null) continue;
                        Bitmap bitmap = BitmapFactory.decodeStream(is);
                        Matrix matrix = new Matrix();
                        int width = bitmap.getWidth();
                        int height = bitmap.getHeight();
                        matrix.postScale(1.3F, 1.3F);
                        Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
                        ImageSpan span = new ImageSpan(context, resizedBitmap, ImageSpan.ALIGN_BOTTOM);
                        stringBuilder.append(String.valueOf(faceElem.getIndex()));
                        stringBuilder.setSpan(span, startIndex, startIndex + getNumLength(faceElem.getIndex()), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        is.close();
                    }catch (IOException e){

                    }
                    break;
                case Text:
                    TIMTextElem textElem = (TIMTextElem) elems.get(i);
                    stringBuilder.append(textElem.getText());
                    break;
            }

        }
        return stringBuilder;
    }


}
