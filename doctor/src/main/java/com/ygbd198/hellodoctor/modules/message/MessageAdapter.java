
package com.ygbd198.hellodoctor.modules.message;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.DoctorMessageEnt;
import com.ygbd198.hellodoctor.common.GeneralAdapterV2;
import com.ygbd198.hellodoctor.ui.LearningVideoActivity;

/**
 * 医生端消息界面adpter
 *
 * @author hubing
 * @version [1.0.0.0, 2017-10-22]
 */
public class MessageAdapter extends GeneralAdapterV2<DoctorMessageEnt> {

    /**
     * 构造函数
     *
     * @param ctx 上下文
     */
    public MessageAdapter(Context ctx) {
        super(ctx, null);
    }

    @Override
    public int getItemLayoutId(int itemViewType) {
        return R.layout.doctor_message_item;
    }

    @Override
    public void convert(ViewHolder holder, DoctorMessageEnt item, int position, int itemViewType) {
        TextView dateTv = holder.getView(R.id.tv_message_date);
        dateTv.setText(item.createTime);

        TextView titleTv = holder.getView(R.id.tv_message_title);
        titleTv.setText(item.title);

        TextView contentTv = holder.getView(R.id.tv_message_content);
        contentTv.setText(item.content);

        TextView operationTv = holder.getView(R.id.tv_message_operation);
        operationTv.setText(item.operation);

        operationTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent intent = new Intent(getContext(), ContactServiceActivity.class);
                intent.putExtra(ContactServiceActivity.KEY_TITLE_RESID, R.string.title_message);
                getContext().startActivity(intent);*/

                getContext().startActivity(new Intent(getContext(), LearningVideoActivity.class));
            }
        });
    }

}
