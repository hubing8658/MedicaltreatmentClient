
package com.ygbd198.hellodoctor.modules.message;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.monty.library.http.BaseCallModel;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.DoctorMessageEnt;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.modules.message.MessageContract.IMessagePresenter;
import com.ygbd198.hellodoctor.modules.message.MessageContract.IMessageView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;

/**
 * 消息页面presenter
 *
 * @author hubing
 * @version [1.0.0.0, 2017-10-22]
 */
public class MessagePresenter implements IMessagePresenter {

    private static final String TAG = "MessagePresenter";

    private Context context;

    private IMessageView messageView;

    private Call<BaseCallModel<ArrayList<DoctorMessageEnt>>> call;

    public MessagePresenter(Context context, IMessageView messageView) {
        this.context = context;
        this.messageView = messageView;
    }

    @Override
    public void getDoctorMessages(int pageNo, int pageSize) {
        // 暂时写死数据返回
        ArrayList<DoctorMessageEnt> data = new ArrayList<>(1);
        DoctorMessageEnt messageEnt = new DoctorMessageEnt();
        messageEnt.title = context.getString(R.string.message_system);
        messageEnt.content = context.getString(R.string.message_system_content);
        String createTime = MySelfInfo.getInstance().createTime;
        if (TextUtils.isEmpty(createTime)) {
            messageEnt.createTime = context.getString(R.string.message_date);
        } else {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                Date date = sdf.parse(createTime);
                sdf = new SimpleDateFormat("yyyy年MM月dd日", Locale.getDefault());
                messageEnt.createTime = sdf.format(date);
            } catch (Exception e) {
                messageEnt.createTime = context.getString(R.string.message_date);
            }
        }

        messageEnt.operation = context.getString(R.string.message_operation);
        data.add(messageEnt);
        messageView.onLoadMessagesSuccess(data);

        String token = MySelfInfo.getInstance().getToken();
        if (TextUtils.isEmpty(token)) {
            Log.i(TAG, "User token is null or empty.");
            messageView.onLoadMessagesFailed();
            return;
        }
        if (call != null && ! call.isCanceled()) {
            call.cancel();
        }
//        call = RetrofitHelper.getInstance().createService(MessageService.class).getDoctorMessages(token, pageNo, pageSize);
//        call.enqueue(new BaseCallBack<BaseCallModel<ArrayList<DoctorMessageEnt>>>() {
//            @Override
//            public void onSuccess(Response<BaseCallModel<ArrayList<DoctorMessageEnt>>> response) {
//                BaseCallModel<ArrayList<DoctorMessageEnt>> callMode = response.body();
//                if (callMode != null) {
//                    messageView.onLoadMessagesSuccess(callMode.data);
//                } else {
//                    messageView.onLoadMessagesFailed();
//                }
//            }
//
//            @Override
//            public void onFailure(String message) {
//                messageView.onLoadMessagesFailed();
//            }
//        });
    }

    @Override
    public void release() {
        if (call != null && ! call.isCanceled()) {
            call.cancel();
            call = null;
        }
    }

}
