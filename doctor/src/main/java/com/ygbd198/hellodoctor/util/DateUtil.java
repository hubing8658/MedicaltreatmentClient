package com.ygbd198.hellodoctor.util;

/**
 * Created by guangjiqin on 2017/8/23.
 */

public class DateUtil {

    public static String secondToMinute(long second) {
        String timeStr = "";
        int min = (int) (second / 60);
        int s = (int) (second % 60);
        timeStr = min + "分" + s + "秒";
        return timeStr;
    }
}
