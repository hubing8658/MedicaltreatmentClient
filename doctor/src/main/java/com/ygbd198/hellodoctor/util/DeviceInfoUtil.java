package com.ygbd198.hellodoctor.util;

import android.app.AppOpsManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Binder;
import android.os.Build;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.TypedValue;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.Reader;
import java.lang.reflect.Method;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

/**
 * 设备信息
 *
 * @author kelin
 */
public class DeviceInfoUtil {

    /**
     * os_version [功能详细描述]
     *
     * @param context
     * @return
     */
    public static String getOsversion(Context context) {
        if (TextUtils.isEmpty(Build.VERSION.RELEASE)) {
            return "";
        } else {

            return Build.VERSION.RELEASE;
        }
    }

    /**
     * 设备名称<BR>
     * [功能详细描述]
     *
     * @param context
     * @return
     */
    public static String getDevicename(Context context) {
        if (TextUtils.isEmpty(Build.MODEL)) {
            return "";
        } else {
            return Build.MODEL;
        }
    }

    /**
     * 获取设备厂商
     *
     * @return
     */
    private static String getManufacturer() {
        if (TextUtils.isEmpty(Build.MANUFACTURER)) {
            return "";
        } else {
            return Build.MANUFACTURER;
        }
    }

    /**
     * 设备是否是小米
     *
     * @return
     */
    public static boolean isXiaoMiDevice() {
        if ("Xiaomi".equalsIgnoreCase(getManufacturer())) {
            return true;
        }
        return false;
    }

    /**
     * 设备是否是华为
     *
     * @return
     */
    public static boolean isHuaWeiDevice() {
        if ("Huawei".equalsIgnoreCase(getManufacturer())) {
            return true;
        }
        return false;
    }

    /**
     * 获取IMEI(设备唯一码), 位数为15或14位时, 才返回IMEI, 否则返回"0"
     *
     * @param mContext
     * @return
     */
    public static String getIMEI(Context mContext) {
        if (null == mContext) {
            return "";
        }
        String did = "0";
        try {
            //mobile 获取设备id
            did = getMobileDeviceId(mContext);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return did;
    }


    /**
     * 获取手机设备id
     *
     * @param mContext
     * @return
     */
    public static String getMobileDeviceId(Context mContext) {
        if (null == mContext) {
            return "";
        }
        String did = null;
        try {
            TelephonyManager tm = (TelephonyManager) mContext
                    .getSystemService(Context.TELEPHONY_SERVICE);
            did = tm.getDeviceId();
        } catch (Exception e) {
            // 捕获位置异常
            e.printStackTrace();
            return "0";
        }
        if (TextUtils.isEmpty(did)) {
            return "0";
        } else {
            Pattern imei_15 = Pattern.compile("^\\d{15}$");// IMEI为15位数字
            Pattern imei_14 = Pattern.compile("^\\d{14}$");// IMEI为14位数字
            if (imei_15.matcher(did).matches()
                    || imei_14.matcher(did).matches()) {
                return did;
            } else {
                return "0";
            }
        }
    }

    /**
     * 获取IMSI
     *
     * @param mContext
     * @return
     */
    public static String getIMSI(Context mContext) {
        if (null == mContext) {
            return "";
        }
        try {
            TelephonyManager tm = (TelephonyManager) mContext
                    .getSystemService(Context.TELEPHONY_SERVICE);
            if (TextUtils.isEmpty(tm.getSimSerialNumber())) {
                return "";
            } else {

                return tm.getSimSerialNumber();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * MAC地址 2c:d9:89:66:a5<BR>
     * [功能详细描述]
     *
     * @param context
     * @return
     */
    public static String getMacAddress(Context context) {
        if (null == context) {
            return "";
        }
        String mac;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mac = getMacAddressOnAndroid6();
        } else {
            mac = getMacAddressOnDefault(context);
        }

        if (TextUtils.isEmpty(mac)) {
            return "0:0:0:0:0:0";
        }
        return mac;
    }

    /**
     * 获取MacAddress
     *
     * @param context
     * @return
     */
    public static String getMacAddressOnDefault(Context context) {
        WifiManager wifi = (WifiManager) context.getApplicationContext()
                .getSystemService(Context.WIFI_SERVICE);
        String mac = "";
        try {
            if (null != wifi && null != wifi.getConnectionInfo()
                    && null != wifi.getConnectionInfo().getMacAddress()) {
                mac = wifi.getConnectionInfo().getMacAddress();
            }
        } catch (Exception e) {
            // 部分系统wifi.getConnectionInfo()内部引起的java.lang.NullPointerException:
            // name == null
        }
        return mac;
    }

    /**
     * Android 6.0 获取MacAddress
     *
     * @return
     */
    private static String getMacAddressOnAndroid6() {
        String str = "";
        String macSerial = "";
        try {
            Process pp = Runtime.getRuntime().exec(
                    "cat /sys/class/net/wlan0/address ");
            InputStreamReader ir = new InputStreamReader(pp.getInputStream());
            LineNumberReader input = new LineNumberReader(ir);

            for (; null != str; ) {
                str = input.readLine();
                if (str != null) {
                    macSerial = str.trim();// 去空格
                    break;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if (macSerial == null || "".equals(macSerial)) {
            try {
                return loadFileAsString("/sys/class/net/eth0/address")
                        .toLowerCase().substring(0, 17);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return null;
    }

    private static String loadFileAsString(String fileName) throws Exception {
        FileReader reader = new FileReader(fileName);
        String text = loadReaderAsString(reader);
        reader.close();
        return text;
    }

    private static String loadReaderAsString(Reader reader) throws Exception {
        StringBuilder builder = new StringBuilder();
        char[] buffer = new char[4096];
        int readLength = reader.read(buffer);
        while (readLength >= 0) {
            builder.append(buffer, 0, readLength);
            readLength = reader.read(buffer);
        }
        return builder.toString();
    }

    /**
     * 获取运营商名称
     *
     * @param mContext
     * @return
     */
    public static String getSimOperatorName(Context mContext) {
        if (null == mContext) {
            return "";
        }
        TelephonyManager tm = (TelephonyManager) mContext
                .getSystemService(Context.TELEPHONY_SERVICE);
        if (TextUtils.isEmpty(tm.getSimOperatorName())) {
            return "";
        } else {
            try {
                return tm.getSimOperatorName();
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        }
    }

    /**
     * 判断网络类型
     *
     * @param context
     * @return
     */
    public static String getNetType(Context context) {
        if (null == context) {
            return "";
        }
        String netType = "";
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        if (info != null && info.isConnected()) {
            if (info.getType() == ConnectivityManager.TYPE_WIFI) {
                // WIFI网络
                netType = "wifi";
            } else if (info.getType() == ConnectivityManager.TYPE_MOBILE) {
                // 流量
                if (info.getSubtype() == TelephonyManager.NETWORK_TYPE_GPRS
                        || info.getSubtype() == TelephonyManager.NETWORK_TYPE_CDMA
                        || info.getSubtype() == TelephonyManager.NETWORK_TYPE_CDMA
                        || info.getSubtype() == TelephonyManager.NETWORK_TYPE_1xRTT
                        || info.getSubtype() == TelephonyManager.NETWORK_TYPE_IDEN) {
                    netType = "2g";
                } else {
                    netType = "3G/4G";
                }
            }
        }
        return netType;
    }


    /**
     * JPID使用 DEVICE_ID + MAC地址 + Serial Number序列号 方式生成
     *
     * @param context
     * @return JPID
     */
    public static String getDeviceId(Context context) {
        //        String wifiMac = "";
        //        try {
        //            // wifi mac地址
        //            WifiManager wifi = (WifiManager) context
        //                    .getSystemService(Context.WIFI_SERVICE);
        //            WifiInfo info = wifi.getConnectionInfo();
        //            wifiMac = info.getMacAddress(); // 8c:be:be:76:f4:64
        //        } catch (Exception e) {
        //        }

        String wifiMac = getMacAddress(context);
        if (TextUtils.isEmpty(wifiMac) || "0:0:0:0:0:0".equals(wifiMac)) {
            wifiMac = "0";
        }

        String imei = "";
        try {
            // IMEI
            TelephonyManager tm = (TelephonyManager) context
                    .getSystemService(Context.TELEPHONY_SERVICE);
            imei = tm.getDeviceId(); // 863360023829835
        } catch (Exception e) {
        }
        if (TextUtils.isEmpty(imei)) {
            imei = "0";
        }

        String sn = "";
        try {
            // 序列号（sn）
            TelephonyManager tm = (TelephonyManager) context
                    .getSystemService(Context.TELEPHONY_SERVICE);
            sn = tm.getSimSerialNumber(); // 89860111871010583772
        } catch (Exception e) {
        }
        if (TextUtils.isEmpty(sn)) {
            sn = "0";
        }

        if ("0".equals(wifiMac) && "0".equals(imei) && "0".equals(sn)) {
            return randomJPID();
        }
        try {
            UUID deviceUuid = new UUID(wifiMac.hashCode(),
                    ((long) imei.hashCode() << 32) | sn.hashCode());
            String JPid = deviceUuid.toString(); // 00000000-65c6-f59e-9261-0b5440ba79bf
            return JPid;
        } catch (Exception e) {
            return randomJPID();
        }
    }

    /**
     * 随机生成JPID
     *
     * @return
     */
    private static String randomJPID() {
        return "self_" + UUID.randomUUID().toString();
    }

    /**
     * 获取手机安装应用列表（非系统应用）
     *
     * @param context
     * @return
     */
    public static String getInstallAppList(Context context) {
        List<PackageInfo> pakageinfos = null;
        try {
            PackageManager pm = context.getPackageManager();
            pakageinfos = pm.getInstalledPackages(
                    PackageManager.GET_UNINSTALLED_PACKAGES);
        } catch (Exception e) {
            e.printStackTrace();
        }
        JSONArray array = new JSONArray();
        if (null == pakageinfos) {
            return array.toString();
        }
        for (PackageInfo packageInfo : pakageinfos) {
            if (filterApp(packageInfo.applicationInfo)) {
                try {
                    JSONObject json = new JSONObject();
                    json.put("packagename", packageInfo.packageName);
                    json.put("firstinstall", packageInfo.firstInstallTime);
                    json.put("lastupdate", packageInfo.lastUpdateTime);
                    array.put(json);
                } catch (Exception e) {
                }
            }

        }
        MyLog.i("", "appList：" + array.toString());
        return array.toString();
    }

    /**
     * 三方应用程序的过滤器
     *
     * @param info
     * @return true 三方应用 false 系统应用
     */
    private static boolean filterApp(ApplicationInfo info) {
        if ((info.flags & ApplicationInfo.FLAG_SYSTEM) == 0) {
            // 代表的用户的应用
            return true;
        } else if ((info.flags & ApplicationInfo.FLAG_UPDATED_SYSTEM_APP) != 0) {
            // 代表的是系统的应用,但是被用户升级了. 用户应用
            return true;
        }
        return false;
    }

    /**
     * 判断当前手机是否有ROOT权限
     *
     * @return
     */
    public static boolean isRoot() {
        boolean bool = false;

        try {
            if ((!new File("/system/bin/su").exists())
                    && (!new File("/system/xbin/su").exists())) {
                bool = false;
            } else {
                bool = true;
            }
            MyLog.print("bool = " + bool);
        } catch (Exception e) {

        }
        return bool;
    }

    /**
     * 判断是否开启浮窗权限,api未公开，使用反射调用
     * api小于21，即5.0以下没有管理浮窗权限入口，直接返回开启true
     *
     * @return true：开启  false：关闭
     */
    public static boolean hasAuthorFloatWin(Context context) {

        if (Build.VERSION.SDK_INT < 21) {
            return true;
        }
        try {
            AppOpsManager appOps = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
            Class c = appOps.getClass();
            Class[] cArg = new Class[3];
            cArg[0] = int.class;
            cArg[1] = int.class;
            cArg[2] = String.class;
            Method lMethod = c.getDeclaredMethod("checkOp", cArg);
            //24是浮窗权限的标记
            return (AppOpsManager.MODE_ALLOWED == (Integer) lMethod.invoke(appOps, 24, Binder.getCallingUid(), context.getPackageName()));
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 根据手机的分辨率从 dp 的单位 转成为 px(像素)
     */
    public static int dip2px(Context context, float d) {
        DisplayMetrics dm = context.getApplicationContext().getResources().getDisplayMetrics();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, d,
                dm);
    }

    /**
     * 根据手机的分辨率从 px(像素) 的单位 转成为 dp
     */
    public static int px2dip(Context context, float d) {
        DisplayMetrics dm = context.getApplicationContext().getResources().getDisplayMetrics();
        return (int) Math.ceil(((d * 160) / dm.densityDpi));
    }


    /**
     * 判断是否有SDcard
     *
     * @return
     */
    public static boolean hasSDCard() {
        String status = Environment.getExternalStorageState();
        if (status.equals(Environment.MEDIA_MOUNTED)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 获取手机设备宽度
     */
    public  static int getWidth(Context context) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        if (dm.widthPixels > dm.heightPixels) {
            return dm.heightPixels;
        } else {
            return dm.widthPixels;
        }
    }

    /**
     * 获取手机设备高度
     */
    public  static int getHeight(Context context) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        if (dm.widthPixels > dm.heightPixels) {
            return dm.widthPixels;
        } else {
            return dm.heightPixels;
        }
    }

    /**
     * 获取屏幕分辨率
     *
     * @param context
     * @return
     */
    public static String getMobileResolution(Context context) {
        int height = getHeight(context);
        int width = getWidth(context);
        return height + "x" + width;
    }
}
