package com.ygbd198.hellodoctor.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.ygbd198.hellodoctor.common.Constants;

import static com.ygbd198.hellodoctor.util.PreferencesManager.getString;

/**
 * Created by Administrator on 2017/8/2 0002.
 */

public class SharePreferencesHelp {

    private static SharedPreferences getSp(Context context) {
        return context.getSharedPreferences(Constants.spName, Context.MODE_PRIVATE);
    }


    public static void putString(Context context, String key, String value) {
        SharedPreferences sp = getSp(context);
        sp.edit().putString(key, value).commit();
    }

    public static String gettString(Context context, String key) {
        SharedPreferences sp = getSp(context);
        return  sp.getString(key,"");
    }

    public static void putInt(Context context, String key, int value) {
        SharedPreferences sp = getSp(context);
        sp.edit().putInt(key, value).commit();
    }

    public static int gettInt(Context context, String key) {
        SharedPreferences sp = getSp(context);
        return sp.getInt(key,-1);
    }

    public static void putLong(Context context, String key, Long value) {
        SharedPreferences sp = getSp(context);
        sp.edit().putLong(key, value).commit();
    }

    public static Long putLong(Context context, String key) {
        SharedPreferences sp = getSp(context);
        return sp.getLong(key,-1);
    }

    public static void putBoolean(Context context, String key, Boolean value) {
        SharedPreferences sp = getSp(context);
        sp.edit().putBoolean(key, value).commit();
    }

    public static boolean putBoolean(Context context, String key) {
        SharedPreferences sp = getSp(context);
        return sp.getBoolean(key, false);
    }

    public static void removeValue(Context context,String key){
        SharedPreferences sp = getSp(context);
        sp.edit().remove(key).commit();
    }
}
