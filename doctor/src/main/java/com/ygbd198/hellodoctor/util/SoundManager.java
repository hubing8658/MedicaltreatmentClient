package com.ygbd198.hellodoctor.util;

import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;

import com.ygbd198.hellodoctor.APP;
import com.ygbd198.hellodoctor.R;

import java.io.IOException;

import static android.content.Context.VIBRATOR_SERVICE;

/**
 * ${Description}
 *
 * @author monty
 * @date 2017/12/6
 */

public class SoundManager {
    private MediaPlayer mediaPlayer;
    private Vibrator vibrator;

    /**
     * 播放铃声和震动
     */
    public void playRingtoneAndVibrator() {
        playRingtone();
        startVibrator();
    }

    /**
     * 停止铃声和震动
     */
    public void stopRingtoneAndVibrator() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
        if (vibrator != null) {
            vibrator.cancel();
            vibrator = null;
        }
    }

    /**
     * 播放短信铃声
     *
     * @throws IOException
     */
    public void playNotification() {
        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setDataSource(APP.getInstance(), RingtoneManager
                    .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
            mediaPlayer.prepare();
            mediaPlayer.start();
            mediaPlayer.setLooping(true); //循环播放

            vibrator = (Vibrator) APP.getInstance().getSystemService(VIBRATOR_SERVICE);
            //按照指定的模式去震动。
            //vibrator.vibrate(1000);
            //数组参数意义：第一个参数为等待指定时间后开始震动，震动时间为第二个参数。后边的参数依次为等待震动和震动的时间
            //第二个参数为重复次数，-1为不重复，0为一直震动
            vibrator.vibrate(new long[]{1000, 1000}, 0);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * 播放来电铃声
     *
     * @throws IOException
     */
    public void playRingtone() {
        try {
            mediaPlayer = new MediaPlayer();
            Uri uri = Uri.parse("android.resource://" + APP.getInstance().getPackageName() + "/raw/" + R.raw.call);
            mediaPlayer.setDataSource(APP.getInstance(), uri);
            mediaPlayer.prepare();
            mediaPlayer.start();
            mediaPlayer.setLooping(true); //循环播放
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 震动
     */
    public void startVibrator() {
        vibrator = (Vibrator) APP.getInstance().getSystemService(VIBRATOR_SERVICE);
        //按照指定的模式去震动。
        //vibrator.vibrate(1000);
        //数组参数意义：第一个参数为等待指定时间后开始震动，震动时间为第二个参数。后边的参数依次为等待震动和震动的时间
        //第二个参数为重复次数，-1为不重复，0为一直震动
        vibrator.vibrate(new long[]{1000, 1000}, 0);
    }
}
