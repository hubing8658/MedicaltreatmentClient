package com.ygbd198.hellodoctor.common;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.BaseAdapter;


import java.util.List;

/**
 * Created by guangjiqin on 2017/8/23.
 */

public abstract class MyBaseAdapter<T> extends BaseAdapter {
    protected Context mContext;
    protected List<T> mData;
    protected LayoutInflater mLayoutInflater;

    public MyBaseAdapter(Context context,List<T> data) {
        this.mData = data;
        this.mContext = context;
        this.mLayoutInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public T getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setData(List<T> data, boolean isClear) {
        if (isClear) {
            mData.clear();
        }
        if (null != data) {
            mData.addAll(data);

        }
        notifyDataSetChanged();

    }

    public void addData(List<T> data) {
        if (null != data && data.size() > 0) {
            mData.addAll(data);
            notifyDataSetChanged();
        }

    }

}
