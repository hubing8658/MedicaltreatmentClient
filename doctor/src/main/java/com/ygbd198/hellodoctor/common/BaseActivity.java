package com.ygbd198.hellodoctor.common;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.monty.library.AppManager;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.widget.BaseTitleBar;
import com.ygbd198.hellodoctor.widget.dialog.LoadingDialog;

/**
 * Created by kelin on 2017/6/9.
 * activity基类
 */

public abstract class BaseActivity extends AppCompatActivity {
    protected LoadingDialog dialog;

    protected BaseTitleBar titleBar;
//
    protected LinearLayout rootView;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_base);
        AppManager.getAppManager().addActivity(this);
        titleBar = (BaseTitleBar)findViewById(R.id.titlebar);
        rootView = (LinearLayout)findViewById(R.id.content_view);
    }

    public void setContentView(@LayoutRes int layoutRes){
        View view = LayoutInflater.from(this).inflate(layoutRes, null);
        setContentView(view);

    }

    public void setContentView(View view){
        rootView.addView(view);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dialog != null) {
            dialog.close();
            dialog = null;
        }
        AppManager.getAppManager().removeActivity(this);
    }

    public void showLoadingDialog(String msg) {
        if (dialog == null) {
            dialog = new LoadingDialog(this);
        }
        dialog.show(msg);
    }

    public void closeLoadingDialog() {
        if(dialog!=null){
            dialog.close();
        }
    }

    protected void initView(){}
    protected void initData(){}
}
