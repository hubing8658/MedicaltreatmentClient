package com.ygbd198.hellodoctor.service;

import com.monty.library.http.BaseCallModel;
import com.ygbd198.hellodoctor.bean.AgreenDataBean;
import com.ygbd198.hellodoctor.bean.DiseaseTypeBean;
import com.ygbd198.hellodoctor.bean.DoctorBean;
import com.ygbd198.hellodoctor.bean.DoctorRoleBean;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by guangjiqin on 2017/8/19.
 */

public interface ApplyBeDocService {

    /**
     * 完善医生资料
     *
     * @param token true string 唯一标识
     *              name true string 姓名
     *              gender true string 性别
     *              education true string 学历
     *              job true string 学术职务
     *              good true string 专长
     *              count false string 0 诊疗人数
     *              introduction true string 个人简介
     * @return
     */
    @FormUrlEncoded
    @POST("doctor/perfectInfo")
    Call<BaseCallModel<Object>> perfectInfo(@Field("token") String token,
                                            @Field("name") String name,
                                            @Field("gender") String gender,
                                            @Field("education") String education,
                                            @Field("job") String job,
                                            @Field("good") String good,
                                            @Field("count") String count,
                                            @Field("introduction") String introduction);

    /**
     * 申请医生获取验证码
     *
     * @param phone
     * @return
     */
    @GET("login/getCodeByApply")
    Call<BaseCallModel<String>> getVerifyCode(@Query("phone") String phone);

    /**
     * 注册成为医生
     *
     * @param phone
     * @param passwrod
     * @param code     短信验证码
     * @return
     */
    @FormUrlEncoded
    @POST("login/doctorApply")
    Call<BaseCallModel<DoctorBean>> doctorApply(@Field("phone") String phone, @Field("password") String passwrod, @Field("code") String code);

    /**
     * 提交申请资料
     * @param 很多
     * @return
     */
    // TODO: 2017/8/20

    /**
     * 获取治病患者类型
     *
     * @param
     * @return
     */
    @GET("treatmentType/getAll")
    Call<BaseCallModel<List<DiseaseTypeBean>>> getDiseaseType(@Query("token") String token);


    /**
     * 获取医生等级
     *
     * @param
     * @return
     */
    @GET("getRole")
    Call<BaseCallModel<List<DoctorRoleBean>>> getDoctorRole(@Query("token") String token);



    /**
     * 自我介绍视频
     *
     * @param token file
     *              type  （1-头像,2-工作证,3-身份证,4-医师资历,5-坐诊照片,7-诊断照片）
     * @return 上传文件的地址
     */
    @Multipart
    @POST("hello-doctor/uploadFile")
    Call<BaseCallModel<List<String>>> doctorVideoUpload(@Part List<MultipartBody.Part> partList);


    /**
     * 保存自我介绍视频
     *

     * @return
     */
    @FormUrlEncoded
    @POST("doctor/saveIntroduceUrl")
    Call<BaseCallModel<Object>> saveIntroduceUrl(@Field("token") String token, @Field("url") String url);


    /**
     * 文件上传接口
     *
     * @param token file
     *              type  （1-头像,2-工作证,3-身份证,4-医师资历,5-坐诊照片,7-诊断照片）
     * @return 上传文件的地址
     */
    @Multipart
    @POST("doctor/uploadFile")
    Call<BaseCallModel<List<String>>> doctorUpload(@Part List<MultipartBody.Part> partList);


    /**
     * 提交医生资料
     * <p>
     * true／false标示必填或非必填
     *
     * @param 参数真jb多 token true string 唯一标识token
     *               name true string 医生名称
     *               localHospital true string 所在医院
     *               roleId true number 职称ID
     *               treatmentTypeId true number 可坐诊类型ID
     *               monday false string 坐诊时间-星期一
     *               tuesday false string 坐诊时间-星期二
     *               wednesday false string 坐诊时间-星期三
     *               thursday false string 坐诊时间-星期四
     *               friday false string 坐诊时间-星期五
     *               saturday false string 坐诊时间-星期六
     *               sunday false string 坐诊时间-星期天
     *               workCertificate true string 工作证,多个地址用逗号隔开
     *               identityCard true string 身份证,多个地址用逗号隔开
     *               physicianCertificate true string 医师资历证,多个地址用逗号隔开
     */
    @FormUrlEncoded
    @POST("doctor/applyInfo")
    Call<BaseCallModel<Object>> commitDoctorInf(@Field("token") String token,
                                                @Field("name") String name,
                                                @Field("localHospital") String localHospital,
                                                @Field("roleId") int roleId,
                                                @Field("treatmentTypeIds") String treatmentTypeId,
                                                @Field("monday") String monday,
                                                @Field("tuesday") String tuesday,
                                                @Field("wednesday") String wednesday,
                                                @Field("thursday") String thursday,
                                                @Field("friday") String friday,
                                                @Field("saturday") String saturday,
                                                @Field("sunday") String sunday,
                                                @Field("workCertificate") String workCertificate,
                                                @Field("identityCard") String identityCard,
                                                @Field("physicianCertificate") String physicianCertificate
    );


    /**
     * 获取成为医生的协议内容
     *
     * @param type   1-医生注册协议、2-患者注册协议、3-功能介绍、4-如何视频问诊、5-视频操作指导、6-关于我们
     * @param pageNo  页码  不传递获取全部
     * @return
     */
    @GET("getContent")
    Call<BaseCallModel<AgreenDataBean>> getTermsContent(@Query("token") String token, @Query("type") int type);


    /**
     * 同意医生条款内容
     *
     * @param token
     * @return
     */
    @GET("doctor/signAgreement")
    Call<BaseCallModel<String>> agreeTerms(@Query("token") String token);

    /**
     * 通过实操演练
     *
     * @param token
     * @return
     */
    @FormUrlEncoded
    @POST("doctor/practicePass")
    Call<BaseCallModel<String>> practicePass(@Field("token") String token);


}

