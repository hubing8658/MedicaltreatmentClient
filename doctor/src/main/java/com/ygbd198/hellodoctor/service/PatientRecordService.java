package com.ygbd198.hellodoctor.service;

import com.monty.library.http.BaseCallModel;
import com.ygbd198.hellodoctor.bean.PatientRecordBean;
import com.ygbd198.hellodoctor.bean.PatientRecordDetailsBean;
import com.ygbd198.hellodoctor.bean.PatientRecordIntegralBean;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by guangjiqin on 2017/9/15.
 */

public interface PatientRecordService  {

    /**
     * 患者记录接口
     *
     * @param token
     * @param
     * @return
     */
    @GET("diagnostic/getRecords")
    Call<BaseCallModel<List<PatientRecordBean>>> getRecords(@Query("token") String token);


    /**
     * 患者积分记录接口
     *
     * @param token
     * @param
     * @return
     */
    @GET("diagnostic/getRecordByUser")
    Call<BaseCallModel<List<PatientRecordIntegralBean>>> getRecordByUser(@Query("token") String token,@Query("userId") String userId);


    /**
     * 患者详情接口
     *
     * @param token
     * @param
     * @return
     */
    @GET("diagnostic/getRecordInfo")
    Call<BaseCallModel<PatientRecordDetailsBean>> getRecordInfo(@Query("token") String token, @Query("diagnosticId") long diagnosticId);
}
