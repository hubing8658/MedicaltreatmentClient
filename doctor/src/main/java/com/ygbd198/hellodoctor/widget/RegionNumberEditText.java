package com.ygbd198.hellodoctor.widget;

/**
 * Created by guangjiqin on 2017/9/6.
 */

import android.content.Context;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;

import com.ygbd198.hellodoctor.util.ToastUtils;

/**
 * Created by Mona.
 * 限制输入数字的范围
 */
public class RegionNumberEditText extends android.support.v7.widget.AppCompatEditText{

    private Context context;
    private int max;
    private int min;

    public void setInfTextChange(InfTextChange infTextChange) {
        this.infTextChange = infTextChange;
    }

    private InfTextChange infTextChange;


    public RegionNumberEditText(Context context) {
        super(context);
        this.context = context;
    }

    public RegionNumberEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    public RegionNumberEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
    }

    /**
     * 设置输入数字的范围
     *
     * @param maxNum 最大数
     * @param minNum 最小数
     */
    public void setRegion(int maxNum, int minNum) {
        setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_NORMAL);
        this.max = maxNum;
        this.min = minNum;
    }

    public void setTextWatcher() {
        addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (start >= 0) {//从一输入就开始判断，
                    if (min != -1 && max != -1) {
                        try {
                            int num = Integer.parseInt(s.toString());
                            //判断当前edittext中的数字(可能一开始Edittext中有数字)是否大于max
                            if (num > max) {
                                s = String.valueOf(max);//如果大于max，则内容为max
                                setText(s);
                                ToastUtils.showToast("兑换积分不能超过" + max + "分");

                            } else if (num < min) {
                                s = String.valueOf(min);//如果小于min,则内容为min
                                setText(s);
                                ToastUtils.showToast("兑换积分不能少于" + min + "分");
                            }
                        } catch (NumberFormatException e) {
                            s = String.valueOf(0);//如果小于min,则内容为min
                            setText(s);
                        }
                        infTextChange.textChange(s.toString());
                        return;
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


   public interface InfTextChange {
        void textChange(String s);
    }
}