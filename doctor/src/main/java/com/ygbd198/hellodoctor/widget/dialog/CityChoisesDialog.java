package com.ygbd198.hellodoctor.widget.dialog;

import android.content.Context;
import android.content.res.AssetManager;
import android.support.v4.app.FragmentManager;
import android.view.View;

import com.bigkoo.pickerview.OptionsPickerView;
import com.monty.library.widget.dialog.BaseBottomDialog;
import com.ygbd198.hellodoctor.bean.ProvinceBean;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;


/**
 * Created by monty on 2017/7/25.
 */

public class CityChoisesDialog extends BaseBottomDialog {
    private ArrayList<ProvinceBean> options1Items = new ArrayList<>();
    private ArrayList<ArrayList<String>> options2Items = new ArrayList<>();
    private OptionsPickerView pvOptions;
    private Thread thread;
    private static final int MSG_LOAD_DATA = 0x0001;
    private static final int MSG_LOAD_SUCCESS = 0x0002;
    private static final int MSG_LOAD_FAILED = 0x0003;

    public Context mContext;

    public CityChoisesDialog(){

    }
    /*public CityChoisesDialog(Context context){
        mContext = context;

    }*/
    @Override
    public int getLayoutRes() {
//        return R.base_dilog_layout.dialog_city_choise;
        return -1;
    }

    @Override
    public void bindView(View v) {
        pvOptions.show(v,false);
    }

    @Override
    public void show(FragmentManager fragmentManager) {

        super.show(fragmentManager);
    }

    public String getJson(Context context, String fileName) {

        StringBuilder stringBuilder = new StringBuilder();
        try {
            AssetManager assetManager = context.getAssets();
            BufferedReader bf = new BufferedReader(new InputStreamReader(
                    assetManager.open(fileName)));
            String line;
            while ((line = bf.readLine()) != null) {
                stringBuilder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }
}
