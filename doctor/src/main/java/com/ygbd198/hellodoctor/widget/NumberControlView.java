package com.ygbd198.hellodoctor.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ygbd198.hellodoctor.R;

/**
 * Created by monty on 2017/8/27.
 */

public class NumberControlView extends LinearLayout {
    private int currentNumber = 0;
    private TextView centerText;
    private TextView leftText;
    private TextView rightText;

    public NumberControlView(Context context) {
        super(context);
        initView();
    }

    public NumberControlView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public NumberControlView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        inflate(getContext(), R.layout.view_number_control, this);
        leftText = (TextView) findViewById(R.id.left);
        centerText = (TextView) findViewById(R.id.center);
        rightText = (TextView) findViewById(R.id.right);

        leftText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentNumber > 0) {
                    centerText.setText("" + (--currentNumber));
                }
                if (onNumberChangeListener != null) {
                    onNumberChangeListener.onNumberChange(currentNumber);
                }
            }
        });

        rightText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentNumber < Integer.MAX_VALUE) {
                    centerText.setText("" + (++currentNumber));
                }
                if (onNumberChangeListener != null) {
                    onNumberChangeListener.onNumberChange(currentNumber);
                }
            }
        });
    }

    public int getNumber() {
        return currentNumber;
    }

    public void setNumer(int number) {
        currentNumber = number;
        centerText.setText(number + "");
    }

    private OnNumberChangeListener onNumberChangeListener;

    public void setOnNumberChangeListener(OnNumberChangeListener onNumberChangeListener) {
        this.onNumberChangeListener = onNumberChangeListener;
    }

    public interface OnNumberChangeListener {
        void onNumberChange(int number);
    }


}
