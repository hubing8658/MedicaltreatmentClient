package com.ygbd198.hellodoctor.widget;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * 自定义ViewPager，可动态设置是否支持滑动
 * Created by monty on 2017/8/27.
 */

public class CustomViewPager extends ViewPager {

    private boolean isCanScroll;

    public CustomViewPager(Context context) {
        super(context);
    }

    public CustomViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        //允许滑动则应该调用父类的方法
        return !isCanScroll || super.onTouchEvent(ev);
//禁止滑动则不做任何操作，直接返回true即可
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent arg0) {
        return isCanScroll && super.onInterceptTouchEvent(arg0);
    }

    //设置是否允许滑动，true是可以滑动，false是禁止滑动
    public void setIsCanScroll(boolean isCanScroll) {
        this.isCanScroll = isCanScroll;
    }
}
