package com.ygbd198.hellodoctor.widget.PopupWindon;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.ColorRes;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.common.Constants;
import com.ygbd198.hellodoctor.widget.PasswordEditText;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by guangjiqin on 2017/8/4.
 * 选择医生等级的pop
 */

public abstract  class SelectLevelPop extends PopupWindow implements View.OnClickListener {

    private String TAG = getClass().getName();
    private Context context;
    private View contentView;
    public abstract int getLevel(int level);//0医师 1专家 2博士级专家
    private int level = -1;

    private TextView tvPhysician;
    private TextView tvSpecialist;
    private TextView tvDoctor;

    private Button btSubmit;



    public void showPop() {
        this.setAnimationStyle(R.anim.pop_show);
        this.showAtLocation(((Activity) context).findViewById(R.id.lin_root), Gravity.BOTTOM, 0, 0);
        WindowManager.LayoutParams layoutParams = ((Activity)context).getWindow().getAttributes();
        layoutParams.alpha = (float) 0.5;
        ((Activity)context).getWindow().setAttributes(layoutParams);
    }

    public SelectLevelPop(final  Context context) {
        this.context = context;

        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        contentView = layoutInflater.inflate(R.layout.layout_select_level,null);


        setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        setBackgroundDrawable(new ColorDrawable(0x00000000));
        setOutsideTouchable(true);
        setFocusable(true);
        setContentView(contentView);

        this.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss() {
                WindowManager.LayoutParams layoutParams = ((Activity)context).getWindow().getAttributes();
                layoutParams.alpha = (float) 1;
                ((Activity)context).getWindow().setAttributes(layoutParams);
            }
        });

        tvPhysician = (TextView) contentView.findViewById(R.id.tv_level_physician);
        tvSpecialist = (TextView) contentView.findViewById(R.id.tv_level_specialist);
        tvDoctor = (TextView) contentView.findViewById(R.id.tv_level_doctor);
        btSubmit = (Button) contentView.findViewById(R.id.bt_submit);

        btSubmit.setOnClickListener(this);
        tvPhysician.setOnClickListener(this);
        tvSpecialist.setOnClickListener(this);
        tvDoctor.setOnClickListener(this);
    }



    @Override
    public void onClick(View v) {

        tvPhysician.setBackgroundResource(R.color.color_e6e6e6);
        tvSpecialist.setBackgroundResource(R.color.color_e6e6e6);
        tvDoctor.setBackgroundResource(R.color.color_e6e6e6);
        switch (v.getId()){
            case R.id.bt_submit://提交
                getLevel(level);
                SelectLevelPop.this.dismiss();
                break;
            case R.id.tv_level_physician://医生
                level = Constants.LEVEL_PHYSICIAN;
                tvPhysician.setBackgroundResource(R.color.white);
                break;
            case R.id.tv_level_specialist://专家
                level = Constants.LEVEL_SPECIALIST;
                tvSpecialist.setBackgroundResource(R.color.white);
                break;
            case R.id.tv_level_doctor://博士级专家
                level = Constants.LEVEL_DOCTOR;
                tvDoctor.setBackgroundResource(R.color.white);
                break;
        }

    }
}
