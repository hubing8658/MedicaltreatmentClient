package com.ygbd198.hellodoctor.widget.PopupWindon;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.SelectPopBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by guangjiqin on 2017/8/24.
 */

public abstract class CommonSelectPop extends PopupWindow {
    public abstract void onClickCommit(SelectPopBean bean);


    private Context context;
    private View contentView;

    private ListView typeList;
    private Button btCommit;

    private TextView tvType;
    private List<SelectPopBean> been;

    private StrAdpter adpter;

    public void setBeans(List<SelectPopBean> been) {
        this.been.clear();
        this.been.addAll(been);
        adpter.notifyDataSetChanged();
    }


    public void setTvType(String string) {
        this.tvType.setText(string);
    }


    public void showPop() {
        this.setAnimationStyle(R.style.pop_anim_style);
        this.showAtLocation(((Activity) context).findViewById(R.id.lin_main), Gravity.BOTTOM, 0, 0);
        WindowManager.LayoutParams layoutParams = ((Activity) context).getWindow().getAttributes();
        layoutParams.alpha = (float) 0.5;
        ((Activity) context).getWindow().setAttributes(layoutParams);

    }

    public CommonSelectPop(final Context context) {
        this.context = context;

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        contentView = layoutInflater.inflate(R.layout.pop_disease_type, null);

        setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        setBackgroundDrawable(new ColorDrawable(0x00000000));
        setOutsideTouchable(true);
        setFocusable(true);
        setContentView(contentView);

        setSoftInputMode(PopupWindow.INPUT_METHOD_NOT_NEEDED);
        setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        this.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss() {
                WindowManager.LayoutParams layoutParams = ((Activity) context).getWindow().getAttributes();
                layoutParams.alpha = 1;
                ((Activity) context).getWindow().setAttributes(layoutParams);
            }
        });

        initView();

        setOnClick();
    }

    private void initView() {
        tvType = (TextView) contentView.findViewById(R.id.tv_type);
        btCommit = (Button) contentView.findViewById(R.id.bt_submit);
        btCommit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonSelectPop.this.dismiss();
                for (SelectPopBean bean : been) {
                    if (bean.isSelect) {
                        onClickCommit(bean);
                    }
                }

            }
        });
        typeList = (ListView) contentView.findViewById(R.id.list_type);
        adpter = new StrAdpter(context, null == been ? been = new ArrayList<>() : been);
        typeList.setAdapter(adpter);
        typeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SelectPopBean bean = been.get(position);
                if (bean.isSelect) {
                    bean.isSelect = false;
                } else {
                    for (SelectPopBean beanTemp : been) {
                        beanTemp.isSelect = false;
                    }
                    bean.isSelect = true;
                }

                adpter.notifyDataSetChanged();

            }
        });
    }

    private void setOnClick() {
    }
}

//适配器
class StrAdpter extends BaseAdapter {

    private Context context;
    private List<SelectPopBean> been;

    public StrAdpter(Context context, List<SelectPopBean> been) {
        this.context = context;
        this.been = been;

    }

    @Override
    public int getCount() {
        return been.size();
    }

    @Override
    public Object getItem(int position) {
        return been.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (null == convertView) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_disease_type, parent, false);
            holder = new Holder();
            holder.tvType = (TextView) convertView.findViewById(R.id.tv_type);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        SelectPopBean bean = been.get(position);
        holder.tvType.setText(bean.string);

        if (bean.isSelect) {
            holder.tvType.setBackgroundColor(Color.parseColor("#ffffff"));
            holder.tvType.setTextColor(Color.parseColor("#f8b9cd"));
        } else {
            holder.tvType.setTextColor(Color.parseColor("#11345e"));
            holder.tvType.setBackgroundColor(Color.parseColor("#e6e6e6"));
        }

        return convertView;
    }


    class Holder {
        TextView tvType;
        TextView tvTime;
    }

}
