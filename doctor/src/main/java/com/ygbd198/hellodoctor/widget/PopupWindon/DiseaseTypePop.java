package com.ygbd198.hellodoctor.widget.PopupWindon;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.DiseaseTypeBean;
import com.ygbd198.hellodoctor.ui.ApplyBeDocActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by guangjiqin on 2017/8/20.
 */

public class DiseaseTypePop extends PopupWindow {

//    public abstract void onClickCommit(DiseaseTypeBean diseaseTypeBean);

//        public abstract void onClickCommit(List<DiseaseTypeBean> diseaseTypeBeans);



    private Context context;
    private View contentView;

    private ListView typeList;
    private Button btCommit;
    private TextView tvType;

    private List<DiseaseTypeBean> diseaseTypeBeans;
    private DiseaseAdpter adpter;
//    private DiseaseTypeBean curDiseaseTypeBean;

    public void setDiseaseTypeBeans(List<DiseaseTypeBean> diseaseTypeBeans) {
        this.diseaseTypeBeans.clear();
        this.diseaseTypeBeans.addAll(diseaseTypeBeans);
        adpter.notifyDataSetChanged();
    }


    public void showPop() {
        this.setAnimationStyle(R.style.pop_anim_style);
        this.showAtLocation(((Activity) context).findViewById(R.id.lin_main), Gravity.BOTTOM, 0, 0);
        WindowManager.LayoutParams layoutParams = ((Activity) context).getWindow().getAttributes();
        layoutParams.alpha = (float) 0.5;
        ((Activity) context).getWindow().setAttributes(layoutParams);

    }

    public DiseaseTypePop(final Context context) {
        this.context = context;

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        contentView = layoutInflater.inflate(R.layout.pop_disease_type, null);

        setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        setBackgroundDrawable(new ColorDrawable(0x00000000));
        setOutsideTouchable(true);
        setFocusable(true);
        setContentView(contentView);

        setSoftInputMode(PopupWindow.INPUT_METHOD_NOT_NEEDED);
        setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        this.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss() {
                WindowManager.LayoutParams layoutParams = ((Activity) context).getWindow().getAttributes();
                layoutParams.alpha = 1;
                ((Activity) context).getWindow().setAttributes(layoutParams);
            }
        });

        initView();

        setOnClick();
    }

    private void initView() {
        tvType = (TextView) contentView.findViewById(R.id.tv_type);
        btCommit = (Button) contentView.findViewById(R.id.bt_submit);
        btCommit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DiseaseTypePop.this.dismiss();
                ((ApplyBeDocActivity) context).diseaseTypes.clear();
                StringBuilder diseaseStrBuilder = new StringBuilder();
                for(DiseaseTypeBean bean :diseaseTypeBeans){
                    if(bean.isChecked){
                        ((ApplyBeDocActivity) context).diseaseTypes.add(bean.id);
                        diseaseStrBuilder.append(bean.typeName);
                        diseaseStrBuilder.append(",");
                    }
                }
                if (((ApplyBeDocActivity) context).diseaseTypes.size() > 0) {
//                    ((ApplyBeDocActivity) context).tvType.setText("已选择");

                    ((ApplyBeDocActivity) context).tvType.setText(diseaseStrBuilder.toString().substring(0,diseaseStrBuilder.toString().length()-1));
                } else {
                    ((ApplyBeDocActivity) context).tvType.setText("未选择");
                }
            }
        });
        typeList = (ListView) contentView.findViewById(R.id.list_type);
        adpter = new DiseaseAdpter(context, null == diseaseTypeBeans ? diseaseTypeBeans = new ArrayList<>() : diseaseTypeBeans);
        typeList.setAdapter(adpter);
        typeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                diseaseTypeBeans.get(position).isChecked = !diseaseTypeBeans.get(position).isChecked;
                for (int i = 0; i < typeList.getChildCount(); i++) {
                    View itemView = typeList.getChildAt(i);
                    ((TextView) itemView.findViewById(R.id.tv_type)).setBackgroundColor(Color.parseColor("#e6e6e6"));
                    ((TextView) itemView.findViewById(R.id.tv_type)).setTextColor(Color.parseColor("#11345e"));
                }
                adpter.notifyDataSetChanged();

            }
        });
    }

    private void setOnClick() {
    }
}

//适配器
class DiseaseAdpter extends BaseAdapter {

    private Context context;
    private List<DiseaseTypeBean> diseaseTypeBeans;

    public DiseaseAdpter(Context context, List<DiseaseTypeBean> diseaseTypeBeans) {
        this.context = context;
        this.diseaseTypeBeans = diseaseTypeBeans;

    }

    @Override
    public int getCount() {
        return diseaseTypeBeans.size();
    }

    @Override
    public Object getItem(int position) {
        return diseaseTypeBeans.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (null == convertView) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_disease_type, parent, false);
            holder = new Holder();
            holder.tvType = (TextView) convertView.findViewById(R.id.tv_type);
            holder.tvTime = (TextView) convertView.findViewById(R.id.tv_time);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        DiseaseTypeBean diseaseTypeBean = diseaseTypeBeans.get(position);
        holder.tvType.setText(diseaseTypeBean.typeName);

        if (diseaseTypeBean.isChecked) {
            holder.tvType.setBackgroundColor(Color.parseColor("#ffffff"));
            holder.tvType.setTextColor(Color.parseColor("#f8b9cd"));
        } else {
            holder.tvType.setTextColor(Color.parseColor("#11345e"));
            holder.tvType.setBackgroundColor(Color.parseColor("#e6e6e6"));
        }

        return convertView;
    }


    class Holder {
        TextView tvType;
        TextView tvTime;
    }
}