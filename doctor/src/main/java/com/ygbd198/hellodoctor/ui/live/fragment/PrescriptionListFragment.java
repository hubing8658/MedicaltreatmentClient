package com.ygbd198.hellodoctor.ui.live.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.bean.Prescription;
import com.ygbd198.hellodoctor.ui.live.DoctorService;
import com.ygbd198.hellodoctor.ui.live.event.DiseaseEvent;
import com.ygbd198.hellodoctor.util.ToastUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;
import retrofit2.Response;

/**
 * 平台药方
 * Created by monty on 2017/8/27.
 */

public class PrescriptionListFragment extends ListFragment {
    public static final int TYPE_PRESCRIPTION = 0;
    public static final int TYPE_NOT_PRESCRIPTION = 1;
    private int type; // 是否为处方药
    private MyAdapter adapter;
    private String treatmentName;

    public static PrescriptionListFragment getInstance(int type) {
        PrescriptionListFragment prescriptionListFragment = new PrescriptionListFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("type", type);
        prescriptionListFragment.setArguments(bundle);
        return prescriptionListFragment;
    }
    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onDiseaseSelectEvent(DiseaseEvent event) {
        this.treatmentName = event.typeName;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        type = getArguments().getInt("type");
        adapter = new MyAdapter(getContext(), new ArrayList<Prescription>());
        setListAdapter(adapter);

        RetrofitHelper.getInstance().createService(DoctorService.class).getDrug(MySelfInfo.getInstance().getToken(), treatmentName, type == 1, 1, 20).enqueue(new BaseCallBack<BaseCallModel<List<Prescription>>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<List<Prescription>>> response) {
                adapter.notifyDataSetChanged(response.body().data);
            }

            @Override
            public void onFailure(String message) {
                ToastUtils.showToast(message);
            }
        });
    }

    public List<Prescription> getCheckedList(){
        return adapter.getCheckedList();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }


    static class MyAdapter extends BaseAdapter {
        private LayoutInflater inflater;
        private List<Prescription> mPrescriptions;

        public MyAdapter(Context context, List<Prescription> prescriptions) {
            this.mPrescriptions = prescriptions;
            this.inflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return mPrescriptions.size();
        }

        @Override
        public Object getItem(int position) {
            return mPrescriptions.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder = null;
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.prescription_simple_item, parent, false);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            final Prescription prescription = mPrescriptions.get(position);

            viewHolder.checkbox.setImageResource(prescription.isChecked()?R.drawable.reg_checkbox_sel:R.drawable.reg_checkbox_nor);
            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    prescription.setChecked(!prescription.isChecked());
                    notifyDataSetChanged();
                }
            });

            Glide.with(convertView.getContext())
                    .load(TextUtils.isEmpty(prescription.getPic()) ? R.drawable.drug_icon : prescription.getPic())
                    .bitmapTransform(new RoundedCornersTransformation(convertView.getContext(), 10, 0))
                    .into(viewHolder.pic);
            viewHolder.tvDrugName.setText(prescription.getDrugName());
            viewHolder.tvUseRules.setText(prescription.getDrugType());
            viewHolder.tvPrice.setText("￥" + prescription.getPrice());
            return convertView;
        }

        public void notifyDataSetChanged(List<Prescription> pList) {
            this.mPrescriptions = pList;
            notifyDataSetChanged();
        }

        public List<Prescription> getCheckedList() {
            List<Prescription> checkedList = new ArrayList<>();
            for (Prescription p : mPrescriptions) {
                if (p.isChecked()) {
                    checkedList.add(p);
                }
            }
            return checkedList;
        }

        static class ViewHolder {
            @BindView(R.id.checkbox)
            ImageView checkbox;
            @BindView(R.id.pic)
            ImageView pic;
            @BindView(R.id.tv_drugName)
            TextView tvDrugName;
            @BindView(R.id.tv_useRules)
            TextView tvUseRules;
            @BindView(R.id.tv_price)
            TextView tvPrice;
            View itemView;

            ViewHolder(View view) {
                itemView = view;
                ButterKnife.bind(this, view);
            }
        }

    }


}
