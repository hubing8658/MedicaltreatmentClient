package com.ygbd198.hellodoctor.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.common.BaseActivity;
import com.ygbd198.hellodoctor.ui.adapter.ReviewFragmentAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2017/8/9 0009.
 */

public class ReviewActivity extends BaseActivity {


    @BindView(R.id.tv1)
    TextView tv1;
    @BindView(R.id.tv2)
    TextView tv2;
    @BindView(R.id.tv3)
    TextView tv3;
    @BindView(R.id.tab1)
    RelativeLayout tab1;
    @BindView(R.id.tab2)
    RelativeLayout tab2;
    @BindView(R.id.tab3)
    RelativeLayout tab3;
    @BindView(R.id.viewpage)
    ViewPager viewPager;

    @BindView(R.id.line_1)
    View line1;
    @BindView(R.id.line_2)
    View line2;
    @BindView(R.id.line_3)
    View line3;

    private ReviewFragmentAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);
        ButterKnife.bind(this);

        initTitle();

        setViewPage();
    }

    private void initTitle() {
        titleBar.showCenterText(R.string.patient_review,R.drawable.rev,0);
    }


    private void setViewPage() {
        adapter = new ReviewFragmentAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        viewPager.setPageMargin(3);
        setSelect(0);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                setSelect(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        tab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelect(0);
            }
        });
        tab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelect(1);
            }
        });
        tab3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelect(2);
            }
        });

    }

    private void setSelect(int position) {
        reSetAll();
        switch (position) {
            case 0:
                line1.setVisibility(View.VISIBLE);
                tv1.setTextColor(getResources().getColor(R.color.pink_ff7fb9));
                break;
            case 1:
                line2.setVisibility(View.VISIBLE);
                tv2.setTextColor(getResources().getColor(R.color.pink_ff7fb9));
                break;
            case 2:
                line3.setVisibility(View.VISIBLE);
                tv3.setTextColor(getResources().getColor(R.color.pink_ff7fb9));
                break;
        }
        viewPager.setCurrentItem(position);
    }

    private void reSetAll() {
        line1.setVisibility(View.GONE);
        line2.setVisibility(View.GONE);
        line3.setVisibility(View.GONE);
        tv1.setTextColor(getResources().getColor(R.color.blue_0b325a));
        tv2.setTextColor(getResources().getColor(R.color.blue_0b325a));
        tv3.setTextColor(getResources().getColor(R.color.blue_0b325a));
    }
}
