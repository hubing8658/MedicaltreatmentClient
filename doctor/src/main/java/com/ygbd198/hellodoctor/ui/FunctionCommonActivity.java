package com.ygbd198.hellodoctor.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.jude.rollviewpager.RollPagerView;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.AgreenDataBean;
import com.ygbd198.hellodoctor.bean.AgreenDetailBean;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.common.BaseActivity;
import com.ygbd198.hellodoctor.service.MyCenterService;
import com.ygbd198.hellodoctor.ui.adapter.AdAdapter2;
import com.ygbd198.hellodoctor.ui.adapter.FunctionAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * 视频操作指导
 * Created by guangjiqin on 2017/9/4.
 */

public class FunctionCommonActivity extends BaseActivity {

    private int guideType; //3功能介绍 4如何视频问政  5视频操指导台

    private TextView tvContent;
    private FunctionAdapter adapter;
    private List<AgreenDetailBean> data;
    private View contentView;
    private RollPagerView rollPagerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contentView = this.getLayoutInflater().inflate(R.layout.activity_guide_common, null);
        rootView.addView(contentView);
        ButterKnife.bind(this);

        getInentData();

        initTitle();

        initView();

        initListView();

        getData();
    }

    private void initListView() {
//        listView = (ListView) contentView.findViewById(R.id.list_content);
        tvContent = (TextView) contentView.findViewById(R.id.tv_content);
        rollPagerView = (RollPagerView) contentView.findViewById(R.id.vp_content);
        data = new ArrayList<>();
        adapter = new FunctionAdapter(this, data);
//        listView.setAdapter(adapter);
    }

    private void getData() {
        RetrofitHelper.getInstance().createService(MyCenterService.class).getTermsContent(MySelfInfo.getInstance().getToken(), guideType).enqueue(new BaseCallBack<BaseCallModel<AgreenDataBean>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<AgreenDataBean>> response) {
                switch (guideType) {
                    case 3:
                        tvContent.setText(response.body().data.content);
                        break;
                    case 4:
                        List<AgreenDetailBean> details = response.body().data.detail;
                        /*listView.setVisibility(View.VISIBLE);
                        tvContent.setVisibility(View.GONE);*/
                        StringBuilder sb = new StringBuilder();
                        for (AgreenDetailBean detail: details ) {
                            sb.append(detail.content);
                            sb.append("\n");
                        }
                        tvContent.setText(sb.toString());
                        break;
                    case 5:
                        contentView.findViewById(R.id.scrollView).setVisibility(View.GONE);
                        rollPagerView.setVisibility(View.VISIBLE);
                        String[] urls = response.body().data.pic.split(",");
                        /*data.clear();
                        for (String url : urls) {
                            AgreenDetailBean bean = new AgreenDetailBean();
                            bean.pic = url;
                            data.add(bean);
                        }*/
                        AdAdapter2 adapter = new AdAdapter2(rollPagerView, Arrays.asList(urls));
                        rollPagerView.setAdapter(adapter);
                        break;
                }
            }

            @Override
            public void onFailure(String message) {
            }
        });
    }

    protected void initView() {


    }

    private void initTitle() {
        switch (guideType) {
            case 3:
                titleBar.showCenterText(R.string.function_introduce,R.drawable.functio_introduction,0);
                break;
            case 4:
                titleBar.showCenterText(R.string.video_introduce,R.drawable.vide_inquiry,0);
                break;
            case 5:
                titleBar.showCenterText(R.string.video_introduce_gu,R.drawable.video_console_guide,0);
                break;
        }

    }

    private void getInentData() {
        guideType = getIntent().getIntExtra("function", -1);
    }
}
