package com.ygbd198.hellodoctor.ui.controller;

import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.ygbd198.hellodoctor.bean.MyIntegralBean;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.service.MyCenterService;
import com.ygbd198.hellodoctor.util.ToastUtils;

import retrofit2.Response;

/**
 * Created by guangjiqin on 2017/8/29.
 */

public abstract class MyIntegralController {

    public abstract void getDataSuccess(MyIntegralBean myIntegralBean);
    public abstract void getDataFailure(String msg);

    public abstract void exchangeSuccess();
    public abstract void exchangeFailrue(String msg);

    private String token;

    public MyIntegralController(){
        token = MySelfInfo.getInstance().getToken();

    }

    //获取银行卡
    public void getData(){
        RetrofitHelper.getInstance().createService(MyCenterService.class).getCards(token).enqueue(new BaseCallBack<BaseCallModel<MyIntegralBean>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<MyIntegralBean>> response) {
                getDataSuccess(response.body().data);
            }

            @Override
            public void onFailure(String msg) {
                ToastUtils.showToast(msg);
                getDataFailure(msg);
            }
        });
    }

    //积分兑换
//    public void exchangeIntegral(long money,long bindBankId){
//        RetrofitHelper.getInstance().createService(MyCenterService.class).takeMoney(token,money,bindBankId).enqueue(new BaseCallBack<BaseCallModel<Object>>() {
//
//            @Override
//            public void onSuccess(Response<BaseCallModel<Object>> response) {
//                exchangeSuccess();
//            }
//
//            @Override
//            public void onFailure(String msg) {
//                ToastUtils.showToast(msg);
//                exchangeFailrue(msg);
//            }
//        });
//    }
}
