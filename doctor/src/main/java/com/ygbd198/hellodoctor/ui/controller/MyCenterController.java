package com.ygbd198.hellodoctor.ui.controller;

import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.ygbd198.hellodoctor.bean.DoctorInfBean;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.service.MyCenterService;

import retrofit2.Response;

/**
 * Created by guangjiqin on 2017/8/27.
 */

public abstract class MyCenterController {

    public abstract void getInfSuccess(DoctorInfBean doctorInfBean);

    public abstract void getInfFailure(String msg);

    private String token;

    public MyCenterController() {
        this.token = MySelfInfo.getInstance().getToken();
    }

    //获取个人资料
    public void getUserInf() {
        RetrofitHelper.getInstance().createService(MyCenterService.class).getDoctorInf(token).enqueue(new BaseCallBack<BaseCallModel<DoctorInfBean>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<DoctorInfBean>> response) {
                getInfSuccess(response.body().data);
                MySelfInfo.getInstance().setName(response.body().data.doctorAttach.name);
                MySelfInfo.getInstance().setPhoneNum(response.body().data.phone);
                MySelfInfo.getInstance().setIntroduceUrl(response.body().data.doctorAttach.introduceUrl);
            }

            @Override
            public void onFailure(String message) {
                getInfFailure(message);
            }
        });
    }
    //=======各种pop=========//
}
