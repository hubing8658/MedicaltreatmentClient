package com.ygbd198.hellodoctor.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.ui.InputUserInfActivity;
import com.ygbd198.hellodoctor.widget.BaseTitleBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by guangjiqin on 2017/8/24.
 */

public class UserInfProgressFragment extends Fragment {

    @BindView(R.id.titleBar)
    BaseTitleBar titleBar;
    Unbinder unbinder;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;

    public UserInfProgressFragment() {

    }

    public static UserInfProgressFragment newInstance(String param1, String param2) {
        UserInfProgressFragment fragment = new UserInfProgressFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_inf_progress, null);
        unbinder = ButterKnife.bind(this, view);

        this.titleBar.setBackBtnVis(false);
        this.titleBar.showCenterText(R.string.treatment_title, R.drawable.tr, 0);

        return view;
    }

    @OnClick(R.id.bt_input_inf)
    public void toInputInfActivity(){
        startActivity(new Intent(getActivity(), InputUserInfActivity.class));
    }
}
