package com.ygbd198.hellodoctor.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ygbd198.hellodoctor.R;

/**
 * Created by guangjiqin on 2017/10/19.
 */

public class ActualOperatingFragment extends Fragment {

    private int imgRes;

    public ActualOperatingFragment() {
        super();
        Bundle args = getArguments();
        if (args != null) {
            this.imgRes = args.getInt("imgRes");
        }
    }

    public static ActualOperatingFragment newInstance(int imgRes) {
        ActualOperatingFragment fragment = new ActualOperatingFragment();
        Bundle args = new Bundle();
        args.putInt("imgRes", imgRes);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_actual_operating,container,false);
        ImageView imageView = (ImageView) view.findViewById(R.id.image);
        imageView.setImageResource(imgRes);
        return view;
    }



}
