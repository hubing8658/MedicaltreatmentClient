package com.ygbd198.hellodoctor.ui.live.presenters;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.monty.library.live.LiveConstants;
import com.monty.library.live.event.MessageEvent;
import com.tencent.av.sdk.AVRoomMulti;
import com.tencent.av.sdk.AVView;
import com.tencent.ilivesdk.ILiveCallBack;
import com.tencent.ilivesdk.ILiveConstants;
import com.tencent.ilivesdk.ILiveMemStatusLisenter;
import com.tencent.ilivesdk.core.ILiveCameraListener;
import com.tencent.ilivesdk.core.ILiveLog;
import com.tencent.ilivesdk.core.ILiveRoomOption;
import com.tencent.livesdk.ILVCustomCmd;
import com.tencent.livesdk.ILVLiveConstants;
import com.tencent.livesdk.ILVLiveManager;
import com.tencent.livesdk.ILVLiveRoomOption;
import com.tencent.livesdk.ILVText;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.bean.WaitingUser;
import com.ygbd198.hellodoctor.common.Constants;
import com.ygbd198.hellodoctor.common.Presenter;
import com.ygbd198.hellodoctor.ui.live.DoctorService;
import com.ygbd198.hellodoctor.ui.live.viewinterface.ILiveView;
import com.ygbd198.hellodoctor.util.SoundManager;
import com.ygbd198.hellodoctor.util.ToastUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

import static com.ygbd198.hellodoctor.ui.live.presenters.LivePresenter.DoctorLiveStatus.WAITING;
import static com.ygbd198.hellodoctor.util.ToastUtils.showToast;


/**
 * Created by monty on 2017/8/7.
 */

public class LivePresenter extends Presenter implements Observer, ILiveRoomOption.onRoomDisconnectListener, ILiveRoomOption.onExceptionListener, ILiveCameraListener, ILiveRoomOption.onRequestViewListener, ILiveMemStatusLisenter {
    private static final String TAG = "monty-" + LivePresenter.class.getSimpleName();
    private ILiveView liveView;

    private List<WaitingUser> waitingUsers; //排队的用户队列
    private WaitingUser currentUser;// 当前连麦的用户
    private DoctorLiveStatus liveStatus = WAITING; // 医生当前状态

    private SoundManager soundManager; // 铃声管理

    private boolean isReceive = true; // 是否接受新患者


    public String getCurrentUserToken() {
        return currentUser.getUserToken();
    }

    public long getCurrentUserId() {
        return currentUser.getId();
    }

    public void recrodBad() {
        soundManager.stopRingtoneAndVibrator();
        RetrofitHelper.getInstance().createService(DoctorService.class).recordBad(MySelfInfo.getInstance().getToken(), getCurrentUserToken()).enqueue(new BaseCallBack<BaseCallModel<String>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<String>> response) {
                // do noting
            }

            @Override
            public void onFailure(String message) {
                // do noting
            }
        });

    }

    public

            /**
             * 医生当前直播状态
             */
    enum DoctorLiveStatus {
        LIVEINING, //正在诊断
        RESTING, //小憩
        WAITING //等待状态，可以连接用户
    }

    public LivePresenter(ILiveView liveView) {
        this.liveView = liveView;
//        InitILiveHelper.initLive();
        MessageEvent.getInstance().addObserver(this);
        soundManager = new SoundManager();
    }

    @Override
    public void onDestory() {
        liveView = null;
        if (disposable != null) {
            disposable.dispose();
        }
        handler = null;
        soundManager.stopRingtoneAndVibrator();
        soundManager = null;
        sendGroupCmd(com.monty.library.live.Constants.AVIMCMD_HOST_LEAVE, "out");
        MessageEvent.getInstance().deleteObserver(this);
        ILVLiveManager.getInstance().quitRoom(null);
        ILVLiveManager.getInstance().onDestory();
    }

    /**
     * 设置是否接受新患者
     */
    public void setReceive() {
        setReceive(!isReceive);
    }

    private void setReceive(final boolean flag) {
        liveView.showLoadingDialog("");
        RetrofitHelper.getInstance().createService(DoctorService.class).isReceive(MySelfInfo.getInstance().getToken(), flag).enqueue(new BaseCallBack<BaseCallModel<String>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<String>> response) {
                isReceive = flag;
                liveView.setIsReceiveSuccess(isReceive);
            }

            @Override
            public void onFailure(String message) {
                liveView.setIsReceiveFailure(message);
            }

            @Override
            public void finish() {
                liveView.closeLoadingDialog();
            }
        });
    }

    private Disposable disposable; // 用于在Activity的onDestroy中停止发送事件，结束心跳

    /**
     * 医生心跳
     */
    private void timer() {
        disposable = io.reactivex.Observable.interval(0, 10, TimeUnit.SECONDS).interval(0, 10, TimeUnit.SECONDS)
                .map(new Function<Long, Boolean>() {
                    @Override
                    public Boolean apply(@NonNull Long aLong) throws Exception {
                        Response<BaseCallModel<String>> execute = RetrofitHelper.getInstance().createService(DoctorService.class)
                                .doctorTimer(MySelfInfo.getInstance().getToken()).execute();  // 使用同步方法，在子线程中调用，直接返回结果
                        Log.i(TAG, "医生心跳 - " + aLong + " ,result -> " + execute.body().msg);
                        if (execute.body().code == 200) { // 后台返回的状态码不是200时，发送事件
                            getWaitUser(null);
                            return true;
                        }

                        return false;
                    }
                }).filter(new Predicate<Boolean>() {
                    @Override
                    public boolean test(Boolean o) throws Exception {
                        Log.i(TAG, "test - " + o);
                        return o;
                    }
                })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Boolean>() {
                    @Override
                    public void accept(@NonNull Boolean b) throws Exception {
                        Log.i("monty", "心跳 -> " + (b ? "成功" : "失败"));
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
//                        ToastUtils.showToast("心跳异常");
                        Log.e("monty", "心跳异常");
                        timer();
                    }
                });

    }

    /**
     * 申请创建房间
     */
    public void applyCreateRoom() {
        liveView.showLoadingDialog("正在创建房间...");
        RetrofitHelper.getInstance().createService(DoctorService.class).createRoom(MySelfInfo.getInstance().getToken()).enqueue(new BaseCallBack<BaseCallModel<String>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<String>> response) {
//                createRoom();
                liveView.closeLoadingDialog();
                if (null != liveView)
                    liveView.enterRoomComplete(true, "");

                setReceive(true);
                timer();// 启动心跳
            }

            @Override
            public void onFailure(String message) {
                ToastUtils.showToast(message);
                liveView.closeLoadingDialog();
                if (null != liveView) {
                    if (disposable != null) { //创建直播间失败后停止心跳
                        disposable.dispose();
                    }
                    liveView.enterRoomComplete(false, message);
                }
            }

            @Override
            public void finish() {
                liveView.closeLoadingDialog();
            }
        });

    }

    /**
     * 创建直播间
     */
    public void createRoom() {
        ILVLiveRoomOption hostOption = new ILVLiveRoomOption(MySelfInfo.getInstance().getToken())
                .controlRole(Constants.HOST_ROLE)
                .videoMode(ILiveConstants.VIDEOMODE_BSUPPORT)
                .authBits(AVRoomMulti.AUTH_BITS_DEFAULT)
                .exceptionListener(this)
                .autoFocus(true)
                .cameraListener(this)
                .setRequestViewLisenter(this)
                .roomDisconnectListener(this);
        int ret = ILVLiveManager.getInstance().createRoom(MySelfInfo.getInstance().getId(), hostOption, new ILiveCallBack() {
            @Override
            public void onSuccess(Object data) {
                ILiveLog.d(TAG, "ILVB-SXB|startEnterRoom->create room sucess");
                liveView.creageRoomComplete(true, "");
            }

            @Override
            public void onError(String module, int errCode, String errMsg) {
                ILiveLog.d(TAG, "ILVB-SXB|createRoom->create room failed:" + module + "|" + errCode + "|" + errMsg);
                showToast(module + "|" + errCode + "|" + errMsg);
            }
        });
        checkEnterReturn(ret);
    }

    /**
     * 检查上次是否正常退出房间，如果没有退出就先退出
     *
     * @param iRet
     */
    private void checkEnterReturn(int iRet) {
        if (ILiveConstants.NO_ERR != iRet) {
            ILiveLog.d(TAG, "ILVB-Suixinbo|checkEnterReturn->enter room failed:" + iRet);
            if (ILiveConstants.ERR_ALREADY_IN_ROOM == iRet) {     // 上次房间未退出处理做退出处理
                ILVLiveManager.getInstance().quitRoom(new ILiveCallBack() {
                    @Override
                    public void onSuccess(Object data) {
                        if (null != liveView) {
                            liveView.quiteRoomComplete(true);
                        }
                    }

                    @Override
                    public void onError(String module, int errCode, String errMsg) {
                        if (null != liveView) {
                            liveView.quiteRoomComplete(true);
                        }
                    }
                });
            } else {
                if (null != liveView) {
                    liveView.quiteRoomComplete(true);
                }
            }
        }
    }

    @Override
    public void update(Observable observable, Object arg) {
        MessageEvent.MsgInfo info = (MessageEvent.MsgInfo) arg;
        processCmdMsg(info);
    }

    /**
     * 解析自定义信令
     *
     * @param info
     */
    private void processCmdMsg(MessageEvent.MsgInfo info) {
        if (null == info.data || !(info.data instanceof ILVCustomCmd)) {
            Log.w("monty", "processCmdMsg->wrong object:" + info.data);
            return;
        }
        ILVCustomCmd cmd = (ILVCustomCmd) info.data;
        if (cmd.getType() == ILVText.ILVTextType.eGroupMsg
                && !(MySelfInfo.getInstance().getId() + "").equals(cmd.getDestId())) { // 此处的chatId就是医生房间id
            Log.d("monty", "processCmdMsg->ingore message from: " + cmd.getDestId() + "/" + (MySelfInfo.getInstance().getId() + ""));
            return;
        }

        String name = info.senderId;
        if (null != info.profile && !TextUtils.isEmpty(info.profile.getNickName())) {
            name = info.profile.getNickName();
        }

        handleCustomMsg(cmd.getCmd(), cmd.getParam(), info.senderId, name);
    }

    private android.os.Handler handler = new android.os.Handler();

    /**
     * 处理自定义信令消息
     *
     * @param action
     * @param param
     * @param identifier
     * @param nickname
     */
    private void handleCustomMsg(int action, String param, String identifier, String nickname) {
        Log.d(TAG, "handleCustomMsg->action: " + action + ",param-> " + param + ",identifier-> " + identifier + ",nickname-> " + nickname);
        if (null == liveView) {
            return;
        }
        switch (action) {
            case ILVLiveConstants.ILVLIVE_CMD_INTERACT_AGREE: // 用户同意上麦
                Log.i("monty", "用户同意上麦");
                startLive(identifier);
                break;
            case ILVLiveConstants.ILVLIVE_CMD_INTERACT_REJECT: // 用户拒绝上麦
                Log.i("monty", "用户拒绝上麦");
                break;
            case LiveConstants.ILVLIVE_CMD_IN_QUEUE: // 用户进入队列 // TODO: 2017/8/15 判断当前是否在诊断，如果没有诊断就让第一个用户上麦，然后刷新麦序列表
                Log.i("monty", "用户进入队列");
                getWaitUser(null);
                if (currentUser == null) {
                    soundManager.playNotification(); //  短信响铃+震动
                }
                break;
            case LiveConstants.ILVLIVE_CMD_OUT_QUEUE: // 用户退出队列 // TODO: 2017/8/15 刷新麦序列表
                Log.i("monty", "用户退出队列");
                getWaitUser(null);
                break;
            case ILVLiveConstants.ILVLIVE_CMD_ENTER: // 房间有其他用户加入到直播房间（群发消息）,接收到此消息后需要调用房间获取用户的接口，刷新用户列表
                Log.i("monty", "有用户进入房间啦 -> " + nickname);
                break;
            case ILVLiveConstants.ILVLIVE_CMD_LEAVE: // 用户退出房间
                Log.i("monty", "有用户退出房间啦 -> " + nickname);
                // 如果当前正在治疗的用户退出，则结束治疗
                if (currentUser != null && currentUser.getUserToken().equals(identifier)) {
                    liveView.showAlert("当前诊断用户退出房间，结束治疗");
                    liveView.resetVisiting();
                }

                break;
            default:
                break;
        }
    }

    interface OnGetWaitUserListener {
        void onSuccess(List<WaitingUser> waitingUsers);

        void onFailure(String msg);
    }

    private void startLive(String userToken) {
        soundManager.stopRingtoneAndVibrator();
        liveView.showCalling(false);
        RetrofitHelper.getInstance().createService(DoctorService.class).startLive(MySelfInfo.getInstance().getToken(), userToken).enqueue(new BaseCallBack<BaseCallModel<Long>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<Long>> response) {
                Log.d("monty", "连接成功");
                liveView.onStartDiagnose(response.body().data, currentUser.getUserName());
            }

            @Override
            public void onFailure(String message) {
                Log.d("monty", "startLive --------> " + message);
                ToastUtils.showToast(message);
            }
        });
    }

    /**
     * 获取队列患者
     */
    public void getWaitUser(final OnGetWaitUserListener ongetWaitUserListener) {
        RetrofitHelper.getInstance().createService(DoctorService.class).getWaitUser(MySelfInfo.getInstance().getToken()).enqueue(new BaseCallBack<BaseCallModel<List<WaitingUser>>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<List<WaitingUser>>> response) {
                List<WaitingUser> data = response.body().data;
                if (ongetWaitUserListener != null) {
                    ongetWaitUserListener.onSuccess(data);
                }
                if (liveView != null) {
                    liveView.refreshWaitingUserlist(data);
                }
            }

            @Override
            public void onFailure(String message) {
                if (ongetWaitUserListener != null) {
                    ongetWaitUserListener.onFailure(message);
                }
            }
        });
    }

    /**
     * 开始诊断（1／拉取麦序队列，2／通知用户可以上麦了）
     */
    public void starDiagnose() {
        if (soundManager != null) {
            soundManager.stopRingtoneAndVibrator();
        }
        getWaitUser(new OnGetWaitUserListener() {
            @Override
            public void onSuccess(List<WaitingUser> waitingUsers) {
                if (waitingUsers != null && waitingUsers.size() > 0) {
                    WaitingUser waitingUser = waitingUsers.get(0);
                    currentUser = waitingUser;
                    liveView.showCalling(true);
                    soundManager.playRingtone();
                    sendC2CCmd(ILVLiveConstants.ILVLIVE_CMD_INVITE, "", currentUser.getUserToken());
                }
            }

            @Override
            public void onFailure(String msg) {
                Log.e("monty", "getWaitUser -> " + msg);
            }
        });
    }

    /**
     * 发消息给患者，让患者下麦
     */
    public void finishDiagnose() {
        sendC2CCmd(ILVLiveConstants.ILVLIVE_CMD_INVITE_CLOSE, "", currentUser.getUserToken());
        currentUser = null;
        /*RetrofitHelper.getInstance().createService(DoctorService.class).endLive(MySelfInfo.getInstance().getToken(),currentUser.getUserToken(),MySelfInfo.getInstance().getToken()).enqueue(new BaseCallBack<BaseCallModel<String>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<String>> response) {
                Log.d("monty","endLive - onSuccess -> ");
                liveView.closeLoadingDialog();
            }

            @Override
            public void onFailure(String message) {
                Log.d("monty","endLive - onFailure -> "+message);
                liveView.closeLoadingDialog();
            }
        });*/
    }


    /**
     * 群发信令（进入房间、退出房间）
     */

    public int sendGroupCmd(int cmd, String param) {
        ILVCustomCmd customCmd = new ILVCustomCmd();
        customCmd.setCmd(cmd);
        customCmd.setParam(param);
        customCmd.setType(ILVText.ILVTextType.eGroupMsg);
        return sendCmd(customCmd);
    }

    public void sendBarrage(String content) {
        sendC2CCmd(LiveConstants.ILVLIVE_CMD_BARRAGE, content, currentUser.getUserToken());
    }

    /**
     * 发送点对点消息（申请排队，取消排队，同意连麦，拒绝连麦）
     *
     * @param cmd
     * @param param
     * @param destId 接收方id
     * @return
     */
    public int sendC2CCmd(final int cmd, String param, String destId) {
        ILVCustomCmd customCmd = new ILVCustomCmd();
        customCmd.setDestId(destId);
        customCmd.setCmd(cmd);
        customCmd.setParam(param);
        customCmd.setType(ILVText.ILVTextType.eC2CMsg);
        return sendCmd(customCmd);
    }

    private int sendCmd(final ILVCustomCmd cmd) {

        return ILVLiveManager.getInstance().sendCustomCmd(cmd, new ILiveCallBack() {
            @Override
            public void onSuccess(Object data) {
                Log.i("monty", "sendCmd->success:" + cmd.getCmd() + "|" + cmd.getDestId());
            }

            @Override
            public void onError(String module, int errCode, String errMsg) {
                Log.e("monty", "sendCmd->failed:" + module + "|" + errCode + "|" + errMsg);
//                Toast.makeText(mContext, "sendCmd->failed:" + module + "|" + errCode + "|" + errMsg, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onRoomDisconnect(int errCode, String errMsg) {
        if (null != liveView) {
            liveView.quiteRoomComplete(true);
        }
    }

    @Override
    public void onException(int exceptionId, int errCode, String errMsg) {
        ILiveLog.e("ILiveLog monty", "房间中出现的可忽略的异常 -> exceptionId:" + exceptionId + ",errCode:" + errCode + ",errMsg:" + errMsg);
    }

    @Override
    public void onCameraEnable(int cameraId) {
        ILiveLog.d("ILiveLog monty", "摄像头打开事件 -> cameraId:" + cameraId);
    }

    @Override
    public void onCameraDisable(int cameraId) {
        ILiveLog.d("ILiveLog monty", "摄像头关闭事件 -> cameraId:" + cameraId);
    }

    @Override
    public void onCameraPreviewChanged(int cameraId) {
        ILiveLog.d("ILiveLog monty", "摄像头数据切换事件 -> cameraId:" + cameraId);
    }

    @Override
    public void onComplete(String[] identifierList, AVView[] viewList, int count, int result, String errMsg) {
        ILiveLog.d("ILiveLog monty", "请求画面回调 - onComplete -> identifierList:" + Arrays.asList(identifierList) + ",viewList:" + Arrays.asList(viewList).toString() + ",count:" + count + ",result:" + result + ",errMsg:" + errMsg);
    }

    @Override
    public boolean onEndpointsUpdateInfo(int eventid, String[] updateList) {
        // eventid 状态码对应 ILiveMemStatusLisenter
        ILiveLog.d("ILiveLog monty", "房间内成员状态回调接口 - onEndpointsUpdateInfo -> eventid:" + eventid + ",updateLsit:" + Arrays.asList(updateList));
        return false;
    }
}
