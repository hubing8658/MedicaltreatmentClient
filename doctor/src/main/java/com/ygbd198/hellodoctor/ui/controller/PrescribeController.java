package com.ygbd198.hellodoctor.ui.controller;

import android.content.Context;
import android.graphics.Color;
import android.view.animation.AnimationUtils;

import com.google.gson.Gson;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.monty.library.okhttp.RequestManger;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.DiseaseTypeBean;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.bean.PrescriptionBean;
import com.ygbd198.hellodoctor.service.ApplyBeDocService;
import com.ygbd198.hellodoctor.ui.PrescribeActivity;
import com.ygbd198.hellodoctor.util.ToastUtils;
import com.ygbd198.hellodoctor.widget.PopupWindon.SearchTypePop;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.Response;

/**
 * Created by guangjiqin on 2017/8/31.
 */

public abstract class PrescribeController {

    public abstract void getDataSuccess(List<PrescriptionBean.Prescription> list);

    public abstract void getDataFailrue(String msg);

    private List<DiseaseTypeBean> medicineTypes;
    private List<DiseaseTypeBean> diseaseTypeBeen;
    private SearchTypePop searchTypePop;
    private SearchTypePop medicineTypePop;

    private Context context;
    private DiseaseTypeBean curDiseaseTypeBean;
    private DiseaseTypeBean curMedicineTypeBean;
    private String token;
    //    private String treatmentName;
    private int ownBrand;      // -1非自有品牌  0全部  1自有品牌
    private boolean isPrescription;


    public PrescribeController(Context context) {
        token = MySelfInfo.getInstance().getToken();
        this.context = context;

        medicineTypes = new ArrayList<>();
        DiseaseTypeBean bean1 = new DiseaseTypeBean();
        bean1.typeName = "自有品牌";
        DiseaseTypeBean bean2 = new DiseaseTypeBean();
        bean2.typeName = "非自有品牌";
        DiseaseTypeBean bean3 = new DiseaseTypeBean();
        bean3.typeName = "全部";
        bean3.isChecked = true;
        medicineTypes.add(bean3);
        medicineTypes.add(bean1);
        medicineTypes.add(bean2);
    }

    public void getData(String url, HashMap<String, Object> params) {
        RequestManger requestManger = RequestManger.getInstance(context);
        requestManger.requestAsy(url, RequestManger.TYPE_GET, params, new RequestManger.ReuCallBack() {
            @Override
            public void onReSuccess(String result, Response response) {
                Gson gson = new Gson();
                getDataSuccess(gson.fromJson(result, PrescriptionBean.class).data);
            }

            @Override
            public void onReFailed(String errorMsg) {
                getDataFailrue(errorMsg);
                ToastUtils.showToast(errorMsg);
            }
        });
    }

    public void getDataByCondition(String url) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", MySelfInfo.getInstance().getToken());
        if (null != curDiseaseTypeBean && !"全部".equals(curDiseaseTypeBean.typeName)) {
            params.put("treatmentName", curDiseaseTypeBean.typeName);
        }
        if (ownBrand != 0) {
            params.put("ownBrand", 1 == ownBrand ? true : false);
        }
        if (isPrescription) {
            params.put("isPrescription", isPrescription);
        }
        getData(url, params);
    }

    public void getType() {
        RetrofitHelper.getInstance().createService(ApplyBeDocService.class).getDiseaseType(token).enqueue(new BaseCallBack<BaseCallModel<List<DiseaseTypeBean>>>() {

            @Override
            public void onSuccess(retrofit2.Response<BaseCallModel<List<DiseaseTypeBean>>> response) {
                diseaseTypeBeen = response.body().data;
            }

            @Override
            public void onFailure(String msg) {
                ToastUtils.showToast(msg);
            }
        });
    }

    public void showDiseaseTypePop(final String url) {
        if (null == searchTypePop) {
            diseaseTypeBeen = (null == diseaseTypeBeen ? new ArrayList<DiseaseTypeBean>() : diseaseTypeBeen);
            DiseaseTypeBean bean = new DiseaseTypeBean();
            bean.typeName = "全部";
            bean.isChecked = true;
            diseaseTypeBeen.add(0, bean);
            searchTypePop = new SearchTypePop(context) {
                @Override
                public void onPopDismiss() {
                    getDataByCondition(url);
                    ((PrescribeActivity) context).tv1.setTextColor(Color.parseColor("#11345e"));
                    ((PrescribeActivity) context).img1.setImageResource(R.drawable.mine_icon_down);
                    ((PrescribeActivity) context).img1.startAnimation(AnimationUtils.loadAnimation(context, R.anim.rotate_hide));
                }

                @Override
                public void onItemOnClick(DiseaseTypeBean diseaseTypeBean) {
                    curDiseaseTypeBean = diseaseTypeBean;
                    searchTypePop.dismiss();
                }
            };
        }

        searchTypePop.setDiseaseTypeBeans(diseaseTypeBeen);
        searchTypePop.showPopTopLeft();
    }

    public void showMedicineTypePop(final String url) {
        if (null == medicineTypePop) {
            medicineTypePop = new SearchTypePop(context) {
                @Override
                public void onPopDismiss() {
                    if(null!=curMedicineTypeBean){
                        switch (curMedicineTypeBean.typeName) {
                            case "全部":
                                ownBrand = 0;
                                break;
                            case "自有品牌":
                                ownBrand = 1;
                                break;
                            case "非自有品牌":
                                ownBrand = -1;
                                break;
                        }
                        getDataByCondition(url);
                    }
                    ((PrescribeActivity) context).tv2.setTextColor(Color.parseColor("#11345e"));
                    ((PrescribeActivity) context).img2.setImageResource(R.drawable.mine_icon_down);
                    ((PrescribeActivity) context).img2.startAnimation(AnimationUtils.loadAnimation(context, R.anim.rotate_hide));

                }

                @Override
                public void onItemOnClick(DiseaseTypeBean diseaseTypeBean) {
                    curMedicineTypeBean = diseaseTypeBean;
                    medicineTypePop.dismiss();
                }
            };
        }
        medicineTypePop.setDiseaseTypeBeans(medicineTypes);
        medicineTypePop.showPopTopCenter();
    }

    public boolean isPrescription() {
        return isPrescription;
    }

    public void setIsPrescription(String url) {
        isPrescription = !isPrescription;
        getDataByCondition(url);
    }

}
