package com.ygbd198.hellodoctor.ui.live.presenters;

import com.monty.library.live.event.MessageEvent;
import com.tencent.TIMManager;
import com.tencent.ilivesdk.ILiveSDK;
import com.tencent.livesdk.ILVLiveConfig;
import com.tencent.livesdk.ILVLiveManager;
import com.ygbd198.hellodoctor.APP;
import com.ygbd198.hellodoctor.common.Constants;

/**
 * Created by monty on 2017/8/7.
 */

public class InitILiveHelper {
    public static void initApp() {
        //初始化avsdk imsdk
        TIMManager.getInstance().disableBeaconReport();
        ILiveSDK.getInstance().initSdk(APP.getInstance(), Constants.SDK_APPID, Constants.ACCOUNT_TYPE);
        initLive();

    }

    public static void initLive(){
        // 初始化直播模块
        ILVLiveConfig liveConfig = new ILVLiveConfig();
        liveConfig.setLiveMsgListener(MessageEvent.getInstance());
        ILVLiveManager.getInstance().init(liveConfig);
    }
}
