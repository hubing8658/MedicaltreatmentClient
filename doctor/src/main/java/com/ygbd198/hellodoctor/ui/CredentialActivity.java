package com.ygbd198.hellodoctor.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.common.BaseActivity;
import com.ygbd198.hellodoctor.util.ToastUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by guangjiqin on 2017/9/11.
 */

public class CredentialActivity extends BaseActivity {

    @BindView(R.id.tv_introduce)
    EditText tvIntroduce;
    @BindView(R.id.btn_complete)
    Button btnComplete;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View contentView = LayoutInflater.from(this).inflate(R.layout.activity_credential,rootView, false);
        rootView.addView(contentView);
        ButterKnife.bind(this);

        initTitle();

        tvIntroduce.setText(getIntent().getStringExtra("introduction"));
    }

    private void initTitle() {
        titleBar.showCenterText("个人简介");
    }

    @OnClick(R.id.btn_complete)
    public void onViewClicked() {
        if(TextUtils.isEmpty(tvIntroduce.getText())){
            ToastUtils.showToast("个人简介允许为空");
            return;
        }
        Intent intent = new Intent();
        intent.putExtra("introduction",tvIntroduce.getText().toString());
        setResult(0x1011,intent);
        finish();
    }
}
