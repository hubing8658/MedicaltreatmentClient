package com.ygbd198.hellodoctor.ui.live;

import com.monty.library.http.BaseCallModel;
import com.ygbd198.hellodoctor.bean.PatientRecordDetailsBean;
import com.ygbd198.hellodoctor.bean.Prescription;
import com.ygbd198.hellodoctor.bean.Taboo;
import com.ygbd198.hellodoctor.bean.WaitingUser;

import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by monty on 2017/8/8.
 */

public interface DoctorService {
    /**
     * 开始坐诊，申請房間
     *
     * @param token
     * @return
     */
    @GET("doctorAttach/createRoom")
    Call<BaseCallModel<String>> createRoom(@Query("token") String token);

    /**
     * 医生端调用-与用户连麦失败，该用户重新排队或者踢出用户
     *
     * @param token 医生token
     * @param userToken 用户token
     *
     * @return
     */
    @GET("doctorAttach/recordBad")
    Call<BaseCallModel<String>> recordBad(@Query("token") String token, @Query("userToken") String userToken);


    /**
     * 结束坐诊，退出房間
     *
     * @param token
     * @return
     */
    @FormUrlEncoded
    @POST("doctorAttach/outRoom")
    Call<BaseCallModel<String>> quitRoom(@Field("token") String token);

    /**
     * 连麦成功
     *
     * @param token
     * @param userToken
     * @return
     */
    @GET("doctorAttach/startLive")
    Call<BaseCallModel<Long>> startLive(@Query("token") String token, @Query("userToken") String userToken);

    /**
     * 结束连麦
     *
     * @param token       医生端调用就用医生token，用户端调用就用用户token
     * @param userToken
     * @param doctorToken
     * @return
     */
    @GET("doctorAttach/endLive")
    Call<BaseCallModel<String>> endLive(@Query("token") String token, @Query("userToken") String userToken, @Query("doctorToken") String doctorToken);

    /**
     * 诊断超时（医生视频诊断超过指定的时间）
     *
     * @param token    医生token
     * @param recordId 诊断记录ID
     * @param second   超时时长，单位秒
     * @return
     */
    @FormUrlEncoded
    @POST("doctorAttach/overtime")
    Call<BaseCallModel<String>> overtime(@Field("token") String token, @Field("recordId") int recordId, @Field("second") int second);

    /**
     * 获取等待用户
     *
     * @param token 医生token
     * @return
     */
    @GET("doctorAttach/getWaitUser")
    Call<BaseCallModel<List<WaitingUser>>> getWaitUser(@Query("token") String token);

    /**
     * 是否接受新患者
     * flag：
     * false - 暂停接受患者
     * true - 继续接受患者
     *
     * @param token
     * @param flag
     * @return
     */
    @GET("doctorAttach/isReceive")
    Call<BaseCallModel<String>> isReceive(@Query("token") String token, @Query("flag") boolean flag);


    @GET("doctorAttach/doctorTimer")
    Call<BaseCallModel<String>> doctorTimer(@Query("token") String token);

    /**
     * 获取平台药方
     *
     * @param token
     * @param treatmentName  病症类型
     * @param ownBrand       是否为自有品牌
     * @param useType        使用方式，1-内服，2-外用
     * @param isPrescription 是否为处方药
     * @param pageNo         页码
     * @param pageSize       每页显示个数
     * @return
     */
    @GET("program/getDrug")
    Call<BaseCallModel<List<Prescription>>> getDrug(@Query("token") String token
            , @Query("treatmentName") String treatmentName
//            , @Query("ownBrand") boolean ownBrand
//            , @Query("useType") int useType
            , @Query("isPrescription") boolean isPrescription
            , @Query("pageNo") int pageNo
            , @Query("pageSize") int pageSize);

    /**
     * 获取平台药方
     *
     * @param token
     * @param treatmentName  病症类型
     * @param ownBrand       是否为自有品牌
     * @param isPrescription 是否为处方药
     * @param pageNo         页码
     * @param pageSize       每页显示个数
     * @return
     */
    @GET("program/getDrug")
    Call<BaseCallModel<List<Prescription>>> getDrug(@Query("token") String token
            , @Query("treatmentName") String treatmentName
            , @Query("ownBrand") boolean ownBrand
//            , @Query("useType") int useType
            , @Query("isPrescription") boolean isPrescription
            , @Query("pageNo") int pageNo
            , @Query("pageSize") int pageSize);

    /**
     * 获取饮食
     *
     * @param token
     * @param treatmentName 患者类型
     * @param pageNo        页码
     * @param pageSize      数量
     * @return
     */
    @GET("diagnostic/getDiet")
    Call<BaseCallModel<List<Taboo>>> getDiet(@Query("token") String token
            , @Query("treatmentName") String treatmentName
            , @Query("pageNo") int pageNo
            , @Query("pageSize") int pageSize);

    /**
     * 获取生活习惯于作息
     *
     * @param token
     * @param treatmentName 患者类型
     * @param pageNo        页码
     * @param pageSize      数量
     * @return
     */
    @GET("diagnostic/getRest")
    Call<BaseCallModel<List<Taboo>>> getRest(@Query("token") String token
            , @Query("treatmentName") String treatmentName
            , @Query("pageNo") int pageNo
            , @Query("pageSize") int pageSize);

    /**
     * 获取禁忌
     *
     * @param token
     * @param treatmentName 患者类型
     * @param pageNo        页码
     * @param pageSize      数量
     * @return
     */
    @GET("diagnostic/getTaboo")
    Call<BaseCallModel<List<Taboo>>> getTaboo(@Query("token") String token
            , @Query("treatmentName") String treatmentName
            , @Query("pageNo") int pageNo
            , @Query("pageSize") int pageSize);

    /**
     * 提交方案
     *
     * @param token
     * @param record
     * @return
     */
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST("diagnostic/submitRecord")
    Call<BaseCallModel<String>> submitRecord(@Query("token") String token, @Body RequestBody record);


}
