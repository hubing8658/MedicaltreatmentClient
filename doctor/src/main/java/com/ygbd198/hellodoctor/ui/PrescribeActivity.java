package com.ygbd198.hellodoctor.ui;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.MySelfInfo;

import com.ygbd198.hellodoctor.bean.PrescriptionBean;
import com.ygbd198.hellodoctor.common.BaseActivity;
import com.ygbd198.hellodoctor.service.Url;
import com.ygbd198.hellodoctor.ui.adapter.PrescribeListAdapter;
import com.ygbd198.hellodoctor.ui.controller.PrescribeController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by guangjiqin on 2017/8/31.
 */

public class PrescribeActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.tab1)
    LinearLayout tab1;
    @BindView(R.id.tab2)
    LinearLayout tab2;
    @BindView(R.id.tab3)
    LinearLayout tab3;
    @BindView(R.id.tab4)
    LinearLayout tab4;

    @BindView(R.id.tv1)
    public TextView tv1;
    @BindView(R.id.tv2)
    public TextView tv2;
    @BindView(R.id.tv3)
    public TextView tv3;
    @BindView(R.id.tv4)
    TextView tv4;

    @BindView(R.id.img1)
    public ImageView img1;
    @BindView(R.id.img2)
    public ImageView img2;
    @BindView(R.id.img3)
    public ImageView img3;
    @BindView(R.id.img4)
    ImageView img4;

    @BindView(R.id.listview)
    ListView listview;

    private String treatmentName = "";//病症类型
    private int ownBrand = 0;//是否为自有品牌   （注：  -1==false  0 == 不填  1 == true）
    private int useType = 0;//服用方式  (注：  1内服 2外用)
    private int isPrescription = 0;//是否为处方药 （注：  -1==false  0 == 不填  1 == true）

    private PrescribeListAdapter adapter;
    private List<PrescriptionBean.Prescription> prescriptions;
    private PrescribeController controller;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View contentView = this.getLayoutInflater().inflate(R.layout.activity_perscribe, null);
        rootView.addView(contentView);

        ButterKnife.bind(this);

        titleBar.showCenterText(R.string.prescribe,R.drawable.prescription_31,0);

        initListView();

        initController();

        setOnclick();

        getData();

    }

    private void setOnclick() {
        tab1.setOnClickListener(this);
        tab2.setOnClickListener(this);
        tab3.setOnClickListener(this);
    }

    private void getData() {
        HashMap<String,Object>paramsMap = new HashMap<>();
        paramsMap.put("token", MySelfInfo.getInstance().getToken());
        controller.getData(Url.GET_PRESCRIBE_LIST, paramsMap);

        controller.getType();
    }

    private void initController() {
        controller = new PrescribeController(this) {
            @Override
            public void getDataSuccess(List<PrescriptionBean.Prescription> list) {
                prescriptions.clear();
                prescriptions.addAll(list);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void getDataFailrue(String msg) {

            }
        };
    }

    private void initListView() {
        prescriptions = new ArrayList<>();
        adapter = new PrescribeListAdapter(this, prescriptions);
        listview.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tab1:
                img1.setImageResource(R.drawable.mine_icon_up);
                img1.startAnimation(AnimationUtils.loadAnimation(this, R.anim.rotate_show));
                tv1.setTextColor(Color.parseColor("#ff7fb9"));
                controller.showDiseaseTypePop(Url.GET_PRESCRIBE_LIST);
                break;
            case  R.id.tab2:
                img2.setImageResource(R.drawable.mine_icon_up);
                img2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.rotate_show));
                tv2.setTextColor(Color.parseColor("#ff7fb9"));
                controller.showMedicineTypePop(Url.GET_PRESCRIBE_LIST);
                break;
            case  R.id.tab3:
                controller.setIsPrescription(Url.GET_PRESCRIBE_LIST);
                if(controller.isPrescription()){
                    tv3.setTextColor(Color.parseColor("#ff7fb9"));
                }else{
                    tv3.setTextColor(Color.parseColor("#11345e"));
                }
                break;
        }
    }
}
