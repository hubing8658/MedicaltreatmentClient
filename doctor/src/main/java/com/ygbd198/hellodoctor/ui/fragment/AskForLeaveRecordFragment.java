package com.ygbd198.hellodoctor.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.AskForLeaveRecodeBean;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.service.MyCenterService;
import com.ygbd198.hellodoctor.ui.adapter.AskForLeaveRecodeAdapter;
import com.ygbd198.hellodoctor.util.ToastUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

/**
 * Created by guangjiqin on 2017/8/30.
 */

public class AskForLeaveRecordFragment extends Fragment {

    private static AskForLeaveRecordFragment askForLeaveRecordFragment;
    private View view;
    private ListView listview;
    private AskForLeaveRecodeAdapter adpter;
    private List<AskForLeaveRecodeBean> data;

    public static AskForLeaveRecordFragment createInstance() {
        if (null == askForLeaveRecordFragment) {
            askForLeaveRecordFragment = new AskForLeaveRecordFragment();
        }
        return askForLeaveRecordFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_ask_for_leave_recode, null);

        initView();

        intData();

        return view;
    }

    private void intData() {
        RetrofitHelper.getInstance().createService(MyCenterService.class).getAskForLeaveRecode(MySelfInfo.getInstance().getToken()).enqueue(new BaseCallBack<BaseCallModel<List<AskForLeaveRecodeBean>>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<List<AskForLeaveRecodeBean>>> response) {
                data.clear();
                data.addAll(response.body().data);
                adpter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(String msg) {
                ToastUtils.showToast(msg);
            }
        });
    }

    private void initView() {
        listview = (ListView) view.findViewById(R.id.listview);
        data = new ArrayList<>();
        adpter = new AskForLeaveRecodeAdapter(getActivity(), data);
    }
}
