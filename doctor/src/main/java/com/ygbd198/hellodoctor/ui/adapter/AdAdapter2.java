package com.ygbd198.hellodoctor.ui.adapter;


import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.jude.rollviewpager.RollPagerView;
import com.jude.rollviewpager.adapter.LoopPagerAdapter;

import java.util.List;


/**
 * Created by monty on 2017/7/31.
 */

public class AdAdapter2 extends LoopPagerAdapter {

    private List<String> urls;

    public AdAdapter2(RollPagerView viewPager, List<String> urls){
        super(viewPager);
        this.urls = urls;

    }

    public AdAdapter2(RollPagerView viewPager) {
        super(viewPager);
    }


    @Override
    public View getView(ViewGroup container, int position) {
        ImageView view = new ImageView(container.getContext());

        Glide.with(container.getContext()).load(urls.get(position)).into(view);

        view.setScaleType(ImageView.ScaleType.CENTER_CROP);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        return view;
    }

    @Override
    public int getRealCount() {
        return urls.size();
    }
}

