package com.ygbd198.hellodoctor.ui.live.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bigkoo.pickerview.TimePickerView;
import com.monty.library.EventBusUtil;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.Record;
import com.ygbd198.hellodoctor.ui.live.event.DiseaseEvent;
import com.ygbd198.hellodoctor.ui.live.event.RevisitEvent;
import com.ygbd198.hellodoctor.util.ToastUtils;
import com.ygbd198.hellodoctor.widget.DatePickerFactory;

import org.greenrobot.eventbus.Subscribe;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.ygbd198.hellodoctor.widget.DatePickerFactory.createTimePicker;

/**
 * 复诊
 * Created by monty on 2017/8/20.
 */
public class LFAppointmentFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.checkbox)
    CheckBox checkbox;
    @BindView(R.id.tv_content)
    TextView tvContent;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.ll_item)
    LinearLayout llItem;
    Unbinder unbinder;

    private TimePickerView datePicker;
    private TimePickerView startTimePicker;
    private TimePickerView endTimePicker;

    private Date _date;
    private Date startDate;
    private Date endDate;

    public DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    public DateFormat timeFormat = new SimpleDateFormat("HH:mm");
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private String dateStr;
    private String startTimeStr;
    private String endTimeStr;

    private Record.Revisit revisit = new Record.Revisit();

    private Record.Revisit tempRevisit = null; // 汇总页面获取的


    public LFAppointmentFragment() {
        // Required empty public constructor
    }

    public static LFAppointmentFragment newInstance() {
        LFAppointmentFragment fragment = new LFAppointmentFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        EventBusUtil.register(this);
    }

    @Subscribe
    public void OnDiseaseEvent(DiseaseEvent diseaseEvent) {
        revisit.revisitContent = diseaseEvent.typeName;
        tvContent.setText(revisit.revisitContent);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lfappointment, container, false);
        unbinder = ButterKnife.bind(this, view);
        checkbox.setChecked(true);
        checkbox.setOnClickListener(null);
        tvTime.setText("-");
        /*checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    tempRevisit = revisit;
                } else {
                    tempRevisit = null;
                }
                EventBusUtil.post(new RevisitEvent(tempRevisit));
            }
        });*/
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBusUtil.unregister(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.tv_time)
    public void onTvTimeClicked() {
//        DatePickerFactory.createNormalTimePicker()
        createStartTimePicker();
    }

    private void createDatePicker(){
        datePicker = DatePickerFactory.createDatePicker(getContext(), "复诊日期", new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {
                _date = date;
                dateStr = dateFormat.format(date);
                String s = formatDate(_date, startDate, endDate);
                tvTime.setText(s);
                revisit.revisitDate = dateStr;
                revisit.revisitTime = startTimeStr + "-" + endTimeStr;
                EventBusUtil.post(new RevisitEvent(revisit));
//                    ToastUtils.showToast("提醒时间-》" + s);
                datePicker.dismissDialog();
            }
        }, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePicker.returnData();
            }
        });
        datePicker.show();
    }

    private void createStartTimePicker() {
        startTimePicker = createTimePicker(getActivity(), "复诊 - 开始时间", new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {
                startDate = date;
                startTimeStr = timeFormat.format(date);
//                String startTime = DatePickerFactory.dateFormat.format(date);
                startTimePicker.dismissDialog();
                createEndTimePicker();
            }
        }, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startTimePicker.returnData();
            }
        });
        startTimePicker.show();
    }

    private void createEndTimePicker() {
        endTimePicker = createTimePicker(getContext(), "复诊 - 结束时间", new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {
                if (date.getTime() > startDate.getTime()) {
                    endDate = date;
                    endTimeStr = timeFormat.format(date);
                    endTimePicker.dismissDialog();
                    createDatePicker();
                } else {
                    ToastUtils.showToast("结束时间必须大于开始时间");
                }
            }
        }, new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                endTimePicker.returnData();
            }
        });
        endTimePicker.show();
    }

    private String formatDate(Date date, Date startDate, Date endDate) {
        StringBuffer dateStr = new StringBuffer();
        dateStr.append(dateFormat.format(date));
        dateStr.append(" ");
        dateStr.append(timeFormat.format(startDate));
        dateStr.append(" ~ ");
        dateStr.append(timeFormat.format(endDate));
        return dateStr.toString();
    }
}
