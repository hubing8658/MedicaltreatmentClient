package com.ygbd198.hellodoctor.ui.live.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.monty.library.EventBusUtil;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.bean.Taboo;
import com.ygbd198.hellodoctor.ui.live.DoctorService;
import com.ygbd198.hellodoctor.ui.live.event.DiseaseEvent;
import com.ygbd198.hellodoctor.ui.live.event.TaboosEvent;
import com.ygbd198.hellodoctor.util.ToastUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Response;

/**
 * 饮食 -1 生活习惯和作息 -2 禁忌 -3
 * Created by monty on 2017/8/20.
 */
public class LFTabooFragment extends ListFragment {
    private int type;// 饮食 -1 生活习惯和作息 -2 禁忌 -3
    private MyAdapter adapter;
    private String treatmentName;

    public static LFTabooFragment newInstance(int type) {
        LFTabooFragment fragment = new LFTabooFragment();
        Bundle args = new Bundle();
        args.putInt("type", type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            type = getArguments().getInt("type");
        }
        EventBusUtil.register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBusUtil.unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onDiseaseSelectEvent(DiseaseEvent event) {
        this.treatmentName = event.typeName;
        getData();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        setListShown(false);
        adapter = new MyAdapter(new ArrayList<Taboo>());
        setListAdapter(adapter);
    }

    private void getData() {
        Call<BaseCallModel<List<Taboo>>> call = null;
        DoctorService service = RetrofitHelper.getInstance().createService(DoctorService.class);
        switch (type) {
            case 1:
                call = service.getDiet(MySelfInfo.getInstance().getToken(), treatmentName, 1, 50);
                break;
            case 2:
                call = service.getRest(MySelfInfo.getInstance().getToken(), treatmentName, 1, 50);
                break;
            case 3:
                call = service.getTaboo(MySelfInfo.getInstance().getToken(), treatmentName, 1, 50);
                break;
        }
        call.enqueue(new BaseCallBack<BaseCallModel<List<Taboo>>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<List<Taboo>>> response) {
                adapter.notifyData(response.body().data);
                setListShown(true);
            }

            @Override
            public void onFailure(String message) {
                ToastUtils.showToast(message);
            }
        });
    }

    /**
     * 返回选中的id（拼装后的String）
     *
     * @param taboos
     * @return
     */
    private String getChecked(List<Taboo> taboos) {
        StringBuffer ids = new StringBuffer();
        for (int i = 0; i < taboos.size(); i++) {
            if (taboos.get(i).isChecked()) {
                ids.append(taboos.get(i).getId() + ",");
            }
        }
        if (ids.toString().equals("")) {
            return "";
        }
        return ids.toString().substring(0, ids.toString().lastIndexOf(","));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    public class MyAdapter extends BaseAdapter {
        public List<Taboo> taboos;

        public MyAdapter(List<Taboo> taboos) {
            this.taboos = taboos;
        }

        @Override
        public int getCount() {
            return taboos.size();
        }

        @Override
        public Object getItem(int position) {
            return taboos.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final ViewHolder viewHolder;
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.fragment_lftaboo_item, parent, false);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            final Taboo taboo = taboos.get(position);
//            Glide.with(convertView.getContext())
//                    .load(TextUtils.isEmpty(taboo.getPic()) ? R.drawable.test_diet : taboo.getPic())
//                    .bitmapTransform(new RoundedCornersTransformation(convertView.getContext(), 10, 0))
//                    .into(viewHolder.imageView);
            if (type == 1) { // 饮食
                viewHolder.tvDrugName.setText(taboo.getName());
                viewHolder.tvTips.setText(taboo.getEffect());
            } else {
                viewHolder.tvDrugName.setText(taboo.getProgramName());
                viewHolder.tvTips.setText(taboo.getDescribe());
            }


            viewHolder.checkbox.setImageResource(taboo.isChecked() ? R.drawable.reg_checkbox_sel : R.drawable.reg_checkbox_nor);
            viewHolder.imageView.setImageResource(R.drawable.test_diet);
            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    viewHolder.checkbox.setChecked(!viewHolder.checkbox.isChecked());

                    taboo.setChecked(!taboo.isChecked());
                    String checkeds = getChecked(taboos);
                    switch (type) {
                        case 1:
                            EventBusUtil.post(new TaboosEvent(TaboosEvent.DIET, checkeds));
                            break;
                        case 2:
                            EventBusUtil.post(new TaboosEvent(TaboosEvent.REST, checkeds));
                            break;
                        case 3:
                            EventBusUtil.post(new TaboosEvent(TaboosEvent.TABOO, checkeds));
                            break;
                    }
                    notifyDataSetChanged();
                }
            });
//            viewHolder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    viewHolder.itemView.performClick();
//                }
//            });
            return convertView;
        }

        public void notifyData(List<Taboo> taboos) {
            this.taboos = taboos;
            notifyDataSetChanged();
        }

        class ViewHolder {
            @BindView(R.id.checkbox)
            ImageView checkbox;
            @BindView(R.id.imageView)
            ImageView imageView;
            @BindView(R.id.tv_drugName)
            TextView tvDrugName;
            @BindView(R.id.tv_tips)
            TextView tvTips;

            View itemView;

            ViewHolder(View view) {
                itemView = view;
                ButterKnife.bind(this, view);
            }
        }
    }
}
