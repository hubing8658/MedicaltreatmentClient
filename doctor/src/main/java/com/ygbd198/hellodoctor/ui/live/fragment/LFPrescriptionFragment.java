package com.ygbd198.hellodoctor.ui.live.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.View;

import com.ygbd198.hellodoctor.bean.Prescription;
import com.ygbd198.hellodoctor.ui.live.adapter.PrescriptionAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * 选中的药方列表
 * Created by monty on 2017/8/27.
 */

public class LFPrescriptionFragment extends ListFragment {
    private PrescriptionAdapter adapter;

    public static LFPrescriptionFragment newInstance() {
        LFPrescriptionFragment lfPrescriptionFragment = new LFPrescriptionFragment();
        return lfPrescriptionFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new PrescriptionAdapter(getContext(), new ArrayList<Prescription>());
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setListAdapter(adapter);
    }
/*
    public List<Prescription> getCheckedData() {
        List<Prescription> data = adapter.getData();
        List<Prescription> checkedData = new ArrayList<>();
        for (Prescription p : data) {
            if (p.isChecked()) {
                checkedData.add(p);
            }
        }
        return checkedData;
    }*/

    public void addNotifyData(List<Prescription> checkedList) {
        Log.d("monty", "addNotifyData - checkedList -> " + checkedList.toString());
//        adapter.notifyData(checkedList);
        adapter.addNotifyData(checkedList);
    }
}
