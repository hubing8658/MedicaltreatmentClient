package com.ygbd198.hellodoctor.ui.live.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.monty.library.EventBusUtil;
import com.ygbd198.hellodoctor.bean.DiseaseTypeBean;
import com.ygbd198.hellodoctor.ui.live.adapter.SymptomAdapter;
import com.ygbd198.hellodoctor.ui.live.event.DiseaseListEvent;
import com.ygbd198.hellodoctor.ui.live.presenters.OtherPersenter;
import com.ygbd198.hellodoctor.util.ToastUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 病症选择
 * Created by monty on 2017/8/20.
 */
public class LFDiseaselistFragment extends ListFragment {
    public static final String TAG = "LFDiseaselistFragment";
    private OtherPersenter otherPersenter;
    private SymptomAdapter adapter;

    public static LFDiseaselistFragment newInstance() {
        LFDiseaselistFragment fragment = new LFDiseaselistFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("monty",TAG+" -> onCreate ");
        otherPersenter = new OtherPersenter();

        adapter = new SymptomAdapter(getContext(), new ArrayList<DiseaseTypeBean>());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        ViewGroup view = (ViewGroup) inflater.inflate(R.base_dilog_layout.fragment_symptom, null);
//        view.addView(super.onCreateView(inflater,container,savedInstanceState));
        return super.onCreateView(inflater, container, savedInstanceState);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d("monty",TAG+" -> onViewCreated ");
        setListAdapter(adapter);
        setListShown(false);
        otherPersenter.getTypes(new OtherPersenter.OnGetTypesListener() {
            @Override
            public void onSuccess(List<DiseaseTypeBean> diseaseTypeList) {
                if (isVisible()) {
                    setListShown(true);
                    adapter.notifyDataSetChanged(diseaseTypeList);
                }
                EventBusUtil.post(new DiseaseListEvent(diseaseTypeList));
            }

            @Override
            public void onFailure(String msg) {
                setListShown(true);
                ToastUtils.showToast(msg);
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (otherPersenter != null) {
            otherPersenter.onDestory();
            otherPersenter = null;
        }

    }

}
