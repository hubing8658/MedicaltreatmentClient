package com.ygbd198.hellodoctor.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ListView;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.ExchangeRecordBean;
import com.ygbd198.hellodoctor.common.BaseActivity;
import com.ygbd198.hellodoctor.ui.adapter.ExchangeRecordAdapter;
import com.ygbd198.hellodoctor.ui.controller.ExchangeIntegralController;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by guangjiqin on 2017/8/28.
 */

public class ExchangeIntegralRecordActivity extends BaseActivity {

    @BindView(R.id.list_exchange_record)
    ListView listview;

    private List<ExchangeRecordBean> exchangeRecordBeans;
    private ExchangeIntegralController controller;
    private ExchangeRecordAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View contentView = this.getLayoutInflater().inflate(R.layout.activity_exchange_record, null);
        rootView.addView(contentView);
        ButterKnife.bind(this);

        initController();

        initTitle();

        initList();

        getData();
    }

    private void initTitle() {
        titleBar.showCenterText("兑换明细",R.drawable.exchange_record,0);

    }

    private void getData() {
        controller.getData();
    }

    private void initList() {
        exchangeRecordBeans = new ArrayList<>();
        adapter = new ExchangeRecordAdapter(this, exchangeRecordBeans);
        listview.setAdapter(adapter);
    }

    private void initController() {
        controller = new ExchangeIntegralController() {
            @Override
            public void getDataSuccess(List<ExchangeRecordBean> exchangeRecordBeen) {
                exchangeRecordBeans.clear();
                sortByTime(exchangeRecordBeen);
                exchangeRecordBeans.addAll(exchangeRecordBeen);
                adapter.notifyDataSetChanged();

            }

            @Override
            public void getDataFailrue() {

            }
        };
    }

    private void sortByTime(List<ExchangeRecordBean> exchangeRecordBeans) {
        Collections.sort(exchangeRecordBeans, new Comparator<ExchangeRecordBean>() {
            @Override
            public int compare(ExchangeRecordBean o1, ExchangeRecordBean o2) {
                if (dataToSecond(o1.createTime) > dataToSecond(o2.createTime)) {
                    return -1;
                } else {
                    return 1;
                }
            }
        });
    }

    private long dataToSecond(String data) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        try {
            return simpleDateFormat.parse(data).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return -1;
    }
}
