package com.ygbd198.hellodoctor.ui.live.dialog;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.monty.library.EventBusUtil;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.monty.library.widget.dialog.BaseBottomDialog;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.DiseaseTypeBean;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.bean.Prescription;
import com.ygbd198.hellodoctor.ui.live.DoctorService;
import com.ygbd198.hellodoctor.ui.live.adapter.SymptomAdapter2;
import com.ygbd198.hellodoctor.ui.live.event.DiseaseEvent;
import com.ygbd198.hellodoctor.util.ToastUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrDefaultHandler2;
import in.srain.cube.views.ptr.PtrFrameLayout;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;
import retrofit2.Response;


/**
 * 获取平台药方Dialog
 * Created by monty on 2017/8/27.
 */

public class PrescriptionDialog2 extends BaseBottomDialog implements RadioGroup.OnCheckedChangeListener, CompoundButton.OnCheckedChangeListener {
    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;
    @BindView(R.id.rb_not_prescription)
    RadioButton rbNotPrescription;
    @BindView(R.id.rb_prescription)
    RadioButton rbPrescription;
    @BindView(R.id.btn_confirm)
    Button btnConfirm;
    @BindView(R.id.rb_symptom)
    RadioButton rbSymptom;
    @BindView(R.id.lv_condition)
    ListView lvCondition;
    @BindView(R.id.lv_yaofang)
    ListView lvYaofang;
    Unbinder unbinder;
    Unbinder unbinder1;
    @BindView(R.id.store_house_ptr_frame)
    PtrClassicFrameLayout mPtrFrame;
    private int type;
    private MyAdapter adapter;

    private OnPrescriptionCheckChangeListener listener;
    private List<DiseaseTypeBean> diseaseTypes;
    private List<DiseaseTypeBean> brands = new ArrayList<>();

    private int pageNo = 1;
    private boolean isLoadMore;

    private List<Prescription> checkedIds = new ArrayList<>();

    public PrescriptionDialog2(List<Integer> checkedIds,List<DiseaseTypeBean> diseaseTypes, OnPrescriptionCheckChangeListener listener) {
        this.diseaseTypes = diseaseTypes;
        this.listener = listener;

        this.brands.add(new DiseaseTypeBean("自有品牌", false));
        this.brands.add(new DiseaseTypeBean("非自有品牌", false));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("monty","进入药方选择*********");
        EventBusUtil.register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBusUtil.unregister(this);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.prescription_dialog2;
    }

    @Override
    public int getHeight() {
        WindowManager wm = (WindowManager) getContext()
                .getSystemService(Context.WINDOW_SERVICE);
        int height = wm.getDefaultDisplay().getHeight();
        return (int) (height * 0.8);
    }

    @Override
    public void bindView(View v) {
        unbinder = ButterKnife.bind(this, v);
//        adapter = new MyPagerAdapter(getChildFragmentManager());
//        viewpager.setAdapter(adapter);
        rbNotPrescription.setOnCheckedChangeListener(this);
        rbPrescription.setOnCheckedChangeListener(this);
        rbSymptom.setOnCheckedChangeListener(this);

//        rbSymptom.setChecked(true);
        radioGroup.setOnCheckedChangeListener(this);
//        viewpager.addOnPageChangeListener(this);
        adapter = new MyAdapter(getContext(), new ArrayList<Prescription>());
        lvYaofang.setAdapter(adapter);
//        disease = diseaseTypes.get(0).typeName;
        rbSymptom.setText(disease);
        isBrand = true;
        isRecipe = false;

        initPullRefresh();
    }

    private void initPullRefresh() {
        mPtrFrame.setLastUpdateTimeRelateObject(this);
        mPtrFrame.setPtrHandler(new PtrDefaultHandler2() {
            @Override
            public void onLoadMoreBegin(PtrFrameLayout frame) {
                isLoadMore = true;
                if (isRecipe) {
                    getRecipeDate(pageNo);
                } else {
                    getData(pageNo);
                }
            }

            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                isLoadMore = false;
                if (isRecipe) {
                    getRecipeDate(1);
                } else {
                    getData(1);
                }
            }
        });

        // the following are default settings
        mPtrFrame.setResistance(1.7f);
        mPtrFrame.setRatioOfHeaderHeightToRefresh(1.2f);
        mPtrFrame.setDurationToClose(200);
        mPtrFrame.setDurationToCloseHeader(1000);
        // default is false
        mPtrFrame.setPullToRefresh(false);
        // default is true
        mPtrFrame.setKeepHeaderWhenRefresh(true);
        mPtrFrame.postDelayed(new Runnable() {
            @Override
            public void run() {
                mPtrFrame.autoRefresh();
            }
        }, 100);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.POSTING)
    public void onDiseaseSelectEvent(DiseaseEvent event) {
        this.disease = event.typeName;
    }

    private String disease; // 症状
    private boolean isBrand; // 是否自有品牌
    private boolean isRecipe; // 是否是处方药

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        switch (checkedId) {
            case R.id.rb_symptom:
//                showSymptomCondition();
                break;
            case R.id.rb_not_prescription:
                isRecipe = false;
//                showBrandCondition();
                break;
            case R.id.rb_prescription:
                if (lvCondition.isShown()) {
                    lvCondition.setVisibility(View.GONE);
                }
                isRecipe = true;
                mPtrFrame.autoRefresh();
                break;
        }
    }

    private void getData(int pageNo) {
        RetrofitHelper.getInstance().createService(DoctorService.class).getDrug(MySelfInfo.getInstance().getToken(), disease, isBrand, isRecipe, pageNo, 20).enqueue(new BaseCallBack<BaseCallModel<List<Prescription>>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<List<Prescription>>> response) {
                if(!PrescriptionDialog2.this.isVisible()||!PrescriptionDialog2.this.isAdded()){
                    return;
                }
                List<Prescription> data = response.body().data;
                if (isLoadMore) {
                    if (data != null && data.size() > 0) {
                        PrescriptionDialog2.this.pageNo++;
                        adapter.addData(data);
                    }else{
                        ToastUtils.showToast("没有更多了");
                    }
                } else {
                    PrescriptionDialog2.this.pageNo = 2;
                    adapter.notifyDataSetChanged(data);
                }
                mPtrFrame.refreshComplete();
            }

            @Override
            public void onFailure(String message) {
                if(!PrescriptionDialog2.this.isVisible()||!PrescriptionDialog2.this.isAdded()){
                    return;
                }
                ToastUtils.showToast(message);
                mPtrFrame.refreshComplete();
            }
        });
    }

    /**
     * 获取处方药
     * @param pageNo
     */
    private void getRecipeDate(int pageNo) {
        RetrofitHelper.getInstance().createService(DoctorService.class).getDrug(MySelfInfo.getInstance().getToken(), disease, isRecipe, pageNo, 20).enqueue(new BaseCallBack<BaseCallModel<List<Prescription>>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<List<Prescription>>> response) {
                if(!PrescriptionDialog2.this.isVisible()||!PrescriptionDialog2.this.isAdded()){
                    return;
                }
                List<Prescription> data = response.body().data;
                if (isLoadMore) {
                    if (data != null && data.size() > 0) {
                        PrescriptionDialog2.this.pageNo++;
                        adapter.addData(data);
                    }else{
                        ToastUtils.showToast("没有更多了");
                    }
                } else {
                    PrescriptionDialog2.this.pageNo = 2;
                    adapter.notifyDataSetChanged(response.body().data);
                }
                mPtrFrame.refreshComplete();
            }

            @Override
            public void onFailure(String message) {
                if(!PrescriptionDialog2.this.isVisible()||!PrescriptionDialog2.this.isAdded()){
                    return;
                }
                ToastUtils.showToast(message);
                mPtrFrame.refreshComplete();
            }
        });
    }

    private void showSymptomCondition() {
        if (lvCondition.isShown() && rbSymptom.isChecked()) {
            lvCondition.setVisibility(View.GONE);
        } else {
            lvCondition.setVisibility(View.VISIBLE);
//            diseaseTypes.get(0).isChecked = true;
            SymptomAdapter2 symptomAdapter = new SymptomAdapter2(getContext(), diseaseTypes);
            symptomAdapter.setOnItemClickListener(new SymptomAdapter2.OnItemClickListener() {
                @Override
                public void onItemClick(View v, int position) {
                    lvCondition.setVisibility(View.GONE);
                    disease = diseaseTypes.get(position).typeName;
                    rbSymptom.setText(disease);
//                    getData(1);

                    mPtrFrame.autoRefresh();
                }
            });
            lvCondition.setAdapter(symptomAdapter);
        }
    }

    private void showBrandCondition() {
        if (lvCondition.isShown() && rbNotPrescription.isChecked()) {
            lvCondition.setVisibility(View.GONE);
        } else {
            lvCondition.setVisibility(View.VISIBLE);

            SymptomAdapter2 symptomAdapter = new SymptomAdapter2(getContext(), brands);
            symptomAdapter.setOnItemClickListener(new SymptomAdapter2.OnItemClickListener() {
                @Override
                public void onItemClick(View v, int position) {
                    lvCondition.setVisibility(View.GONE);
                    isBrand = position == 0;
                    isRecipe = false;
                    mPtrFrame.autoRefresh();
                }
            });
            lvCondition.setAdapter(symptomAdapter);
        }
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            buttonView.setTextColor(ContextCompat.getColor(getContext(), R.color.pink3));
        } else {
            buttonView.setTextColor(ContextCompat.getColor(getContext(), R.color.blue));
        }
    }


    @OnClick(R.id.btn_confirm)
    public void onViewClicked() {
//        ToastUtils.showToast("一共选择 " + adapter.getCheckedList().size() + " 项");
        listener.onCheckedComplete(adapter.getCheckedList());
        dismissAllowingStateLoss();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);

        unbinder1 = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @OnClick({R.id.rb_symptom, R.id.rb_not_prescription, R.id.rb_prescription})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rb_symptom:
                showSymptomCondition();
                break;
            case R.id.rb_not_prescription:
                showBrandCondition();
                break;
            case R.id.rb_prescription:
                break;
        }
    }


    public interface OnPrescriptionCheckChangeListener {
        void onCheckedComplete(List<Prescription> checkedList);
    }

    public void add2CheckedIds(Prescription prescription){
        Prescription prescription1 = null;
        for (int i = 0; i < checkedIds.size(); i++) {
            if(checkedIds.get(i).getId() == prescription.getId()){
                prescription1 = checkedIds.get(i);
            }
        }
        if(prescription1 == null){
            checkedIds.add(prescription);
            Log.d("monty","新增一个药品->"+prescription.getId());
        }else{
            checkedIds.remove(prescription1);
            Log.d("monty","删除一个药品->"+prescription.getId());
        }
    }

    class MyAdapter extends BaseAdapter {
        private LayoutInflater inflater;
        private List<Prescription> mPrescriptions;

        public MyAdapter(Context context, List<Prescription> prescriptions) {
            this.mPrescriptions = prescriptions;
            this.inflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return mPrescriptions.size();
        }

        @Override
        public Object getItem(int position) {
            return mPrescriptions.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder = null;
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.prescription_simple_item2, parent, false);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            final Prescription prescription = mPrescriptions.get(position);
            Log.d("monty","id->"+prescription.getId()+",ischecked->"+prescription.isChecked());
            viewHolder.checkbox.setImageResource(prescription.isChecked() ? R.drawable.reg_checkbox_sel : R.drawable.reg_checkbox_nor);
            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    prescription.setChecked(!prescription.isChecked());
                    add2CheckedIds(prescription);
                    notifyDataSetChanged();
                }
            });

            Glide.with(convertView.getContext())
                    .load(TextUtils.isEmpty(prescription.getPic()) ? R.drawable.drug_icon : prescription.getPic())
                    .bitmapTransform(new RoundedCornersTransformation(convertView.getContext(), 10, 0))
                    .into(viewHolder.pic);
            viewHolder.tvDrugName.setText(prescription.getDrugName());
            viewHolder.tvUseRules.setText(prescription.getDrugType());
            viewHolder.tvPrice.setText("￥" + prescription.getPrice());
            return convertView;
        }

        public void addData(List<Prescription> pList){

            for (int i = 0; i < pList.size(); i++) {
                for (int j = 0; j < checkedIds.size(); j++) {
                    if(pList.get(i).getId() == checkedIds.get(j).getId()){
                        pList.get(i).setChecked(true);
                    }
                }
            }

            this.mPrescriptions.addAll(pList);
            notifyDataSetChanged();
        }

        public void notifyDataSetChanged(List<Prescription> pList) {
            for (int i = 0; i < pList.size(); i++) {
                for (int j = 0; j < checkedIds.size(); j++) {
                    if(pList.get(i).getId() == checkedIds.get(j).getId()){
                        pList.get(i).setChecked(true);
                    }
                }
            }
            this.mPrescriptions = pList;
            notifyDataSetChanged();
        }

        public List<Prescription> getCheckedList() {
           /* List<Prescription> checkedList = new ArrayList<>();
            for (Prescription p : mPrescriptions) {
                if (p.isChecked()) {
                    checkedList.add(p);
                }
            }
            return checkedList;*/
           return checkedIds;
        }

        class ViewHolder {
            @BindView(R.id.checkbox)
            ImageView checkbox;
            @BindView(R.id.pic)
            ImageView pic;
            @BindView(R.id.tv_drugName)
            TextView tvDrugName;
            @BindView(R.id.tv_useRules)
            TextView tvUseRules;
            @BindView(R.id.tv_price)
            TextView tvPrice;
            View itemView;

            ViewHolder(View view) {
                itemView = view;
                ButterKnife.bind(this, view);
            }
        }

    }
}
