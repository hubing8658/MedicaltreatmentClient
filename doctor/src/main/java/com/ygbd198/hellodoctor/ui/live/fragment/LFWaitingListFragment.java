package com.ygbd198.hellodoctor.ui.live.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.WaitingUser;
import com.ygbd198.hellodoctor.rxjava.RxCountDown;
import com.ygbd198.hellodoctor.ui.live.adapter.WaitingUserAdapter;
import com.ygbd198.hellodoctor.util.ToastUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * 排队用户
 * Created by monty on 2017/8/20.
 */
public class LFWaitingListFragment extends Fragment {

    @BindView(R.id.tv_waitingUserCount)
    TextView tvWaitingUserCount;
    @BindView(R.id.lv_waitingUserList)
    ListView lvWaitingUserList;
    Unbinder unbinder;
    @BindView(R.id.btn_start)
    Button btnStart;
    @BindView(R.id.btn_rest)
    Button btnRest;

    private boolean isVisiting; // 是否在坐诊中

    private WaitingUserAdapter adapter;

    private LFWaitingListListener mListener;

    public LFWaitingListFragment() {

    }

    /**
     * 是否是在坐诊中
     * @param isVisiting
     * @return
     */
    public static LFWaitingListFragment newInstance(boolean isVisiting) {
        LFWaitingListFragment fragment = new LFWaitingListFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("isVisiting",isVisiting);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments()!=null){
            isVisiting = getArguments().getBoolean("isVisiting");
        }
    }

    public void notifyWaitingUserList(List<WaitingUser> waitingUsers) {
        if (this.isVisible()) {
            tvWaitingUserCount.setText("排号中的患者(" + waitingUsers.size() + ")");
            adapter.notifyDataSetChanged(waitingUsers);
        }
    }

    public void startComplete(boolean isSuccess,String errorMsg){
        if(isSuccess){
            btnStart.setText("小憩一下(60s)");
            btnStart.setAlpha(0.3f);
            btnStart.setEnabled(false);
        }else{
            ToastUtils.showToast(errorMsg);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lfwaiting_list, container, false);
        unbinder = ButterKnife.bind(this, view);
        if(isVisiting){
            btnStart.setVisibility(View.GONE);
        }else{
            btnRest.setVisibility(View.GONE);
        }
        adapter = new WaitingUserAdapter(getContext(), new ArrayList<WaitingUser>());
        lvWaitingUserList.setAdapter(adapter);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof LFWaitingListListener) {
            mListener = (LFWaitingListListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.btn_start)
    public void onBtnStartClicked() {
        mListener.start();
    }

    @OnClick(R.id.btn_rest)
    public void onBtnRestClicked() {
        RxCountDown.countdown(60).subscribe(new Observer<Integer>() {
            @Override
            public void onSubscribe(Disposable d) {
                btnRest.setEnabled(false);
                btnRest.setAlpha(0.3f);
            }

            @Override
            public void onNext(Integer value) {
                btnRest.setText("小憩一下("+value+"s)");
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {
                btnRest.setText("小憩一下(60s)");
                btnRest.setEnabled(true);
                btnRest.setAlpha(1f);
            }
        });

    }

    public interface LFWaitingListListener {
        void start();
    }
}
