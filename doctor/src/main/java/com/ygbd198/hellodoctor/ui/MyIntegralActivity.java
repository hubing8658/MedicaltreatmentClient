package com.ygbd198.hellodoctor.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.MyIntegralBean;
import com.ygbd198.hellodoctor.common.BaseActivity;
import com.ygbd198.hellodoctor.ui.controller.MyIntegralController;
import com.ygbd198.hellodoctor.util.ToastUtils;
import com.ygbd198.hellodoctor.widget.dialog.CheckPhoneNumDialog;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.ygbd198.hellodoctor.R.id.img_visible_integral;

/**
 * Created by guangjiqin on 2017/8/28.
 * 我的账户Activity
 */

public class MyIntegralActivity extends BaseActivity {

    @BindView(R.id.textView4)
    TextView tvIntergral;
    @BindView(R.id.tv_bank_card)
    TextView tvBanck;
    @BindView(img_visible_integral)
    ImageView imgVisibleIntegral;

    private int myIntegral;
    private long cardNum = -1;

    private MyIntegralController controller;
    private MyIntegralBean myIntegralBean;
    private CheckPhoneNumDialog checkPhoneNumDialog;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 1) {
            initData();
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View contentView = this.getLayoutInflater().inflate(R.layout.activity_my_integral, null);
        rootView.addView(contentView);
        ButterKnife.bind(this);

        titleBar.showCenterText(R.string.user_integral, R.drawable.my_integral, 0);

        initView();

        initController();

        initData();

    }

    private void initController() {
        controller = new MyIntegralController() {
            @Override
            public void getDataSuccess(MyIntegralBean myIntegralBean) {
                MyIntegralActivity.this.myIntegralBean = myIntegralBean;
                myIntegral = (myIntegralBean.integral < 0 ? 0 : myIntegralBean.integral);
                if (null == myIntegralBean.bank) {
                    tvBanck.setText("未绑定银行卡");
                } else {
                    tvBanck.setText(myIntegralBean.bank.name);
                    cardNum = myIntegralBean.bank.id;
                }
                tvIntergral.setText("" + myIntegral);
                if (null != checkPhoneNumDialog) {
                    checkPhoneNumDialog.setBank(myIntegralBean.bank);
                }

            }

            @Override
            public void getDataFailure(String msg) {

            }

            @Override
            public void exchangeSuccess() {

            }

            @Override
            public void exchangeFailrue(String msg) {

            }
        };
    }

    protected void initData() {
        controller.getData();
    }

    @OnClick(R.id.bt_exchange)//兑换积分
    public void exchange() {
        if (cardNum < 0) {
            ToastUtils.showToast("尚未绑定银行卡");
            return;
        }

        showExchangePop();

//        controller.exchangeIntegral(myIntegral, cardNum);// TODO: 2017/8/29 (兑换积分)
    }

    private void showExchangePop() {
        if (null == checkPhoneNumDialog) {
            checkPhoneNumDialog = new CheckPhoneNumDialog(this);
        }
        checkPhoneNumDialog.setBank(null == myIntegralBean.bank ? myIntegralBean.bank = new MyIntegralBean.BankBean() : myIntegralBean.bank);
        checkPhoneNumDialog.showDialog();

    }

    @OnClick(R.id.tv_bank_card)//我的银行卡
    public void bankCard() {
//        if (cardNum < 0) {
//            ToastUtils.showToast("尚未绑定银行卡");
//            return;
//        }
        Intent intent = new Intent(MyIntegralActivity.this, MyBankCardsActivity.class);
        intent.putExtra("cardBean", myIntegralBean.bank);
        startActivityForResult(intent, 1);
    }

    @OnClick(R.id.exchange_details)//兑换明细
    public void exchangeDet() {
        startActivity(new Intent(MyIntegralActivity.this, ExchangeIntegralRecordActivity.class));
    }

    @OnClick(R.id.integrate_record)//积分记录
    public void integrateRecord() {
        startActivity(new Intent(MyIntegralActivity.this, IntegraDetailActivity.class));
        ToastUtils.showToast("积分记录");
    }


    @OnClick(img_visible_integral)
    public void onVisibelIntegralClicked() {

        imgVisibleIntegral.setSelected(!imgVisibleIntegral.isSelected());

        if (imgVisibleIntegral.isSelected()) {
            imgVisibleIntegral.setImageResource(R.drawable.login_icon_hide);
            int length = tvIntergral.getText().toString().length();
            StringBuilder hideString = new StringBuilder();
            for (int i = 0; i < length; i++) {
                hideString.append("*");
            }
            tvIntergral.setText(hideString.toString());
        } else {
            imgVisibleIntegral.setImageResource(R.drawable.login_icon_display);
            tvIntergral.setText("" + myIntegral);
        }
    }
}
