package com.ygbd198.hellodoctor.ui;


import android.app.Activity;
import android.os.Bundle;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;

import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.service.ApplyBeDocService;
import com.ygbd198.hellodoctor.util.ToastUtils;

import retrofit2.Response;

/**
 * Created by guangjiqin on 2017/11/6.
 */

public class ActualOperatingActivity2 extends Activity implements View.OnClickListener {

    private int imgCode = 1;
    private ScaleAnimation scaleAni;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        scaleAni = new ScaleAnimation(0.8f, 1.0f, 0.8f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        scaleAni.setDuration(300);
        scaleAni.setRepeatMode(Animation.REVERSE);
        scaleAni.setRepeatCount(Animation.INFINITE);


        setContentView(R.layout.activity_sc_1);
        setOnClick(findViewById(R.id.bt));
        findViewById(R.id.arrow).setAnimation(scaleAni);

    }

    private void setOnClick(View view) {
        view.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        imgCode++;
//        ToastUtils.showToast(""+imgCode);
        switch (imgCode) {
            case 2:
                setContentView(R.layout.activity_sc_2);
                setOnClick(findViewById(R.id.bt));
                findViewById(R.id.arrow).setAnimation(scaleAni);
                break;
            case 3:
                setContentView(R.layout.activity_sc_3);
                setOnClick(findViewById(R.id.bt));
                findViewById(R.id.arrow).setAnimation(scaleAni);
                break;
            case 4:
                setContentView(R.layout.activity_sc_4);
                setOnClick(findViewById(R.id.bt));
                findViewById(R.id.arrow).setAnimation(scaleAni);
                break;
            case 5:
                setContentView(R.layout.activity_sc_5);
                setOnClick(findViewById(R.id.bt));
                findViewById(R.id.arrow).setAnimation(scaleAni);
                break;
            case 6:
                setContentView(R.layout.activity_sc_6);
                setOnClick(findViewById(R.id.bt));
                findViewById(R.id.arrow).setAnimation(scaleAni);
                break;
            case 7:
                setContentView(R.layout.activity_sc_7);
                setOnClick(findViewById(R.id.bt));
                findViewById(R.id.arrow).setAnimation(scaleAni);
                break;
            case 8:
                setContentView(R.layout.activity_sc_8);
                setOnClick(findViewById(R.id.bt));
                findViewById(R.id.arrow).setAnimation(scaleAni);
                break;
            case 9:
                setContentView(R.layout.activity_sc_9);
                setOnClick(findViewById(R.id.bt));
                findViewById(R.id.arrow).setAnimation(scaleAni);
                break;
            case 10:
                setContentView(R.layout.activity_sc_10);
                setOnClick(findViewById(R.id.bt));
                findViewById(R.id.arrow).setAnimation(scaleAni);
                break;
            case 11:
                setContentView(R.layout.activity_sc_11);
                setOnClick(findViewById(R.id.bt));
                findViewById(R.id.arrow).setAnimation(scaleAni);
                break;
            case 12:
                setContentView(R.layout.activity_sc_12);
                setOnClick(findViewById(R.id.bt));
                findViewById(R.id.arrow).setAnimation(scaleAni);
                break;
            case 13:
                setContentView(R.layout.activity_sc_13);
                setOnClick(findViewById(R.id.bt));
                findViewById(R.id.arrow).setAnimation(scaleAni);
                break;
            case 14:
                setContentView(R.layout.activity_sc_14);
                setOnClick(findViewById(R.id.bt));
                findViewById(R.id.arrow).setAnimation(scaleAni);
                break;
            case 15:
                setContentView(R.layout.activity_sc_15);
                setOnClick(findViewById(R.id.bt));
                findViewById(R.id.arrow).setAnimation(scaleAni);
                break;
            case 16:
                setContentView(R.layout.activity_sc_16);
                setOnClick(findViewById(R.id.bt));
                findViewById(R.id.arrow).setAnimation(scaleAni);
                break;
            case 17:
                setContentView(R.layout.activity_sc_17);
                setOnClick(findViewById(R.id.bt));
                findViewById(R.id.arrow).setAnimation(scaleAni);
                break;
            case 18:
                setContentView(R.layout.activity_sc_18);
                setOnClick(findViewById(R.id.bt));
                findViewById(R.id.arrow).setAnimation(scaleAni);
                break;
            case 19:
                setContentView(R.layout.activity_sc_19);
                setOnClick(findViewById(R.id.bt));
                findViewById(R.id.arrow).setAnimation(scaleAni);

                break;
            case 20:
                setContentView(R.layout.activity_sc_20);
                findViewById(R.id.arrow).setAnimation(scaleAni);
                findViewById(R.id.bt).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ToastUtils.showToast("发送通过实操演练");
                        RetrofitHelper.getInstance().createService(ApplyBeDocService.class).practicePass(MySelfInfo.getInstance().getToken()).enqueue(new BaseCallBack<BaseCallModel<String>>() {

                            @Override
                            public void onSuccess(Response<BaseCallModel<String>> response) {
                                ToastUtils.showToast("通过实操演练");
                                ActualOperatingActivity2.this.finish();
                            }

                            @Override
                            public void onFailure(String msg) {
                                ToastUtils.showToast(msg);
                            }
                        });
                    }
                });
                break;
        }


    }
}
