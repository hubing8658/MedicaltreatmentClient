package com.ygbd198.hellodoctor.ui.live.viewinterface;

/**
 * Created by monty on 2017/8/9.
 */

public interface ILoadingDialog {
    void showLoadingDialog(String msg);

    void closeLoadingDialog();
}
