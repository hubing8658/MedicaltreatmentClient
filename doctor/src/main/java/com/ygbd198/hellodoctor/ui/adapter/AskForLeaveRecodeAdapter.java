package com.ygbd198.hellodoctor.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.AskForLeaveRecodeBean;
import com.ygbd198.hellodoctor.common.MyBaseAdapter;

import java.util.List;

/**
 * Created by guangjiqin on 2017/8/30.
 */

public class AskForLeaveRecodeAdapter extends MyBaseAdapter {

    public AskForLeaveRecodeAdapter(Context context, List data) {
        super(context, data);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            holder = new Holder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_ask_for_leave_recode, null);
            holder.tvType = (TextView) convertView.findViewById(R.id.tv_type);
            holder.tvTime = (TextView) convertView.findViewById(R.id.tv_time);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        AskForLeaveRecodeBean bean = (AskForLeaveRecodeBean) mData.get(position);
        holder.tvType.setText(bean.type);
        holder.tvTime.setText(bean.startTime + "-" + bean.endTime);
        return convertView;
    }

    class Holder {
        TextView tvType;
        TextView tvTime;
    }
}
