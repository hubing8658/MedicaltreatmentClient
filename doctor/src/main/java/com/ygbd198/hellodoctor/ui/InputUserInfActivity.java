package com.ygbd198.hellodoctor.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.common.BaseActivity;
import com.ygbd198.hellodoctor.ui.controller.InputUserInfController;
import com.ygbd198.hellodoctor.util.ToastUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**IM
 * Created by guangjiqin on 2017/8/24.
 */

public class InputUserInfActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.tv_name)
    public TextView tvName;
    @BindView(R.id.tv_gender)
    public TextView tvGender;
    @BindView(R.id.tv_education)
    public TextView tvEducation;
    @BindView(R.id.tv_function)
    public TextView tvFunction;
    @BindView(R.id.tv_specialty)
    public TextView tvSpecialty;
    @BindView(R.id.tv_visitor_num)
    public TextView tvVisitorNum;
    @BindView(R.id.tv_introduce)
    public TextView tvIntroduce;

    private InputUserInfController controller;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View contentView = this.getLayoutInflater().inflate(R.layout.activity_input_user_inf, null);
        rootView.addView(contentView);
        ButterKnife.bind(this);

        initTitle();

        initView();

        initController();
    }

    @OnClick(R.id.bt_next)
    public void commitInf() {
        String name = tvName.getText().toString().trim();
        String gender = tvGender.getText().toString().trim();
        String education = tvEducation.getText().toString().trim();
        String function = tvFunction.getText().toString().trim();
        String specialty = tvSpecialty.getText().toString().trim();
        String visitorNum = tvVisitorNum.getText().toString().trim();
        String introduce = tvIntroduce.getText().toString().trim();

        if (name.isEmpty()) {
            ToastUtils.showToast("用户名不能为空");
            return;
        }
        if (gender.isEmpty()) {
            ToastUtils.showToast("性别还未选择");
            return;
        }
        if (education.isEmpty()) {
            ToastUtils.showToast("学历还未选择");
            return;
        }
        if (function.isEmpty()) {
            ToastUtils.showToast("学术职务不能为空");
            return;
        }
        if (specialty.isEmpty()) {
            ToastUtils.showToast("专长不能为空");
            return;
        }
        if (introduce.isEmpty()) {
            ToastUtils.showToast("个人简介不能为空");
            return;
        }

        controller.saveUserInf(name, gender, education, function, specialty, visitorNum, introduce);
    }

    @Override
    protected void initView() {
        tvName.setOnClickListener(this);
        tvGender.setOnClickListener(this);
        tvEducation.setOnClickListener(this);
        tvFunction.setOnClickListener(this);
        tvSpecialty.setOnClickListener(this);
        tvVisitorNum.setOnClickListener(this);
        tvIntroduce.setOnClickListener(this);
    }

    private void initController() {
        controller = new InputUserInfController(this, MySelfInfo.getInstance().getToken()) {
            @Override
            public void commitInfSuccess() {
                ToastUtils.showToast("个人资料已完善");
                Intent intentRecordVideo = new Intent(InputUserInfActivity.this, RecordVideoActivity.class);
                startActivity(intentRecordVideo);
                finish();
            }

            @Override
            public void commitInfFailure(String msg) {
                ToastUtils.showToast(msg);
            }

        };
    }

    private void initTitle() {
        titleBar.showCenterText("完善资料");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.tv_name:
//                controller.showUserNameInppop();
//                break;
            case R.id.tv_gender:
                controller.showGenderpPop();
                break;
            case R.id.tv_education:
                controller.showEducationPop();
                break;
//            case R.id.tv_function:
//                controller.showFunctionInppop();
//                break;
//            case R.id.tv_introduce:
//                controller.showintroductionInppop();
//                break;
//            case R.id.tv_visitor_num:
//                controller.showvisitorNumInputPop();
//                break;
//            case R.id.tv_specialty:
//                controller.showSpecialtynppop();
//                break;

        }
    }
}
