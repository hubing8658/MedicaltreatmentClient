package com.ygbd198.hellodoctor.ui.live.fragment;

import android.support.v4.app.Fragment;

/**
 * 注释
 *
 * @e-mail mwu@szrlzz.com
 * Created by monty on 2017/8/20.
 */
@Deprecated
public class DistributeFragment{
    public static final String WAITINGUSERLIST = "WaitingUserList"; //排队的用户列表
    public static final String DISEASELIST = "diseaseList"; // 病症列表
    public static final String PRESCRIPTIONLIST = "prescription"; // 药方
    public static final String SUBSEQUENT_VISIT = "subsequent_visit"; // 方案
    public static final String BITE_AND_SUP = "bite_and_sup"; // 饮食
    public static final String WORK_AND_REST = "work_and_rest"; // 作息
    public static final String TABOOS = "taboos"; // 禁忌
    public static final String SCHEME_SUMMARY = "scheme_summary"; // 方案汇总

    public DistributeFragment(String tag, Fragment fragment) {
        this.tag = tag;
        this.fragment = fragment;
    }

    private String tag;
    private Fragment fragment;

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Fragment getFragment() {
        return fragment;
    }

    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }
}
