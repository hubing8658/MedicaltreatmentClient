package com.ygbd198.hellodoctor.ui.live.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.DiseaseTypeBean;

import java.util.List;

/**
 * 病症列表(不发送事件)
 * Created by monty on 2017/8/22.
 */

public class SymptomAdapter2 extends BaseAdapter {
    private Context context;
    private List<DiseaseTypeBean> diseaseTypeList;

    public SymptomAdapter2(Context context, List<DiseaseTypeBean> diseaseTypeList) {
        this.context = context;
        this.diseaseTypeList = diseaseTypeList;
    }

    @Override
    public int getCount() {
        return diseaseTypeList.size();
    }

    @Override
    public Object getItem(int position) {
        return diseaseTypeList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.layout_symptom_item, parent, false);
            viewHolder.tvType = (TextView) convertView.findViewById(R.id.tv_type);
            viewHolder.ivCheck = (ImageView) convertView.findViewById(R.id.iv_check);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.tvType.setText((position + 1) + "." + diseaseTypeList.get(position).typeName);
        if (diseaseTypeList.get(position).isChecked) {
            viewHolder.ivCheck.setImageResource(R.mipmap.home_icon_ri);
        } else {
            viewHolder.ivCheck.setImageResource(0);
        }
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (DiseaseTypeBean diseaseType : diseaseTypeList) {
                    diseaseType.isChecked = false;
                }
                diseaseTypeList.get(position).isChecked = true;
                notifyDataSetChanged();
                onItemClickListener.onItemClick(v,position);
            }
        });
        return convertView;
    }
    private OnItemClickListener onItemClickListener;
    public interface OnItemClickListener{
        void onItemClick(View v,int position);
    }
    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }

    public void notifyDataSetChanged(List<DiseaseTypeBean> diseaseTypeList) {
        this.diseaseTypeList = diseaseTypeList;
        notifyDataSetChanged();
    }

    static class ViewHolder {
        TextView tvType;
        ImageView ivCheck;
    }
}
