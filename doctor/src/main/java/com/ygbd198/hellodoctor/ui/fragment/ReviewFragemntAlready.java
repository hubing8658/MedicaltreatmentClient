package com.ygbd198.hellodoctor.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.bean.ReviewBean;
import com.ygbd198.hellodoctor.service.MyCenterService;
import com.ygbd198.hellodoctor.ui.adapter.ReviewListAdapter;
import com.ygbd198.hellodoctor.util.ToastUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

/**
 * Created by guangjiqin on 2017/9/11.
 */

public class ReviewFragemntAlready extends Fragment {


    private View view;
    private static ReviewFragemntAlready fragment;

    private ListView listView;
    private ReviewListAdapter adapter;
    private List<ReviewBean> data;

    public static ReviewFragemntAlready createInstance() {
        if (null == fragment) {
            fragment = new ReviewFragemntAlready();
        }
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_review_list, null);

        initView();

        initData();

        return view;
    }

    private void initData() {
//        data.clear();
//        for (int i = 0; i < 10; i++) {
//            ReviewBean bean = new ReviewBean();
//            bean.name = "name " + i;
//            bean.time = "2017-01-0" + i;
//            data.add(bean);
//        }
//        adapter.notifyDataSetChanged();

        RetrofitHelper.getInstance().createService(MyCenterService.class).getRevisits(MySelfInfo.getInstance().getToken(), "2").enqueue(new BaseCallBack<BaseCallModel<List<ReviewBean>>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<List<ReviewBean>>> response) {
                data.clear();
                data.addAll(response.body().data);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(String message) {
                ToastUtils.showToast(message);
            }
        });
    }

    private void initView() {
        listView = (ListView) view.findViewById(R.id.listview);
        data = new ArrayList<>();
        adapter = new ReviewListAdapter(getActivity(), data);
        listView.setAdapter(adapter);
    }
}
