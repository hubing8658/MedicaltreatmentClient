package com.ygbd198.hellodoctor.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.bigkoo.pickerview.TimePickerView;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.CurVisitorTimeBean;
import com.ygbd198.hellodoctor.common.BaseActivity;
import com.ygbd198.hellodoctor.ui.controller.ChangeVisitorTimeController;
import com.ygbd198.hellodoctor.util.SerializableMap;
import com.ygbd198.hellodoctor.widget.DatePickerFactory;
import com.ygbd198.hellodoctor.widget.VisitorTimeView.SetVisitorTimeView;
import com.ygbd198.hellodoctor.widget.VisitorTimeView.TimeBoxEntity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by guangjiqin on 2017/8/30.
 */

public class ChangeVisitorTimeActivity extends BaseActivity {

    private ChangeVisitorTimeController controller;
    private List<String> strTemp;

    private TimePickerView timePickerView;


    @BindView(R.id.tv_year)
    TextView tvYear;
    @BindView(R.id.tv_month)
    TextView tvMonth;
    @BindView(R.id.tv_day)
    TextView tvDay;

    @BindView(R.id.timeview1)
    SetVisitorTimeView timeView1;
    @BindView(R.id.timeview2)
    SetVisitorTimeView timeView2;
    @BindView(R.id.timeview3)
    SetVisitorTimeView timeView3;
    @BindView(R.id.timeview4)
    SetVisitorTimeView timeView4;
    @BindView(R.id.timeview5)
    SetVisitorTimeView timeView5;
    @BindView(R.id.timeview6)
    SetVisitorTimeView timeView6;
    @BindView(R.id.timeview7)
    SetVisitorTimeView timeView7;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View contentView = this.getLayoutInflater().inflate(R.layout.activity_change_visitor_time, null);
        rootView.addView(contentView);
        ButterKnife.bind(this);

        initTitle();

        initController();

        initData();

    }

    @OnClick(R.id.rel_change_time)
    public void changeData() {
//        DateChooseWheelViewDialog endDateChooseDialog = new DateChooseWheelViewDialog(this,
//                new DateChooseWheelViewDialog.DateChooseInterface() {
//                    @Override
//                    public void getDateTime(String time, boolean longTimeChecked) {
//
//                        String regex = "\\d*";
//                        Pattern p = Pattern.compile(regex);
//                        Matcher m = p.matcher(time);
//
//                        strTemp.clear();
//                        while (m.find()) {
//                            if (!"".equals(m.group())) {
//                                strTemp.add(m.group());
//                            }
//                        }
//
//                        tvYear.setText(strTemp.get(0));
//                        tvMonth.setText(strTemp.get(1));
//                        tvDay.setText(strTemp.get(2));
//
//                    }
//                });
//        endDateChooseDialog.setTimePickerGone(false);
//        endDateChooseDialog.setDateDialogTitle("请选择开始变更时间");
//        endDateChooseDialog.showDateChooseDialog();

        timePickerView = DatePickerFactory.createDatePicker(this, "请选择开始变更时间", new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {
                timePickerView.dismissDialog();
                tvYear.setText("" + (1900 + date.getYear()));
                tvMonth.setText("" + (1+date.getMonth()));
                tvDay.setText("" + (date.getDate()));

            }
        }, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timePickerView.returnData();
            }
        });
        timePickerView.show();

    }

    protected void initData() {
        controller.getTime();
    }

    private void initController() {
        controller = new ChangeVisitorTimeController() {
            @Override
            public void getTimeSuccess(CurVisitorTimeBean curVisitorTimeBean) {
                timeView1.setTime(timeChange(curVisitorTimeBean.monday));
                timeView2.setTime(timeChange(curVisitorTimeBean.tuesday));
                timeView3.setTime(timeChange(curVisitorTimeBean.wednesday));
                timeView4.setTime(timeChange(curVisitorTimeBean.thursday));
                timeView5.setTime(timeChange(curVisitorTimeBean.friday));
                timeView6.setTime(timeChange(curVisitorTimeBean.saturday));
                timeView7.setTime(timeChange(curVisitorTimeBean.sunday));
            }

            @Override
            public void getTimeFailrue(String msg) {

            }

            @Override
            public void setTimeSuccess() {

            }

            @Override
            public void setTimeFailrue(String msg) {

            }
        };
    }

    private void initTitle() {
        titleBar.showCenterText(R.string.changetime_cen_title,R.drawable.treatmenttime,0);
        titleBar.setBackBtnClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back();
                finish();
            }
        });

        //默认设置一周后
        Calendar now = Calendar.getInstance();
        long curTime = now.getTimeInMillis() + (8 * 24 * 60 * 60 * 1000);

        Date d = new Date(curTime);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd");
        String strData = sdf.format(d);


        String regex = "\\d*";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(strData);

        strTemp = new ArrayList<>();
        while (m.find()) {
            if (!"".equals(m.group())) {
                strTemp.add(m.group());
            }
        }

        tvYear.setText(strTemp.get(0));
        tvMonth.setText(strTemp.get(1));
        tvDay.setText(strTemp.get(2));


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        back();
    }

    private void back() {
        //构造返回结果
        Map<String, String> workTime = new HashMap<>();
        List<SetVisitorTimeView> timeViews = new ArrayList<>();
        timeViews.add(timeView1);
        timeViews.add(timeView2);
        timeViews.add(timeView3);
        timeViews.add(timeView4);
        timeViews.add(timeView5);
        timeViews.add(timeView6);
        timeViews.add(timeView7);
        for (int i = 0; i < timeViews.size(); i++) {
            List<TimeBoxEntity> timeBoxs = timeViews.get(i).timeBoxEntities;
            String tempTime = "";
            for (TimeBoxEntity timeBoxEntity : timeBoxs) {
                if (!"添加时间".equals(timeBoxEntity.time)) {
                    tempTime = tempTime + "," + timeBoxEntity.time;
                    workTime.put(String.valueOf(i + 1), tempTime);
                }
            }

        }

        //发送返回结果
        Intent intent = new Intent();
        Bundle bundle = new Bundle();

        SerializableMap tmpmap = new SerializableMap();
        tmpmap.setMap(workTime);

        bundle.putSerializable("workTime", tmpmap);

        String data = strTemp.get(0) + "-" + strTemp.get(1) + "-" + strTemp.get(2);
        intent.putExtra("startData", data);

        intent.putExtras(bundle);
        setResult(1, intent);
    }
}
