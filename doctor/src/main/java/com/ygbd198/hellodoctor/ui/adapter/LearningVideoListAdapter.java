package com.ygbd198.hellodoctor.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.VedioBean;
import com.ygbd198.hellodoctor.common.MyBaseAdapter;
import com.ygbd198.hellodoctor.util.DateUtil;
import com.ygbd198.hellodoctor.util.imageloader.GlideImageHelper;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by guangjiqin on 2017/8/23.
 */

public class LearningVideoListAdapter extends MyBaseAdapter {


    public LearningVideoListAdapter(Context context, List data) {
        super(context, data);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            holder = new Holder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_learning_vedio, null);
            holder.imgVedio = (ImageView) convertView.findViewById(R.id.img_vedio);
            holder.tvTitle = (TextView) convertView.findViewById(R.id.tv_title);
            holder.tvContent = (TextView) convertView.findViewById(R.id.tv_content);
            holder.tvTime = (TextView) convertView.findViewById(R.id.tv_time);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        VedioBean bean = (VedioBean) mData.get(position);
        GlideImageHelper.showImage(mContext, bean.pic, R.drawable.logo, R.drawable.logo, holder.imgVedio);
        holder.tvTitle.setText(bean.name);
        holder.tvContent.setText(bean.introduction);
        if (bean.isRead) {
            holder.tvTime.setText("已观看");

        } else {
            holder.tvTime.setText(DateUtil.secondToMinute(bean.longTime));

        }
        return convertView;
    }

    class Holder {
        ImageView imgVedio;
        TextView tvTitle;
        TextView tvContent;
        TextView tvTime;
    }

}
