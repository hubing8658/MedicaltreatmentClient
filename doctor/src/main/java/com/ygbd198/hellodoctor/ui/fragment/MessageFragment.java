
package com.ygbd198.hellodoctor.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.DoctorMessageEnt;
import com.ygbd198.hellodoctor.modules.message.MessageAdapter;
import com.ygbd198.hellodoctor.modules.message.MessageContract.IMessagePresenter;
import com.ygbd198.hellodoctor.modules.message.MessageContract.IMessageView;
import com.ygbd198.hellodoctor.modules.message.MessagePresenter;
import com.ygbd198.hellodoctor.widget.BaseTitleBar;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * 消息 Created by monty on 2017/8/31.
 */
public class MessageFragment extends Fragment implements IMessageView {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    Unbinder unbinder;

    @BindView(R.id.titleBar)
    BaseTitleBar titleBar;

    @BindView(R.id.lv_doctor_message)
    ListView doctorMessageLv;

    private IMessagePresenter presenter;

    private MessageAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the base_dilog_layout for this fragment
        View view = inflater.inflate(R.layout.fragment_me, container, false);
        unbinder = ButterKnife.bind(this, view);

        titleBar.showCenterText(R.string.title_message, R.drawable.message, 0);
        titleBar.setBackBtnVis(false);

        initView(view);
        loadData();

        return view;
    }

    private void initView(View rootView) {
        adapter = new MessageAdapter(getActivity());
        doctorMessageLv.setAdapter(adapter);
    }

    private void loadData() {
        presenter = new MessagePresenter(getActivity(), this);
        presenter.getDoctorMessages(1, 10);
    }

    @Override
    public void onDestroyView() {
        if (unbinder != null) {
            unbinder.unbind();
        }
        super.onDestroyView();
    }

    @Override
    public void onLoadMessagesSuccess(List<DoctorMessageEnt> messageEnts) {
        adapter.setData(messageEnts);
    }

    @Override
    public void onLoadMessagesFailed() {
    }

}
