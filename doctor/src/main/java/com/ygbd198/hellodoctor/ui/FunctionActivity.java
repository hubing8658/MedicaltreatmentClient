package com.ygbd198.hellodoctor.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.LinearLayout;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.common.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by guangjiqin on 2017/8/31.
 */

public class FunctionActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.lin_function)
    LinearLayout linFunction;
    @BindView(R.id.lin_how)
    LinearLayout linHow;
    @BindView(R.id.lin_guide)
    LinearLayout linGuide;
    @BindView(R.id.lin_guide_video)
    LinearLayout linGuideVideo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View contentView = this.getLayoutInflater().inflate(R.layout.activity_function, null);
        rootView.addView(contentView);
        ButterKnife.bind(this);

        setOnClick();

        titleBar.showCenterText(R.string.function,R.drawable.function_guide,0);
    }

    private void setOnClick() {
        linFunction.setOnClickListener(this);
        linHow.setOnClickListener(this);
        linGuide.setOnClickListener(this);
        linGuideVideo.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(FunctionActivity.this, FunctionCommonActivity.class);
        switch (v.getId()) {
            case R.id.lin_function://  3-功能介绍、4-如何视频问诊、5-视频操作指导、
                intent.putExtra("function", 3);
                startActivity(intent);
                break;
            case R.id.lin_how:
                intent.putExtra("function", 4);
                startActivity(intent);
                break;
            case R.id.lin_guide:
                intent.putExtra("function", 5);
                startActivity(intent);
                break;
            case R.id.lin_guide_video:
                startActivity(new Intent(this, LearningVideoActivity.class));
                break;
            default:
                break;
        }

    }
}
