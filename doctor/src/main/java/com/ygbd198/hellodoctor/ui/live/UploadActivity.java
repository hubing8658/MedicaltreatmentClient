package com.ygbd198.hellodoctor.ui.live;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.GridView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.UploadRetrofitHelper;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.common.BaseActivity;
import com.ygbd198.hellodoctor.service.ApplyBeDocService;
import com.ygbd198.hellodoctor.util.ToastUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;
import top.zibin.luban.Luban;

public class UploadActivity extends BaseActivity {
    @BindView(R.id.gridview)
    GridView gridview;
    @BindView(R.id.btn_upload)
    Button btnUpload;
    private String path;
    private File[] images;
    private boolean[] checkedStatus;
    public static final int REQUEST_CODE_UPLOAD = 0x102;

    public static void GotoActivity(Activity context, String path) {
        Intent intent = new Intent(context, UploadActivity.class);
        intent.putExtra("path", path);
        context.startActivityForResult(intent, REQUEST_CODE_UPLOAD);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        ButterKnife.bind(this);

        titleBar.showCenterText(R.string.title_upload, R.drawable.treatment_plan, 0);
        titleBar.setBackBtnClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_OK);
                finish();
            }
        });
        try {
            path = getIntent().getStringExtra("path");
            File file = new File(path);
            images = file.listFiles();
            checkedStatus = new boolean[images.length];
            gridview.setAdapter(new ImageAdapter());

            btnUpload.setText("上传(0/" + images.length + ")");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @OnClick(R.id.btn_upload)
    public void onViewClicked() {
        List<File> files = new ArrayList<>();
        for (int i = 0; i < checkedStatus.length; i++) {
            if (checkedStatus[i]) {
                files.add(images[i]);
            }
        }
        if (getCheckedCount() == 0) {
            ToastUtils.showToast("请勾选要上传的图片");
        } else {
            uploadFile(files);
        }
    }

    class UploadProcess {
        public int count;
        public int position;
    }

    /**
     * 文件上传(同步方法，在子线程中执行)
     */
    public void uploadFile(final List<File> files) {
        Observable.fromIterable(files)
                .map(new Function<File, File>() {
                    @Override
                    public File apply(@NonNull File s) throws Exception {
                        File file = Luban.with(UploadActivity.this).get(s.getAbsolutePath());
                        Log.d("monty", "压缩完成,压缩后的路径 -> " + file.getAbsolutePath());
                        return file;
                    }
                })
                .map(new Function<File, UploadProcess>() {
                    int position = 0;

                    @Override
                    public UploadProcess apply(File file) throws Exception {
                        Log.d("monty", "uploadFile -> " + file.getAbsolutePath());
                        MultipartBody.Builder builder = new MultipartBody.Builder()
                                .setType(MultipartBody.FORM)//表单类型
                                .addFormDataPart("token", MySelfInfo.getInstance().getToken())
                                .addFormDataPart("type", "7");

                        RequestBody imageBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                        builder.addFormDataPart("file", file.getName(), imageBody);//imgfile 后台接收图片流的参数名

                        List<MultipartBody.Part> parts = builder.build().parts();
                        Log.d("monty", "开始上传请求");
                        UploadProcess uploadProcess = new UploadProcess();
                        uploadProcess.count = files.size();

                        Response<BaseCallModel<List<String>>> response = UploadRetrofitHelper.getInstance().uploadImage(ApplyBeDocService.class).doctorUpload(parts).execute();
//                        Log.d("monty", "文件上传 -> result -> " + response.body().toString());
                        if (response.body().code == 200) {
                            uploadProcess.position = ++position;
                        } else {
                            throw new RuntimeException(response.body().msg);
                        }

                        return uploadProcess;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UploadProcess>() {
                    ProgressDialog progressDialog;

                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.d("monty", "文件上传 -> onSubscribe");
                        btnUpload.setEnabled(false);
                        progressDialog = new ProgressDialog(UploadActivity.this);
                        progressDialog.setCancelable(false);
                        progressDialog.setMessage("正在上传:" + 0 + "/" + files.size());
                        progressDialog.show();
                    }

                    @Override
                    public void onNext(UploadProcess value) {
                        Log.d("monty", "文件上传 -> onNext");

                        progressDialog.setMessage("正在上传:" + value.position + "/" + value.count);
                        if (value.position == value.count) {
                            progressDialog.dismiss();
//                            ToastUtils.showToast("文件上传完成");
                            new AlertDialog.Builder(UploadActivity.this).setMessage("截图上传完成").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    setResult(RESULT_OK);
                                    finish();
                                }
                            }).show();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("monty", "文件上传 -> onError -> " + e.toString());
                        ToastUtils.showToast("文件上传失败" + e.toString());
                        btnUpload.setEnabled(true);
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onComplete() {
                        Log.d("monty", "文件上传 -> onComplete");
                        btnUpload.setEnabled(true);
                        progressDialog.dismiss();
                    }
                });
    }

    private int getCheckedCount() {
        int count = 0;
        for (int i = 0; i < checkedStatus.length; i++) {
            if (checkedStatus[i]) {
                count++;
            }
        }
        return count;
    }


    public class ImageAdapter extends BaseAdapter {

        public ImageAdapter() {
            for (int i = 0; i < images.length; i++) {
                checkedStatus[i] = false;
            }
        }

        @Override
        public int getCount() {
            return images.length;
        }

        @Override
        public Object getItem(int position) {
            return images[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder = null;
            if (convertView == null) {
                convertView = LayoutInflater.from(UploadActivity.this).inflate(R.layout.upload_item, parent, false);
                viewHolder = new ViewHolder(convertView);
                int screenWidth = getScreenWidth(UploadActivity.this);
                AbsListView.LayoutParams param = new AbsListView.LayoutParams(screenWidth / 3, screenWidth / 3);
                convertView.setLayoutParams(param);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    checkedStatus[position] = isChecked;
                    btnUpload.setText("上传(" + getCheckedCount() + "/" + images.length + ")");
                }
            });

            final ViewHolder finalViewHolder = viewHolder;
            viewHolder.image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finalViewHolder.checkbox.setChecked(!finalViewHolder.checkbox.isChecked());
                }
            });
            Log.d("monty", "file path -> " + images[position]);
            Glide.with(UploadActivity.this)
                    .load(images[position])
                    .centerCrop()
                    .into(viewHolder.image);
            return convertView;
        }

        class ViewHolder {
            @BindView(R.id.image)
            ImageView image;
            @BindView(R.id.checkbox)
            CheckBox checkbox;

            ViewHolder(View view) {
                ButterKnife.bind(this, view);
            }
        }
    }

    public static int getScreenWidth(Context context) {
        WindowManager manager = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        Display display = manager.getDefaultDisplay();
        return display.getWidth();
    }
    /*public static int dipToPx(Context context, int dip) {
        if(density <= 0.0F) {
            density = context.getResources().getDisplayMetrics().density;
        }

        return (int)((float)dip * density + 0.5F);
    }*/


}
