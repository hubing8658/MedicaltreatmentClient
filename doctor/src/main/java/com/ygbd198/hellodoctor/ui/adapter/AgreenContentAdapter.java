package com.ygbd198.hellodoctor.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.AgreenDetailBean;
import com.ygbd198.hellodoctor.common.MyBaseAdapter;

import java.util.List;

/**
 * Created by guangjiqin on 2017/8/26.
 */

public class AgreenContentAdapter extends MyBaseAdapter {


    public AgreenContentAdapter(Context context, List data) {
        super(context, data);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder = null;
        if (convertView == null) {
            holder = new Holder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_agreement_content, null);
            holder.tvContent = (TextView) convertView.findViewById(R.id.tv_content);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        AgreenDetailBean bean = (AgreenDetailBean) mData.get(position);
        holder.tvContent.setText(bean.content);

        return convertView;
    }

    class Holder {
        TextView tvContent;
    }
}
