package com.ygbd198.hellodoctor.ui.live;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.ygbd198.hellodoctor.R;

/**
 * 单张图片预览
 */
public class PreviewActivity extends AppCompatActivity {

    public static void GotoActivity(Context context,String path){
        Intent intent = new Intent(context,PreviewActivity.class);
        intent.putExtra("path",path);
        context.startActivity(intent);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);
        ImageView imageView = (ImageView) findViewById(R.id.imageview);
        String path = getIntent().getStringExtra("path");
        Glide.with(this).load(path).into(imageView);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
