package com.ygbd198.hellodoctor.ui;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.monty.library.FileUtil;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.DiseaseTypeBean;
import com.ygbd198.hellodoctor.bean.DoctorBean;
import com.ygbd198.hellodoctor.bean.DoctorRoleBean;
import com.ygbd198.hellodoctor.common.BaseActivity;
import com.ygbd198.hellodoctor.ui.controller.BeDocController;
import com.ygbd198.hellodoctor.util.SerializableMap;
import com.ygbd198.hellodoctor.util.ToastUtils;
import com.ygbd198.hellodoctor.util.imageloader.GlideImageHelper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Administrator on 2017/8/3 0003.
 * 业务说明：
 * 填写资料后提交，跳转到视频录制页面，页面录制完毕后返回这个页面，上传成功结束该页面，跳转
 * 到首页（3个table页面的地方，显示当前视频学习进度，实习演练等等）
 */

public class ApplyBeDocActivity extends BaseActivity {


    private String TAG = getClass().getName();

//    private String [] strIdentityCards;//身份证地址（多个逗号隔开）
//    private String []strEmployeeCards;//工作证地址
//    private String []strOtherCards ;//其他证件的地址


    private Map<Integer, String> employeeCardsnew = new HashMap<>();//学历证
    private Map<Integer, String> identityCards = new HashMap<>();//转化为string 身份证地址（多个逗号隔开）
    private Map<Integer, String> otherCards = new HashMap<>();//其他证件的地址

    public int doctorRole = -1;//医生角色id
    public int diseaseType = -1;//疾病类型id
    public List<Integer> diseaseTypes = new ArrayList<>();
    public static Map<String, String> workTime = new HashMap<>();

    public String monday;//每天工作时间
    public String tuesday;
    public String wednesday;
    public String thursday;
    public String friday;
    public String saturday;
    public String sunday;


    private BeDocController controller;


    @BindView(R.id.tv_name)
    public EditText tvName;
    @BindView(R.id.tv_hospital)
    public EditText tvHospital;
    @BindView(R.id.tv_level)
    public TextView tvLevel;
    @BindView(R.id.tv_type)
    public TextView tvType;
    @BindView(R.id.tv_visitor_time)
    public TextView tvTime;


    @BindView(R.id.img_diploma_front)
    ImageView imgDiplomaFront;
    @BindView(R.id.img_diploma_back)
    ImageView imgDiplomaBack;
    @BindView(R.id.img_diploma_hold)
    ImageView imgDiplomaHold;

    @BindView(R.id.img_identity_card_front)
    ImageView imgIdentityFront;
    @BindView(R.id.img_identity_card_back)
    ImageView imgIdentityBack;
    @BindView(R.id.img_identity_card_hold)
    ImageView imgIdentityHold;

    @BindView(R.id.img_doc_aptitude_1)
    ImageView imgDoc1;
    @BindView(R.id.img_doc_aptitude_2)
    ImageView imgDoc2;
    @BindView(R.id.img_doc_aptitude_3)
    ImageView imgDoc3;

    private DoctorBean bean;


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

//        if(null == data || null == data.getData()){
//            return;
//        }

        if (requestCode == 999 && resultCode == 999) {
            finish();
        }
        String path = Environment.getExternalStorageDirectory().toString() + "/helloDoctorCache";//设置缓存文件夹
        File file = null;
        if (requestCode < 10) {
            file = new File(path + "/" + String.valueOf(requestCode) + ".jpg");//拍照文件位置
        } else if (requestCode > 50) {
            if (null != data && null != data.getData()) {
                file = new File(FileUtil.getPath(this,data.getData()));//图库文件
            }

        } else {
            file = new File(path + "/" + String.valueOf(requestCode) + ".mp4");//录像文件位置
        }

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 4;

        Bitmap bitmap = null;

        if (requestCode > 50 ) {
            try {
                if (null == data || null == data.getData()) {
                    return;
                }
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            if (requestCode != 11) {//11是视频
                bitmap = BitmapFactory.decodeFile(path + "/" + String.valueOf(requestCode) + ".jpg", options);
            }
        }


        switch (requestCode) {
            case 100:
            case 1:

                if(null == bitmap){
                    return;
                }
                imgDiplomaFront.setImageBitmap(bitmap);
                break;
            case 200:
            case 2:

                if(null == bitmap){
                    return;
                }
                imgDiplomaBack.setImageBitmap(bitmap);
                break;
            case 300:
            case 3:

                if(null == bitmap){
                    return;
                }
                imgDiplomaHold.setImageBitmap(bitmap);
                break;
            case 4:
            case 400:

                if(null == bitmap){
                    return;
                }
                imgIdentityFront.setImageBitmap(bitmap);
                break;
            case 5:
            case 500:

                if(null == bitmap){
                    return;
                }
                imgIdentityBack.setImageBitmap(bitmap);
                break;
            case 6:
            case 600:

                if(null == bitmap){
                    return;
                }
                imgIdentityHold.setImageBitmap(bitmap);
                break;
            case 7:
            case 700:

                if(null == bitmap){
                    return;
                }
                imgDoc1.setImageBitmap(bitmap);
                break;
            case 8:
            case 800:

                if(null == bitmap){
                    return;
                }
                imgDoc2.setImageBitmap(bitmap);
                break;
            case 9:
            case 900:

                if(null == bitmap){
                    return;
                }
                imgDoc3.setImageBitmap(bitmap);
                break;
            case 10://工作时间
                workTime.clear();
                SerializableMap tempData = (SerializableMap) data.getExtras().get("workTime");
                workTime.putAll(tempData.getMap());
                if (workTime.size() == 0) {
                    tvTime.setText("未设置");
                } else {
                    tvTime.setText("已设置");
                }
                break;
            case 11://上传视屏
                showLoadingDialog("视频上传中，请稍候");
                controller.uploadVideoFile(requestCode, file);
                break;
            default:
                break;
        }
        if (requestCode < 10 || requestCode > 99) {

            if(null == bitmap){
                return;
            }

            showLoadingDialog("照片上传中请稍后");
            dialog.setCancelable(false);

            int type = -1;
            switch (requestCode) {
                case 100:
                case 1:
                case 200:
                case 2:
                case 300:
                case 3:
                    type = 2;
                    break;
                case 4:
                case 400:
                case 5:
                case 500:
                case 6:
                case 600:
                    type = 3;
                    break;
                case 7:
                case 700:
                case 8:
                case 800:
                case 9:
                case 900:
                    type = 4;
                    break;
            }
            controller.uploadFile(requestCode, file, type);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        verifyStoragePermissions(this);

        View contentView = this.getLayoutInflater().inflate(R.layout.activity_apply_be_doc, null);
        rootView.addView(contentView);
        ButterKnife.bind(this);

        getIntentData();

        initTitle();

        initController();

        initViewData();
    }

    private void initController() {
        //// TODO: 2017/8/20 (测试)
        controller = new BeDocController(this, bean.getToken()) {
            //        controller = new BeDocController(this, "eh1231871583wt") {
            @Override
            public void getTypeSuccess(List<DiseaseTypeBean> diseaseTypeBeen) {
            }

            @Override
            public void getTypeFailure(String msg) {


            }

            @Override
            public void uploadFileSuccess(int imgId, String url) {
                Log.i("gg", "imgId = " + imgId);


                String path = Environment.getExternalStorageDirectory().toString() + "/helloDoctorCache";//设置缓存文件夹
                File file = new File(path);
                File files[] = file.listFiles();
                if (null != files && files.length > 0) {
                    for (File f : files) {
                        if (f.isFile()) {
                            f.delete();
                        }
                    }
                    file.delete();
                }

                closeLoadingDialog();
                switch (imgId) {
                    case 100:
                    case 1:
                    case 200:
                    case 2:
                    case 300:
                    case 3:
                        ToastUtils.showToast("图片上传成功");
                        employeeCardsnew.put(imgId, url);
                        switch (imgId) {
                            case 100:
                            case 1:
                                GlideImageHelper.showImage(ApplyBeDocActivity.this, url, R.drawable.logo, R.drawable.logo, imgDiplomaFront);
                                break;
                            case 200:
                            case 2:
                                GlideImageHelper.showImage(ApplyBeDocActivity.this, url, R.drawable.logo, R.drawable.logo, imgDiplomaBack);
                                break;
                            case 300:
                            case 3:
                                GlideImageHelper.showImage(ApplyBeDocActivity.this, url, R.drawable.logo, R.drawable.logo, imgDiplomaHold);
                                break;
                        }
                        break;
                    case 4:
                    case 400:
                    case 5:
                    case 500:
                    case 6:
                    case 600:
                        ToastUtils.showToast("图片上传成功");
                        identityCards.put(imgId, url);
                        switch (imgId) {
                            case 4:
                            case 400:
                                GlideImageHelper.showImage(ApplyBeDocActivity.this, url, R.drawable.logo, R.drawable.logo, imgIdentityFront);
                                break;
                            case 5:
                            case 500:
                                GlideImageHelper.showImage(ApplyBeDocActivity.this, url, R.drawable.logo, R.drawable.logo, imgIdentityBack);
                                break;
                            case 6:
                            case 600:
                                GlideImageHelper.showImage(ApplyBeDocActivity.this, url, R.drawable.logo, R.drawable.logo, imgIdentityHold);
                                break;
                        }
                        break;
                    case 7:
                    case 700:
                    case 8:
                    case 800:
                    case 9:
                    case 900:
                        ToastUtils.showToast("图片上传成功");
                        otherCards.put(imgId, url);
                        switch (imgId) {
                            case 7:
                            case 700:
                                GlideImageHelper.showImage(ApplyBeDocActivity.this, url, R.drawable.logo, R.drawable.logo, imgDoc1);
                                break;
                            case 8:
                            case 800:
                                GlideImageHelper.showImage(ApplyBeDocActivity.this, url, R.drawable.logo, R.drawable.logo, imgDoc2);
                                break;
                            case 9:
                            case 900:
                                GlideImageHelper.showImage(ApplyBeDocActivity.this, url, R.drawable.logo, R.drawable.logo, imgDoc3);
                                break;
                        }

                        break;
                    case 10://工作时间
                        break;
                    case 11://上传视屏
                        ToastUtils.showToast("视频上传成功,审核结果1-3个工作日内会以短信的形式通知您，请留意!");
                        finish();
                        break;
                }
                Log.i("gg", "employeeCardsnew = " + employeeCardsnew.size());
                Log.i("gg", "identityCards = " + identityCards.size());
                Log.i("gg", "otherCards = " + otherCards.size());


            }

            @Override
            public void uploadFileFailure(String msg) {
                closeLoadingDialog();
                ToastUtils.showToast("上传失败");
            }

            @Override
            public void getRoleSuccess(List<DoctorRoleBean> doctorRoleBeen) {
            }

            @Override
            public void getRoleFailure(String msg) {

            }

            @Override
            public void commitDoctorInfSuccess() {//提交成功录制认证视屏
//                showLoadingDialog("视频上传中，请稍后");
                controller.toMakeVideo(11);
            }

            @Override
            public void commitDoctorInfFailure(String msg) {

            }
        };
    }

    private void getIntentData() {
        bean = getIntent().getParcelableExtra("bean");
    }

    private void initViewData() {
        //获取治疗类型
        controller.getTypes();
        //获取医生类型
        controller.getRole();
    }

    private void initTitle() {
        tvName.setHint("请点击输入用户名");
        tvHospital.setHint("请点击输入所在医院");
        tvLevel.setText("未选择");
        tvType.setText("未选择");
        tvTime.setText("未选择");
        titleBar.showCenterText("申请", R.drawable.apply, 0);
    }


    @OnClick(R.id.tv_name)
    public void setName() {
//        controller.showUserNameInppop();
    }


    @OnClick(R.id.tv_hospital)
    public void setHospital() {
//        controller.showHospitalNameInpop();
    }

    @OnClick(R.id.tv_level)
    public void setLevel() {
        controller.showSelectLevelPop();
    }

    @OnClick(R.id.tv_type)
    public void setType() {
        controller.showDiseaseTypePop();
    }

    @OnClick(R.id.tv_visitor_time)
    public void toSetVisitorActivity() {
        startActivityForResult(new Intent(this, VisitorTimeActivity.class), 10);
    }

    @OnClick(R.id.bt_next)
    public void toAgreementActivity() {
        Log.e("gg", "diseaseTypes = " + diseaseTypes.size());

        //=========准备参数========
        String name = tvName.getText().toString().trim();         //名字
        String hospital = tvHospital.getText().toString().trim(); //医院
        int roleId = doctorRole;                                  //医生等级
        String tempDiseaseType = "";                        //看病类型(修改为多个传参)
        for (Integer id : diseaseTypes) {
            if (tempDiseaseType.isEmpty()) {
                tempDiseaseType = "" + id;
            } else {
                tempDiseaseType = tempDiseaseType + "," + id;
            }
        }
        monday = workTime.get("1");                               //工作时间
        tuesday = workTime.get("2");
        wednesday = workTime.get("3");
        thursday = workTime.get("4");
        friday = workTime.get("5");
        saturday = workTime.get("6");
        sunday = workTime.get("7");
        //身份证
        String strIdentityCards = "";
        for (String value : identityCards.values()) {
            strIdentityCards = strIdentityCards + "," + value;
        }
        //学历证
        String strEmployeeCardsnew = "";
        for (String value : employeeCardsnew.values()) {
            strEmployeeCardsnew = strEmployeeCardsnew + "," + value;
        }
        //其他证
        String strOtherCards = "";
        for (String value : otherCards.values()) {
            strOtherCards = strOtherCards + "," + value;
        }
        //==========校对参数非空========


        //==========发请求提交==========

        Log.e("gg", "name = " + name);
        Log.e("gg", "hospital = " + hospital);
        Log.e("gg", "roleId = " + roleId);
        Log.e("gg", "tempDiseaseType = " + tempDiseaseType);
        Log.e("gg", "monday = " + monday);
        Log.e("gg", "tuesday = " + tuesday);
        Log.e("gg", "wednesday = " + wednesday);
        Log.e("gg", "thursday = " + thursday);
        Log.e("gg", "friday = " + friday);
        Log.e("gg", "saturday = " + saturday);
        Log.e("gg", "sunday = " + sunday);

        Log.i("gg", "employeeCardsnew = " + employeeCardsnew.size());
        Log.i("gg", "identityCards = " + identityCards.size());
        Log.i("gg", "otherCards = " + otherCards.size());

        Log.e("gg", "strIdentityCards = " + strIdentityCards);
        Log.e("gg", "strEmployeeCardsnew = " + strEmployeeCardsnew);
        Log.e("gg", "strOtherCards = " + strOtherCards);


        if (name.isEmpty()) {
            ToastUtils.showToast("用户名不能为空！");
            return;
        }
        if (hospital.isEmpty()) {
            ToastUtils.showToast("医院名不能为空！");
            return;
        }
        if (roleId == -1) {
            ToastUtils.showToast("职称不能为空！");
            return;
        }
        if (tempDiseaseType.isEmpty()) {
            ToastUtils.showToast("可诊类型不能为空！");
            return;
        }
        if (workTime.size() == 0) {
            ToastUtils.showToast("未设置就诊时间！");
            return;
        }
        if (employeeCardsnew.size() < 3) {
            ToastUtils.showToast("学历证照缺少或上传中，请稍后！");
            return;
        }
        if (identityCards.size() < 3) {
            ToastUtils.showToast("身份证照缺少或上传中，请稍后！");
            return;
        }


        if (otherCards.size() < 1) {
            ToastUtils.showToast("相关医生资历照缺少或上传中，请稍后！！");
            return;
        }

        controller.commitDoctorInf(name, hospital, roleId, tempDiseaseType,
                monday, tuesday, wednesday, thursday, friday, saturday, sunday,
                strIdentityCards, strEmployeeCardsnew, strOtherCards);
    }


    @OnClick(R.id.img_diploma_front)
    public void getDiplomaFront() {
        controller.showGetPhotoPop(1);
    }

    @OnClick(R.id.img_diploma_back)
    public void getDiplomaBack() {
        controller.showGetPhotoPop(2);
    }

    @OnClick(R.id.img_diploma_hold)
    public void getDiplomaHold() {
        controller.showGetPhotoPop(3);
    }

    @OnClick(R.id.img_identity_card_front)
    public void getIdeFront() {
        controller.showGetPhotoPop(4);
    }

    @OnClick(R.id.img_identity_card_back)
    public void getIdeBack() {
        controller.showGetPhotoPop(5);
    }

    @OnClick(R.id.img_identity_card_hold)
    public void getIdeHold() {
        controller.showGetPhotoPop(6);
    }

    @OnClick(R.id.img_doc_aptitude_1)
    public void getAptitude1() {
        controller.showGetPhotoPop(7);
    }

    @OnClick(R.id.img_doc_aptitude_2)
    public void getAptitude2() {
        controller.showGetPhotoPop(8);
    }

    @OnClick(R.id.img_doc_aptitude_3)
    public void getAptitude3() {
        controller.showGetPhotoPop(9);
    }


    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            "android.permission.READ_EXTERNAL_STORAGE",
            "android.permission.WRITE_EXTERNAL_STORAGE",
            "android.permission.CAMERA"
    };


    private void verifyStoragePermissions(Activity activity) {

        try {
            //检测是否有写的权限
            int permission = ActivityCompat.checkSelfPermission(activity,
                    "android.permission.READ_EXTERNAL_STORAGE");
            if (permission != PackageManager.PERMISSION_GRANTED) {
                // 没有写的权限，去申请写的权限，会弹出对话框
                ActivityCompat.requestPermissions(activity, PERMISSIONS_STORAGE, REQUEST_EXTERNAL_STORAGE);
            }
        } catch (Exception e) {
            Log.e("gg", "======================");
            e.printStackTrace();
        }
    }

}
