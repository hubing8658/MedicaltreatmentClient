package com.ygbd198.hellodoctor.ui.controller;

import android.content.Context;
import android.text.InputType;

import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.bean.SelectPopBean;
import com.ygbd198.hellodoctor.service.ApplyBeDocService;
import com.ygbd198.hellodoctor.ui.InputUserInfActivity;
import com.ygbd198.hellodoctor.util.ToastUtils;
import com.ygbd198.hellodoctor.widget.PopupWindon.CommonSelectPop;
import com.ygbd198.hellodoctor.widget.PopupWindon.InputPop;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

/**
 * Created by guangjiqin on 2017/8/24.
 */

public abstract class InputUserInfController {

    private Context context;
    private InputPop usernameInputPop;
    private InputPop functionInputPop;
    private InputPop specialtyInputPop;
    private CommonSelectPop genderpPop;
    private CommonSelectPop educationPop;
    private InputPop introductionPop;
    private InputPop visitorNumPop;


    public abstract void commitInfSuccess();

    public abstract void commitInfFailure(String msg);

    private String token;

    public InputUserInfController(Context context, String token) {
        this.context = context;
        this.token = token;
    }


//    token true string 唯一标识
//    name true string 姓名
//    gender true string 性别
//    education true string 学历
//    job true string 学术职务
//    good true string 专长
//    count false string 0 诊疗人数
//    introduction true string 个人简介
    //保存完善个人资料
    public void saveUserInf(String name, String gender, String education, String jod, String good, String count, String introduction) {
        RetrofitHelper.getInstance().createService(ApplyBeDocService.class).perfectInfo(MySelfInfo.getInstance().getToken(), name, gender, education, jod, good, count, introduction).enqueue(new BaseCallBack<BaseCallModel<Object>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<Object>> response) {
                commitInfSuccess();
            }

            @Override
            public void onFailure(String msg) {
                ToastUtils.showToast(msg);
                commitInfFailure(msg);
            }
        });

    }


    //选择学历pop
    public void showEducationPop() {
        if (null == educationPop) {
            educationPop = new CommonSelectPop(context) {

                @Override
                public void onClickCommit(SelectPopBean bean) {
                    ((InputUserInfActivity) context).tvEducation.setText(bean.string);
                }
            };

        }
        List<SelectPopBean> been = new ArrayList<>();
        been.add(new SelectPopBean("专科"));
        been.add(new SelectPopBean("本科"));
        been.add(new SelectPopBean("硕士"));
        been.add(new SelectPopBean("博士"));
        educationPop.setBeans(been);
        educationPop.setTvType("请选择您的学历：");
        educationPop.showPop();
    }

    //选择性别pop
    public void showGenderpPop() {
        if (null == genderpPop) {
            genderpPop = new CommonSelectPop(context) {

                @Override
                public void onClickCommit(SelectPopBean bean) {
                    ((InputUserInfActivity) context).tvGender.setText(bean.string);
                }
            };

        }
        List<SelectPopBean> been = new ArrayList<>();
        been.add(new SelectPopBean("男"));
        been.add(new SelectPopBean("女"));
        genderpPop.setBeans(been);
        genderpPop.setTvType("请选择您的性别：");
        genderpPop.showPop();
    }


    //输入个人简介
    public void showintroductionInppop() {
        if (null == introductionPop) {
            introductionPop = new InputPop(context) {
                @Override
                public void onClickCommit(String content) {
                    ((InputUserInfActivity) context).tvIntroduce.setText(content.trim().isEmpty() ? "未填写" : content);
                }
            };
            introductionPop.setTitle("请输入个人简介");
        }
        introductionPop.showPop();
    }


    //输入人数
    public void showvisitorNumInputPop() {
        if (null == visitorNumPop) {
            visitorNumPop = new InputPop(context) {
                @Override
                public void onClickCommit(String content) {
                    ((InputUserInfActivity) context).tvVisitorNum.setText(content.trim().isEmpty() ? "未填写" : content);
                }
            };
            visitorNumPop.setTitle("请输入诊疗人数");
        }
        visitorNumPop.getEtInput().setInputType(InputType.TYPE_CLASS_NUMBER
        );
        visitorNumPop.showPop();
    }


    //输入名字pop
    public void showUserNameInppop() {
        if (null == usernameInputPop) {
            usernameInputPop = new InputPop(context) {
                @Override
                public void onClickCommit(String content) {
                    ((InputUserInfActivity) context).tvName.setText(content.trim().isEmpty() ? "未填写" : content);
                }
            };
            usernameInputPop.setTitle(R.string.input_user_name);
        }
        usernameInputPop.showPop();
    }

    //输入特长pop
    public void showSpecialtynppop() {
        if (null == specialtyInputPop) {
            specialtyInputPop = new InputPop(context) {
                @Override
                public void onClickCommit(String content) {
                    ((InputUserInfActivity) context).tvSpecialty.setText(content.trim().isEmpty() ? "未填写" : content);
                }
            };
            specialtyInputPop.setTitle("请输入您的专长");
        }
        specialtyInputPop.showPop();
    }

    //输入学术职务
    public void showFunctionInppop() {
        if (null == functionInputPop) {
            functionInputPop = new InputPop(context) {
                @Override
                public void onClickCommit(String content) {
                    ((InputUserInfActivity) context).tvFunction.setText(content.trim().isEmpty() ? "未填写" : content);
                }
            };
            functionInputPop.setTitle("请输入您的学术职务");
        }
        functionInputPop.showPop();
    }
}
