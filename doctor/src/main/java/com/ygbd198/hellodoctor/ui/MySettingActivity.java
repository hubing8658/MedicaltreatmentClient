package com.ygbd198.hellodoctor.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.view.View;

import android.widget.Button;
import android.widget.LinearLayout;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.QuitEvent;
import com.ygbd198.hellodoctor.common.BaseActivity;
import com.ygbd198.hellodoctor.util.ToastUtils;

import org.greenrobot.eventbus.EventBus;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by guangjiqin on 2017/10/18.
 */

public class MySettingActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.lin_change_ps)
    LinearLayout btChangePs;
    @BindView(R.id.lin_wipe_cache)
    LinearLayout btWipeCache;
    @BindView(R.id.lin_about_us)
    LinearLayout btAboutUs;
    @BindView(R.id.bt_quit)
    Button btQuit;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View contentView = this.getLayoutInflater().inflate(R.layout.activity_setting, null);
        rootView.addView(contentView);
        ButterKnife.bind(this);

        titleBar.showCenterText(R.string.settings, R.drawable.setting, 0);

        initView();

        setOnClick();

    }

    private void setOnClick() {
        btChangePs.setOnClickListener(this);
        btWipeCache.setOnClickListener(this);
        btAboutUs.setOnClickListener(this);
        btQuit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lin_change_ps://改密码
                this.startActivity(new Intent(this, ModifyPasswordActivity.class));
                break;
            case R.id.lin_wipe_cache://清缓存
                String path = Environment.getExternalStorageDirectory().toString() + "/helloDoctorCache";//设置缓存文件夹
                File file = new File(path);
                File files[] = file.listFiles();
                if (null != files && files.length > 0) {
                    for (File f : files) {
                        if (f.isFile()) {
                            f.delete();
                        }
                    }
                    file.delete();
                }
                ToastUtils.showToast("缓存已清除");

                break;
            case R.id.lin_about_us://关于我们
                this.startActivity(new Intent(this, AboutUsActivity.class));
                break;
            case R.id.bt_quit:
                finish();
                startActivity(new Intent(this, LoginActivity.class));
                EventBus.getDefault().post(new QuitEvent());
                break;
        }
    }
}
