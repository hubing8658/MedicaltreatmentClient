package com.ygbd198.hellodoctor.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.common.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by guangjiqin on 2017/8/9.
 */

public class AboutUsActivity extends BaseActivity {

    @BindView(R.id.tv_content)
    TextView tvContent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View contentView = this.getLayoutInflater().inflate(R.layout.activity_about_us, null);
        rootView.addView(contentView);
        ButterKnife.bind(this);

        titleBar.showCenterText(R.string.about_us,R.drawable.about_us,0);
        titleBar.setBackBtnVis(true);

        tvContent.setText("1、  Dr.Hello is  China's leading mobile online video inquiring platform, focusing on skin health management. The platform is jointly built by the Morse pharmaceutical factory in Germany and the national well-known chain pharmacy system .\n" +
                "2、 Dr. Hello's team consists of doctors from 3-A-grade hospitals, with the job titles of these doctors higher than attending physicians and Dr. Lian as core member of the team. The platform provides online inquiring services, as well as electronic prescription, drug delivery and other intelligent treatment services for skin patient users. Users can meet with the doctors through graphic, video and other forms of communication anytime anywhere to acquire the professional, continuous health management service.\n" +
                "1、Ｈello医生,中国领先的移动在线视频会诊服务平台，专注于皮肤健康管理，由德国默氏大药厂联合全国知名连锁药房系统倾力打造。\n" +
                "2、Hello医生团队以廉翠红博士为核心，由三甲医院主治医师以上资格的医生组成，为皮肤类患者用户提供在线问诊服务。平台开通了电子处方、延伸医嘱、送药上门等智慧医疗服务。用户可通过图文、视频等多种形式随时随地进行快捷问诊，获得专业、持续的健康管理服务。");

    }
}
