package com.ygbd198.hellodoctor.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.bean.PatientRecordDetailsBean;
import com.ygbd198.hellodoctor.common.BaseActivity;
import com.ygbd198.hellodoctor.service.PatientRecordService;
import com.ygbd198.hellodoctor.ui.adapter.PatientRecordDetailsAdapter;
import com.ygbd198.hellodoctor.ui.live.ImageActivity;
import com.ygbd198.hellodoctor.util.ToastUtils;
import com.ygbd198.hellodoctor.util.imageloader.GlideImageHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by guangjiqin on 2017/9/11.
 */

public class PatientRecordDetailsActivity extends BaseActivity {

    @BindView(R.id.listview)
    ListView listview;

    private List<PatientRecordDetailsBean.RecordDrugsBean> data;
    private PatientRecordDetailsAdapter adapter;


    private View contentView;
    private View headView;

    private long id;

    //headview相关控件
    private ImageView imgPhoto;
    private TextView tvName;
    private TextView tvTime;
    private LinearLayout linPhoto;
    private LinearLayout linType;
    private TextView tvType;
    private LinearLayout linRecord;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contentView = this.getLayoutInflater().inflate(R.layout.activity_patient_record_details, null);
        rootView.addView(contentView);
        ButterKnife.bind(this);

        titleBar.showCenterText("患者记录详情", R.drawable.records_details, 0);
        id = getIntent().getLongExtra("id", 0);

        initView();

        initHeadView();

        initData();

    }

    private void initHeadView() {
        imgPhoto = (ImageView) headView.findViewById(R.id.img_photo);
        tvName = (TextView) headView.findViewById(R.id.tv_name);
        tvTime = (TextView) headView.findViewById(R.id.tv_time);
        linPhoto = (LinearLayout) headView.findViewById(R.id.lin_photo);
        linType = (LinearLayout) headView.findViewById(R.id.lin_type);
        tvType = (TextView) headView.findViewById(R.id.tv_type);
        linRecord = (LinearLayout) headView.findViewById(R.id.lin_record);
    }

    @Override
    protected void initData() {
        RetrofitHelper.getInstance().createService(PatientRecordService.class).getRecordInfo(MySelfInfo.getInstance().getToken(), id).enqueue(new BaseCallBack<BaseCallModel<PatientRecordDetailsBean>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<PatientRecordDetailsBean>> response) {
                final PatientRecordDetailsBean bean = response.body().data;

                //设置headview
                GlideImageHelper.showImage(PatientRecordDetailsActivity.this, bean.userPic, R.drawable.logo, R.drawable.logo, imgPhoto);
                tvName.setText(bean.userName);
                tvTime.setText(bean.createTime);
                tvType.setText(bean.treatmentName);

                linPhoto.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (null != bean && null != bean.recordPic && !bean.recordPic.isEmpty()) {
                            final String[] userPhotos = bean.recordPic.split(",");
                            if (userPhotos.length == 0) {
                                ToastUtils.showToast("无诊断图片");
                            } else {
                                ImageActivity.GotoActivity(PatientRecordDetailsActivity.this, userPhotos);
                            }
                        } else {
                            ToastUtils.showToast("无诊断图片");
                        }
                    }
                });
                //设置listVIew
                data.clear();
                data.addAll(bean.recordDrugs);
                //只添加用户已经购买的药品
                /*for (PatientRecordDetailsBean.RecordDrugsBean recordDrugsBean : bean.recordDrugs) {
                    if ("true".equals(recordDrugsBean.isBuy)) {
                        data.add(recordDrugsBean);
                    }
                }*/
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(String msg) {
                ToastUtils.showToast(msg);
            }
        });
    }

    @Override
    protected void initView() {
        headView = this.getLayoutInflater().inflate(R.layout.item_patient_record_details_headview, null);
        data = new ArrayList<>();
        adapter = new PatientRecordDetailsAdapter(this, data);
        listview.addHeaderView(headView);
        listview.setAdapter(adapter);
    }
}
