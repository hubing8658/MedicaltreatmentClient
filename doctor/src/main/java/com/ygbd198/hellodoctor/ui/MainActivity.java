
package com.ygbd198.hellodoctor.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.monty.library.AppManager;
import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.DoctorInfBean;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.bean.QuitEvent;
import com.ygbd198.hellodoctor.service.MyCenterService;
import com.ygbd198.hellodoctor.ui.adapter.PagerAdapter;
import com.ygbd198.hellodoctor.ui.fragment.MessageFragment;
import com.ygbd198.hellodoctor.ui.fragment.MyCenterFragment;
import com.ygbd198.hellodoctor.ui.fragment.TreatmentFragment;
import com.ygbd198.hellodoctor.ui.fragment.TreatmentLearningFragment;
import com.ygbd198.hellodoctor.ui.fragment.UserInfProgressFragment;
import com.ygbd198.hellodoctor.util.ToastUtils;
import com.ygbd198.hellodoctor.widget.MainBottomBar;
import com.ygbd198.hellodoctor.widget.dialog.LoadingDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements MainBottomBar.onTabSelectedListener, ViewPager.OnPageChangeListener {

    @BindView(R.id.vp_main)
    ViewPager vpMain;

    @BindView(R.id.bottomBar)
    MainBottomBar bottomBar;

    private PagerAdapter pagerAdapter;

    private List<Fragment> fragments;

    private long lastClickTime;

    private LoadingDialog dialog;

    private int statu = 1; // 1视频fragment 2实操学习进度fragment 3资料完善fragment
    private boolean hasCheck;

    public static void GotoActivity(Context context, boolean hasCheck) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra("hasCheck", hasCheck);
        context.startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkStatus();
    }

    /*
    private void checkStatus(int status) {
        switch (status) {
            case 0: // 不可用，停留在登录页面，提示账号不可用
                ToastUtils.showToast("账号不可用");
                break;
            case 1:// 合格医生，走正常流程
                break;
            case 101:// 已注册成功，资料未填写完成跳转到证书上传资料填写页面
                break;
            case 102:// 已提交申请，正在审核；停在登录页面 提示“正在审核您的信息，审核结果1-3个工作日内会以短信的形式通知您，请留意!”
                break;
            case 103: // 审核失败需重新提交申请；提示“审核失败请重新提交申请” 点确定 跳转到上传证书资料填写页面
                break;
            case 104: // 审核通过但未观看完视频；跳转到坐诊页面-显示视频观看进度Fragment
                break;
            case 105: // 观看完视频但未实操演练；跳转到坐诊页面-显示视频观看进度Fragment ，如果进度为100则实操演练可点击，跳转到功能指南页面
                break;
            case 106: // 通过实操演练未完善资料；跳转到坐诊页面-显示资料完成度(进度框),按钮显示完善资料
                break;
            case 107: // 完善资料正在审核中，走正常流程，显示坐诊页面，点击坐诊提示资料正在审核中
                break;
            case 108: // 审核失败需重新完善；跳转到完善资料页面
                break;
            case 109: // 显示坐诊页面点击坐诊时直接跳转到签订协议页面、
                break;
        }
    }*/
    private void checkStatus() {
        if (hasCheck && vpMain.getCurrentItem() == 0) {
            showLoadingDialog("");
            RetrofitHelper.getInstance().createService(MyCenterService.class).getDoctorInf(MySelfInfo.getInstance().getToken()).enqueue(new BaseCallBack<BaseCallModel<DoctorInfBean>>() {
                @Override
                public void onSuccess(Response<BaseCallModel<DoctorInfBean>> response) {
                    closeLoadingDialog();
                    if (response.body().data == null) {
                        ToastUtils.showToast("获取资料失败");
                        return;
                    }
                    statu = response.body().data.statu;
                    Log.d("monty", "MainActivity - status -> " + statu);

                    switch (statu) {
                        case 104: // 审核通过但未观看完视频；跳转到坐诊页面-显示视频观看进度Fragment TreatmentLearningFragment
                            fragments.set(0, TreatmentLearningFragment.newInstance("", ""));
                            pagerAdapter.notifyDataSetChanged();
                            break;
                        case 105: // 观看完视频但未实操演练；跳转到坐诊页面-显示视频观看进度Fragment ，如果进度为100则实操演练可点击，跳转到功能指南页面 TreatmentLearningFragment
                            fragments.set(0, TreatmentLearningFragment.newInstance("", ""));
                            pagerAdapter.notifyDataSetChanged();
                            break;
                        case 106: // 通过实操演练未完善资料；跳转到坐诊页面-显示资料完成度(进度框),按钮显示完善资料 UserInfProgressFragment
                            fragments.set(0, UserInfProgressFragment.newInstance("", ""));
                            pagerAdapter.notifyDataSetChanged();
                            break;
                        case 107: // 完善资料正在审核中，走正常流程，显示坐诊页面，点击坐诊提示资料正在审核中
                            fragments.set(0, TreatmentFragment.newInstance(107));
                            pagerAdapter.notifyDataSetChanged();
                            break;
                        case 108: // 审核失败需重新完善；跳转到完善资料页面
//                            context.startActivity(new Intent(context, InputUserInfActivity.class));
                            fragments.set(0, UserInfProgressFragment.newInstance("", ""));
                            pagerAdapter.notifyDataSetChanged();
                            break;
                        case 109: // 显示坐诊页面点击坐诊时直接跳转到签订协议页面
                            fragments.set(0, TreatmentFragment.newInstance(109));
                            pagerAdapter.notifyDataSetChanged();
                            break;
                        default:
                            break;
                    }
                }

                @Override
                public void onFailure(String message) {
                    ToastUtils.showToast(message);
                    closeLoadingDialog();
                }
            });
        }
    }

    public void showUserInfProgressFragment(){
        fragments.set(0, UserInfProgressFragment.newInstance("", ""));
        pagerAdapter.notifyDataSetChanged();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
        getIntentData();
        checkStatus();
        bottomBar.setOnTabSelectedListener(this);
        bottomBar.setFistTab();
        initFragment();
        pagerAdapter = new PagerAdapter(getSupportFragmentManager(), fragments);
        vpMain.setAdapter(pagerAdapter);
        vpMain.addOnPageChangeListener(this);
    }


    private void getIntentData() {
        /*if ("BeDocLogin".equals(getIntent().getStringExtra("tag"))) {
            statu = 2;
        }else */
        hasCheck = getIntent().getBooleanExtra("hasCheck", false);
    }

    private void initFragment() {
        fragments = new ArrayList<>();
        fragments.add(TreatmentFragment.newInstance());
        /*switch (statu) {
            case 1:
                fragments.add(TreatmentFragment.newInstance());
                break;
            case 2:
                fragments.add(TreatmentLearningFragment.newInstance("", ""));
                break;
            case 3:
                fragments.add(UserInfProgressFragment.newInstance("", ""));
                break;
        }*/

//        fragments.add(DoctorMessageFragment.newInstance());
        fragments.add(new MessageFragment());
        fragments.add(MyCenterFragment.newInstance("我的", ""));
    }

    @Override
    public void onBackPressed() {
        if (System.currentTimeMillis() - lastClickTime < 2000) {
            super.onBackPressed();
            AppManager.getAppManager().exitApp(this);
        } else {
            lastClickTime = System.currentTimeMillis();
            Toast.makeText(this, "再按一次退出Hello颜值", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onTagSelected(int position, int tabId, String tabName) {
        vpMain.setCurrentItem(position);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        bottomBar.getContainer().getChildAt(position).performClick();
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    @Subscribe(threadMode = ThreadMode.MAIN)//退出app
    public void quitApp(QuitEvent quitEvent) {
        finish();
    }


    public void showLoadingDialog(String msg) {
        if (dialog == null) {
            dialog = new LoadingDialog(this);
        }
        dialog.show(msg);
    }

    public void closeLoadingDialog() {
        if (dialog != null) {
            dialog.close();
        }
    }

}
