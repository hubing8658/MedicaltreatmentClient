package com.ygbd198.hellodoctor.ui.live.viewinterface;

import com.tencent.av.sdk.AVView;
import com.ygbd198.hellodoctor.bean.WaitingUser;

import java.util.List;

/**
 * Created by monty on 2017/8/7.
 */

public interface ILiveView extends ILoadingDialog{

    void setIsReceiveSuccess(boolean isReceive);

    void setIsReceiveFailure(String msg);

    void quiteRoomComplete(boolean isSuccess);

    void creageRoomComplete(boolean isSuccess,String errorMsg);

    void enterRoomComplete(boolean isSuccess,String errorMsg);

    void refreshWaitingUserlist(List<WaitingUser> waitingUsers);

    void onStartDiagnose(long diagnosticId,String name);

    void showCalling(boolean isShow);

    //房间异常
    void onException(int exceptionId, int errCode, String errMsg);

    //请求画面回调
    void onComplete(String[] identifierList, AVView[] viewList, int count, int result, String errMsg);

    //房间异常退出回调(一般为断网)
    void onRoomDisconnect(int errCode, String errMsg);

    void resetVisiting();

    void showAlert(String s);
}
