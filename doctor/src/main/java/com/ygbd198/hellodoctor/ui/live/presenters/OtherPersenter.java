package com.ygbd198.hellodoctor.ui.live.presenters;

import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.ygbd198.hellodoctor.bean.DiseaseTypeBean;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.common.Presenter;
import com.ygbd198.hellodoctor.service.ApplyBeDocService;
import com.ygbd198.hellodoctor.util.ToastUtils;

import java.util.List;

import retrofit2.Response;

/**
 * Created by monty on 2017/8/22.
 */

public class OtherPersenter extends Presenter {

    public interface OnGetTypesListener{
        void onSuccess(List<DiseaseTypeBean> diseaseTypeList);
        void onFailure(String msg);
    }

    public void getTypes(final OnGetTypesListener onGetTypesListener) {
        RetrofitHelper.getInstance().createService(ApplyBeDocService.class).getDiseaseType(MySelfInfo.getInstance().getToken()).enqueue(new BaseCallBack<BaseCallModel<List<DiseaseTypeBean>>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<List<DiseaseTypeBean>>> response) {
                onGetTypesListener.onSuccess(response.body().data);
            }

            @Override
            public void onFailure(String msg) {
                ToastUtils.showToast(msg);
                onGetTypesListener.onFailure(msg);
            }
        });

    }

    @Override
    public void onDestory() {

    }
}
