package com.ygbd198.hellodoctor.ui;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.common.BaseActivity;
import com.ygbd198.hellodoctor.common.Constants;
import com.ygbd198.hellodoctor.ui.controller.LoginController;
import com.ygbd198.hellodoctor.util.SharePreferencesHelp;
import com.ygbd198.hellodoctor.util.ToastUtils;
import com.ygbd198.hellodoctor.widget.PasswordEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity {

    @BindView(R.id.cb_rememberPassword)
    CheckBox cbRemPs;
    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.et_pwdLayout)
    PasswordEditText etPwdLayout;


    private LoginController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View contentView = this.getLayoutInflater().inflate(R.layout.activity_login, null);
        rootView.addView(contentView);
        ButterKnife.bind(this);
        controller = new LoginController(LoginActivity.this);
        String userName = SharePreferencesHelp.gettString(this, Constants.KEY_USER_NAME);
        String password = SharePreferencesHelp.gettString(this, Constants.KEY_PASS_WORD);
        this.etPwdLayout.setHint("请输入6-20位密码");
        /*if (BuildConfig.DEBUG && TextUtils.isEmpty(userName)) {
            etPhone.setText("18503004957");
            etPwdLayout.setPassword("123456");
        }*/
        if (!TextUtils.isEmpty(userName) && !TextUtils.isEmpty(password)) {
            cbRemPs.setChecked(true);
            etPhone.setText(userName);//"13712340001"
            etPwdLayout.setPassword(password);//123456
        } else {
            cbRemPs.setChecked(false);
        }


    }

    @OnClick(R.id.btn_login)
    public void onLogin() {
        onRememberPassword();
        controller.login(etPhone.getText().toString(), etPwdLayout.getPassword());
    }

    @OnClick(R.id.cb_rememberPassword)
    public void onRememberPassword() {
        if (etPhone.getText().toString().trim().isEmpty() || etPwdLayout.getPassword().isEmpty()) {
            cbRemPs.setChecked(false);
            ToastUtils.showToast(this, R.string.name_pasw_null);
        } else {
            if (cbRemPs.isChecked()) {
                controller.rememberPassword(this, etPhone.getText().toString().trim(), etPwdLayout.getPassword());
            } else {
                controller.clearPassword(this);
            }
        }
    }


    @OnClick(R.id.btn_forgottenPassword)
    public void onForgottenPassword() {
        controller.forgottenPassword();
    }
}
