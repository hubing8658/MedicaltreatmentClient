package com.ygbd198.hellodoctor.ui.controller;

import android.content.Context;
import android.text.InputType;

import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.ygbd198.hellodoctor.bean.DiseaseTypeBean;
import com.ygbd198.hellodoctor.bean.MyIntegralBean;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.service.MyCenterService;
import com.ygbd198.hellodoctor.ui.ChangeBankCardsActivity;
import com.ygbd198.hellodoctor.util.ToastUtils;
import com.ygbd198.hellodoctor.widget.PopupWindon.InputPop;
import com.ygbd198.hellodoctor.widget.PopupWindon.SearchTypePop;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

/**
 * Created by guangjiqin on 2017/8/29.
 */

public abstract class MyBankCardsController {

    public abstract void changeCardsFailure(String msg);

    public abstract void changeCardsSuccess(MyIntegralBean myIntegralBean);

    private List<DiseaseTypeBean> banks = new ArrayList<>();

    private String token;
    private Context context;

    private InputPop namePop;
    private InputPop phoneNumPop;
    private InputPop numPop;
    private SearchTypePop banksPop;


    public MyBankCardsController(Context context) {
        this.context = context;
        token = MySelfInfo.getInstance().getToken();
        getBankType();
    }


    public void showNumInppop() {
        if (null == numPop) {
            numPop = new InputPop(context) {
                @Override
                public void onClickCommit(String content) {
                    ((ChangeBankCardsActivity) context).tvCardNum.setText(content);
                }
            };
            numPop.setTitle("请输入银行卡号");
        }
        numPop.getEtInput().setInputType(InputType.TYPE_CLASS_NUMBER);
        numPop.showPop();
    }

    public void showNameInppop() {
        if (null == namePop) {
            namePop = new InputPop(context) {
                @Override
                public void onClickCommit(String content) {
                    ((ChangeBankCardsActivity) context).tvName.setText(content);
                }
            };
            namePop.setTitle("请输入持卡人姓名");
        }
        namePop.showPop();
    }


    //更换银行卡
    public void changeCard(String name, /*String type,*/ long number,String code) {
        RetrofitHelper.getInstance().createService(MyCenterService.class).bindChangeCard(token, name, /*type,*/ number,code).enqueue(new BaseCallBack<BaseCallModel<MyIntegralBean>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<MyIntegralBean>> response) {
                changeCardsSuccess(response.body().data);
            }

            @Override
            public void onFailure(String message) {
                ToastUtils.showToast(message);
                changeCardsFailure(message);
            }
        });
    }

    //获取银行种类信息等
    private void getBankType() {
        RetrofitHelper.getInstance().createService(MyCenterService.class).getBanks(token).enqueue(new BaseCallBack<BaseCallModel<List<String>>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<List<String>>> response) {
                for(String bank:response.body().data){
                    DiseaseTypeBean bean = new DiseaseTypeBean();
                    bean.typeName = bank;
                    banks.add(bean);
                }
            }

            @Override
            public void onFailure(String message) {
                ToastUtils.showToast("获取银行列表失败");
            }
        });
    }


    public void showBanksPop(){
        if (null == banksPop) {
            banksPop = new SearchTypePop(context) {
                @Override
                public void onPopDismiss() {
                }

                @Override
                public void onItemOnClick(DiseaseTypeBean diseaseTypeBean) {
                    ((ChangeBankCardsActivity) context).tvBanck.setText(diseaseTypeBean.typeName);
                }
            };
        }
        banksPop.setDiseaseTypeBeans(banks);
        banksPop.showPopTopRight();
    }

    public void showPhoneNumPop(){
        if (null == phoneNumPop) {
            phoneNumPop = new InputPop(context) {
                @Override
                public void onClickCommit(String content) {
                    ((ChangeBankCardsActivity) context).tvPhonrNum.setText(content);
                }
            };
            phoneNumPop.setTitle("请输入银行预留手机号");
        }
        phoneNumPop.showPop();
    }
}
