package com.ygbd198.hellodoctor.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.bean.PatientRecordBean;
import com.ygbd198.hellodoctor.bean.PatientRecordIntegralBean;
import com.ygbd198.hellodoctor.common.BaseActivity;
import com.ygbd198.hellodoctor.service.PatientRecordService;
import com.ygbd198.hellodoctor.ui.adapter.PatientRecordAdapter;
import com.ygbd198.hellodoctor.ui.adapter.PatientRecordIntegralAdapter;
import com.ygbd198.hellodoctor.util.ToastUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by Administrator on 2017/9/10 0010.
 * 患者记录
 */

public class PatientRecordIntegralActivity extends BaseActivity {

    @BindView(R.id.listview)
    ListView listView;

    private View contentView;
    private List<PatientRecordIntegralBean> data;
    private PatientRecordIntegralAdapter adapter;
    private long id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contentView = this.getLayoutInflater().inflate(R.layout.activity_patient_record, null);
        rootView.addView(contentView);
        ButterKnife.bind(this);

        titleBar.showCenterText(getIntent().getStringExtra("name"));

        id = getIntent().getLongExtra("id", -1);
        Log.e("gg","id = "+id);

        initView();

        initData();
    }

    @Override
    protected void initView() {
        listView = (ListView) contentView.findViewById(R.id.listview);
        data = new ArrayList<>();
        adapter = new PatientRecordIntegralAdapter(this, data);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(PatientRecordIntegralActivity.this, PatientRecordDetailsActivity.class);
                intent.putExtra("id", data.get(position).diagnosticId);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void initData() {
//        for (int i = 0; i < 10; i++) {
//            PatientRecordIntegralBean bean = new PatientRecordIntegralBean();
//            bean.integral = "1000" + i;
//            bean.time = "time:" + i;
//            data.add(bean);
//            adapter.notifyDataSetChanged();
//
//        }
        RetrofitHelper.getInstance().createService(PatientRecordService.class).getRecordByUser(MySelfInfo.getInstance().getToken(), String.valueOf(id)).enqueue(new BaseCallBack<BaseCallModel<List<PatientRecordIntegralBean>>>() {

            @Override
            public void onSuccess(Response<BaseCallModel<List<PatientRecordIntegralBean>>> response) {
                data.clear();
                data.addAll(response.body().data);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(String msg) {
                ToastUtils.showToast(msg);
            }
        });
    }
}
