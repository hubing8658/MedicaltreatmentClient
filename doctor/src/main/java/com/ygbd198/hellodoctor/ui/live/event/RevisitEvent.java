package com.ygbd198.hellodoctor.ui.live.event;

import com.ygbd198.hellodoctor.bean.Record;

/**
 * Created by monty on 2017/9/11.
 */

public class RevisitEvent {
    public Record.Revisit revisit;
    public RevisitEvent(Record.Revisit revisit){
        this.revisit = revisit;
    }
}
