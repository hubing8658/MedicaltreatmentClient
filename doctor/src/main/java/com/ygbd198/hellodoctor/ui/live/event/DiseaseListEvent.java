package com.ygbd198.hellodoctor.ui.live.event;

import com.ygbd198.hellodoctor.bean.DiseaseTypeBean;

import java.util.List;

/**
 * 病症类型列表Event
 * Created by monty on 2017/10/5.
 */

public class DiseaseListEvent {

    public List<DiseaseTypeBean> diseaseTypes;
    public DiseaseListEvent(List<DiseaseTypeBean> diseaseTypes){
        this.diseaseTypes = diseaseTypes;
    }
}
