package com.ygbd198.hellodoctor.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.DocumentBean;
import com.ygbd198.hellodoctor.common.MyBaseAdapter;
import com.ygbd198.hellodoctor.util.imageloader.GlideImageHelper;

import java.util.List;

/**
 * Created by guangjiqin on 2017/9/13.
 */

public class DocumentListAdapter extends MyBaseAdapter {

    public DocumentListAdapter(Context context, List data) {
        super(context, data);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            holder = new Holder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_document_img, null);
            holder.imgDoc = (ImageView) convertView.findViewById(R.id.img_doc);
            holder.tvDocName = (TextView) convertView.findViewById(R.id.tv_name);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        DocumentBean bean = (DocumentBean) mData.get(position);
        if(null == bean.url || bean.url.isEmpty()){
            holder.imgDoc.setImageResource(R.drawable.icon_add_photo);// TODO: 2017/9/13 (添加符号)
        }else{
            GlideImageHelper.showImage(mContext, bean.url, R.drawable.logo, R.drawable.logo, holder.imgDoc);

        }

        return convertView;
    }

    class Holder {
        ImageView imgDoc;
        TextView tvDocName;
    }
}
