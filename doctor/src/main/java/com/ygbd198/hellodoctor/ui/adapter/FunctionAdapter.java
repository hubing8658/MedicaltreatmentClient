package com.ygbd198.hellodoctor.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.AgreenDetailBean;
import com.ygbd198.hellodoctor.common.MyBaseAdapter;
import com.ygbd198.hellodoctor.util.imageloader.GlideImageHelper;

import java.util.List;

/**
 * Created by guangjiqin on 2017/8/26.
 */

public class FunctionAdapter extends MyBaseAdapter {

    private boolean isVisible;

    public void imageViewSetVisible(boolean isVisible) {
        this.isVisible = isVisible;
    }

    public FunctionAdapter(Context context, List data) {
        super(context, data);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder = null;
        if (convertView == null) {
            holder = new Holder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_function_content, null);
            holder.tvContent = (TextView) convertView.findViewById(R.id.tv_content);
            holder.tvTitle = (TextView) convertView.findViewById(R.id.tv_title);
            holder.img = (ImageView) convertView.findViewById(R.id.img);

            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        AgreenDetailBean bean = (AgreenDetailBean) mData.get(position);
        holder.tvContent.setText(bean.content);
        holder.tvTitle.setText(bean.title);
        if(isVisible){
            GlideImageHelper.showImage(mContext, bean.pic, R.drawable.logo, R.drawable.logo, holder.img);
        }else{
            holder.img.setVisibility(View.GONE);
        }

        return convertView;
    }

    class Holder {
        TextView tvTitle;
        TextView tvContent;
        ImageView img;
    }
}
