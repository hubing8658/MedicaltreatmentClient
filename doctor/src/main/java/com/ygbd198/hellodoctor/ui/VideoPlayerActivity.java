package com.ygbd198.hellodoctor.ui;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.CompleteLeariningVideoEvent;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.common.BaseActivity;
import com.ygbd198.hellodoctor.service.LearningVideoService;
import com.ygbd198.hellodoctor.util.DateUtil;
import com.ygbd198.hellodoctor.util.ToastUtils;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by guangjiqin on 2017/8/23.
 */

public class VideoPlayerActivity extends BaseActivity {

    @BindView(R.id.video_view)
    VideoView videoPlay;
    @BindView(R.id.imgbt_video_control)
    ImageButton imgbtControl;
    @BindView(R.id.imgbt_full_screen)
    ImageButton imgbtFullScreen;
    @BindView(R.id.prg_progress)
    SeekBar progress;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.tv_content)
    TextView tvContent;
    @BindView(R.id.rel_content)
    RelativeLayout relContent;

    private int id;
    private String name;
    private String uri;
    private String introduction;

    private boolean isPlayer;
    private boolean isShowControl;
    private boolean isFullScreen;
//    private String token = "eh1231871583wt";

    private MediaPlayer mp;

    Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            try {
                if (isPlayer) {
                    progress.setProgress(videoPlay.getCurrentPosition());
                    tvTime.setText(DateUtil.secondToMinute(videoPlay.getCurrentPosition() / 1000) + "/" + DateUtil.secondToMinute(mp.getDuration() / 1000));
                }
                progress.setSecondaryProgress(videoPlay.getBufferPercentage());
                handler.post(runnable);
            } catch (Exception e) {
                e.printStackTrace();
                handler.removeCallbacks(runnable);
                tvTime.setText("");
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View contentView = this.getLayoutInflater().inflate(R.layout.activity_video_player, null);
        rootView.addView(contentView);
        ButterKnife.bind(this);

        getIntentData();

        initTitle();

        initOnClick();

        initView();
    }

    private void initOnClick() {
        imgbtControl.setOnClickListener(new View.OnClickListener() {//开始暂停控制
            @Override
            public void onClick(View v) {
                if (isPlayer) {
                    videoPlay.pause();
                    imgbtControl.setImageResource(R.drawable.apply_icon_pause);
                } else {
                    imgbtControl.setImageResource(R.drawable.apply_icon_play);
                    videoPlay.start();
                }
                isPlayer = !isPlayer;
            }
        });

        videoPlay.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return false;
            }
        });//控制布局显示控制
        imgbtFullScreen.setOnClickListener(new View.OnClickListener() {//全屏控制
            @Override
            public void onClick(View v) {
                if (isFullScreen) {
                    titleBar.setVisibility(View.VISIBLE);
                    relContent.setVisibility(View.VISIBLE);
                    imgbtFullScreen.setImageResource(R.drawable.full);
                } else if (null != mp) {
                    imgbtFullScreen.setImageResource(R.drawable.part);

                    if (mp.getVideoWidth() > mp.getVideoHeight()) {
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                    } else {
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

                    }
                    titleBar.setVisibility(View.GONE);
                    relContent.setVisibility(View.GONE);
                }
                isFullScreen = !isFullScreen;
            }
        });

        progress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    videoPlay.seekTo(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                videoPlay.pause();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (isPlayer) {
                    videoPlay.start();
                } else {
                    videoPlay.pause();
                }
            }
        });
    }

    @Override
    protected void initView() {
        tvContent.setText(introduction);
        videoPlay.setVideoPath(uri);
        videoPlay.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                VideoPlayerActivity.this.mp = mp;

                if (mp.getVideoWidth() > mp.getVideoHeight()) {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                } else {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }

                progress.setMax(mp.getDuration());
                videoPlay.start();
                isPlayer = true;
                imgbtControl.setImageResource(R.drawable.apply_icon_pause);
                handler.postDelayed(runnable, 100);
            }
        });
        videoPlay.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                isPlayer = false;
                imgbtControl.setImageResource(R.drawable.apply_icon_play);
                if (MySelfInfo.getInstance().getStatu() == 104) {  // 未观看过视频才更新状态 ,如果是其他状态说明还没到这一步，或者已经看过了，不需要更新状态
                    //播放完毕发请求保存设置为已观看
                    RetrofitHelper.getInstance().createService(LearningVideoService.class).seeVideo(MySelfInfo.getInstance().getToken(), id).enqueue(new BaseCallBack<BaseCallModel<Object>>() {
                        @Override
                        public void onSuccess(Response<BaseCallModel<Object>> response) {
                            ToastUtils.showToast("该视频学习完成");
                            finish();
                            EventBus.getDefault().post(new CompleteLeariningVideoEvent());
                        }

                        @Override
                        public void onFailure(String message) {
                            ToastUtils.showToast(message);
                        }
                    });
                }
            }
        });

    }

    private void initTitle() {
        titleBar.showCenterText(name);
    }

    private void getIntentData() {
        Intent intent = getIntent();
        id = intent.getIntExtra("id", Integer.valueOf(-1));
        name = intent.getStringExtra("name");
        uri = intent.getStringExtra("uri");
        introduction = intent.getStringExtra("introduction");
    }


}
