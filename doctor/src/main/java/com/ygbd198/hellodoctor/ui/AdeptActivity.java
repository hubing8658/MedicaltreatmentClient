package com.ygbd198.hellodoctor.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.common.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by guangjiqin on 2017/9/11.
 */

public class AdeptActivity extends BaseActivity {

    @BindView(R.id.tv_type)
    TextView tvType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View contentView = this.getLayoutInflater().inflate(R.layout.activity_adept, null);
        rootView.addView(contentView);
        ButterKnife.bind(this);

        tvType.setText(getIntent().getStringExtra("type"));

    }
}
