package com.ygbd198.hellodoctor.ui.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.monty.library.http.BaseCallBack;
import com.monty.library.http.BaseCallModel;
import com.monty.library.http.RetrofitHelper;
import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.bean.CompleteLeariningVideoEvent;
import com.ygbd198.hellodoctor.bean.DoctorInfBean;
import com.ygbd198.hellodoctor.bean.MySelfInfo;
import com.ygbd198.hellodoctor.bean.VedioBean;
import com.ygbd198.hellodoctor.service.LearningVideoService;
import com.ygbd198.hellodoctor.service.MyCenterService;
import com.ygbd198.hellodoctor.ui.ActualOperatingActivity2;
import com.ygbd198.hellodoctor.ui.LearningVideoActivity;
import com.ygbd198.hellodoctor.util.ToastUtils;
import com.ygbd198.hellodoctor.widget.BaseTitleBar;
import com.ygbd198.hellodoctor.widget.TorusView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Response;

/**
 * Created by guangjiqin on 2017/8/23.
 * 坐诊学习fragment
 */

public class TreatmentLearningFragment extends Fragment {

    @BindView(R.id.titleBar)
    BaseTitleBar titleBar;
    Unbinder unbinder;

    @BindView(R.id.torus_view)
    TorusView torusView;

    @BindView(R.id.tv_title)
    TextView tvTitle;

    @BindView(R.id.bt_drill)
    Button btDrill;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;

    private List<String> idList;
    private int progress;

    public TreatmentLearningFragment() {

    }

    public static TreatmentLearningFragment newInstance(String param1, String param2) {
        TreatmentLearningFragment fragment = new TreatmentLearningFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_treatment_learning, null);
        unbinder = ButterKnife.bind(this, view);

        this.titleBar.setBackBtnVis(false);
        this.titleBar.showCenterText(R.string.treatment_title, R.drawable.tr, 0);

        EventBus.getDefault().register(this);

        chackVideoId();

        initData();

        return view;
    }


    @Subscribe(threadMode = ThreadMode.MAIN)//刷新数据
    public void refresh(CompleteLeariningVideoEvent event) {
        Log.e("gg", "请求进度页面数据");
        RetrofitHelper.getInstance().createService(MyCenterService.class).getDoctorInf(MySelfInfo.getInstance().getToken()).enqueue(new BaseCallBack<BaseCallModel<DoctorInfBean>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<DoctorInfBean>> response) {
                MySelfInfo.getInstance().setPracticeVideoIds(response.body().data.practiceVideoIds);
                initData();
            }

            @Override
            public void onFailure(String message) {
                ToastUtils.showToast(message);
            }
        });
    }

    private void chackVideoId() {
        String videoId = (null == MySelfInfo.getInstance().getPracticeVideoIds() ? "" : MySelfInfo.getInstance().getPracticeVideoIds());
        idList = new ArrayList<>();
        if (!videoId.isEmpty()) {
            String[] ids = videoId.split(",");
            for (int i = 0; i < ids.length; i++) {
                int j = 0;
                for (String id : idList) {
                    if (!id.equals(ids[i])) {
                        j++;
                    }
                }
                if (j == idList.size()) {
                    idList.add(ids[i]);
                }
            }
        }
    }

    protected void initData() {
        RetrofitHelper.getInstance().createService(LearningVideoService.class).getVedioList(MySelfInfo.getInstance().getToken()).enqueue(new BaseCallBack<BaseCallModel<List<VedioBean>>>() {
            @Override
            public void onSuccess(Response<BaseCallModel<List<VedioBean>>> response) {
                if (response.body().data == null || response.body().data.size() == 0) {
                    ToastUtils.showToast("未找到实操培训视频，请联系客服");
                    torusView.setProgress(0);
                    return;
                }
                progress = (idList.size() * 100) / response.body().data.size();
                torusView.setProgress(progress);
                tvTitle.setText("已观看的视频：" + progress + "%");

                if (progress == 100) {
                    btDrill.setTextColor(Color.parseColor("#f8b9cd"));
                    btDrill.setBackgroundResource(R.drawable.button_blue_bg);
                } else {
                    btDrill.setTextColor(Color.parseColor("#ffffff"));
                    btDrill.setBackgroundResource(R.drawable.button_gray_bg);
                }
            }

            @Override
            public void onFailure(String message) {
                ToastUtils.showToast(message);
            }
        });

    }

    @OnClick(R.id.bt_learn_video)
    public void toLearningVideoList() {
        startActivity(new Intent(getActivity(), LearningVideoActivity.class));
    }

    @OnClick(R.id.bt_drill)
    public void toDrill() {
        if (progress != 100) {
            ToastUtils.showToast("需要看完培训视频哦");
        } else {
//            startActivity(new Intent(getActivity(), ActualOperatingActivity.class));
            startActivity(new Intent(getActivity(), ActualOperatingActivity2.class));

        }
    }
}
