package com.ygbd198.hellodoctor.ui.live.event;

/**
 * 病症类型Event
 * Created by monty on 2017/9/8.
 */

public class DiseaseEvent {
    public String typeName;
    public DiseaseEvent(String typeName){
        this.typeName = typeName;
    }
}
