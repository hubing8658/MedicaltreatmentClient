package com.ygbd198.hellodoctor.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ygbd198.hellodoctor.R;
import com.ygbd198.hellodoctor.common.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GuideActivity extends BaseActivity {

    @BindView(R.id.logo)
    ImageView logo;
    @BindView(R.id.lable)
    TextView lable;
    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.btn_register)
    Button btnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View contentView = this.getLayoutInflater().inflate(R.layout.activity_guide, null);
        rootView.addView(contentView);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_login)
    public void onBtnLoginClicked() {
        finish();
        startActivity(new Intent(this,LoginActivity.class));
    }

    @OnClick(R.id.btn_register)
    public void onBtnRegisterClicked() {

//        startActivity(new Intent(this,AgreementActivity.class));//// TODO: 2017/8/25 (直接跳到同意页面调试)
//        startActivity(new Intent(this,ApplyBeDocActivity.class));// TODO: 2017/8/20 (测试 跳过手机认证步骤)
        startActivity(new Intent(this,ApplyBeDocLoginActivity.class));
    }
}
