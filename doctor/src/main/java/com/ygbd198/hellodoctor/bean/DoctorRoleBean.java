package com.ygbd198.hellodoctor.bean;

/**
 * Created by guangjiqin on 2017/8/22.
 */

public class DoctorRoleBean {
    /**
     * id : 1
     * role : 医生
     * englishName : SENIOR
     * price : 30元/次
     * priceExplain : 医生的价格说明
     */

    public int id;
    public String role;
    public String englishName;
    public String price;
    public String priceExplain;
}
