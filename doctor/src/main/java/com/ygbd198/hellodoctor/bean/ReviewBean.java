package com.ygbd198.hellodoctor.bean;


/**
 * Created by guangjiqin on 2017/9/11.
 */

public class ReviewBean {
//    public String name;
//    public String time;

    /**
     * id : 7
     * doctorId : 105
     * userId : 2
     * treatmentName : 熊猫眼
     * revisitDate : 2017-09-20
     * revisitStartTime : 14:00
     * revisitEndTime : 16:00
     * revisitContent : 看情况
     * createTime : 2017-08-25 14:30:39
     * isRevisit : null
     * diagnosticId : null
     * time : null
     * originalDate : null
     * isDelay : null
     * userName : lyj
     * userPic : null
     */

    public int id;
    public int doctorId;
    public int userId;
    public String treatmentName;
    public String revisitDate;
    public String revisitStartTime;
    public String revisitEndTime;
    public String revisitContent;
    public String createTime;
    public boolean isRevisit;
    public String diagnosticId;
    public String time;
    public String originalDate;
    public boolean isDelay;
    public String userName;
    public String userPic;
}
