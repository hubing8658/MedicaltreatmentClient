package com.ygbd198.hellodoctor.bean;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

/**
 * 要提交的数据
 * Created by monty on 2017/9/2.
 */

public class Record {
    /**
     * 治疗记录Id
     */
    public long diagnosticId;
    /**
     * 治疗类型
     */
    public String treatmentName;
    /**
     * 护士Id
     */
    public long nurseId;
    /**
     * 药品
     */
    public List<Drug> drugs;
    /**
     * 饮食
     */
    public String diets;
    /**
     * 作息
     */
    public String rests;
    /**
     * 禁忌
     */
    public String taboos;
    public Revisit revisits;

    /**
     * 药品
     */
    public static class Drug {
        /*{
            "drugId": 2,
			"useInfo": "每日3次，每次2粒",
			"useTime": "餐前", //使用时间点，餐前、餐后等
			"useType": 1, //使用方式，1-内服，2-外用
			"count": 2,
			"promptTime": "2017-08-19 03:32:00,2017-08-19 04:32:00"
		}*/
        public long drugId;
        public String useInfo;
        public String useTime;
        public int useType;
        public int count;
        public String promptTime;
    }

    /**
     * 复诊
     */
    public static class Revisit {
        public String revisitDate;
        public String revisitTime;
        public String revisitContent;
    }

    public String parse2Json() {
        // serializeNulls()保留null值
        Gson gson = new GsonBuilder().serializeNulls().create();
        return gson.toJson(this);
    }
}
