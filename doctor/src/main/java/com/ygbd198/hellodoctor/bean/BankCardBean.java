package com.ygbd198.hellodoctor.bean;

/**
 * Created by guangjiqin on 2017/8/29.
 */

public class BankCardBean {
    /**
     * 银行卡javabean
     *
     * id : 1
     * doctorId : 105
     * name : 中国银行
     * type : 储蓄卡
     * afterFour : 6789
     * createTime : 2017-08-24 14:37:59
     * statu : true
     */

    public int id;
    public int doctorId;
    public String name;
    public String type;
    public int afterFour;
    public String createTime;
    public boolean statu;

    @Override
    public String toString() {
        return "BankCardBean{" +
                "id=" + id +
                ", doctorId=" + doctorId +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", afterFour=" + afterFour +
                ", createTime='" + createTime + '\'' +
                ", statu=" + statu +
                '}';
    }
}
