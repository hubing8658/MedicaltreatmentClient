package com.ygbd198.hellodoctor.bean;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

/**
 * Created by monty on 2017/7/22.
 */
@Entity
public class User {
    private String sig;
    private String createTime;
    private boolean enable;
    @Id
    private long id;
    private boolean messageReminder;
    private String password;
    private String phone;
    private String token;
    private String name;
    private String openIdWX;
    private String openIdQQ;
    private String photo;
    private String gender;
    private String age;
    private String area;


    @Generated(hash = 1081053982)
    public User(String sig, String createTime, boolean enable, long id,
                boolean messageReminder, String password, String phone, String token,
                String name, String openIdWX, String openIdQQ, String photo,
                String gender, String age, String area) {
        this.sig = sig;
        this.createTime = createTime;
        this.enable = enable;
        this.id = id;
        this.messageReminder = messageReminder;
        this.password = password;
        this.phone = phone;
        this.token = token;
        this.name = name;
        this.openIdWX = openIdWX;
        this.openIdQQ = openIdQQ;
        this.photo = photo;
        this.gender = gender;
        this.age = age;
        this.area = area;
    }


    @Generated(hash = 586692638)
    public User() {
    }


    @Override
    public String toString() {
        return "User{" +
                "sig='" + sig + '\'' +
                ", createTime='" + createTime + '\'' +
                ", enable=" + enable +
                ", id=" + id +
                ", messageReminder=" + messageReminder +
                ", password='" + password + '\'' +
                ", phone=" + phone +
                ", token='" + token + '\'' +
                ", name='" + name + '\'' +
                ", openIdWX='" + openIdWX + '\'' +
                ", openIdQQ='" + openIdQQ + '\'' +
                ", photo='" + photo + '\'' +
                ", gender='" + gender + '\'' +
                ", age='" + age + '\'' +
                ", area='" + area + '\'' +
                '}';
    }


    public String getArea() {
        return this.area;
    }


    public void setArea(String area) {
        this.area = area;
    }


    public String getAge() {
        return this.age;
    }


    public void setAge(String age) {
        this.age = age;
    }


    public String getGender() {
        return this.gender;
    }


    public void setGender(String gender) {
        this.gender = gender;
    }


    public String getPhoto() {
        return this.photo;
    }


    public void setPhoto(String photo) {
        this.photo = photo;
    }


    public String getOpenIdQQ() {
        return this.openIdQQ;
    }


    public void setOpenIdQQ(String openIdQQ) {
        this.openIdQQ = openIdQQ;
    }


    public String getOpenIdWX() {
        return this.openIdWX;
    }


    public void setOpenIdWX(String openIdWX) {
        this.openIdWX = openIdWX;
    }


    public String getName() {
        return this.name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public String getToken() {
        return this.token;
    }


    public void setToken(String token) {
        this.token = token;
    }


    public String getPhone() {
        return this.phone;
    }


    public void setPhone(String phone) {
        this.phone = phone;
    }


    public String getPassword() {
        return this.password;
    }


    public void setPassword(String password) {
        this.password = password;
    }


    public boolean getMessageReminder() {
        return this.messageReminder;
    }


    public void setMessageReminder(boolean messageReminder) {
        this.messageReminder = messageReminder;
    }


    public long getId() {
        return this.id;
    }


    public void setId(long id) {
        this.id = id;
    }


    public boolean getEnable() {
        return this.enable;
    }


    public void setEnable(boolean enable) {
        this.enable = enable;
    }


    public String getCreateTime() {
        return this.createTime;
    }


    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }


    public String getSig() {
        return this.sig;
    }


    public void setSig(String sig) {
        this.sig = sig;
    }


}
