package com.ygbd198.hellodoctor.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by monty on 2017/8/27.
 */

public class Prescription {

    /**
     * id : 1
     * adminId : null
     * applyDrugId : 102
     * treatmentName : 熊猫眼
     * drugType : null
     * drugName : 滴眼液
     * producers : 广东惠州
     * effect : 非常好
     * useInfos : null
     * defaultUseInfo : 每日3次
     * useTimes : null
     * defaultUseTime : null
     * everydayCount : null
     * dosage : null
     * useType : null
     * pic : null
     * sideEffect : false
     * ownBrand : true
     * minUnit : 个
     * maxUnit : 盒
     * price : 20
     * drugstorePrice : null
     * isPrescription : true
     * createTime : 2017-08-26 18:53:16
     * statu : true
     * lastUpdateTime : null
     */

    private int id;
    private int adminId;
    private int applyDrugId;
    private String treatmentName;
    private String drugType;
    private String drugName;
    private String producers;
    private String effect;
    private String useInfos;
    private String defaultUseInfo;
    private String useTimes;
    private String defaultUseTime;
    private int everydayCount;
    private int dosage;
    private int useType;
    private String pic;
    private boolean sideEffect;
    private boolean ownBrand;
    private String minUnit;
    private String maxUnit;
    private double price;
    private double drugstorePrice;
    private boolean isPrescription;
    private String createTime;
    private boolean statu;
    private String lastUpdateTime;
    private String promptTime;
    private List<String> promptTimes = new ArrayList<>();


    private boolean isChecked;
    private boolean expand;

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public boolean isExpand() {
        return expand;
    }

    public void setExpand(boolean expand) {
        this.expand = expand;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAdminId() {
        return adminId;
    }

    public void setAdminId(int adminId) {
        this.adminId = adminId;
    }

    public int getApplyDrugId() {
        return applyDrugId;
    }

    public void setApplyDrugId(int applyDrugId) {
        this.applyDrugId = applyDrugId;
    }

    public String getTreatmentName() {
        return treatmentName;
    }

    public void setTreatmentName(String treatmentName) {
        this.treatmentName = treatmentName;
    }

    public String getDrugType() {
        return drugType;
    }

    public void setDrugType(String drugType) {
        this.drugType = drugType;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public String getProducers() {
        return producers;
    }

    public void setProducers(String producers) {
        this.producers = producers;
    }

    public String getEffect() {
        return effect;
    }

    public void setEffect(String effect) {
        this.effect = effect;
    }

    public String getUseInfos() {
        return useInfos;
    }

    public void setUseInfos(String useInfos) {
        this.useInfos = useInfos;
    }

    public String getDefaultUseInfo() {
        return defaultUseInfo;
    }

    public void setDefaultUseInfo(String defaultUseInfo) {
        this.defaultUseInfo = defaultUseInfo;
    }

    public String getUseTimes() {
        return useTimes;
    }

    public void setUseTimes(String useTimes) {
        this.useTimes = useTimes;
    }

    public String getDefaultUseTime() {
        return defaultUseTime;
    }

    public void setDefaultUseTime(String defaultUseTime) {
        this.defaultUseTime = defaultUseTime;
    }

    public int getEverydayCount() {
        return everydayCount;
    }

    public void setEverydayCount(int everydayCount) {
        this.everydayCount = everydayCount;
    }

    public int getDosage() {
        return dosage;
    }

    public void setDosage(int dosage) {
        this.dosage = dosage;
    }

    public int getUseType() {
        return useType;
    }

    public void setUseType(int useType) {
        this.useType = useType;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public boolean isSideEffect() {
        return sideEffect;
    }

    public void setSideEffect(boolean sideEffect) {
        this.sideEffect = sideEffect;
    }

    public boolean isOwnBrand() {
        return ownBrand;
    }

    public void setOwnBrand(boolean ownBrand) {
        this.ownBrand = ownBrand;
    }

    public String getMinUnit() {
        return minUnit;
    }

    public void setMinUnit(String minUnit) {
        this.minUnit = minUnit;
    }

    public String getMaxUnit() {
        return maxUnit;
    }

    public void setMaxUnit(String maxUnit) {
        this.maxUnit = maxUnit;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getDrugstorePrice() {
        return drugstorePrice;
    }

    public void setDrugstorePrice(double drugstorePrice) {
        this.drugstorePrice = drugstorePrice;
    }

    public boolean isPrescription() {
        return isPrescription;
    }

    public void setPrescription(boolean prescription) {
        isPrescription = prescription;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public boolean isStatu() {
        return statu;
    }

    public void setStatu(boolean statu) {
        this.statu = statu;
    }

    public String getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(String lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public String getPromptTime() {
        StringBuilder promptBuilder = new StringBuilder();
        for (int i = 0; i < promptTimes.size(); i++) {
            promptBuilder.append(promptTimes.get(i));
            if (i != promptTimes.size() - 1) {
                promptBuilder.append(",");
            }
        }
        return promptBuilder.toString();
    }

    public void addPromptTime(String str){
        promptTimes.add(str);
    }
    public void removePromptTime(int position){
        promptTimes.remove(position);
    }

    public void setPromptTime(String promptTime) {
        this.promptTime = promptTime;
    }

    @Override
    public String toString() {
        return "Prescription{" +
                "id=" + id +
                ", adminId=" + adminId +
                ", applyDrugId=" + applyDrugId +
                ", treatmentName='" + treatmentName + '\'' +
                ", drugType='" + drugType + '\'' +
                ", drugName='" + drugName + '\'' +
                ", producers='" + producers + '\'' +
                ", effect='" + effect + '\'' +
                ", useInfos='" + useInfos + '\'' +
                ", defaultUseInfo='" + defaultUseInfo + '\'' +
                ", useTimes='" + useTimes + '\'' +
                ", defaultUseTime='" + defaultUseTime + '\'' +
                ", everydayCount=" + everydayCount +
                ", dosage=" + dosage +
                ", useType=" + useType +
                ", pic='" + pic + '\'' +
                ", sideEffect=" + sideEffect +
                ", ownBrand=" + ownBrand +
                ", minUnit='" + minUnit + '\'' +
                ", maxUnit='" + maxUnit + '\'' +
                ", price=" + price +
                ", drugstorePrice=" + drugstorePrice +
                ", isPrescription=" + isPrescription +
                ", createTime='" + createTime + '\'' +
                ", statu=" + statu +
                ", lastUpdateTime='" + lastUpdateTime + '\'' +
                ", promptTime='" + promptTime + '\'' +
                ", promptTimes=" + promptTimes +
                ", isChecked=" + isChecked +
                ", expand=" + expand +
                '}';
    }
}
