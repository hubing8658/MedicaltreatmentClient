package com.ygbd198.hellodoctor.bean;

import com.google.gson.annotations.SerializedName;
import com.ygbd198.hellodoctor.common.BaseBean;

import java.util.List;

/**
 * Created by monty on 2017/8/27.
 */

public class PrescriptionBean extends BaseBean {

    public List<Prescription> data;

    public class Prescription{
        /**
         * id : 1
         * adminId : null
         * applyDrugId : 102
         * treatmentName : 斑
         * drugType : 内服
         * drugName : 汤臣倍健天然维生素C
         * drugStandard : 600mg/片*100片
         * producers : 汤臣倍健股份有限公司
         * effect : 抗氧化
         * useInfos : null
         * defaultUseInfo : null
         * useTimes : 不限,早晚,早上,饭前,睡前,饭中,饭后
         * defaultUseTime : 不限
         * everydayCount : 1
         * dosage : 1
         * useType : 1
         * pic : http://123.207.9.75:8082/userfiles/1/images/drugs/2017/10/%E6%B1%A4%E8%87%A3%E5%80%8D%E5%81%A5%E5%A4%A9%E7%84%B6%E7%BB%B4%E7%94%9F%E7%B4%A0C.jpg
         * sideEffect : false
         * ownBrand : false
         * minUnit : 片
         * maxUnit : 瓶
         * price : 110.0
         * drugstorePrice : null
         * isPrescription : false
         * createTime : 2017-08-26 19:59:07
         * statu : true
         * lastUpdateTime : 2017-10-18 15:32:59
         * component : 维生素C、糖粉、玉米淀粉…
         * shape : 片状
         * applicableScope : 4岁以上需要补充维生素C者
         * badEffect : 尚不明确
         * taboo : 尚不明确
         * matters : 本品不能代替药物，不宜超过推荐量或与同类营养素补充剂同时食用。
         * effectiveTime : 24个月
         * storage : 置阴凉干燥处
         * standardNumber : 国食健字G20050179
         */


        public int id;
        public int adminId;
        public int applyDrugId;
        public String treatmentName;
        public String drugType;
        public String drugName;
        public String drugStandard;
        public String producers;
        public String effect;
        public String useInfos;
        public String defaultUseInfo;
        public String useTimes;
        public String defaultUseTime;
        public int everydayCount;
        public int dosage;
        public int useType;
        public String pic;
        public boolean sideEffect;
        public boolean ownBrand;
        public String minUnit;
        public String maxUnit;
        public Float price;
        public double drugstorePrice;
        public boolean isPrescription;
        public String createTime;
        public boolean statu;
        public String lastUpdateTime;
        public String component;
        public String shape;
        public String applicableScope;
        public String badEffect;
        public String taboo;
        public String matters;
        public String effectiveTime;
        public String storage;
        public String standardNumber;

        public boolean isChecked;


        public boolean isOpen;
    }


    
}
