package com.ygbd198.hellodoctor.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by guangjiqin on 2017/8/19.
 */
@Deprecated
public class ApplyBeDocLoginBean implements Parcelable {
    /**
     * id : 105
     * phone : 13332965197
     * password : 5701117D3416
     * workCertificate : null
     * identityCard : null
     * physicianCertificate : null
     * photo : null
     * gender : null
     * education : null
     * good : null
     * statu : 101
     * appointmentTime : null
     * integral : 0
     * token : b1502871319994
     * createTime : 2017-08-16 16:15:19
     * sig : eJxlkEtPg0AUhff8CsIWY2d4Y*KCFEuoVGyQpnVDgBnIDcqr07Gk8b*ro0lJvNvvyzkn9yLJsqy8RMltXpbdqWUZm3qqyHeygpSbK*x7IFnOMn0k-yA99zDSLK8YHQW0dGRrCM0dILRlUMGfUWATaY6Ndey6rjHzjqTJRJnQsPGdopuG4cwVqAXcPKTLcOub211TDBC8TV2csILvo-2qakxkDY5deIuUW856cX58PlQfYb2xfQJBWtrRa7OqA*4moXeY-DEenrCqxqlnqDty4uvl5N3PKhm8--7kOntGOR2P0LVC0BA2saajn1OkT*kL-IpdfQ__
     */

    public int id;
    public long phone;
    public String password;
    public String workCertificate;
    public String identityCard;
    public String physicianCertificate;
    public String photo;
    public String gender;
    public String education;
    public String good;
    public int statu;
    public String appointmentTime;
    public int integral;
    public String token;
    public String createTime;
    public String sig;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeLong(this.phone);
        dest.writeString(this.password);
        dest.writeString(this.workCertificate);
        dest.writeString(this.identityCard);
        dest.writeString(this.physicianCertificate);
        dest.writeString(this.photo);
        dest.writeString(this.gender);
        dest.writeString(this.education);
        dest.writeString(this.good);
        dest.writeInt(this.statu);
        dest.writeString(this.appointmentTime);
        dest.writeInt(this.integral);
        dest.writeString(this.token);
        dest.writeString(this.createTime);
        dest.writeString(this.sig);
    }

    public ApplyBeDocLoginBean() {
    }

    protected ApplyBeDocLoginBean(Parcel in) {
        this.id = in.readInt();
        this.phone = in.readLong();
        this.password = in.readString();
        this.workCertificate = in.readString();
        this.identityCard = in.readString();
        this.physicianCertificate = in.readString();
        this.photo = in.readString();
        this.gender = in.readString();
        this.education = in.readString();
        this.good = in.readString();
        this.statu = in.readInt();
        this.appointmentTime = in.readString();
        this.integral = in.readInt();
        this.token = in.readString();
        this.createTime = in.readString();
        this.sig = in.readString();
    }

    public static final Parcelable.Creator<ApplyBeDocLoginBean> CREATOR = new Parcelable.Creator<ApplyBeDocLoginBean>() {
        @Override
        public ApplyBeDocLoginBean createFromParcel(Parcel source) {
            return new ApplyBeDocLoginBean(source);
        }

        @Override
        public ApplyBeDocLoginBean[] newArray(int size) {
            return new ApplyBeDocLoginBean[size];
        }
    };
}
