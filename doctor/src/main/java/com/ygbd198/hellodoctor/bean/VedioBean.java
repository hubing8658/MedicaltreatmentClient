package com.ygbd198.hellodoctor.bean;

/**
 * Created by guangjiqin on 2017/8/23.
 */

public class VedioBean  {
    /**
     * id : 1
     * name : 实操培训第一集
     * introduction : 第一集简介
     * longTime : 3240
     * pic : 第一集图片路径
     * videoUrl : 第一集视频路径
     */

    public int id;
    public String name;
    public String introduction;
    public int longTime;
    public String pic;
    public String videoUrl;


    public boolean isRead;//是否已观看标示
}
