package com.ygbd198.hellodoctor.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by guangjiqin on 2017/8/29.
 */

public class MyIntegralBean implements Parcelable {
    /**
     * bank : {"id":1,"doctorId":105,"name":"中国银行","type":"储蓄卡","afterFour":6789,"createTime":"2017-08-24 14:37:59","statu":true}
     * integral : 100
     */

    public BankBean bank;
    public int integral;

    public static class BankBean implements Parcelable {
        /**
         * id : 1
         * doctorId : 105
         * name : 中国银行
         * type : 储蓄卡
         * afterFour : 6789
         * createTime : 2017-08-24 14:37:59
         * statu : true
         */

        public int id;
        public int doctorId;
        public String name;
        public String type;
        public int afterFour;
        public String createTime;
        public boolean statu;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.id);
            dest.writeInt(this.doctorId);
            dest.writeString(this.name);
            dest.writeString(this.type);
            dest.writeInt(this.afterFour);
            dest.writeString(this.createTime);
            dest.writeByte(this.statu ? (byte) 1 : (byte) 0);
        }

        public BankBean() {
        }

        protected BankBean(Parcel in) {
            this.id = in.readInt();
            this.doctorId = in.readInt();
            this.name = in.readString();
            this.type = in.readString();
            this.afterFour = in.readInt();
            this.createTime = in.readString();
            this.statu = in.readByte() != 0;
        }

        public static final Creator<BankBean> CREATOR = new Creator<BankBean>() {
            @Override
            public BankBean createFromParcel(Parcel source) {
                return new BankBean(source);
            }

            @Override
            public BankBean[] newArray(int size) {
                return new BankBean[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.bank, flags);
        dest.writeInt(this.integral);
    }

    public MyIntegralBean() {
    }

    protected MyIntegralBean(Parcel in) {
        this.bank = in.readParcelable(BankBean.class.getClassLoader());
        this.integral = in.readInt();
    }

    public static final Parcelable.Creator<MyIntegralBean> CREATOR = new Parcelable.Creator<MyIntegralBean>() {
        @Override
        public MyIntegralBean createFromParcel(Parcel source) {
            return new MyIntegralBean(source);
        }

        @Override
        public MyIntegralBean[] newArray(int size) {
            return new MyIntegralBean[size];
        }
    };

    @Override
    public String toString() {
        return "MyIntegralBean{" +
                "bank=" + bank +
                ", integral=" + integral +
                '}';
    }
}
