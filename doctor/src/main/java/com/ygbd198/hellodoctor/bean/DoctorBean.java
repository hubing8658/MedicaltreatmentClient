package com.ygbd198.hellodoctor.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by monty on 2017/7/30.
 */
public class DoctorBean implements Parcelable {

    /**
     * id : 1
     * phone : 13732965191
     * password : 5701117D3416
     * workCertificate : null
     * physicianCertificate : null
     * photo : null
     * gender : null
     * education : null
     * good : null
     * job : null
     * statu : 4
     * appointmentTime : null
     * balance : 1633
     * integral : 2654
     * token : c31439571b2349d99d1ff4a8182905b8
     * sig : eJxlkF1PgzAUhu-3Kwi3M67t4asmXjCDZhEik2niFSm0xUJkrOuYzPjfVTQR47l9nvd9k-M2syzL3sTZOSvL7aE1uRk6YVsXlo3ss1-YdYrnzOSg*T8oXjulRc6kEXqEHiCfIDR1FBetUVL9GCVgB6jr44KAQzmlHEvpsAAHhCK3CCbJPW-ycX4MYuezF1zH*aOoaoRJ9HC1WqbmVC3NCqcZ4xra*6HKrgPWJ5n3ND*ghqSiGer4jqa7UEVhPd89G6-g8eIo4r6X9WNSQ7ypFmENMmrWtwPTCcDNEa0vJ5NGvXx-CbsI*dhHHp3QXui92rajQBB2MQH0dfbsffYBV-1jeg__
     * doctorAttach : null
     * time:string
     */
    private int id;
    private long phone;
    private String password;
    private String workCertificate;
    private String physicianCertificate;
    private String photo;
    private String gender;
    private String education;
    private String good;
    private String job;
    private int statu;
    private String identityCard;
    private String appointmentTime;
    private String practiceVideoIds;
    private int balance;
    private int integral;
    private String token;
    private String sig;
    private String doctorAttach;
    private String time;

    /**
     * 注册时间，格式-->2017-10-17 18:39:10
     */
    public String createTime;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeLong(this.phone);
        dest.writeString(this.password);
        dest.writeString(this.workCertificate);
        dest.writeString(this.physicianCertificate);
        dest.writeString(this.photo);
        dest.writeString(this.gender);
        dest.writeString(this.education);
        dest.writeString(this.good);
        dest.writeString(this.job);
        dest.writeInt(this.statu);
        dest.writeString(this.identityCard);
        dest.writeString(this.appointmentTime);
        dest.writeString(this.practiceVideoIds);
        dest.writeInt(this.balance);
        dest.writeInt(this.integral);
        dest.writeString(this.token);
        dest.writeString(this.sig);
        dest.writeString(this.doctorAttach);
        dest.writeString(this.time);
        dest.writeString(this.createTime);
    }

    public DoctorBean() {
    }

    protected DoctorBean(Parcel in) {
        this.id = in.readInt();
        this.phone = in.readLong();
        this.password = in.readString();
        this.workCertificate = in.readString();
        this.physicianCertificate = in.readString();
        this.photo = in.readString();
        this.gender = in.readString();
        this.education = in.readString();
        this.good = in.readString();
        this.job = in.readString();
        this.statu = in.readInt();
        this.identityCard = in.readString();
        this.appointmentTime = in.readString();
        this.practiceVideoIds = in.readString();
        this.balance = in.readInt();
        this.integral = in.readInt();
        this.token = in.readString();
        this.sig = in.readString();
        this.doctorAttach = in.readString();
        this.time = in.readString();
        this.createTime = in.readString();
    }

    public static final Creator<DoctorBean> CREATOR = new Creator<DoctorBean>() {
        @Override
        public DoctorBean createFromParcel(Parcel source) {
            return new DoctorBean(source);
        }

        @Override
        public DoctorBean[] newArray(int size) {
            return new DoctorBean[size];
        }
    };

    @Override
    public String toString() {
        return "DoctorBean{" +
                "id=" + id +
                ", phone=" + phone +
                ", password='" + password + '\'' +
                ", workCertificate='" + workCertificate + '\'' +
                ", physicianCertificate='" + physicianCertificate + '\'' +
                ", photo='" + photo + '\'' +
                ", gender='" + gender + '\'' +
                ", education='" + education + '\'' +
                ", good='" + good + '\'' +
                ", job='" + job + '\'' +
                ", statu=" + statu +
                ", identityCard='" + identityCard + '\'' +
                ", appointmentTime='" + appointmentTime + '\'' +
                ", practiceVideoIds='" + practiceVideoIds + '\'' +
                ", balance=" + balance +
                ", integral=" + integral +
                ", token='" + token + '\'' +
                ", sig='" + sig + '\'' +
                ", doctorAttach='" + doctorAttach + '\'' +
                ", time='" + time + '\'' +
                ", createTime='" + createTime + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getPhone() {
        return phone;
    }

    public void setPhone(long phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getWorkCertificate() {
        return workCertificate;
    }

    public void setWorkCertificate(String workCertificate) {
        this.workCertificate = workCertificate;
    }

    public String getPhysicianCertificate() {
        return physicianCertificate;
    }

    public void setPhysicianCertificate(String physicianCertificate) {
        this.physicianCertificate = physicianCertificate;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getGood() {
        return good;
    }

    public void setGood(String good) {
        this.good = good;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public int getStatu() {
        return statu;
    }

    public void setStatu(int statu) {
        this.statu = statu;
    }

    public String getIdentityCard() {
        return identityCard;
    }

    public void setIdentityCard(String identityCard) {
        this.identityCard = identityCard;
    }

    public String getAppointmentTime() {
        return appointmentTime;
    }

    public void setAppointmentTime(String appointmentTime) {
        this.appointmentTime = appointmentTime;
    }

    public String getPracticeVideoIds() {
        return practiceVideoIds;
    }

    public void setPracticeVideoIds(String practiceVideoIds) {
        this.practiceVideoIds = practiceVideoIds;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public int getIntegral() {
        return integral;
    }

    public void setIntegral(int integral) {
        this.integral = integral;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getSig() {
        return sig;
    }

    public void setSig(String sig) {
        this.sig = sig;
    }

    public String getDoctorAttach() {
        return doctorAttach;
    }

    public void setDoctorAttach(String doctorAttach) {
        this.doctorAttach = doctorAttach;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
