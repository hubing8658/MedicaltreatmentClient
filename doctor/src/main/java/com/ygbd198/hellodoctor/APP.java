package com.ygbd198.hellodoctor;

import android.content.Intent;

import com.bumptech.glide.Glide;
import com.monty.library.AppManager;
import com.monty.library.BaseApp;
import com.tencent.bugly.imsdk.Bugly;
import com.ygbd198.hellodoctor.ui.live.presenters.InitILiveHelper;

/**
 * Created by monty on 2017/7/30.
 */

public class APP extends BaseApp {

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        Glide.get(this);
        InitILiveHelper.initApp();
        Bugly.init(this, "ba83228751", true);
    }

    public static BaseApp getInstance() {
        return instance;
    }

    @Override
    public void startLoginActivity() {
        AppManager.getAppManager().finishAllActivity();
        Intent intent = new Intent("com.ygbd198.hellodoctor.login");
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
