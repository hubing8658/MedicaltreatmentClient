package com.monty.library;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.monty.library.http.BaseCallModel;
import com.monty.library.http.UploadRetrofitHelper;

import java.io.File;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;
import top.zibin.luban.Luban;


/**
 * Created by monty on 2017/10/12.
 */

public class RxUpload {
    public static class UploadProcess {
        public int count;
        public int position;
        public String data;
    }
    public static Observable upload(final Context context, final List<File> files, final String token, final int type){
        return Observable.fromIterable(files)
                .map(new Function<File, File>() {
                    @Override
                    public File apply(@NonNull File s) throws Exception {
                        File file = Luban.with(context).get(s.getAbsolutePath());
                        Log.d("monty","压缩完成,压缩后的路径 -> "+file.getAbsolutePath());
                        return file;
                    }
                })
                .map(new Function<File, UploadProcess>() {
                    int position = 0;
                    @Override
                    public UploadProcess apply(File file) throws Exception {
                        Log.d("monty", "uploadFile -> " + file.getAbsolutePath());
                        MultipartBody.Builder builder = new MultipartBody.Builder()
                                .setType(MultipartBody.FORM)//表单类型
                                .addFormDataPart("token", token)
                                .addFormDataPart("type", String.valueOf(type));

                        RequestBody imageBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                        builder.addFormDataPart("file", file.getName(), imageBody);//imgfile 后台接收图片流的参数名

                        List<MultipartBody.Part> parts = builder.build().parts();

                        Log.d("monty", "开始上传请求");
                        UploadProcess uploadProcess = new UploadProcess();
                        uploadProcess.count = files.size();

                        Response<BaseCallModel<List<String>>> response = UploadRetrofitHelper.getInstance().uploadImage(UploadService.class).doctorUpload(parts).execute();
                        Log.d("monty", "文件上传 -> result -> " + response.body().toString());
                        uploadProcess.data = response.body().data.get(0);
                        if (response.body().code == 200) {
                            uploadProcess.position = ++position;
                        } else {
                            throw new RuntimeException(response.body().msg);
                        }

                        return uploadProcess;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }
    public static Observable userUpload(final Context context, final List<File> files, final String token, final int type){
        return Observable.fromIterable(files)
                .map(new Function<File, File>() {
                    @Override
                    public File apply(@NonNull File s) throws Exception {
                        File file = Luban.with(context).get(s.getAbsolutePath());
                        Log.d("monty","压缩完成,压缩后的路径 -> "+file.getAbsolutePath());
                        return file;
                    }
                })
                .map(new Function<File, UploadProcess>() {
                    int position = 0;
                    @Override
                    public UploadProcess apply(File file) throws Exception {
                        Log.d("monty", "uploadFile -> " + file.getAbsolutePath());
                        MultipartBody.Builder builder = new MultipartBody.Builder()
                                .setType(MultipartBody.FORM)//表单类型
                                .addFormDataPart("token", token)
                                .addFormDataPart("type", type+"");

                        RequestBody imageBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                        builder.addFormDataPart("file", file.getName(), imageBody);//imgfile 后台接收图片流的参数名

                        List<MultipartBody.Part> parts = builder.build().parts();
                        Log.d("monty", "开始上传请求");
                        UploadProcess uploadProcess = new UploadProcess();
                        uploadProcess.count = files.size();

                        Response<BaseCallModel<String>> response = UploadRetrofitHelper.getInstance().uploadImage(UploadService.class).userUpload(parts).execute();
                        Log.d("monty", "文件上传 -> result -> " + response.body().toString());
                        uploadProcess.data = response.body().data;
                        if (response.body().code == 200) {
                            uploadProcess.position = ++position;
                        } else {
                            throw new RuntimeException(response.body().msg);
                        }

                        return uploadProcess;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }
}
