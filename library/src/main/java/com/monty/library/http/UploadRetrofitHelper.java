package com.monty.library.http;

import com.monty.library.HeaderInterceptor;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by kelin on 2017/6/9.
 * RetrofitHelper
 */

public class UploadRetrofitHelper {
    public static String VIDEO_UPLOAD_HOST = "http://123.207.9.75:8081/";
    private static String UPLOAD_HOST = "http://139.199.204.33:8081/";


    private static Retrofit.Builder retrofitBuilder = new Retrofit.Builder();

    private static Retrofit retrofit;
    private static final int DEFAULT_TIMEOUT = 60;//超时时长，单位：秒
    private volatile static UploadRetrofitHelper instance;

    private UploadRetrofitHelper() {
        getApiRetrofit();
    }

    /**
     * 初始化 Retrofit
     */
    private static Retrofit getApiRetrofit() {
        if (retrofit == null) {
            OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder();
            okHttpBuilder.connectTimeout(60, TimeUnit.SECONDS);
            okHttpBuilder.writeTimeout(100, TimeUnit.SECONDS);
            okHttpBuilder.readTimeout(60, TimeUnit.SECONDS);
            okHttpBuilder.addInterceptor(new HeaderInterceptor());
            retrofit = retrofitBuilder
                    .client(okHttpBuilder.build())
                    .baseUrl(UPLOAD_HOST)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build();
        }
        return retrofit;
    }

    /**
     * 创建数据请求服务
     */
    public static UploadRetrofitHelper getInstance() {
        if (instance == null) {
            synchronized (UploadRetrofitHelper.class) {
                if (instance == null) {
                    instance = new UploadRetrofitHelper();
                }
            }
        }
        return instance;
    }

    /**
     * 上传图片
     */
    public static <T> T uploadImage(final Class<T> service) {
        return retrofit.create(service);
    }

    public static void changeApiBaseUrl(String newApiBaseUrl) {
        UPLOAD_HOST = newApiBaseUrl;
        retrofit = retrofitBuilder
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(newApiBaseUrl)
                .build();
    }

}
