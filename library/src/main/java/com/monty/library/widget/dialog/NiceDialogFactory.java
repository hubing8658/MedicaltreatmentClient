package com.monty.library.widget.dialog;

import android.support.annotation.LayoutRes;
import android.support.v7.widget.AppCompatRatingBar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.monty.library.R;
import com.othershe.nicedialog.BaseNiceDialog;
import com.othershe.nicedialog.NiceDialog;
import com.othershe.nicedialog.ViewConvertListener;
import com.othershe.nicedialog.ViewHolder;

/**
 * Created by monty on 2017/9/6.
 */

public class NiceDialogFactory {
    public interface OnPositiveClickListener {
        void onPositiveClick(BaseNiceDialog baseNiceDialog);
    }

    public static BaseNiceDialog createBaseDialog(@LayoutRes int layoutResId, ViewConvertListener listener) {
        return NiceDialog.init()
                .setLayoutId(layoutResId)     //设置dialog布局文件
                .setConvertListener(listener)   //进行相关View操作的回调
                .setDimAmount(0.5f)     //调节灰色背景透明度[0-1]，默认0.5f
//                .setShowBottom(true)     //是否在底部显示dialog，默认flase
                .setMargin(30)     //dialog左右两边到屏幕边缘的距离（单位：dp），默认0dp
//                .setWidth()     //dialog宽度（单位：dp），默认为屏幕宽度
//                .setHeight()     //dialog高度（单位：dp），默认为WRAP_CONTENT
                .setOutCancel(true)     //点击dialog外是否可取消，默认true
                .setAnimStyle(R.style.BaseDialog_AnimationStyle);     //设置dialog进入、退出的动画style(底部显示的dialog有默认动画)

    }

    /**
     * 通用Dialog 包含一个标题，一个message，一个取消按钮，一个确定按钮  内容可设置颜色
     * @link{R.layout.base_dilog_layout}
     * @param title
     * @param message
     * @param onPositiveClickListener
     * @return
     */
    public static BaseNiceDialog  createComfirmDialog(final String title, final String message, final OnPositiveClickListener onPositiveClickListener,final int colorRes) {
        return NiceDialog.init()
                .setLayoutId(R.layout.base_dilog_layout)
                .setConvertListener(new ViewConvertListener() {
                    @Override
                    protected void convertView(ViewHolder viewHolder, final BaseNiceDialog baseNiceDialog) {
                        ((TextView) viewHolder.getView(R.id.tv_title)).setText(title);
                        ((TextView) viewHolder.getView(R.id.tv_message)).setText(message);
                        ((TextView) viewHolder.getView(R.id.tv_message)).setTextColor(colorRes);
                        viewHolder.getView(R.id.btn_positive).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                onPositiveClickListener.onPositiveClick(baseNiceDialog);
                            }
                        });
                        viewHolder.getView(R.id.btn_negative).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                baseNiceDialog.dismiss();
                            }
                        });
                    }
                })   //进行相关View操作的回调
                .setDimAmount(0.5f)     //调节灰色背景透明度[0-1]，默认0.5f
//                .setShowBottom(true)     //是否在底部显示dialog，默认flase
                .setMargin(30)     //dialog左右两边到屏幕边缘的距离（单位：dp），默认0dp
//                .setWidth()     //dialog宽度（单位：dp），默认为屏幕宽度
//                .setHeight()     //dialog高度（单位：dp），默认为WRAP_CONTENT
                .setOutCancel(true)     //点击dialog外是否可取消，默认true
                .setAnimStyle(android.R.style.Animation_Dialog);     //设置dialog进入、退出的动画style(底部显示的dialog有默认动画)

    }

    /**
     * 通用Dialog 包含一个标题，一个message，一个取消按钮，一个确定按钮
     * @link{R.layout.base_dilog_layout}
     * @param title
     * @param message
     * @param onPositiveClickListener
     * @return
     */
    public static BaseNiceDialog  createComfirmDialog(final String title, final String message, final OnPositiveClickListener onPositiveClickListener) {
        return NiceDialog.init()
                .setLayoutId(R.layout.base_dilog_layout)
                .setConvertListener(new ViewConvertListener() {
                    @Override
                    protected void convertView(ViewHolder viewHolder, final BaseNiceDialog baseNiceDialog) {
                        ((TextView) viewHolder.getView(R.id.tv_title)).setText(title);
                        ((TextView) viewHolder.getView(R.id.tv_message)).setText(message);
                        viewHolder.getView(R.id.btn_positive).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                onPositiveClickListener.onPositiveClick(baseNiceDialog);
                            }
                        });
                        viewHolder.getView(R.id.btn_negative).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                baseNiceDialog.dismiss();
                            }
                        });
                    }
                })   //进行相关View操作的回调
                .setDimAmount(0.5f)     //调节灰色背景透明度[0-1]，默认0.5f
//                .setShowBottom(true)     //是否在底部显示dialog，默认flase
                .setMargin(30)     //dialog左右两边到屏幕边缘的距离（单位：dp），默认0dp
//                .setWidth()     //dialog宽度（单位：dp），默认为屏幕宽度
//                .setHeight()     //dialog高度（单位：dp），默认为WRAP_CONTENT
                .setOutCancel(true)     //点击dialog外是否可取消，默认true
                .setAnimStyle(android.R.style.Animation_Dialog);     //设置dialog进入、退出的动画style(底部显示的dialog有默认动画)

    }
    /**
     * 通用Dialog 包含一个标题，一个message，没有取消按钮，一个确定按钮
     * @link{R.layout.base_dilog_layout}
     * @param title
     * @param message
     * @param onPositiveClickListener
     * @return
     */
    public static BaseNiceDialog  createTipDialog(final String title, final String message, final OnPositiveClickListener onPositiveClickListener) {
        return NiceDialog.init()
                .setLayoutId(R.layout.tip_dialog_layout)
                .setConvertListener(new ViewConvertListener() {
                    @Override
                    protected void convertView(ViewHolder viewHolder, final BaseNiceDialog baseNiceDialog) {
                        ((TextView) viewHolder.getView(R.id.tv_message)).setText(message);
                        ((Button)viewHolder.getView(R.id.btn_positive)).setText("确认");
                        viewHolder.getView(R.id.btn_positive).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                onPositiveClickListener.onPositiveClick(baseNiceDialog);
                            }
                        });
                    }
                })   //进行相关View操作的回调
                .setDimAmount(0.5f)     //调节灰色背景透明度[0-1]，默认0.5f
//                .setShowBottom(true)     //是否在底部显示dialog，默认flase
                .setMargin(30)     //dialog左右两边到屏幕边缘的距离（单位：dp），默认0dp
//                .setWidth()     //dialog宽度（单位：dp），默认为屏幕宽度
//                .setHeight()     //dialog高度（单位：dp），默认为WRAP_CONTENT
                .setOutCancel(true)     //点击dialog外是否可取消，默认true
                .setAnimStyle(android.R.style.Animation_Dialog);     //设置dialog进入、退出的动画style(底部显示的dialog有默认动画)

    }
    /**
     * 通用Dialog 包含一个标题，一个message，没有取消按钮，一个确定按钮
     * @link{R.layout.base_dilog_layout}
     * @param message
     * @param onPositiveClickListener
     * @return
     */
    public static BaseNiceDialog  createTip2Dialog(final String message, final OnPositiveClickListener onPositiveClickListener) {
        return NiceDialog.init()
                .setLayoutId(R.layout.base_dilog_layout)
                .setConvertListener(new ViewConvertListener() {
                    @Override
                    protected void convertView(ViewHolder viewHolder, final BaseNiceDialog baseNiceDialog) {
                        ((TextView) viewHolder.getView(R.id.tv_title)).setVisibility(View.GONE);
                        ((TextView) viewHolder.getView(R.id.tv_message)).setText(message);
                        viewHolder.getView(R.id.btn_positive).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                onPositiveClickListener.onPositiveClick(baseNiceDialog);
                            }
                        });
                        viewHolder.getView(R.id.btn_negative).setVisibility(View.GONE);
                        viewHolder.getView(R.id.btn_positive).setBackgroundResource(R.drawable.dialog_bottom_btn_blue_bg);
                    }
                })   //进行相关View操作的回调
                .setDimAmount(0.5f)     //调节灰色背景透明度[0-1]，默认0.5f
//                .setShowBottom(true)     //是否在底部显示dialog，默认flase
                .setMargin(30)     //dialog左右两边到屏幕边缘的距离（单位：dp），默认0dp
//                .setWidth()     //dialog宽度（单位：dp），默认为屏幕宽度
//                .setHeight()     //dialog高度（单位：dp），默认为WRAP_CONTENT
                .setOutCancel(false)     //点击dialog外是否可取消，默认true
                .setAnimStyle(android.R.style.Animation_Dialog);     //设置dialog进入、退出的动画style(底部显示的dialog有默认动画)

    }

    public interface OnStarDialogListener {
        void onNegativeClick(BaseNiceDialog baseNiceDialog);
        void onPositiveClick(BaseNiceDialog baseNiceDialog,AppCompatRatingBar ratingBar);
    }
    /**
     * 通用Dialog 包含一个标题，一个message，没有取消按钮，一个确定按钮
     * @link{R.layout.base_dilog_layout}
     * @param title
     * @return
     */
    public static BaseNiceDialog  createStarDialog(final String title, final OnStarDialogListener onStarDialogListener) {
        return NiceDialog.init()
                .setLayoutId(R.layout.star_dilog_layout)
                .setConvertListener(new ViewConvertListener() {
                    @Override
                    protected void convertView(ViewHolder viewHolder, final BaseNiceDialog baseNiceDialog) {
                        ((TextView) viewHolder.getView(R.id.tv_title)).setText(title);
                        final AppCompatRatingBar ratingBar = (AppCompatRatingBar) viewHolder.getView(R.id.rb);

                        viewHolder.getView(R.id.btn_positive).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                onStarDialogListener.onPositiveClick(baseNiceDialog,ratingBar);
                            }
                        });
                        viewHolder.getView(R.id.btn_negative).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                onStarDialogListener.onNegativeClick(baseNiceDialog);
                            }
                        });
                    }
                })   //进行相关View操作的回调
                .setDimAmount(0.5f)     //调节灰色背景透明度[0-1]，默认0.5f
//                .setShowBottom(true)     //是否在底部显示dialog，默认flase
                .setMargin(30)     //dialog左右两边到屏幕边缘的距离（单位：dp），默认0dp
//                .setWidth()     //dialog宽度（单位：dp），默认为屏幕宽度
//                .setHeight()     //dialog高度（单位：dp），默认为WRAP_CONTENT
                .setOutCancel(true)     //点击dialog外是否可取消，默认true
                .setAnimStyle(android.R.style.Animation_Dialog);     //设置dialog进入、退出的动画style(底部显示的dialog有默认动画)

    }

}
