package com.monty.library.okhttp;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.util.Log;

import com.google.gson.Gson;
import com.monty.library.HeaderInterceptor;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by guangjiqin on 2017/9/1.
 */

public class RequestManger {

//    public static String baseUrl = "http://123.207.9.75:8080";
    public static String baseUrl = "http://123.207.9.75";
    private final String TAG = getClass().getName();
    private static final MediaType MEDIA_TYPE_JSON = MediaType.parse("application/json;charset=utf-8");

    private static RequestManger mRequestManger;
    public static final int TYPE_GET = 0;
    public static final int TYPE_POST = 1;
    public static final int TYPE_FROM = 2;
    public OkHttpClient mOkHttpClient;
    private Handler okHttpHandler;


    public RequestManger(Context context) {
        mOkHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(new HeaderInterceptor())
                .build();

        okHttpHandler = new Handler(context.getMainLooper());
    }

    public static RequestManger getInstance(Context context) {
        RequestManger inst = mRequestManger;
        if (inst == null) {
            synchronized (RequestManger.class) {
                inst = mRequestManger;
                if (inst == null) {
                    inst = new RequestManger(context.getApplicationContext());
                    mRequestManger = inst;
                }
            }
        }
        return inst;
    }

    public <T> Call requestAsy(String url, int type, HashMap<String, Object> paramsMap, ReuCallBack callBack) {
        Call call = null;
        switch (type) {
            case TYPE_GET:
                call = requestAsynGet(url, paramsMap, callBack);
                break;
            case TYPE_POST:
                call = requestAsynPost(url, paramsMap, callBack);
                break;
            case TYPE_FROM:
                break;
        }
        return call;
    }

    private <T> Call requestAsynGet(String url, HashMap<String, Object> paramsMap, final ReuCallBack callBack) {
        StringBuilder tempParams = new StringBuilder();
        try {
            int pos = 0;
            for (String key : paramsMap.keySet()) {
                if (pos > 0) {
                    tempParams.append("&");
                }
                tempParams.append(String.format("%s=%s", key, URLEncoder.encode(paramsMap.get(key).toString(), "utf-8")));
                pos++;
            }
            String tempUrl = String.format("%s/%s?%s", baseUrl, url, tempParams.toString());

            Log.e(TAG, " 地址 = " + tempUrl.toString());
            Log.e(TAG, " 类型 = get请求");

            final Request request = addHeaders().url(tempUrl).build();
            final Call call = mOkHttpClient.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    failedCallBack("请求失败", callBack);
                    Log.e(TAG, e.toString());
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (response.isSuccessful()) {
                        String string = response.body().string();
                        Log.e(TAG, "请求结果 = " + string);
                        successCallBack(string, callBack, response);
                    } else {
                        failedCallBack("服务器异常", callBack);
                    }

                }
            });
            return call;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    private <T> Call requestAsynPost(String url, HashMap<String, Object> paramsMap, final ReuCallBack callBack) {
        try {
            Gson gson = new Gson();
            RequestBody body = RequestBody.create(MEDIA_TYPE_JSON, gson.toJson(paramsMap).toString());

            Log.e(TAG, " 地址 = " + baseUrl + url.toString());
            Log.e(TAG, " 类型 = post请求");
            Log.e(TAG, " 参数 = " + gson.toJson(paramsMap).toString());

            final Request request = addHeaders().url(baseUrl + url).post(body).build();
            final Call call = mOkHttpClient.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    failedCallBack("请求失败", callBack);
                    Log.e(TAG, e.toString());
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (response.isSuccessful()) {
                        String string = response.body().string();
                        Log.i(TAG, "请求结果 = " + string);
                        successCallBack(string, callBack, response);
                    } else {
                        failedCallBack("服务器异常", callBack);
                    }

                }
            });
            return call;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public interface ReuCallBack {
        void onReSuccess(String result, Response response);

        void onReFailed(String errorMsg);

    }

    private Request.Builder addHeaders() {
        Request.Builder builder = new Request.Builder().addHeader("Connection", "keep-alive")
                .addHeader("platform", "2")
                .addHeader("phoneModel", Build.MODEL)
                .addHeader("systemVersion", Build.VERSION.RELEASE)
                .addHeader("appVersion", "1.0.0");

        return builder;
    }

    private <T> void failedCallBack(final String msg, final ReuCallBack callBack) {
        okHttpHandler.post(new Runnable() {
            @Override
            public void run() {
                if (callBack != null) {
                    callBack.onReFailed(msg);
                }
            }
        });
    }

    private <T> void successCallBack(final String msg, final ReuCallBack callBack, final Response response) {
        okHttpHandler.post(new Runnable() {
            @Override
            public void run() {
                if (callBack != null) {
                    callBack.onReSuccess(msg, response);
                }
            }
        });
    }
}

