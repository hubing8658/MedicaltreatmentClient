package com.monty.library;

import com.monty.library.http.BaseCallModel;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by guangjiqin on 2017/8/19.
 */

public interface UploadService {


    /**
     * 文件上传接口
     *
     * @param token file
     *              type  （1-头像,2-工作证,3-身份证,4-医师资历,5-坐诊照片,6自我介绍视频）
     * @return 上传文件的地址
     */
    @Multipart
    @POST("doctor/uploadFile")
    Call<BaseCallModel<List<String>>> doctorUpload(@Part List<MultipartBody.Part> partList);

    /**
     * 用户端文件上传
     *
     * @param token file
     *              type  1-头像,2-投诉截图
     * @return 上传文件的地址
     */
    @Multipart
    @POST("user/uploadFile")
    Call<BaseCallModel<String>> userUpload(@Part List<MultipartBody.Part> partList);


}

