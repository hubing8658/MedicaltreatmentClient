package com.monty.library.live.event;

import android.util.Log;

import com.tencent.TIMMessage;
import com.tencent.TIMUserProfile;
import com.tencent.livesdk.ILVCustomCmd;
import com.tencent.livesdk.ILVLiveConfig;
import com.tencent.livesdk.ILVText;

import java.util.Observable;

/**
 * 消息观察者
 */
public class MessageEvent extends Observable implements ILVLiveConfig.ILVLiveMsgListener {
    public final static int MSGTYPE_TEXT = 0;
    public final static int MSGTYPE_CMD = 1;
    public final static int MSGTYPE_OTHER = 2;

    private volatile static MessageEvent instance;

    public class MsgInfo {
        public int msgType;
        public Object data;
        public String senderId;
        public TIMUserProfile profile;

        public MsgInfo(int type, String id, TIMUserProfile user, Object obj) {
            msgType = type;
            senderId = id;
            profile = user;
            data = obj;

        }
    }

    private MessageEvent() {
    }

    public static MessageEvent getInstance() {
        if (null == instance) {
            synchronized (MessageEvent.class) {
                if (null == instance) {
                    instance = new MessageEvent();
                }
            }
        }
        return instance;
    }

    @Override
    public void onNewTextMsg(ILVText text, String SenderId, TIMUserProfile userProfile) {
        Log.i("monty", "onNewTextMsg - text:" + text + ",SenderId:" + SenderId );
        setChanged();
        notifyObservers(new MsgInfo(MSGTYPE_TEXT, SenderId, userProfile, text));
    }

    @Override
    public void onNewCustomMsg(ILVCustomCmd cmd, String id, TIMUserProfile userProfile) {
        Log.i("monty", "onNewCustomMsg - cmd:"+cmd.getCmd()+", destId:" + cmd.getDestId() + ", getParam:" + cmd.getParam() + ",getDestId:" + cmd.getDestId() + ",id:" + id );
        setChanged();
        notifyObservers(new MsgInfo(MSGTYPE_CMD, id, userProfile, cmd));
    }

    @Override
    public void onNewOtherMsg(TIMMessage message) {
        Log.i("monty", "onNewOtherMsg - message:" + message.toString());
        setChanged();
        notifyObservers(new MsgInfo(MSGTYPE_OTHER, message.getSender(), message.getSenderProfile(), message));
    }

    /**
     * 清理消息监听
     */
    public void clear() {
        instance = null;
    }
}
